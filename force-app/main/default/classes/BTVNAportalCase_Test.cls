@isTEST
 private class BTVNAportalCase_Test{

    private static testmethod void testBTVNAportalCase(){
    Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter';
        a.phone = '(303) 555-5555';
        insert a;
        
            Opportunity o = new Opportunity();
                o.Name = 'Test';
                o.AccountId = a.Id;
                o.BTVNA_Request_Type__c = 'Site Survey';
                o.LeadSource='BTVNA Portal';
                o.Property_Type__c='Private';
                o.misc_code_siu__c='N999';
                o.CloseDate=system.Today();
                o.StageName='Closed Won';
                 o.Restricted_Programming__c = 'True';
            insert o;
        
            Opportunity o2 = new Opportunity();
                o2.Name = 'Test';
                o2.AccountId = a.Id;
                o2.BTVNA_Request_Type__c = 'Installation/Activation';
                o2.Landlord_Permission__c = 'yes';
        		o2.Point_of_Entry__c = 'yes';
        		o2.Roof_Access__c = 'yes';
        		o2.LeadSource='BTVNA Portal';
                o2.Property_Type__c='Private';
        		o2.TVs_Installed__c = 'yes';
                o2.misc_code_siu__c='N999';
                o2.CloseDate=system.Today();
                o2.StageName='Closed Won';
                o2.Restricted_Programming__c = 'True';
            insert o2;
 
            }
            }