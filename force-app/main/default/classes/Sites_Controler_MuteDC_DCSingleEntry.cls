public class Sites_Controler_MuteDC_DCSingleEntry {

    private final Tenant_Disconnects__c M;
    
 public string error {get; set;}

       public Sites_Controler_MuteDC_DCSingleEntry(ApexPages.StandardController stdcontroller) {
     M = (Tenant_Disconnects__c)stdController.getRecord();        }     public PageReference onsave() {        if (M.Account_Number__c =='')    {       error='Please enter Account Number.';             return null;            }            else{             M.Account__c=ApexPages.currentPage().getParameters().get('acctId');              insert M;             return new PageReference('/MuteDC/Site_MuteDCThankYou');     }    }}