public class AmDocs_CalloutClass_OPPOrderIDCheck {

public class person {
    public cls_ImplGetProductOrderStatusRestOutput[] ImplGetProductOrderStatusRestOutput;
    public cls_serviceStatusOutput serviceStatusOutput;
    public cls_orderStatusOutput orderStatusOutput;
}
class cls_ImplGetProductOrderStatusRestOutput{
}

class cls_serviceStatusOutput{
    public String status;
    public String errorDescription;
}

class cls_orderStatusOutput{
    public String code;
    public String value;
}


    @future(callout=true)

    public static void AmDocsCalloutClassOPPOrderIDCheck(String Id) {
    
list<Opportunity> O = [select id, Name, AmDocs_Order_ID__c, AmDocs_SiteID__c, AmDocs_ServiceID__c, Amdocs_CustomerID__c, AmDocs_Site_Id__c from Opportunity where Id = :id ];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);

list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     

HttpRequest request = new HttpRequest();
    String endpoint = A[0].End_Point_Environment__c+'/commerce/order/'+O[0].AmDocs_Order_ID__c+'/getProductOrderStatus?sc=SS&lo=EN&ca=SF';
    request.setEndPoint(endpoint);
    request.setTimeout(101000);
    request.setHeader('Content-Type', 'application/json');
    request.setHeader('Accept', 'application/json'); 
    request.setHeader('User-Agent', 'SFDC-Callout/45.0');
    request.setMethod('GET');
    String authorizationHeader = A[0].UXF_Token__c; request.setHeader('Authorization', authorizationHeader);
    
HttpResponse response = new HTTP().send(request);
    if (response.getStatusCode() >= 199 && response.getStatusCode() <= 299) 
        { 
            String strjson = response.getbody();
               
        JSONParser parser = JSON.createParser(strjson); 
            parser.nextToken();
            parser.nextToken(); 
            parser.nextToken(); 
        person obj = (person)parser.readValueAs( person.class);
      
        System.debug(response.toString());
        System.debug('STATUS:'+response.getStatus());
        System.debug('STATUS_CODE:'+response.getStatusCode());
        system.debug('DEBUG Respnse Body: ' +response.getBody());
        System.debug('DEBUG Get Respnose to string: ' +response.toString());
        System.debug('DEBUG Get Respnose to strjson: ' +strjson);    
        System.debug('DEBUG obj: ' +obj);
        System.debug('DEBUG obj.ServiceStatusOutput: ' +obj.ServiceStatusOutput);
//        System.debug('DEBUG obj.ServiceStatusOutput.status: ' +obj.ServiceStatusOutput.status);
        System.debug('DEBUG ImplGetProductOrderStatusRestOutput: ' +obj.ImplGetProductOrderStatusRestOutput);
//        System.debug('DEBUG obj.orderStatusOutput.value: ' +obj.orderStatusOutput.value);
        System.debug('DEBUG obj.orderStatusOutput: ' +obj.orderStatusOutput);
    
    Opportunity sbc = new Opportunity(); {
            sbc.id=id; 
            sbc.API_Status__c=String.valueOf(response.getStatusCode());
            if(obj.orderStatusOutput != Null  && obj.serviceStatusOutput != Null) { 
                sbc.AmDocs_Transaction_Code__c = +obj.ServiceStatusOutput.status;
                sbc.AmDocs_Transaction_Description__c='Work Order ID: ' +O[0].AmDocs_Order_ID__c + ' is ' +obj.orderStatusOutput.value;
            }
            else if(obj.serviceStatusOutput != Null  && obj.orderStatusOutput == Null) {                 sbc.AmDocs_Transaction_Code__c = +obj.ServiceStatusOutput.status;                sbc.AmDocs_Transaction_Description__c='Work Order ID: ' +O[0].AmDocs_Order_ID__c + ' is ' +obj.serviceStatusOutput.errorDescription;
            }
            else if(obj.orderStatusOutput == Null  && obj.serviceStatusOutput == Null) {                 sbc.AmDocs_Transaction_Code__c = 'Network Error';                sbc.AmDocs_Transaction_Description__c='Network Error';
            }
        }
        update sbc;
        System.debug('sbc'+sbc); 
        
        API_Log__c apil = new API_Log__c(); { 
            apil.Record__c=id;
            apil.Object__c='Opportunity';
            apil.Status__c='SUCCESS';
            apil.Results__c=strjson;
            apil.API__c='Check Work Order ID';
            apil.ServiceID__c=O[0].AmDocs_ServiceID__c;
            apil.User__c=UserInfo.getUsername();
        insert apil;
        }
 }
    
    else if (response.getStatusCode() >= 300 ) 
        { 
            String strjson = response.getbody();
                System.debug(response.toString());
                System.debug('STATUS:'+response.getStatus());
   
        Opportunity sbc = new Opportunity(); {
            sbc.id=id; 
            sbc.API_Status__c=String.valueOf(response.getStatusCode());
            sbc.AmDocs_Transaction_Code__c = 'Network Error';
            sbc.AmDocs_Transaction_Description__c='Network Error';
        }
        update sbc;
        System.debug('sbc'+sbc); 
        
        API_Log__c apil2 = new API_Log__c(); { 
            apil2.Record__c=id;
            apil2.Object__c='Opportunity';
            apil2.Status__c='SUCCESS';
            apil2.Results__c=String.valueOf(response.getBody());
            apil2.API__c='Check Work Order ID';
            apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
            apil2.User__c=UserInfo.getUsername();
        insert apil2;
        }
    }
}
}