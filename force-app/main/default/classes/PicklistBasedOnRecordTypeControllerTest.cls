@isTest
public class PicklistBasedOnRecordTypeControllerTest {
    
    @isTest Public static void getPicklistValues()
    {
        Opportunity Opp= TestDataFactoryforInternal.createOpportunity();
        List<Recordtype> RecTypeDevName=[Select DeveloperName from Recordtype where id=:opp.RecordTypeId limit 1];
       
       
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                               'Complete',
                                                               '{"controllerValues":{},"defaultValue":{"attributes":null,"label":"Bulk Only","validFor":[],"value":"Digital Bulk"},"eTag":"cbcae565c8263b850b86162d0c57890e","url":"/services/data/v44.0/ui-api/object-info/Opportunity/picklist-values/01260000000LuQMAA0/AmDocs_tenantType__c","values":[{"attributes":null,"label":"Bulk Only","validFor":[],"value":"Digital Bulk"}]}',
                                                               null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
         PicklistBasedOnRecordTypeController.getPicklistValueBasedonRecordType('Opportunity','test', RecTypeDevName[0].developerName);
        Test.stopTest();
    }

}