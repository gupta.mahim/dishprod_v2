Public class ValidateTEAddressCtrlComp    
{
    @AuraEnabled
    public static Id current_TE_Id{get;set;}
    @AuraEnabled
    public static boolean callNetQual{get;set;}
    @AuraEnabled
    public static boolean validation_msg_exist{get;set;}
    
    @AuraEnabled
    public static NetQualDataTE getValidateTEAddress(String recId)
    {
        NetQualDataTE NetQualDataObj = new NetQualDataTE();
        callNetQual = false;
        current_TE_Id=recId;
        
        Tenant_Equipment__c current_TE = [select id,Invoke_NetQual__c,Override_NetQual__c,Valid_Address__c, Address_Validation_Message__c, Amdocs_Tenant_ID__c  from Tenant_Equipment__c where id=:current_TE_Id];
        
        boolean invokeNetQual = current_TE.Invoke_NetQual__c;
        boolean overrideNetQual = current_TE.Override_NetQual__c;
        boolean validAddress = current_TE.Valid_Address__c;
        System.debug('invokeNetQual='+invokeNetQual+', overrideNetQual='+overrideNetQual+', validAddress='+validAddress);
        //Milestone: MS-001253 - Utkarsh - Start
        if(overrideNetQual == false && validAddress == false && invokeNetQual == true && current_TE.Amdocs_Tenant_ID__c == Null)
        //Milestone: MS-001253 - Utkarsh - End
        {
            system.debug('callNetQual'+callNetQual);
            callNetQual=true;            
        }     
        
        else
        {
            callNetQual = false;            
        }
        
        if(callNetQual == true)
        {
            ValidateAdressCtrl.validateTenantEquip(current_TE_Id);
        }
        
        validation_msg_exist = false;
        Address_Valid_Msg_TE__c Addrs1 = Address_Valid_Msg_TE__c.getValues(recId);
        system.debug('Addrs1'+Addrs1);
        if(Addrs1 != null){
            if (Addrs1.TEValidMessage__c != null && Addrs1.TEValidMessage__c != ''){
                validation_msg_exist = true;
                NetQualDataObj.validationMsgValueWr = Addrs1.TEValidMessage__c;
            }else{
                validation_msg_exist = false;
            }
        }
        
        // NetQualDataObj.vMsgWr = validation_msg_exist;
        system.debug('callNetQual'+callNetQual);
        NetQualDataObj.overRideNetQualWr = overrideNetQual;
        NetQualDataObj.callNetQWr = callNetQual;
        NetQualDataObj.validationMsgWr = validation_msg_exist;
        //    NetQualDataObj.validationMsgValueWr = validation_msg;        
        return NetQualDataObj;
    }
    
    @AuraEnabled
    public static void removeValidationMsg(String recId, Boolean callNQl, Boolean  vMsgExist)
    {
        current_TE_Id = recId;
        System.debug('Inside "removeValidationMsg" function');
        System.debug('validation_msg_exist : ' + vMsgExist + ' && callNetQual : ' + callNQl);
        String validationMsgRemove;
        List<Address_Valid_Msg_TE__c> addList = new List<Address_Valid_Msg_TE__c>(); 
        if(callNQl == true)
        {
            Tenant_Equipment__c current_TE=[Select Id, Address_Validation_Message__c from Tenant_Equipment__c where Id=:current_TE_Id];
            
            Address_Valid_Msg_TE__c Addrs2 = Address_Valid_Msg_TE__c.getValues(recId);
            system.debug('Addrs2'+Addrs2);
            if(Addrs2 == null){
                Address_Valid_Msg_TE__c Addrs = new Address_Valid_Msg_TE__c();
                Addrs.Name = recId;    
                Addrs.TEValidMessage__c = current_TE.Address_Validation_Message__c;
                addList.add(Addrs);
            }
            
            current_TE.Address_Validation_Message__c = null;
            try{
                Insert addList;
                update current_TE;
            }catch(Exception e){
                System.debug('Exception message is : ' + e.getMessage());
            }
        }
        
    }
    
    /* 
* This method deleted the custom setting record if the validation message is present on the record.
*/    
    @AuraEnabled
    public static String updateValidationMsg(String recId, Boolean callNQl, Boolean  vMsgExist)
    {
        current_TE_Id = recId;
        System.debug('Inside "removeValidationMsg" function');
        System.debug('validation_msg_exist : ' + vMsgExist + ' && callNetQual : ' + callNQl);
        String validationMsgRemove;
        if(vMsgExist == true && callNQl == false)
        {
            Address_Valid_Msg_TE__c AddrsMsg = Address_Valid_Msg_TE__c.getValues(recId);
            //  system.debug('AddrsMsg'+AddrsMsg);
            //AddrsMsg.TEValidMessage__c = null;
            if(AddrsMsg != null){
                try{
                    delete AddrsMsg;
                    //    validationMsgRemove = AddrsMsg.TEValidMessage__c;
                }catch(Exception e){
                    System.debug('Exception message is : ' + e.getMessage());
                }                
            }
        }
        return null;
    }
    
    /* 
* It is a wrapper class to store the variables of getValidateOpptyAddress.
*/     
    public class NetQualDataTE
    {
        @AuraEnabled
        public boolean callNetQWr {get;set;}
        
        @AuraEnabled
        public boolean validationMsgWr{get;set;}
        
        @AuraEnabled
        public boolean overRideNetQualWr{get;set;}
        
        @AuraEnabled
        public String validationMsgValueWr{get;set;}
        
    }
    
    
}