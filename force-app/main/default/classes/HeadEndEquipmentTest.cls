@isTest
public class HeadEndEquipmentTest 
{
   @isTest Static void TestStatusCode200()
    { 
       
       Opportunity Opp=TestDataFactoryforinternal.createopportunity(1);
       Equipment__c HE=TestDataFactoryforinternal.createHeadEndEquipment(Opp,3);
       Opportunity Opp1=TestDataFactoryforinternal.createopportunity(1);
       Equipment__c HE1=TestDataFactoryforinternal.createHeadEndEquipment(Opp1,3);
       update HE1;
       Test.startTest(); 
       SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
       Test.setMock(HttpCalloutMock.class, fakeResponse);
       Test.stopTest(); 
    }
   @isTest Static void TestStatusCode100()
    {  
       
       Opportunity Opp=TestDataFactoryforinternal.createopportunity(1);
       Equipment__c HE=TestDataFactoryforinternal.createHeadEndEquipment(Opp,1);
       update HE;
       Equipment__c HE1=TestDataFactoryforinternal.createHeadEndEquipment(Opp,2);
       update HE1;
       Test.startTest(); 
       SingleRequestMock fakeResponse = new SingleRequestMock(100,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
       Test.setMock(HttpCalloutMock.class, fakeResponse);
       Test.stopTest(); 
    }
   @isTest Static void TESTOrderId()
    {   
      
       Opportunity Opp=TestDataFactoryforinternal.createopportunity(1);
       Equipment__c HE=TestDataFactoryforinternal.createHeadEndEquipment(Opp,1);
       update HE;
       Equipment__c HE1=TestDataFactoryforinternal.createHeadEndEquipment(Opp,2);
       update HE1;
       Test.startTest(); 
       SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":null,"responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":null,"ownerServiceId":"1335840"}}',
                                                 null);
       Test.setMock(HttpCalloutMock.class, fakeResponse);
       Test.stopTest(); 
  }
     @isTest Static void TestresponseStatus()
  {    
       
       Opportunity Opp=TestDataFactoryforinternal.createopportunity(1);
       Equipment__c HE1=TestDataFactoryforinternal.createHeadEndEquipment(Opp,2);
       update HE1;
       Test.startTest(); 
       SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":null,"equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":null,"ownerServiceId":"1335840"}}',
                                                 null);
       Test.setMock(HttpCalloutMock.class, fakeResponse);
       Test.stopTest(); 
  }
 
 
}