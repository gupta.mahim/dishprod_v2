@isTest
public class ToDoTest {    
    @isTest Private static void testMethod1(){
        ToDoController.loadData();
        
        ToDo__c todo = New ToDo__c();
        todo.Reminder_Date__c=system.today();
        todo.Due_Date__c=system.today();
        todo.Description__c ='TestDesc';
        insert todo; 
        
        ToDoController.saveTodoRecord(todo);
    }
}