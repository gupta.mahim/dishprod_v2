@isTest 
    global class ExampleCalloutMock implements HttpCalloutMock{
     global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody(AddressScrubCallout.testResponse);
       // req.setMethod('POST');
        req.setHeader('Accept','application/json');
        req.setHeader('User-Agent','SFDC-Callout/44.0');
        req.setHeader('Content-type','application/json');
      //  req.setBody(jsonBody);
       // req.setHeader('Content-length',string.valueOf(jsonBody.length()));
        return res;
}
        
 }