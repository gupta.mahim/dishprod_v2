@IsTest
Public class AmdocsMoveReceiverTest
{
    @isTest static void testTEMove()  
    {
          //String responsebody='{"ImplMoveReceiverRestOutput":{"informationMessages":[{"errorCode":"1069","errorDescription":"Move Receiver input validation Failed - Cannot move receiver to Bulk Product with service id 726610"}]}}';
          String responsebody='{"ImplMoveReceiverRestOutput":{"orderID":"783216","responseStatus":"SUCCESS"}}';
          
          
          Opportunity Opp1= TestDataFactoryforInternal.createOpportunity(1);
          Tenant_Account__c TA=TestDataFactoryforInternal.createTenantAccount(opp1);
          TA.AmDocs_ServiceID__c='726610';
          update TA;
          Tenant_Equipment__c teRec=TestDataFactoryforInternal.createTenantEquipment(Opp1);
          teRec.Action__c='Active';
          teRec.Move_Receiver_To__c='726610';
          
          SingleRequestMock mockResponse = new SingleRequestMock(200,'Complete',responsebody, null);
          Test.setMock(HttpCalloutMock.class, mockResponse);
          
          update teRec;
          
    }
    @isTest static void testHEMove()  
    {
          //String responsebody='{"ImplMoveReceiverRestOutput":{"informationMessages":[{"errorCode":"1069","errorDescription":"Move Receiver input validation Failed - Cannot move receiver to Bulk Product with service id 726610"}]}}';
          String responsebody='{"ImplMoveReceiverRestOutput":{"orderID":"783216","responseStatus":"SUCCESS"}}';
          
          
          Opportunity Opp1= TestDataFactoryforInternal.createOpportunity(1);
          Tenant_Account__c TA=TestDataFactoryforInternal.createTenantAccount(opp1);
          TA.AmDocs_ServiceID__c='726610';
          update TA;
          Equipment__c HE=TestDataFactoryforinternal.createHeadEndEquipment(Opp1,1);
          HE.Action__c='Active';
          HE.Move_Receiver_To__c='726610';
          
          SingleRequestMock mockResponse = new SingleRequestMock(200,'Complete',responsebody, null);
          Test.setMock(HttpCalloutMock.class, mockResponse);
          
          update HE;
          
    }
    @isTest static void testDigitalBulkEquipMove()  
    {
          Opportunity Opp1= TestDataFactoryforInternal.createOpportunity(1);
          AmdocsMoveReceiverCtrl.createTEFromOppty('Test','Test',Opp1);
    }
    @isTest static void testDigitalBulkEquipMoveByTE()  
    {
          Opportunity Opp1= TestDataFactoryforInternal.createOpportunity(1);
          Tenant_Equipment__c teRec=TestDataFactoryforInternal.createTenantEquipment(Opp1);
          AmdocsMoveReceiverCtrl.createTEFromTE('Test','Test',teRec);
    }
    @isTest static void testCreateLogs()  
    {
          String responsebody='{"ImplMoveReceiverRestOutput":{"orderID":"783216","responseStatus":"SUCCESS"}}';
          Opportunity Opp1= TestDataFactoryforInternal.createOpportunity(1);
          Tenant_Account__c TA=TestDataFactoryforInternal.createTenantAccount(opp1);
          TA.AmDocs_ServiceID__c='726610';
          update TA;
          Tenant_Equipment__c teRec=TestDataFactoryforInternal.createTenantEquipment(Opp1);
          Equipment__c HE=TestDataFactoryforinternal.createHeadEndEquipment(Opp1,1);
          
          AmDocs_CalloutClass_MoveRvr.writeTimerError(teRec.id,'726610','Tenant Equipment');
          AmDocs_CalloutClass_MoveRvr.writeTimerError(HE.id,'726610','Headend Equipment');
          
          AmdocsMoveReceiverCtrl.createAPILog(teRec.id,'726610','SUCCESS','Response result','Tenant Equipment');
          AmdocsMoveReceiverCtrl.createAPILog(HE.id,'726610','SUCCESS','Response result','Headend Equipment');
          
          AmDocs_CalloutClass_MoveRvr.logAPIResult(teRec.id,'SUCCESS','Response result','TE');
          AmDocs_CalloutClass_MoveRvr.logAPIResult(HE.id,'SUCCESS','Response result','HE');
          
          Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock()); 
    }
}