public class CheckRecordTypeAccess {
   
    Public class Flowinput
    {
        
        //@Invocablevariable
       // Public String test;
         @Invocablevariable
        Public integer count=0;
    }

    
    Public class FlowOutput
    {
        
        @Invocablevariable
        Public String Recid;
         @Invocablevariable
        Public integer count=0;
    }

    @InvocableMethod(label='checkrecordtypeaccess')
     public static list<flowOutput> getOpportunityRecordTypes(list<flowinput> req)
   {
        List<flowOutput> RecordTypeIdlist= new list<flowOutput>();
        flowOutput output = new FlowOutput();
        Integer count=0;
        list <RecordType> RecordTypeid=new list<RecordType>();
        Recordtypeid=[Select id from RecordType where Name='Retailer Bulk' OR Name='PCO' OR Name='Integrator Bulk'];
        Set<id> Recordtypeidset=new set<id>();
        for(RecordType rt:Recordtypeid)
        {
            Recordtypeidset.add(rt.id);
        }
        for(RecordTypeInfo info: Opportunity.SObjectType.getDescribe().getRecordTypeInfos())
        {
          if(info.isAvailable() && info.getName()!='Master'&&RecordTypeidset.contains(info.getRecordTypeid()))
          {
              
             
            output.Recid=info.getRecordTypeId();
             output.count+=1;
           
           
           }
        }
         RecordTypeIdlist.add(output); 
       system.debug('teting'+RecordTypeIdlist);
           return RecordTypeIdlist;
   }
}