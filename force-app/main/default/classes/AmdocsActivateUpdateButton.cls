public with sharing class AmdocsActivateUpdateButton {
    
    String OppId = '';
   
    public AmdocsActivateUpdateButton(ApexPages.StandardController controller){
        OppId = ApexPages.currentPage().getParameters().get('id');
    }
    
    
    public Pagereference ActivateUpdate() {
        Opportunity ThisOpp = [Select Id, LastActivateUpdate__c , NextAvailableActivateUpdateTime__c, IsActiveAmdocs__c, HasSingedContract__c, Amdocs_CustomerID__c, AmDocs_SiteID__c, AmDocs_ServiceID__c, RecordTypeId from Opportunity where ID = :OppId];
        
// create customer for mdu direct and integrator
        if( ThisOpp.Amdocs_CustomerID__c == Null && ThisOpp.Amdocs_SiteID__c == NULL && ThisOpp.AmDocs_ServiceID__c == NULL && ((ThisOpp.LastActivateUpdate__c == Null) || ( ThisOpp.NextAvailableActivateUpdateTime__c < system.Now() )) && ( ThisOpp.RecordTypeId == '01260000000LuQM' || ThisOpp.RecordTypeId == '0126000000053vp')  && ThisOpp.IsActiveAmdocs__c == TRUE){
                ThisOpp.AmDocs_CreateCustomer__c = TRUE;
                ThisOpp.LastActivateUpdate__c = system.Now();
                update ThisOpp;
                return  new Pagereference('/'+OppId);}
               
// create customer for everything not mdu direct or integrator                
else        
        if( ThisOpp.Amdocs_CustomerID__c == Null && ThisOpp.Amdocs_SiteID__c == NULL && ThisOpp.AmDocs_ServiceID__c == NULL && ((ThisOpp.LastActivateUpdate__c == Null) || ( ThisOpp.NextAvailableActivateUpdateTime__c < system.Now() )) && ( ThisOpp.RecordTypeId != '01260000000LuQM' || ThisOpp.RecordTypeId != '0126000000053vp') && ThisOpp.HasSingedContract__c == TRUE  && ThisOpp.IsActiveAmdocs__c == TRUE){
                ThisOpp.AmDocs_CreateCustomer__c = TRUE;
                ThisOpp.LastActivateUpdate__c = system.Now();
                update ThisOpp;
                return  new Pagereference('/'+OppId);}        

// create site for mdu direct and integrator                
else
        if( ThisOpp.Amdocs_CustomerID__c != Null && ThisOpp.Amdocs_SiteID__c == NULL && ThisOpp.Amdocs_ServiceID__c == NULL && ((ThisOpp.LastActivateUpdate__c == Null) || (ThisOpp.NextAvailableActivateUpdateTime__c < system.Now() )) && ( ThisOpp.RecordTypeId == '01260000000LuQM' || ThisOpp.RecordTypeId == '0126000000053vp')  && ThisOpp.IsActiveAmdocs__c == TRUE ){
                ThisOpp.AmDocs_CreateSite__c = TRUE;
                ThisOpp.LastActivateUpdate__c = system.Now();
                update ThisOpp;
                return  new Pagereference('/'+OppId);}    

// create site for everything not mdu direct and integrator                
else
        if( ThisOpp.Amdocs_CustomerID__c != Null && ThisOpp.Amdocs_SiteID__c == NULL && ThisOpp.Amdocs_ServiceID__c == NULL && ((ThisOpp.LastActivateUpdate__c == Null) || (ThisOpp.NextAvailableActivateUpdateTime__c < system.Now() )) && ( ThisOpp.RecordTypeId != '01260000000LuQM' || ThisOpp.RecordTypeId != '0126000000053vp') && ThisOpp.HasSingedContract__c == TRUE  && ThisOpp.IsActiveAmdocs__c == TRUE ){
                ThisOpp.AmDocs_CreateSite__c = TRUE;
                ThisOpp.LastActivateUpdate__c = system.Now();
                update ThisOpp;
                return  new Pagereference('/'+OppId);}                              

// create bulk for MDU direct and integrator
else
        if( ThisOpp.Amdocs_CustomerID__c != Null && ThisOpp.Amdocs_SiteID__c != NULL && ThisOpp.Amdocs_ServiceID__c == NULL && ((ThisOpp.LastActivateUpdate__c == Null) || ( ThisOpp.NextAvailableActivateUpdateTime__c < system.Now() )) && ( ThisOpp.RecordTypeId == '01260000000LuQM' || ThisOpp.RecordTypeId == '0126000000053vp')  && ThisOpp.IsActiveAmdocs__c == TRUE){
                ThisOpp.AmDocs_CreateBulk__c = TRUE;
                ThisOpp.LastActivateUpdate__c = system.Now();
                update ThisOpp;
                return  new Pagereference('/'+OppId);} 

// Create bulk for everything not MDU direct or integrator
else
        if( ThisOpp.Amdocs_CustomerID__c != Null && ThisOpp.Amdocs_SiteID__c != NULL && ThisOpp.Amdocs_ServiceID__c == NULL && ((ThisOpp.LastActivateUpdate__c == Null) || (ThisOpp.NextAvailableActivateUpdateTime__c < system.Now() )) && ( ThisOpp.RecordTypeId != '01260000000LuQM' || ThisOpp.RecordTypeId != '0126000000053vp') &&  ThisOpp.HasSingedContract__c == TRUE  && ThisOpp.IsActiveAmdocs__c == TRUE){
                ThisOpp.AmDocs_CreateBulk__c = TRUE;
                ThisOpp.LastActivateUpdate__c = system.Now();
                update ThisOpp;
                return  new Pagereference('/'+OppId);}    
                                  
// update bulk for MDU direct and integrator
else
        if( ThisOpp.Amdocs_CustomerID__c != Null && ThisOpp.Amdocs_SiteID__c != NULL && ThisOpp.Amdocs_ServiceID__c != NULL && ((ThisOpp.LastActivateUpdate__c == Null) || ( ThisOpp.NextAvailableActivateUpdateTime__c < system.Now() )) && ( ThisOpp.RecordTypeId == '01260000000LuQM' || ThisOpp.RecordTypeId == '0126000000053vp')  && ThisOpp.IsActiveAmdocs__c == TRUE ){
                ThisOpp.AmDocs_BulkUpdateAll__c = TRUE;               
                ThisOpp.LastActivateUpdate__c = system.Now();
                update ThisOpp;
                return  new Pagereference('/'+OppId);}

// update bulk for everything not MDU direct or integrator
else
        if( ThisOpp.Amdocs_CustomerID__c != Null && ThisOpp.Amdocs_SiteID__c != NULL && ThisOpp.Amdocs_ServiceID__c != NULL && ((ThisOpp.LastActivateUpdate__c == Null) || (ThisOpp.NextAvailableActivateUpdateTime__c < system.Now() )) && ( ThisOpp.RecordTypeId != '01260000000LuQM' || ThisOpp.RecordTypeId != '0126000000053vp') && ThisOpp.HasSingedContract__c == TRUE  && ThisOpp.IsActiveAmdocs__c == TRUE ){
                ThisOpp.AmDocs_BulkUpdateAll__c = TRUE;               
                ThisOpp.LastActivateUpdate__c = system.Now();            
                update ThisOpp;
                return  new Pagereference('/'+OppId);}
                
// Within 5 minutes
else
        if( ThisOpp.NextAvailableActivateUpdateTime__c > system.Now() ){
                return  new Pagereference('/apex/ActivateUpdateWait');}         
            return  new Pagereference('/'+OppId);
    }
}