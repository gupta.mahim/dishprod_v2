public class SmartboxTrgHandler
{
    public static void doBeforeInsert(List<Smartbox__c> newSBData)
    {
        doDupeLocate(newSBData,null,true);
        doSBoxProCamSNFix(newSBData,null, true);
       EquipDupeHandler.handleSBDupes(newSBData,false);
    }
    public static void doAfterInsert(Map<Id,Smartbox__c> newSBData,Map<Id,Smartbox__c> oldSBData)
    {
        doDupeDelete(newSBData.values());
        doSmartBoxEquipment_AccountingCopy(newSBData.values());
    }
    public static void doBeforeUpdate(Map<Id,Smartbox__c> newSBData,Map<Id,Smartbox__c> oldSBData)
    {
        doDupeLocate(newSBData.values(),oldSBData,false);
        doVerzionCallout(newSBData.values());
        doSBoxProCamSNFix(newSBData.values(),oldSBData, false);
        doAmDocsBulkSBoxCallout(newSBData.values());
    }
    public static void doAfterUpdate(Map<Id,Smartbox__c> newSBData,Map<Id,Smartbox__c> oldSBData)
    {
        EquipDupeCaseHandler.createCaseForSmartbox(oldSBData,newSBData);
        doSBoxEquipmentRecallRejectDelete(newSBData.values());
        doDupeDelete(newSBData.values());
        createRAForSmartbox(newSBData.values());
        doSmartBoxEquipment_AccountingCopy(newSBData.values());
    }
    private static void doSBoxEquipmentRecallRejectDelete(List<Smartbox__c> sbData)
    {
        Smartbox__c sbeqa = sbData[0];
          if (sbeqa.Opportunity__c != null && sbeqa.Flags__c == 'Delete' ){Set<Id> sbeqaIds = new Set<Id>();Set<Id> sbopIds = new Set<Id>();for (Smartbox__c sbeqa2: sbData){sbopIds.add(sbeqa2.Opportunity__c);sbeqaIds.add(sbeqa2.Id);}list<Smartbox__c> sbeqa3 = [select id, Flags__c, Status__c, Opportunity__c from Smartbox__c where ID in :sbeqaIds AND Flags__c = 'Delete' ];Database.delete(sbeqa3);}
    }
    private static void doVerzionCallout(List<Smartbox__c> sbData)
    { system.debug('listsc'+sbData);
        // Login Process 1st get the M2M token using the key, secerete and such then use the M2M token to get the VZ Token. All other request require sending the VZ Token.
        Smartbox__c S = sbData[0];
    
     
        // Get the M2M Token
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == true && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == false)
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_GetM2MToken.makeCalloutM2M(S.Id); System.debug('IF 1' +S);}
        else
        
        // Get the VZ-M2M Token
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == true && S.Verizon_Do_It_Action__c == false)
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_GetVZToken.makeCallout(S.Id); System.debug('IF 2' +S);}
        else
        
        // Function Suspend
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Suspend')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Suspend.makeCalloutSuspend(S.Id); System.debug('IF 3' +S);}
        else
        
        // Function Restore
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Restore')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Restore.makeCalloutRestore(S.Id); System.debug('IF 4' +S);}
        else
        
        // Function Deactivate
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Deactivate')
        { S.Verizon_Do_It_Get_M2M_Token__c = false;
         S.Verizon_Do_It_Login__c = false;
         S.Verizon_Do_It_Action__c = false; 
         Verizon_CalloutClass_Deactivate.makeCalloutDeactivate(S.Id); 
         System.debug('IF 5' +S);}
        else
        
        // Function Activate
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Activate')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Activate.makeCalloutActivate(S.Id); System.debug('IF 6' +S);}
        else
        
        // Function Add new Account
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'CreateAccount')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_CreateAccount.makeCalloutCreateAccount(S.Id); System.debug('IF 7' +S);}
        else
        
        // Function SMS Up - 120
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSUp120')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Up120.makeCalloutSMS_Up120(S.Id); System.debug('IF 8' +S);}
        else
        
        // Function SMS Up
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSUp')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Up.makeCalloutSMS_Up(S.Id); System.debug('IF 10' +S);}
        else    
            
        // Function SMS Down
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSDown')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Down.makeCalloutSMS_Down(S.Id); System.debug('IF 9' +S);}
        else
        
        // Function SMS Up - 120 4G
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSUP120-4G')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Up120_4G.makeCalloutSMS_Up120(S.Id); System.debug('IF 10' +S);}
        else
        
        // Function SMS Up 4G
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSUP4G')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Up_4G.makeCalloutSMS_Up(S.Id); System.debug('IF 11' +S);}
        else    
            
        // Function SMS Down 4G
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSDown-4G')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Down_4G.makeCalloutSMS_Down(S.Id); System.debug('IF 12' +S);}
        else
        
        // Function Activate 4G
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Activate-4G')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Activate_4G.makeCalloutActivate(S.Id); System.debug('IF 13' +S);}
        else 
        
        // Function Deactivate 4G
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Deactivate-4G')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Deactivate_4G.makeCalloutDeactivate(S.Id); System.debug('IF 14' +S);}
        else
        
        // Function Suspend 4G
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Suspend-4G')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Suspend_4G.makeCalloutSuspend(S.Id); System.debug('IF 15' +S);}
        else
        
        // Function Restore 4G
        if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Restore-4G')
        { S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Restore_4G.makeCalloutRestore(S.Id); System.debug('IF 16' +S);}
        
    }
    private static void doDupeLocate(List<Smartbox__c> sbData,Map<Id,Smartbox__c> oldSBData, boolean isInsert)
    {
        Map<String, Smartbox__c> SBoxMap =new Map <String, Smartbox__c>();
        for (Smartbox__c s :sbData) {
            
            // Make sure we don't treat a Serial_Number__c that isn't changing during an update as a duplicate.
        
            if ((Smartbox__c.Serial_Number__c !=null ) && (s.NoTrigger__c == False )&& (isInsert || (s.Unique_Opp_SN__c != oldSBData.get(s.Id).Unique_Opp_SN__c))) {
                // Make sure another new Smartbox__c isn't also a duplicate 
        
                if (SBoxMap.containsKey(s.Unique_Opp_SN__c)) {s.CAID__c = 'DUPE';}else {SBoxMap.put(s.Unique_Opp_SN__c, s);}
           }
        }
        
        // Using a single database query, find all the Smartbox__c's in
        // the database that have the same Serial_Number__c address as any
        // of the Smartbox__cs being inserted or updated.
        
        for (Smartbox__c s : [SELECT Unique_Opp_SN__c FROM Smartbox__c
                          WHERE Unique_Opp_SN__c IN :SboxMap.KeySet() AND (NoTrigger__c = False)]) {            Smartbox__c newSbox = SBoxMap.get(s.Unique_Opp_SN__c);          newSbox.CAID__c = 'DUPE';
        }
    }
    private static void doSBoxProCamSNFix(List<Smartbox__c> sbData,Map<Id,Smartbox__c> oldSBData, boolean isInsert)
    {
        Map<String, Smartbox__c> SBoxMap =new Map <String, Smartbox__c>();
        for (Smartbox__c s : sbData) {
            
            // Make sure we don't treat a Serial_Number__c that isn't changing during an update as a duplicate.
        
            if ((Smartbox__c.Serial_Number__c !=null ) && (s.NoTrigger__c == False )&& (isInsert || (s.Unique_SN_no_CAID__c != oldSBData.get(s.Id).Unique_SN_no_CAID__c))) {
        
       
                if (SBoxMap.containsKey(s.Unique_SN_no_CAID__c)) { s.Second_ProCam__c = TRUE;}
                else {
                    SBoxMap.put(s.Unique_SN_no_CAID__c, s);
                }
           }
        }
        
        // Using a single database query, find all the Smartbox__c's in
        // the database that have the same Serial_Number__c address as any
        // of the Smartbox__cs being inserted or updated.
        
        for (Smartbox__c s : [SELECT Unique_SN_no_CAID__c FROM Smartbox__c
                          WHERE Unique_SN_no_CAID__c IN :SboxMap.KeySet() AND Second_ProCam__c != TRUE AND (NoTrigger__c = False)]) { Smartbox__c newSbox = SBoxMap.get(s.Unique_SN_no_CAID__c); newSbox.Second_ProCam__c = TRUE; }
    }
    private static void doDupeDelete(List<Smartbox__c> sbData)
    {
        List<Smartbox__c> deleterows  = new  List<Smartbox__c>();
        for(Smartbox__c sbd :[SELECT Id, CAID__c, NoTrigger__c, Type_of_Equipment__c FROM Smartbox__c WHERE Id IN : sbData AND (CAID__C = 'DUPE' OR CSG_Status__c = 'Drop Completed') AND (NoTrigger__c = False) AND (Type_of_Equipment__c != 'Cell Modem (Fusion Wireless)') ]) {deleterows.add(sbd);
        }
        if(deleterows.SIZE() != 0){Database.DeleteResult[] deleterows_del = Database.DELETE(deleterows);}
    }
    private static void createRAForSmartbox(List<Smartbox__c> sbData)
    {
        List<RA__c> lstRA=new List<RA__c>();
        for( Smartbox__c SB : sbData) {
          if(SB.Smartbox_Leased__c == true && SB.CaseID__c != null && SB.RA_Number__c != null ) {
              RA__c SBA = new RA__c(
                  cid__c = SB.CaseID__c,
                  RANumber__c =  SB.RA_Number__c,
                  Shipping_Tracking__c = SB.Shipping_Tracking__c,
                  Serial_Number__c = SB.Serial_Number__c,
                  SmartCard__c = SB.SmartCard__c,
                  CAID__c = SB.CAID__c,
                  Opportunity__c = SB.Opportunity__c
                  );

                  lstRA.add(SBA);
            }
        }
        if(lstRA.size()>0)
        {
            insert lstRA;
        }
    }
    private static void doAmDocsBulkSBoxCallout(List<Smartbox__c> sbData)
    {
        // Processes for SF->AmDocs to call @Future API callout triigers.
        Smartbox__c S = sbData[0];
        
        if(S.Action__c == 'Send Trip / HIT')
        {S.Action__c = 'Trip / HIT Pending';S.API_Status__c = '';S.AmDocs_FullString_Return__c = '';S.AmDocs_Order_ID__c = '';S.AmDocs_Transaction_Code__c = '';S.AmDocs_Transaction_Description__c = '';AmDocs_CalloutClass_SBResend.AmDocsMakeCalloutSBResend(S.Id); System.debug('IF 1' +S);}
        // else
    }
    private static void doSmartBoxEquipment_AccountingCopy(List<Smartbox__c> sbData)
    {
            List<Smartbox_Accounting__c> accInsert=new List<Smartbox_Accounting__c>();
            for (Smartbox__c SB: sbData)    {
            if((SB.Smartbox_Leased__c == true && SB.Opportunity__c != '0066000000PRyC1') || (SB.Smartbox_Leased__c == false && SB.Opportunity__c != '0066000000PRyC1'))
                {Smartbox_Accounting__c SBA = new Smartbox_Accounting__c(
                Chassis_Serial__c = SB.Chassis_Serial__c,  
                CAID__c = SB.CAID__c,
                CSG_Sub_Account__c = SB.CSG_Sub_Account__c,
                MEID__c = SB.MEID__c,
                Opportunity__c = SB.Opportunity__c,
                CSG_Master__c = SB.CSG_Account_Number__c,
                Opp_Launch_Date__c = SB.Opp_Launch_Date__c,
                Part_Number__c = SB.Part_Number__c,    
                Serial_Number__c = SB.Serial_Number__c,
                SmartCard__c = SB.SmartCard__c,
                Opportunity_Status__c = SB.SF_Status__c,
                SF_Status__c = SB.Status__c,
                CSG_Status__c = SB.CSG_Status__c,
                Smartbox_Leased__c=SB.Smartbox_Leased__c,
                Equipment_Added_to_SF_Date__c = SB.CreatedDate,
                Equipment_Last_Modified_Date__c = SB.LastModifiedDate,
                CaseRequestedCompletionDate__c = SB.CaseRequestedCompletionDate__c,
                CaseRequestType__c = SB.CaseRequestType__c,
                Case_Number__c = SB.Case_Number__c,
                Most_Recent_Case_Create_Date__c = SB.Most_Recent_Case_Create_Date__c,
                Most_Recent_Case_ID__c = SB.Most_Recent_Case_ID__c,
                Most_Recent_Case_Number__c = SB.Most_Recent_Case_Number__c,
                Most_Recent_Case_Pre_Activation_Date__c = SB.Most_Recent_Case_Pre_Activation_Date__c,
                Most_Recent_Case_RA__c = SB.Most_Recent_Case_RA__c,
                Most_Recent_Case_Record_Type__c = SB.Most_Recent_Case_Record_Type__c,
                Most_Recent_Case_Request_Completion_Date__c = SB.Most_Recent_Case_Request_Completion_Date__c,
                Most_Recent_Case_Type__c = SB.Most_Recent_Case_Type__c,
                Most_Recent_Property_Bulk_Launch_Date__c = SB.Most_Recent_Property_Bulk_Launch_Date__c,
                Most_Recent_Property_Status__c = SB.Most_Recent_Property_Status__c,
                Type_of_Equipment__c = SB.Type_of_Equipment__c);
 
                accInsert.add(SBA); 
            }
        }
        if(accInsert.size()>0)
        {
            insert accInsert;
        }
    }
}