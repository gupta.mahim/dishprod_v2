public class AmDocs_CalloutClass_DBulkResendAll {

public class person {
    public String serviceID;
    public String orderID;
    public String responseStatus;
    public cls_informationMessages[] informationMessages;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}

    @future(callout=true)

    public static void AmDocs_CalloutClass_DBulkResendAll(String Id) {
    
list<Tenant_Equipment__c> T = [select id, Amdocs_Tenant_ID__c, Name, Reset__c, Action__c, Tenant_Account__r.Id, Mass_Trip_Requested__c, Opportunity__c from Tenant_Equipment__c where Id = :id AND Action__c = 'Mass Trip / HIT - Pending' AND Mass_Trip_Requested__c = TRUE Limit 1];
    System.debug('RESULTS of the LIST lookup to the Tenant Equipment object' +T);
    
        if(T[0].Amdocs_Tenant_ID__c == '' || T[0].Amdocs_Tenant_ID__c == Null ){
  
      Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
          sbc.id=id;
          sbc.API_Status__c='Error';
          sbc.Amdocs_Transaction_Code__c='Error';
          sbc.Action__c = 'Mass Trip / HIT - Pending';
          sbc.Mass_Trip_Requested__c = TRUE;
          if( (T[0].Amdocs_Tenant_ID__c == '' || T[0].  Amdocs_Tenant_ID__c == Null )) {
              sbc.Amdocs_Transaction_Description__c = T[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
          }
      update sbc;
      }
      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='Tenant Equipment';
          apil2.Status__c='ERROR';
          apil2.Results__c=T[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
          apil2.API__c='Mass Trip/Resend';
          apil2.ServiceID__c=T[0].Amdocs_Tenant_ID__c;
          apil2.User__c=UserInfo.getUsername();
      insert apil2;
  }
 
  
  }
  if(T[0].Amdocs_Tenant_ID__c != '' && T[0].Amdocs_Tenant_ID__c != Null ){        
      Datetime dt1 = System.Now();
      list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateUpdateTime__c from API_LOG__c where ServiceID__c = :T[0].Amdocs_Tenant_ID__c AND NextAvailableActivateUpdateTime__c >= :dt1 Order By NextAvailableActivateUpdateTime__c  DESC Limit 1];
            
       if( (timer.size() > 0 )  || T[0].Reset__c == 'ChewbaccaIsTesting' ) {
           system.debug('DEBUG API LOG Timer size : ' +timer.size());
           system.debug('DEBUG API LOG Info' +timer);
           system.debug('DEBUG timer 1 : ' +dt1);

       Tenant_Equipment__c sbc = new Tenant_Equipment__c(); { sbc.id=id; sbc.API_Status__c='Error';  sbc.Amdocs_Transaction_Code__c='Error'; sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions'; update sbc; }
       API_Log__c apil2 = new API_Log__c();{ apil2.Record__c=id; apil2.Object__c='Tenant Equipment'; apil2.Status__c='ERROR'; apil2.Results__c='You must wait atleast (2) two minutes between transactions'; apil2.API__c='Mass TRIP/Resend'; apil2.ServiceID__c=T[0].AmDocs_Tenant_ID__c; apil2.User__c=UserInfo.getUsername(); insert apil2; 
     }
   }   

   else if(timer.size() < 1) {
    
    String TenAID = T[0].Amdocs_Tenant_ID__c;
        System.debug('RESULTS of the LIST lookup to the Tenant Equipments Account ID: ' +TenAID);
        
    list<Tenant_Equipment__c> T2 = [select id, Amdocs_Tenant_ID__c, Name, Reset__c, Action__c, Mass_Trip_Requested__c, Opportunity__c from Tenant_Equipment__c where Amdocs_Tenant_ID__c = :TenAID AND Mass_Trip_Requested__c = TRUE Limit 4500];
        System.debug('RESULTS of the LIST lookup to the Tenant Equipment (2) object' +T2);

    list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the AmDocs Login object' +A);    
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        jsonObj.writeFieldName('ImplResendEquipmentRestInput');
        jsonObj.writeStartObject();
            jsonObj.writeStringField('orderActionType', 'RL');
        jsonObj.writeEndObject();
    jsonObj.writeEndObject();
String finalJSON = jsonObj.getAsString();
    System.debug('DEBUG 0 ======== finalJSON: ' + finalJSON);
    

    HttpRequest request = new HttpRequest(); 
        String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+T[0].Amdocs_Tenant_ID__c+'/resendEquipment?sc=SS&lo=EN&ca=SF'; 
        request.setEndPoint(endpoint); 
        request.setBody(jsonObj.getAsString()); 
        request.setHeader('Content-Type', 'application/json'); 
        request.setHeader('Accept', 'application/json'); 
        request.setHeader('User-Agent', 'SFDC-Callout/45.0'); 
        request.setMethod('POST'); 
        String authorizationHeader = A[0].UXF_Token__c ; 
        request.setHeader('Authorization', authorizationHeader); 
        
        HttpResponse response = new HTTP().send(request); 
        if (response.getStatusCode() == 200) { 
            String strjson = response.getbody(); 
            JSONParser parser = JSON.createParser(strjson); 
            parser.nextToken(); parser.nextToken(); parser.nextToken(); 
            person obj = (person)parser.readValueAs( person.class);
                System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
                System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
                System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
                System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);


        List<Tenant_Equipment__c> TEList = new List<Tenant_Equipment__c>();
        if(T.size() > 0) { 
            System.debug('DEBUG 7 ======== T Size = ' +T.size());
            if(obj.responseStatus == 'SUCCESS' && T2.size() > 0 && obj.orderID != Null){ 
                System.debug('DEBUG 8 ======== obj.responseStatus: ' + obj.responseStatus);
                System.debug('DEBUG 9 ======== obj.orderID: ' + obj.orderID);
                System.debug('DEBUG 10 ======== T2 Size = ' + T2.size());                
                for(Integer i = 0; i < T2.size(); i++){
                    Tenant_Equipment__c sbc = new Tenant_Equipment__c();
                        sbc.Id=T2[i].Id;
                        sbc.Action__c='Active';
                        sbc.Mass_Trip_Requested__c = False;
                        sbc.AmDocs_Transaction_Description__c='MASS TRIP/HIT COMPLETED'; 
                        sbc.AmDocs_Transaction_Code__c='SUCCESS';

                      TEList.add(sbc);
                          System.debug('DEBUG 2 === evList: '   + TEList);
                }
            }
        }
        update TEList;
//        Map<Id, Tenant_Equipment__c> TEMap = new Map<Id, Tenant_Equipment__C>(TEList);
//        update TEMap;

       API_Log__c apil2 = new API_Log__c();{
           apil2.Record__c=id;
           apil2.Object__c='Tenant Equipment';
           apil2.Status__c='SUCCESS';
           apil2.Results__c=strjson;
           apil2.API__c='Mass TRIP/Resend';
           apil2.ServiceID__c=T[0].Amdocs_Tenant_ID__c;
           apil2.User__c=UserInfo.getUsername();
      insert apil2;
      }
   }
   else if (response.getStatusCode() != 200) { 
       String strjson = response.getbody(); 
//       JSONParser parser = JSON.createParser(strjson); 
//       parser.nextToken(); parser.nextToken(); 
//       person obj = (person)parser.readValueAs( person.class);
    
       Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
           sbc.id=id; 
           sbc.action__c='TRIP - FAILED'; 
           sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
           sbc.AmDocs_Transaction_Description__c=String.valueOf(response.getStatus()); 
           update sbc;
       }
       API_Log__c apil2 = new API_Log__c();{
           apil2.Record__c=id;
           apil2.Object__c='Tenant Equipment';
           apil2.Status__c='ERROR';
           apil2.Results__c=strjson;
           apil2.API__c='Mass TRIP/Resend';
           apil2.ServiceID__c=T[0].Amdocs_Tenant_ID__c;
       apil2.User__c=UserInfo.getUsername();
  insert apil2;
  }
       }
   }
}}}