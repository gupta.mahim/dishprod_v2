/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 13 July 2020
Created for: Milestone# MS-003294
Description: To check logged in User Access
-------------------------------------------------------------*/

public class guideMeAccountAccessCheck {
    
    public class FlowInput{
        @Invocablevariable
        public string objectName;
        
        @Invocablevariable
        public string fieldName;
    }
    
    public class FlowOutput{
        @Invocablevariable
        public Boolean userHaveAccess = true;
        
        @Invocablevariable
        public String noAccessMessage;
    }
    
     @InvocableMethod (label='guideMeUserAccessForAccount')
    public static List<Flowoutput> guideMeUserAccessForAccount(List<FlowInput> inputValue)
    {
        userAccessCheck.userAccessCheckResponse response = new userAccessCheck.userAccessCheckResponse();
        FlowOutput output = new FlowOutput();
        List<FlowOutput> result= new list<FlowOutput>();
        result.add(output);
        
        profile currentUserProfile = [select Name from profile where id = :userinfo.getProfileId() LIMIT 1];
        string currentUserProfileName = currentUserProfile.Name;
        string fullFieldName = inputValue[0].objectName + '.' + inputValue[0].fieldName;
        
        //list<ObjectPermissions> userObjectAccess = [SELECT Id FROM ObjectPermissions WHERE sObjectType = :inputValue[0].objectName AND ParentId IN
 		//									  (SELECT Id FROM permissionset WHERE PermissionSet.Profile.Name = :currentUserProfileName)];
        
        list<PermissionSetAssignment> userObjAccess = [SELECT Id FROM PermissionSetAssignment WHERE Assignee.Id= :userinfo.getUserId() AND 
				  PermissionSetId in (SELECT ParentId FROM ObjectPermissions WHERE PermissionsRead = TRUE AND SObjectType = :inputValue[0].objectName)];
        
        list<PermissionSetAssignment> userFieldAccess = [SELECT Id FROM PermissionSetAssignment WHERE Assignee.Id= :userinfo.getUserId() AND 
                  PermissionSetId in (Select ParentId from FieldPermissions where SobjectType = :inputValue[0].objectName 
                                      and Field = :fullFieldName and PermissionsRead = true)];
        
        if(userObjAccess.size()>0 && userFieldAccess.size()>0)
            response.currentUserHaveAccess = true;
        else{
            response.currentUserHaveAccess = false;
            response.noAccessMsg = System.Label.CustomButtonNoAccessMsg;
        }    
       
        output.userHaveAccess = response.currentUserHaveAccess;
        output.noAccessMessage = response.noAccessMsg;
                
        return result;
    }

}