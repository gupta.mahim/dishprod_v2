@isTest
public class QuoteLineTrigger_Test {
    @isTest(SeeAllData=true) 
    static void handleQuoteLinesTest(){ 
        QuoteLineTriggerHandler.handleQuoteLines(SIMP_TestDataFactory.createQuoteWithQuoteLines(1,1,''));
    }
    
    @isTest(SeeAllData=true)
    static void handleQuoteLinesTestUpd(){
        QuoteLineTriggerHandler.handleQuoteLines(SIMP_TestDataFactory.createQuoteWithQuoteLines(1,1,'update'));
    }
}