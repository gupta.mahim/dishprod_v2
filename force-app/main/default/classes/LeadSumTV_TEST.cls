@IsTest
private class LeadSumTV_TEST {
    static testMethod void validateLeadSumTV() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1-2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        TV_s_Non_HD__c=1);
        
        insert L;
        
       L = [SELECT Number_of_TVs__c FROM Lead WHERE Id =:L.Id];
       System.debug('Number of TVs after trigger fired: ' + L.Number_of_TVs__c);

       // Test that the trigger correctly updated the Number of TV's field
    
       System.assertEquals('1-2', L.Number_of_TVs__c);
    }
    static testMethod void validateLeadSumTV2() {
        Lead LL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1-2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        TV_s_Non_HD__c=3);
        
        insert LL;
        
       LL = [SELECT Number_of_TVs__c FROM Lead WHERE Id =:LL.Id];
       System.debug('Number of TVs after trigger fired: ' + LL.Number_of_TVs__c);

       // Test that the trigger correctly updated the Number of TV's field
    
       System.assertEquals('3-6', LL.Number_of_TVs__c);
    }
    static testMethod void validateLeadSumTV3() {
        Lead LLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1-2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        TV_s_Non_HD__c=25);
        
        insert LLL;
        
       LLL = [SELECT Number_of_TVs__c FROM Lead WHERE Id =:LLL.Id];
       System.debug('Number of TVs after trigger fired: ' + LLL.Number_of_TVs__c);

       // Test that the trigger correctly updated the Number of TV's field
    
       System.assertEquals('7+', LLL.Number_of_TVs__c);
    }
    static testMethod void validateLeadSumTV4() {
        Lead LLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1-2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        TV_s_HD__c=2);
        
        insert LLLL;
        
       LLLL = [SELECT Number_of_TVs__c FROM Lead WHERE Id =:LLLL.Id];
       System.debug('Number of TVs after trigger fired: ' + LLLL.Number_of_TVs__c);

       // Test that the trigger correctly updated the Number of TV's field
    
       System.assertEquals('1-2', LLLL.Number_of_TVs__c);
    }
    static testMethod void validateLeadSumTV5() {
        Lead LLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1-2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        TV_s_HD__c=5);
        
        insert LLLLL;
        
       LLLLL = [SELECT Number_of_TVs__c FROM Lead WHERE Id =:LLLLL.Id];
       System.debug('Number of TVs after trigger fired: ' + LLLLL.Number_of_TVs__c);

       // Test that the trigger correctly updated the Number of TV's field
    
       System.assertEquals('3-6', LLLLL.Number_of_TVs__c);
    }
    static testMethod void validateLeadSumTV6() {
        Lead LLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1-2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        TV_s_HD__c=10);
        
        insert LLLLLL;
        
       LLLLLL = [SELECT Number_of_TVs__c FROM Lead WHERE Id =:LLLLLL.Id];
       System.debug('Number of TVs after trigger fired: ' + LLLLLL.Number_of_TVs__c);

       // Test that the trigger correctly updated the Number of TV's field
    
       System.assertEquals('7+', LLLLLL.Number_of_TVs__c);
    }
    static testMethod void validateLeadSumTV7() {
        Lead LLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1-2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        TV_s_HD__c=1,
        TV_s_Non_HD__c=1);
        
        insert LLLLLLL;
        
       LLLLLLL = [SELECT Number_of_TVs__c FROM Lead WHERE Id =:LLLLLLL.Id];
       System.debug('Number of TVs after trigger fired: ' + LLLLLLL.Number_of_TVs__c);

       // Test that the trigger correctly updated the Number of TV's field
    
       System.assertEquals('1-2', LLLLLLL.Number_of_TVs__c);
    }
    static testMethod void validateLeadSumTV8() {
        Lead LLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1-2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        TV_s_HD__c=3,
        TV_s_Non_HD__c=2);
        
        insert LLLLLLLL;
        
       LLLLLLLL = [SELECT Number_of_TVs__c FROM Lead WHERE Id =:LLLLLLLL.Id];
       System.debug('Number of TVs after trigger fired: ' + LLLLLLLL.Number_of_TVs__c);

       // Test that the trigger correctly updated the Number of TV's field
    
       System.assertEquals('3-6', LLLLLLLL.Number_of_TVs__c);
    }
    static testMethod void validateLeadSumTV9() {
        Lead LLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        TV_s_HD__c=10,
        TV_s_Non_HD__c=25);
        
        insert LLLLLLLLL;
        
       LLLLLLLLL = [SELECT Number_of_TVs__c FROM Lead WHERE Id =:LLLLLLLLL.Id];
       System.debug('Number of TVs after trigger fired: ' + LLLLLLLLL.Number_of_TVs__c);

       // Test that the trigger correctly updated the Number of TV's field
    
       System.assertEquals('7+', LLLLLLLLL.Number_of_TVs__c);
    }
}