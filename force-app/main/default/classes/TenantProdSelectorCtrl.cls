public class TenantProdSelectorCtrl
{
    static boolean multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
    @AuraEnabled
    public static OppProductsData initProdSelector(String taId)
    {
        OppProductsData prodData=new OppProductsData();
        Tenant_Account__c theTA=null;
        Boolean forcePricebookSelection = false;
        Pricebook2 theBook;
        Map<String,List<String>> existingGFEntries=new Map<String,List<String>>();
        //String tpbId='a11630000017o26';
        Tenant_Pricebook__c tpbId=Null;
         tpbId =[select id from Tenant_Pricebook__c where Pricebook_Name__c ='Incremental' Limit 1 ];
        theTA = [select Id,Type__c  from Tenant_Account__c where Id = :taId limit 1];        
        List<ProdFamilyData> prodFamilies=initProductFamilies(theTA.Type__c);
        prodData.prodFamilies=prodFamilies;
        prodData.languageList=initLanguages();

        
        // If products were previously selected need to put them in the "selected products" section to start with
        //opportunityLineItem[] shoppingCart  = [select Id, Quantity, TotalPrice, UnitPrice, Description, PriceBookEntryId, PriceBookEntry.Name, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PriceBookEntry.Product2.Maintenance__c, PriceBookEntry.PriceBook2Id from opportunityLineItem where PriceBookEntry.Product2.Maintenance__c != true AND OpportunityId=:theOpp.Id];
         Tenant_Product_Line_Item__c[] shoppingCart  = [select Id,Tenant_ProductEntry__r.Tenant_Product__r.Product_Selector_Family__c,Amdocs_Individual_Price__c,Amdocs_Incremental_Price__c,Tenant_ProductEntry__r.Grandfathered__c,Tenant_ProductEntry__r.Tenant_Product__r.Name,Price__c,Action__c, Tenant_ProductEntry__r.Id,Family__c from Tenant_Product_Line_Item__c where Tenant_Account__c=:theTA.Id and (Action__c='ADD' OR Action__c='Active') and Family__c!='Fee' order by Tenant_ProductEntry__r.Tenant_Product__r.Name];       
        // Check if Opp has a pricebook associated yet
        
        List<ExistingProdData> existingProdData=new List<ExistingProdData>();
        Map<String,List<ExistingProdDataEntry>> mapOppData=new Map<String,List<ExistingProdDataEntry>>();
        for(Tenant_Product_Line_Item__c oppLI:shoppingCart)
        {
            List<ExistingProdDataEntry> oppLIs=mapOppData.get(oppLI.Family__c);
            if(oppLIs==null)
            {
                oppLIs=new List<ExistingProdDataEntry>();
                mapOppData.put(oppLI.Family__c,oppLIs);
            }
             if(oppLI.Tenant_ProductEntry__r.Grandfathered__c==true)
            {
                List<String> gfEntries=existingGFEntries.get(oppLI.Tenant_ProductEntry__r.Tenant_Product__r.Product_Selector_Family__c);
                if(gfEntries==null){gfEntries=new List<String>();}
                gfEntries.add(oppLI.Tenant_ProductEntry__r.Id);
                existingGFEntries.put(oppLI.Tenant_ProductEntry__r.Tenant_Product__r.Product_Selector_Family__c,gfEntries);
            }
            ExistingProdDataEntry entry=new ExistingProdDataEntry();
            
            entry.name=oppLI.Tenant_ProductEntry__r.Tenant_Product__r.Name;
            if(theTA.Type__c=='Individual')
            {
                entry.UnitPrice=oppLI.Amdocs_Individual_Price__c;
            }else if(theTA.Type__c=='Incremental')
            {
                entry.UnitPrice=oppLI.Amdocs_Incremental_Price__c;
            }
            
            
            oppLIs.add(entry);
        }
        Decimal GrandTotal=0.00;
        for(String prodFamily:mapOppData.keySet())
        {
            ExistingProdData existingProdDataRec=new ExistingProdData();
            
            existingProdDataRec.family =prodFamily;
            
            List<ExistingProdDataEntry> eProds=mapOppData.get(prodFamily);
            
            existingProdDataRec.prods=eProds;
            
            Decimal TotalPrice=0.00;
            for(ExistingProdDataEntry pEntry:eProds)
            {
                if(pEntry.UnitPrice!=null)TotalPrice+=pEntry.UnitPrice;
                if(pEntry.UnitPrice!=null)GrandTotal+=pEntry.UnitPrice;
            }
            existingProdDataRec.TotalPrice=TotalPrice;
            existingProdData.add(existingProdDataRec);
        }
        prodData.existingProducts=shoppingCart;
        prodData.existingProdData=existingProdData;
        prodData.GrandTotal=GrandTotal;
        prodData.priceBookId=tpbId.id;
        prodData.tenantType=theTA.Type__c;
        /*if(theOpp.Pricebook2Id == null)
        {
            Pricebook2[] activepbs = [select Id, Name from Pricebook2 where Id = '01s60000000dMje'];
            if(activepbs.size() == 2){ forcePricebookSelection = true;    theBook = new Pricebook2();}
            else{theBook = activepbs[0];}
        }
        else{
            theBook = theOpp.Pricebook2;
            prodData.priceBookId=theBook.id; 
        }*/
        prodData.existingGFEntries=existingGFEntries;
        return prodData;
    }
    
    @AuraEnabled
    public static OppProductsData loadProductData(String taId,String tenantType,String theBookId,String family,String searchText,String prodLanguage,String strExistingGFProd)
    {
        OppProductsData prodData=new OppProductsData();
        
        List<ProdData> prodDataList=new List<ProdData>();
        
        Map<String,List<String>> existingGFProds=(Map<String,List<String>>)JSON.deserialize(strExistingGFProd, Map<String,List<String>>.class);
        Tenant_ProductEntry__c[] availableProducts=loadProductEntries(existingGFProds,theBookId,tenantType,family,searchText,prodLanguage);
        
        for(Tenant_ProductEntry__c pbEntry:availableProducts)
        {
            ProdData aProdData=new ProdData();
            
            aProdData.productEntry=pbEntry;
            if(tenantType=='Individual')
            {
                aProdData.UnitPrice=pbEntry.Tenant_Product__r.Individual_Price__c;
            }else if(tenantType=='Incremental')
            {
                aProdData.UnitPrice=pbEntry.Tenant_Product__r.Incremental_Price__c;
            }
            prodDataList.add(aProdData);
        }
        
        prodData.availableProducts=prodDataList;
        return prodData;
    }
    private static Tenant_ProductEntry__c[] loadProductEntries(Map<String,List<String>> existingGFProds,String theBookId,String tenantType,String family,String searchText,String prodLanguage)
    {
        Tenant_ProductEntry__c[] availableProducts=null;
        Tenant_ProductEntry__c[] existingGFProducts=null;
        String strAny='Any';
        
        System.debug('Book Id=['+theBookId+']');
        String qString = 'select Id,Tenant_Pricebook__c, Tenant_Product__r.Name, Tenant_Product__r.Product_Selector_Family__c,'
        +' Tenant_Product__r.Individual_Price__c,Tenant_Product__r.Incremental_Price__c from Tenant_ProductEntry__c where Grandfathered__c = False and Active__c=true'
        +'  and Tenant_Product__r.Product_Selector_Family__c!=null and Tenant_Pricebook__c = \'' + theBookId + '\'';
        
         if(existingGFProds!=null && existingGFProds.size()>0 && existingGFProds.get(family)!=null && existingGFProds.get(family).size()>0)
        {
            List<String> gfProds=existingGFProds.get(family);
            existingGFProducts=[select Id,Tenant_Pricebook__c, Tenant_Product__r.Name, Tenant_Product__r.Product_Selector_Family__c,Tenant_Product__r.Individual_Price__c,Tenant_Product__r.Incremental_Price__c from Tenant_ProductEntry__c where Id in:gfProds];
            
            //qString +=' and ((Grandfathered__c = False and IsActive=true) or (Id in '+gfProds+')';
        }
        
        if(!String.IsEmpty(searchText))
        qString+= ' and Tenant_Product__r.Name like\'%'+searchText+'%\'';
        
        qString+= ' and Tenant_Product__r.Product_Selector_Family__c=\''+family+'\'';
        
        if(!String.IsEmpty(prodLanguage))
        qString+= ' and Tenant_Product__r.Language__c =\''+prodLanguage+'\'';
        
        qString+= ' and (Tenant_Product__r.Tenant_Product_Type__c =\''+tenantType+'\' Or Tenant_Product__r.Tenant_Product_Type__c =\''+strAny+'\')';
        
        qString+= ' order by Tenant_Product__r.Name';
        
        availableProducts= database.query(qString);
        if(existingGFProducts!=null && existingGFProducts.size()>0)
        {
            availableProducts.addAll(existingGFProducts);
        }
        return availableProducts;    
    }
    @AuraEnabled
    public static void saveProducts(String taId,String strNewProdData,String strRemoveProdData)
    {
        if(String.isBlank(strNewProdData) || String.isBlank(strRemoveProdData)){return;}
        List<String> newProdData=(List<String>)JSON.deserialize(strNewProdData, List<String>.class);
        List<Tenant_Product_Line_Item__c> removeProdData=(List<Tenant_Product_Line_Item__c>)JSON.deserialize(strRemoveProdData, List<Tenant_Product_Line_Item__c>.class);
        performSave(taId,newProdData,removeProdData);
        
    }
     @AuraEnabled
    public static void performSave(String taId, List<String> newProdData,List<Tenant_Product_Line_Item__c> removeProdData)
    {
    
        List<Tenant_Product_Line_Item__c> oppList=new List<Tenant_Product_Line_Item__c>();
        Tenant_Product_Line_Item__c[] existingProds  = [select Id,Tenant_ProductEntry__r.Tenant_Product__r.Name,Price__c,Action__c, Tenant_ProductEntry__r.Id,Family__c from Tenant_Product_Line_Item__c where Tenant_Account__c=:taId];       
        Map<Id,Tenant_Product_Line_Item__c> mapExistingProds=new Map<Id,Tenant_Product_Line_Item__c>();
        
        for(Tenant_Product_Line_Item__c tpLI:existingProds){mapExistingProds.put(tpLI.Tenant_ProductEntry__r.Id,tpLI);}
        
        boolean prodExist=true;
        
        for(String pbE:newProdData)
        {
            
            Tenant_Product_Line_Item__c oppLI=mapExistingProds.get(pbe);
            if(oppLI==null){oppLI=new Tenant_Product_Line_Item__c();prodExist=false;}
            
            if(prodExist==false){oppLI.Tenant_ProductEntry__c=pbE;oppLI.Tenant_Account__c=taId;}
            
            
            oppLI.Action__c='ADD';
            
            oppList.add(oppLI);
        }
        for(Tenant_Product_Line_Item__c existoppLi:removeProdData)
        {
            String nextAction=existoppLi.Action__c=='Active'?'REMOVE':'Removed';
            existoppLi.Action__c=nextAction;
            
            oppList.add(existoppLi);
        }
        upsert oppList;
        System.debug('Prod Saved');
    }
    @AuraEnabled
    public static List<ProdFamilyData> initProductFamilies(String tenantType)
    {
        List<ProdFamilyData> prodData=new List<ProdFamilyData>();
        
        Schema.DescribeFieldResult fieldResult = Tenant_Product__c.Product_Selector_Family__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple)
        {
            String label=pickListVal.getLabel();
            if(label!='Fee')
            {
                Boolean isCore=label.indexof('Core')!=-1;
                if(isCore && tenantType=='Incremental')
                {
                    continue;
                }
                prodData.add(new ProdFamilyData(label,isCore));
            }
            
        }
        
        return prodData;
    }
    @AuraEnabled
    public static List<String> initLanguages()
    {
        List<String> languageData=new List<String>();
        
        Schema.DescribeFieldResult fieldResult = Tenant_Product__c.Language__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple)
        {
            languageData.add(pickListVal.getLabel());
        }
        
        return languageData;
    }
    public class OppProductsData
    {
        @AuraEnabled
        public String priceBookId {get;set;}
        
        @AuraEnabled
        public String tenantType {get;set;}
        
        @AuraEnabled
        public Map<String,List<String>> existingGFEntries {get;set;}
        
        @AuraEnabled
        public List<ProdFamilyData> prodFamilies{get;set;}
        
        @AuraEnabled
        public List<String> languageList{get;set;}
        
        @AuraEnabled
        public ProdData[] availableProducts;
        
        @AuraEnabled
        public Tenant_Product_Line_Item__c[] existingProducts;
        
        @AuraEnabled
        public ExistingProdData[] existingProdData;
        
        @AuraEnabled
        public Decimal GrandTotal{get;set;}
        
    }
    public class ProdData
    {
        @AuraEnabled
        public boolean isSelected {get;set;}
        
        @AuraEnabled
        public boolean isExisting {get;set;}
        
        @AuraEnabled
        public Decimal UnitPrice{get;set;}
        
        @AuraEnabled
        public Tenant_ProductEntry__c productEntry{get;set;}
    }
    public class ExistingProdDataEntry
    {
        @AuraEnabled
        public String Name {get;set;}
        
        @AuraEnabled
        public Decimal UnitPrice{get;set;}
    }
    public class ExistingProdData
    {
        @AuraEnabled
        public String family {get;set;}
        
        @AuraEnabled
        public List<ExistingProdDataEntry> prods{get;set;}
        
        @AuraEnabled
        public Decimal TotalPrice{get;set;}
    }
    public class ProdFamilyData
    {
        ProdFamilyData(String inCategoryName,boolean inIsCore)
        {
            categoryName=inCategoryName;
            isCore=inIsCore;
        }
        @AuraEnabled
        public String categoryName{get;set;}
        
        @AuraEnabled
        public boolean isCore{get;set;}
    }
}