@isTEST
 private class CaseOppUpdate_Test{

    private static testmethod void testCaseOppUpdate(){
    Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter';
        a.phone = '(303) 555-5555';
        insert a;
        
            Opportunity o = new Opportunity();
                o.Name = 'Test';
                o.AccountId = a.Id;
                o.BTVNA_Request_Type__c = 'Site Survey';
                o.LeadSource='BTVNA Portal';
                o.Property_Type__c='Private';
                o.misc_code_siu__c='N999';
                o.CloseDate=system.Today();
                o.StageName='Closed Won';
                 o.Restricted_Programming__c = 'True';
            insert o;

  Case c = new Case();
                   
                    c.Opportunity__c = o.Id;
                    c.AccountId = a.Id;
                    c.Status = 'Request Completed';
                    c.Origin = 'PRM';
                    c.RequestedAction__c = 'Activation';
                    c.RecordTypeId = '0126000000017LQ';
                    c.Number_of_Accounts__c = 2;
                    c.CSG_Account_Number__c = '82551234567890';
                    
                    c.Requested_Actvation_Date_Time__c = system.Today();
                      insert c;
                      update c;
            }
            
  private static testmethod void testCaseOppUpdate2(){
    Account a2 = new Account();
        a2.Name = 'Test Account';
        a2.Programming__c = 'Starter';
        a2.phone = '(303) 555-5555';
        insert a2;
        
            Opportunity o2 = new Opportunity();
                o2.Name = 'Test';
                o2.AccountId = a2.Id;
                o2.BTVNA_Request_Type__c = 'Pre-Built';
                o2.LeadSource='BTVNA Portal';
                o2.Property_Type__c='Private';
                o2.misc_code_siu__c='N999';
                o2.CloseDate=system.Today();
                o2.StageName='Closed Won';
                 o2.Restricted_Programming__c = 'True';
            insert o2;

  Case c2 = new Case();
                   
                    c2.Opportunity__c = o2.Id;
                    c2.AccountId = a2.Id;
                    c2.Status = 'Request Completed';
                    c2.Origin = 'PRM';
                    c2.RequestedAction__c = 'Pre-Build Activation';
                    c2.RecordTypeId = '0126000000017LQ';
                    c2.Number_of_Accounts__c = 2;
                    c2.CSG_Account_Number__c = '82551234567890';
                    
                    c2.Requested_Actvation_Date_Time__c = system.Today();
                      insert c2;
                      update c2;
            }
            
   private static testmethod void testCaseOppUpdate3(){
      Account a3 = new Account();
        a3.Name = 'Test Account';
        a3.Programming__c = 'Starter';
        a3.phone = '(303) 555-5555';
        insert a3;
        
            Opportunity o3 = new Opportunity();
                o3.Name = 'Test';
                o3.AccountId = a3.Id;
                o3.BTVNA_Request_Type__c = 'Pre-Built';
                o3.LeadSource='BTVNA Portal';
                o3.Property_Type__c='Private';
                o3.misc_code_siu__c='N999';
                o3.CloseDate=system.Today();
                o3.StageName='Closed Won';
                 o3.Restricted_Programming__c = 'True';
            insert o3;

  Case c3 = new Case();
                   
                    c3.Opportunity__c = o3.Id;
                    c3.AccountId = a3.Id;
                    c3.Status = 'Request Completed';
                    c3.Origin = 'PRM';
// removed 3.14.17 in attempted to convert trigger to flow                    c3.RequestedAction__c = 'Disconnection';
c3.RequestedAction__c = 'Active'; // added 3.14.17 see above
                    c3.RecordTypeId = '0126000000017LQ';
                    c3.Number_of_Accounts__c = 2;
                    c3.CSG_Account_Number__c = '82551234567890';
                    
                    c3.Requested_Actvation_Date_Time__c = system.Today();
                      insert c3;
                      update c3;
                   
                    
 
            }
            
 private static testmethod void testCaseOppUpdate4(){
    Account a4 = new Account();
        a4.Name = 'Test Account';
        a4.Programming__c = 'Starter';
        a4.phone = '(303) 555-5555';
        insert a4;
        
            Opportunity o4 = new Opportunity();
                o4.Name = 'Test';
                o4.AccountId = a4.Id;
                o4.BTVNA_Request_Type__c = 'Pre-Built';
                o4.LeadSource='BTVNA Portal';
                o4.Property_Type__c='Private';
                o4.misc_code_siu__c='N999';
                o4.CloseDate=system.Today();
                o4.StageName='Closed Won';
                 o4.Restricted_Programming__c = 'True';
            insert o4;

  Case c4 = new Case();
                   
                    c4.Opportunity__c = o4.Id;
                    c4.AccountId = a4.Id;
                    c4.Status = 'Form Submitted';
                    c4.Origin = 'Direct';
                    c4.RequestedAction__c = 'Activation';
                    c4.RecordTypeId = '0126000000017LQ';
                    c4.Number_of_Accounts__c = 2;
                    c4.CSG_Account_Number__c = '82551234567890';
                    
                    c4.Requested_Actvation_Date_Time__c = system.Today();
                      insert c4;
                      update c4;

            }    
    private static testmethod void testCaseOppUpdate5(){
    Account a5 = new Account();
        a5.Name = 'Test Account';
        a5.Programming__c = 'Starter';
        a5.phone = '(303) 555-5555';
        insert a5;
        
            Opportunity o5 = new Opportunity();
                o5.Name = 'Test';
                o5.AccountId = a5.Id;
                o5.BTVNA_Request_Type__c = 'Site Survey';
                o5.LeadSource='BTVNA Portal';
                o5.Property_Type__c='Private';
                o5.misc_code_siu__c='N999';
                o5.CloseDate=system.Today();
                o5.StageName='Closed Won';
                 o5.Restricted_Programming__c = 'True';
            insert o5;

  Case c5 = new Case();
                   
                    c5.Opportunity__c = o5.Id;
                    c5.AccountId = a5.Id;
                    c5.Status = 'Request Completed';
                    c5.Origin = 'PRM';
                    c5.RequestedAction__c = 'Account Pre-Build';
                    c5.RecordTypeId = '0126000000017LQ';
                    c5.Number_of_Accounts__c = 2;
                    c5.CSG_Account_Number__c = '82551234567890';
                    
                    c5.Requested_Actvation_Date_Time__c = system.Today();
                      insert c5;
                      update c5;
    }    

    private static testmethod void testCaseOppUpdate6(){
    Account a6 = new Account();
        a6.Name = 'Test Account';
        a6.Programming__c = 'Starter';
        a6.phone = '(303) 555-5555';
        insert a6;
        
   Opportunity o6 = new Opportunity();
                o6.Name = 'Test';
                o6.AccountId = a6.Id;
                o6.BTVNA_Request_Type__c = 'Site Survey';
                o6.LeadSource='BTVNA Portal';
                o6.Property_Type__c='Private';
                o6.misc_code_siu__c='N999';
                o6.CloseDate=system.Today();
                o6.StageName='Closed Won';
                o6.Restricted_Programming__c = 'True';
                o6.Pricebook2id = '01s60000000EIEL';
            insert o6;

    opportunityLineItem oli6 = new opportunityLineItem();
                oli6.QUANTITY=1;
                oli6.PRICEBOOKENTRYID='01u600000043h31AAA';
                oli6.OPPORTUNITYID=o6.id;    
                oli6.TotalPrice = 1.11;
                insert oli6;
    
  Case c6 = new Case();
                   
                    c6.Opportunity__c = o6.Id;
                    c6.AccountId = a6.Id;
                    c6.Status = 'Request Completed';
                    c6.Origin = 'PRM';
                    c6.RequestedAction__c = 'Activation';
                    c6.RecordTypeId = '0126000000017LQ';
                    c6.Number_of_Accounts__c = 2;
                    c6.CSG_Account_Number__c = '82551234567890';
                   
                    c6.Requested_Actvation_Date_Time__c = system.Today();
                      insert c6;
                      update c6;
            }

    private static testmethod void testCaseOppUpdate7(){
    Account A7 = new Account();
        A7.Name = 'Test Account';
        A7.Programming__c = 'Starter';
        A7.phone = '(303) 555-5555';
        insert A7;
        
   Opportunity O7 = new Opportunity();
                O7.Name = 'Test';
                O7.AccountId = A7.Id;
                O7.BTVNA_Request_Type__c = 'Site Survey';
                O7.LeadSource='BTVNA Portal';
                O7.Property_Type__c='Private';
                O7.misc_code_siu__c='N999';
                O7.Property_Status__c = 'Disconnected';
                O7.CloseDate=system.Today();
                O7.StageName='Chewbacca';
                O7.CSG_Account_Number__c = '82561234569123';
                O7.Restricted_Programming__c = 'True';
                O7.Pricebook2id = '01s60000000EIEL';
                O7.Disconnect_Date__c = system.Today();
            insert O7;

    opportunityLineItem Oli7 = new opportunityLineItem();
                Oli7.QUANTITY=1;
                Oli7.PRICEBOOKENTRYID='01u600000043h31AAA';
                Oli7.OPPORTUNITYID=O7.id;    
                Oli7.TotalPrice = 1.11;
                insert Oli7;
    
  Case C7 = new Case();
                   
                    C7.Opportunity__c = O7.Id;
                    C7.AccountId = A7.Id;
                    C7.Status = 'Request Completed';
                    C7.Origin = 'PRM';
                    C7.RequestedAction__c = 'Activation';
                    C7.RecordTypeId = '0126000000017LQ';
                    C7.Number_of_Accounts__c = 2;
                    C7.CSG_Account_Number__c = '82551234567890';
                   
                    C7.Requested_Actvation_Date_Time__c = system.Today();
                      insert C7;
                      update C7;
            }            
            
    private static testmethod void testCaseOppUpdate8(){
    Account A8 = new Account();
        A8.Name = 'Test Account';
        A8.Programming__c = 'Starter';
        A8.phone = '(303) 555-5555';
        insert A8;
        
   Opportunity O8 = new Opportunity();
                O8.Name = 'Test';
                O8.AccountId = A8.Id;
                O8.BTVNA_Request_Type__c = 'Site Survey';
                O8.LeadSource='BTVNA Portal';
                O8.Property_Type__c='Private';
                O8.misc_code_siu__c='N999';
                O8.Property_Status__c = 'Disconnected';
                O8.CloseDate=system.Today();
                O8.StageName='NOT Closed Won';
                O8.CSG_Account_Number__c = '8256123456789123';
                O8.Disconnect_Date__c =system.Today();
                O8.Restricted_Programming__c = 'True';
                O8.Pricebook2id = '01s60000000EIEL';
            insert O8;

    opportunityLineItem Oli8 = new opportunityLineItem();
                Oli8.QUANTITY=1;
                Oli8.PRICEBOOKENTRYID='01u600000043h31AAA';
                Oli8.OPPORTUNITYID=O8.id;    
                Oli8.TotalPrice = 1.11;
                insert Oli8;
    
  Case C8 = new Case();
                   
                    C8.Opportunity__c = O8.Id;
                    C8.AccountId = A8.Id;
                    C8.Status = 'Request Completed';
                    C8.Origin = 'PRM';
                    C8.RequestedAction__c = 'Pre-Build';
                    C8.RecordTypeId = '0126000000017LQ';
                    C8.Number_of_Accounts__c = 2;
                    C8.CSG_Account_Number__c = '82551234567890';
                   
                    C8.Requested_Actvation_Date_Time__c = system.Today();
                      insert C8;
                      update C8;
            }
                private static testmethod void testCaseOppUpdate9(){
    Account A9 = new Account();
        A9.Name = 'Test Account';
        A9.Programming__c = 'Starter';
        A9.phone = '(303) 555-5555';
        insert A9;
        
   Opportunity O9 = new Opportunity();
                O9.Name = 'Test';
                O9.AccountId = A9.Id;
                O9.BTVNA_Request_Type__c = 'Site Survey';
                O9.LeadSource='BTVNA Portal';
                O9.Property_Type__c='Private';
                O9.misc_code_siu__c='N999';
                O9.Property_Status__c = 'Disconnected';
                O9.CloseDate=system.Today();
                O9.StageName='NOT Closed Won';
                O9.CSG_Account_Number__c = '8256123456799123';
                O9.Disconnect_Date__c =system.Today();
                O9.Restricted_Programming__c = 'True';
                O9.Pricebook2id = '01s60000000EIEL';
            insert O9;

    opportunityLineItem Oli9 = new opportunityLineItem();
                Oli9.QUANTITY=1;
                Oli9.PRICEBOOKENTRYID='01u600000043h31AAA';
                Oli9.OPPORTUNITYID=O9.id;    
                Oli9.TotalPrice = 1.11;
                insert Oli9;
    
  Case C9 = new Case();
                   
                    C9.Opportunity__c = O9.Id;
                    C9.AccountId = A9.Id;
                    C9.Status = 'Request Completed';
                    C9.Origin = 'PRM';
                    C9.RequestedAction__c = 'Pre-Activation';
                    C9.RecordTypeId = '0126000000017LQ';
                    C9.Number_of_Accounts__c = 2;
                    C9.CSG_Account_Number__c = '82551234567990';
                   
                    C9.Requested_Actvation_Date_Time__c = system.Today();
                      insert C9;
                      update C9;
            }
private static testmethod void testCaseOppUpdate10(){
    Account A10 = new Account();
        A10.Name = 'Test Account';
        A10.Programming__c = 'Starter';
        A10.phone = '(303) 555-5555';
        insert A10;
        
   Opportunity O10 = new Opportunity();
                O10.Name = 'Test';
                O10.AccountId = A10.Id;
                O10.BTVNA_Request_Type__c = 'Site Survey';
                O10.LeadSource='BTVNA Portal';
                O10.Property_Type__c='Private';
                O10.misc_code_siu__c='N100';
                O10.Property_Status__c = 'Disconnected';
                O10.CloseDate=system.Today();
                O10.StageName='NOT Closed Won';
                O10.CSG_Account_Number__c = '8256123456789';
                O10.Disconnect_Date__c =system.Today();
                O10.Restricted_Programming__c = 'True';
                O10.Pricebook2id = '01s60000000EIEL';
            insert O10;

    opportunityLineItem Oli10 = new opportunityLineItem();
                Oli10.QUANTITY=1;
                Oli10.PRICEBOOKENTRYID='01u600000043h31AAA';
                Oli10.OPPORTUNITYID=O10.id;    
                Oli10.TotalPrice = 1.11;
                insert Oli10;
    
  Case C10 = new Case();
                   
                    C10.Opportunity__c = O10.Id;
                    C10.AccountId = A10.Id;
                    C10.Status = 'Request Completed';
                    C10.Origin = 'PRM';
                    C10.RequestedAction__c = 'Activation Pending';
                    C10.RecordTypeId = '0126000000017LQ';
                    C10.Number_of_Accounts__c = 2;
                    C10.CSG_Account_Number__c = '8255123456789';
                   
                    C10.Requested_Actvation_Date_Time__c = system.Today();
                      insert C10;
                      update C10;
            }            
    private static testmethod void testCaseOppUpdate11(){
    Account A11 = new Account();
        A11.Name = 'Test Account';
        A11.Programming__c = 'Starter';
        A11.phone = '(303) 555-5555';
        insert A11;
        
   Opportunity O11 = new Opportunity();
                O11.Name = 'Test';
                O11.AccountId = A11.Id;
                O11.BTVNA_Request_Type__c = 'Site Survey';
                O11.LeadSource='BTVNA Portal';
                O11.Property_Type__c='Private';
                O11.misc_code_siu__c='N101';
                O11.Property_Status__c = 'Disconnected';
                O11.CloseDate=system.Today();
                O11.StageName='NOT Closed Won';
                O11.CSG_Account_Number__c = '8256123456789';
                O11.Disconnect_Date__c =system.Today();
                O11.Restricted_Programming__c = 'True';
                O11.Pricebook2id = '01s60000000EIEL';
            insert O11;

    opportunityLineItem Oli11 = new opportunityLineItem();
                Oli11.QUANTITY=1;
                Oli11.PRICEBOOKENTRYID='01u600000043h31AAA';
                Oli11.OPPORTUNITYID=O11.id;    
                Oli11.TotalPrice = 1.11;
                insert Oli11;
    
  Case C11 = new Case();
                   
                    C11.Opportunity__c = O11.Id;
                    C11.AccountId = A11.Id;
                    C11.Status = 'Activation Pending';
                    C11.Origin = 'PRM';
                    C11.RequestedAction__c = 'Pre-Activation + Activation';
                    C11.RecordTypeId = '0126000000017LQ';
                    C11.Number_of_Accounts__c = 2;
                    C11.CSG_Account_Number__c = '8255123456789';
                    C11.Pre_Activation_Date__c = system.Today();
                    C11.Requested_Actvation_Date_Time__c = system.Today();
                      insert C11;
                      update C11;
            }       

    private static testmethod void testCaseOppUpdate12(){
    Account A12 = new Account();
        A12.Name = 'Test Account';
        A12.Programming__c = 'Starter';
        A12.phone = '(303) 555-5555';
        insert A12;
        
   Opportunity O12 = new Opportunity();
                O12.Name = 'Test';
                O12.AccountId = A12.Id;
                O12.BTVNA_Request_Type__c = 'Site Survey';
                O12.LeadSource='BTVNA Portal';
                O12.Property_Type__c='Private';
                O12.misc_code_siu__c='N101';
                O12.Property_Status__c = 'Disconnected';
                O12.CloseDate=system.Today();
                O12.StageName='Closed Won';
                O12.CSG_Account_Number__c = '8256123456789';
                O12.Disconnect_Date__c =system.Today();
                O12.Restricted_Programming__c = 'True';
                O12.Pricebook2id = '01s60000000EIEL';
            insert O12;

    opportunityLineItem Oli12 = new opportunityLineItem();
                Oli12.QUANTITY=1;
                Oli12.PRICEBOOKENTRYID='01u600000043h31AAA';
                Oli12.OPPORTUNITYID=O12.id;    
                Oli12.TotalPrice = 1.11;
                insert Oli12;
    
  Case C12 = new Case();
                   
                    C12.Opportunity__c = O12.Id;
                    C12.AccountId = A12.Id;
                    C12.Status = 'Activation Pending';
                    C12.Origin = 'PRM';
                    C12.RequestedAction__c = 'Pre-Activation + Activation';
                    C12.RecordTypeId = '0126000000017LQ';
                    C12.Number_of_Accounts__c = 2;
                    C12.CSG_Account_Number__c = '8255123456789';
                    C12.Pre_Activation_Date__c = system.Today();
                    C12.Requested_Actvation_Date_Time__c = system.Today();
                      insert C12;
                      update C12;
            }
    private static testmethod void testCaseOppUpdate13(){
    Account A13 = new Account();
        A13.Name = 'Test Account';
        A13.Programming__c = 'Starter';
        A13.phone = '(303) 555-5555';
        insert A13;
        
   Opportunity O13 = new Opportunity();
                O13.Name = 'Test';
                O13.AccountId = A13.Id;
                O13.BTVNA_Request_Type__c = 'Site Survey';
                O13.LeadSource='BTVNA Portal';
                O13.Property_Type__c='Private';
                O13.misc_code_siu__c='N101';
                O13.Property_Status__c = 'Disconnected';
                O13.CloseDate=system.Today();
                O13.StageName='Chewbacca';
                O13.CSG_Account_Number__c = '';
                O13.Disconnect_Date__c =system.Today();
                O13.Restricted_Programming__c = 'True';
                O13.Pricebook2id = '01s60000000EIEL';
            insert O13;

    opportunityLineItem Oli13 = new opportunityLineItem();
                Oli13.QUANTITY=1;
                Oli13.PRICEBOOKENTRYID='01u600000043h31AAA';
                Oli13.OPPORTUNITYID=O13.id;    
                Oli13.TotalPrice = 1.11;
                insert Oli13;
    
  Case C13 = new Case();
                   
                    C13.Opportunity__c = O13.Id;
                    C13.AccountId = A13.Id;
                    C13.Status = 'Activation Pending';
                    C13.Origin = 'PRM';
                    C13.RequestedAction__c = 'Pre-Activation + Activation';
                    C13.RecordTypeId = '0126000000017LQ';
                    C13.Number_of_Accounts__c = 2;
                    C13.CSG_Account_Number__c = '';
                    C13.Pre_Activation_Date__c = system.Today();
                    C13.Requested_Actvation_Date_Time__c = system.Today();
                      insert C13;
                      update C13;
            }


            
}