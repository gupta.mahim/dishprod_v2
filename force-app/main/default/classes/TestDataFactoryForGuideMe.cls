@isTest
public class TestDataFactoryForGuideMe {
    
    public static AmDocs_Login__c createAmDocsLogin(){
    AmDocs_Login__c aml = new AmDocs_Login__c();
        aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;
        return aml;
    }
    
    public static Account createAccount(){
        Account acct1 = new Account();
        acct1.Name = 'Test Account 1';
        acct1.ToP__c = 'Integrator';
        acct1.Pyscical_Address__c = '1011 Collie Path';
        acct1.City__c = 'Round Rock';
        acct1.State__c = 'TX';
        acct1.Zip__c = '78664';
        acct1.Amdocs_CreateCustomer__c = false;
        acct1.Distributor__c = 'PACE';
        acct1.OE_AR__c = '12345';
        acct1.Amdocs_CustomerID__c = '123';
        acct1.Amdocs_BarID__c = '456';
        acct1.Amdocs_FAID__c = '789';
        insert acct1;
        return acct1;
    }
    
    public static Opportunity createOpportunity(Id accId){
        Opportunity opp1 = New Opportunity();
        opp1.Name = 'Test Opp 123456789 Testing opp1';
        opp1.CloseDate=system.Today();
        opp1.StageName='Closed Won';
        opp1.First_Name_of_Property_Representative__c = 'Mahim';
        opp1.Name_of_Property_Representative__c = 'Gupta';
        opp1.Business_Address__c = '1011 Collie Path';
        opp1.Business_City__c = 'Round Rock';
        opp1.Business_State__c = 'TX';
        opp1.Business_Zip_Code__c = '78664';
        opp1.Billing_Contact_Phone__c = '512-383-5201';
        opp1.Billing_Contact_Email__c = 'mahim.gupta@dish.com';
        opp1.Smartbox_Leased__c = false;
        opp1.AmDocs_SiteID__c = '22222';
        opp1.AccountId = accId;
        opp1.AmDocs_tenantType__c = 'Incremental';
        opp1.AmDocs_ServiceID__c='012';
        opp1.Amdocs_CustomerID__c = '123';
        opp1.Amdocs_BarID__c = '456';
        opp1.Amdocs_FAID__c = '789';
        opp1.System_Type__c='QAM';
        opp1.Category__c='FTG';
        opp1.Address__c='address';
        opp1.City__c='Austin';
        opp1.State__c='TX';
        opp1.Zip__c='78664';
        opp1.Phone__c='5122965581';
        opp1.AmDocs_Property_Sub_Type__c='Dormitory';
        opp1.Number_of_Units__c=100;
        opp1.Smartbox_Leased__c=false;
        opp1.RecordTypeId='012600000005ChE';
        insert opp1;
        return opp1;
    }
    
    public static Tenant_Account__c createTenantAccount(Id oppId){
    	Tenant_Account__c ta = new Tenant_Account__c();
        ta.Address__c ='1011 Coliie Path';
        ta.City__c = 'Round Rock';
        ta.State__c = 'TX';
        ta.Zip__c = '78664';
        ta.Unit__c = '1A';
        ta.Phone_Number__c = '5123835201';
        ta.Email_Address__c = 'mahim.gupta@dish.com';
        ta.First_Name__c = 'Mahim';
        ta.Last_Name__c = 'Gupta';
        ta.Type__c = 'Incremental';
        ta.Opportunity__c = oppId;
        ta.Action__c = 'DupeDisconnect';
        insert ta;
        return ta;
    }
    
    public static Tenant_Account__c createTenantAccount2(Id oppId){
    	Tenant_Account__c ta = new Tenant_Account__c();
        ta.Address__c ='1011 Coliie Path';
        ta.City__c = 'Round Rock';
        ta.State__c = 'TX';
        ta.Zip__c = '78664';
        ta.Unit__c = '1A';
        ta.Phone_Number__c = '5123835201';
        ta.Email_Address__c = 'mahim.gupta@dish.com';
        ta.First_Name__c = 'Mahim2';
        ta.Last_Name__c = 'Gupta';
        ta.Type__c = 'Incremental';
        ta.Opportunity__c = oppId;
        ta.AmDocs_ServiceID__c = '123456';
        ta.Action__c = 'DupeDisconnect';
        insert ta;
        return ta;
    }
    
    public static Tenant_Account__c createTenantAccount3(Id oppId){
    	Tenant_Account__c ta = new Tenant_Account__c();
        ta.Address__c ='1011 Coliie Path';
        ta.City__c = 'Round Rock';
        ta.State__c = 'TX';
        ta.Zip__c = '78664';
        ta.Unit__c = '1A';
        ta.Phone_Number__c = '5123835201';
        ta.Email_Address__c = 'mahim.gupta@dish.com';
        ta.First_Name__c = 'Mahim3';
        ta.Last_Name__c = 'Gupta';
        ta.Type__c = 'Incremental';
        ta.Opportunity__c = oppId;
        ta.AmDocs_ServiceID__c = '123457';
        ta.Action__c = 'DupeDisconnect';
        ta.Reset__c = 'ChewbaccaIsTesting';
        insert ta;
        return ta;
    }
 
    public static Tenant_Equipment__c  createTenantEquipment(Id oppId, Id taId){
        Tenant_Equipment__c te = New Tenant_Equipment__c ();
        te.Opportunity__c = oppId;
        te.Name = 'R123456789';
        te.Smart_Card__c = 'S1234567891';
        te.Location__c = 'D';
        te.Action__c = 'DupeDisconnect';
        te.Address__c ='1011 Coliie Path';
        te.City__c = 'Round Rock';
        te.State__c = 'TX';
        te.Zip_Code__c = '78664';
        te.Unit__c = '1A';
        te.Phone_Number__c = '5123835201';
        te.Email_Address__c = 'mahim.gupta@dish.com';
        te.Customer_First_Name__c = 'Jerry';
        te.Customer_Last_Name__c = 'Clifft';
        te.Tenant_Account__c = taId;
        te.Amdocs_Tenant_ID__c = '123456';
        insert te;
        return te;
    }
    
    public static Tenant_Equipment__c  createTenantEquipment1(Id oppId, Id taId){
        Tenant_Equipment__c te1 = new Tenant_Equipment__c();
        te1.Opportunity__c = oppId;
        te1.Name = 'R123456788';
        te1.Smart_Card__c = 'S1234567891';
        te1.Location__c = 'D';
        te1.Action__c = 'DupeDisconnect';
        te1.Address__c ='1011 Coliie Path';
        te1.City__c = 'Round Rock';
        te1.State__c = 'TX';
        te1.Zip_Code__c = '78664';
        te1.Unit__c = '1A';
        te1.Phone_Number__c = '5123835201';
        te1.Email_Address__c = 'jerry.clifft@dish.com';
        te1.Customer_First_Name__c = 'Jerry';
        te1.Customer_Last_Name__c = 'Clifft';
        te1.Tenant_Account__c = taId;
        //te1.Amdocs_Tenant_ID__c = '123456';
        te1.Reset__c = 'ChewbaccaIsTesting' ;
        insert te1;
        return te1;
    }
    
    public static Tenant_Equipment__c  createTenantEquipment2(Id oppId, Id taId){
        Tenant_Equipment__c te2 = new Tenant_Equipment__c();
        te2.Opportunity__c = oppId;
        te2.Name = 'R123456787';
        te2.Smart_Card__c = 'S1234567891';
        te2.Location__c = 'D';
        te2.Action__c = 'DupeDisconnect';
        te2.Address__c ='1011 Coliie Path';
        te2.City__c = 'Round Rock';
        te2.State__c = 'TX';
        te2.Zip_Code__c = '78664';
        te2.Unit__c = '1A';
        te2.Phone_Number__c = '5123835201';
        te2.Email_Address__c = 'jerry.clifft@dish.com';
        te2.Customer_First_Name__c = 'Jerry';
        te2.Customer_Last_Name__c = 'Clifft';
        te2.Tenant_Account__c = taId;
        te2.Amdocs_Tenant_ID__c = '123456';
        te2.Reset__c = 'ChewbaccaIsTesting' ;
        insert te2;
        return te2;
    }  
    
     public static Tenant_Equipment__c  createTenantEquipment3(Id oppId, Id taId){
        Tenant_Equipment__c te3 = new Tenant_Equipment__c();
        te3.Opportunity__c = oppId;
        te3.Name = 'R123456789';
        te3.Smart_Card__c = 'S1234567891';
        te3.Location__c = 'D';
        te3.Action__c = 'DupeRemove';
        te3.Address__c ='1011 Coliie Path';
        te3.City__c = 'Round Rock';
        te3.State__c = 'TX';
        te3.Zip_Code__c = '78664';
        te3.Unit__c = '1A';
        te3.Phone_Number__c = '5123835201';
        te3.Email_Address__c = 'jerry.clifft@dish.com';
        te3.Customer_First_Name__c = 'Jerry';
        te3.Customer_Last_Name__c = 'Clifft';
        te3.Tenant_Account__c = taId;
        te3.Amdocs_Tenant_ID__c = '123457';
        te3.Reset__c = 'ChewbaccaIsTesting' ;
        insert te3;
        return te3;
    }  
    
    public static Tenant_Equipment__c  createTenantEquipment4(Id oppId, Id taId){
        Tenant_Equipment__c te4 = new Tenant_Equipment__c();
        te4.Opportunity__c = oppId;
        te4.Name = 'R123456789';
        te4.Smart_Card__c = 'S1234567891';
        te4.Location__c = 'D';
        te4.Action__c = 'DupeDisconnect';
        te4.Address__c ='1011 Coliie Path';
        te4.City__c = 'Round Rock';
        te4.State__c = 'TX';
        te4.Zip_Code__c = '78664';
        te4.Unit__c = '1A';
        te4.Phone_Number__c = '5123835201';
        te4.Email_Address__c = 'jerry.clifft@dish.com';
        te4.Customer_First_Name__c = 'Jerry';
        te4.Customer_Last_Name__c = 'Clifft';
        te4.Tenant_Account__c = taId;
        te4.Amdocs_Tenant_ID__c = '123456';
        te4.Reset__c = 'ChewbaccaIsTesting' ;
        insert te4;
        return te4;
    }
    
    public static Tenant_Equipment__c  createTenantEquipment5(Id oppId, Id taId){
        Tenant_Equipment__c te4 = new Tenant_Equipment__c();
        te4.Opportunity__c = oppId;
        te4.Name = 'R123456789';
        te4.Smart_Card__c = 'S1234567891';
        te4.Location__c = 'D';
        te4.Action__c = 'Active';
        te4.Address__c ='1011 Coliie Path';
        te4.City__c = 'Round Rock';
        te4.State__c = 'TX';
        te4.Zip_Code__c = '78664';
        te4.Unit__c = '1A';
        te4.Phone_Number__c = '5123835201';
        te4.Email_Address__c = 'jerry.clifft@dish.com';
        te4.Customer_First_Name__c = 'Jerry';
        te4.Customer_Last_Name__c = 'Clifft';
        te4.Tenant_Account__c = taId;
        te4.Amdocs_Tenant_ID__c = '123456';
        te4.Reset__c = 'ChewbaccaIsTesting' ;
        insert te4;
        return te4;
    }
    
    public static Tenant_Equipment__c  createTenantEquipment6(Id oppId, Id taId){
        Tenant_Equipment__c te3 = new Tenant_Equipment__c();
        te3.Opportunity__c = oppId;
        te3.Name = 'R123456777';
        te3.Smart_Card__c = 'S1234567891';
        te3.Location__c = 'D';
        te3.Action__c = 'DupeRemove';
        te3.Address__c ='1011 Coliie Path';
        te3.City__c = 'Round Rock';
        te3.State__c = 'TX';
        te3.Zip_Code__c = '78664';
        te3.Unit__c = '1A';
        te3.Phone_Number__c = '5123835201';
        te3.Email_Address__c = 'jerry.clifft@dish.com';
        te3.Customer_First_Name__c = 'Jerry';
        te3.Customer_Last_Name__c = 'Clifft';
        te3.Amdocs_Tenant_ID__c = '123457';
        te3.Reset__c = 'ChewbaccaIsTesting' ;
        insert te3;
        return te3;
    } 
    
    public static Tenant_Equipment__c  createTenantEquipment7(Id oppId, Id taId){
        Tenant_Equipment__c te3 = new Tenant_Equipment__c();
        te3.Opportunity__c = oppId;
        te3.Name = 'R223456789';
        te3.Smart_Card__c = 'S1234567891';
        te3.Location__c = 'D';
        te3.Action__c = 'DupeRemove';
        te3.Address__c ='1011 Coliie Path';
        te3.City__c = 'Round Rock';
        te3.State__c = 'TX';
        te3.Zip_Code__c = '78664';
        te3.Unit__c = '1A';
        te3.Phone_Number__c = '5123835201';
        te3.Email_Address__c = 'jerry.clifft@dish.com';
        te3.Customer_First_Name__c = 'Jerry';
        te3.Customer_Last_Name__c = 'Clifft';
        te3.Tenant_Account__c = taId;
        te3.Amdocs_Tenant_ID__c = '123457';
        te3.Reset__c = 'ChewbaccaIsTesting' ;
        insert te3;
        return te3;
    }  
    
    public static Equipment__c  createHeadEndEquipment(Id oppId){
        Equipment__c et = new Equipment__c();
        et.Opportunity__c = oppId;
        et.Name = 'R123456789';
        et.Receiver_S__c = 'S123456789';
        et.Location__c = 'C';
        et.Action__c = 'DupeREMOVE';
        et.Location__c = 'C';
        insert et;
        return et;
    }
    
    public static Smartbox__c  createSmartBoxEquipment(Id oppId){
        Smartbox__c sb = new Smartbox__c();
        sb.Opportunity__c = oppId;
        sb.CAID__c = 'R123456789';
        sb.SmartCard__c = 'S345678912';
        sb.Location__c = 'C';
        sb.Action__c = 'DupeREMOVE';
        sb.Serial_Number__c = 'LALPSC011652';
        sb.Chassis_Serial__c = 'LALPFB001327';
        sb.NoTrigger__c = True;
        insert sb;
        return sb;
    }
    
    public static Equipment__c  createHeadEndEquipment2(Id oppId){
        Equipment__c et = new Equipment__c();
        et.Opportunity__c = oppId;
        et.Name = 'R123456789';
        et.Receiver_S__c = 'S123456789';
        et.Location__c = 'C';
        et.Action__c = 'Active';
        et.Location__c = 'C';
        insert et;
        return et;
    }
    
    public static Smartbox__c  createSmartBoxEquipment2(Id oppId){
        Smartbox__c sb = new Smartbox__c();
        sb.Opportunity__c = oppId;
        sb.CAID__c = 'R123456789';
        sb.SmartCard__c = 'S345678912';
        sb.Location__c = 'C';
        sb.Action__c = 'Active';
        sb.Serial_Number__c = 'LALPSC011652';
        sb.Chassis_Serial__c = 'LALPFB001327';
        sb.NoTrigger__c = True;
        insert sb;
        return sb;
    }
}