@isTest
 public without sharing class  SmartBoxTrgTest
{ 
  Private static testMethod void InsertSmartBoxEquipment() 
  {
        Opportunity Opp1= TestDataFactoryforInternal.createOpportunity();
        Opportunity Opp2= TestDataFactoryforInternal.createOpportunity();
        SmartBox__c SBtoCreate=TestDataFactoryforInternal.createSmartBox(Opp1);
        SmartBox__c SBtoCreate1=TestDataFactoryforInternal.createSmartBox(Opp2);
  }
  
  Private static testMethod void UpdateSmartBoxEquipment() 
  {
         
         
         Opportunity Opp1= TestDataFactoryforInternal.createOpportunity();
         SmartBox__c SBtoCreate=TestDataFactoryforInternal.createSmartBox(Opp1);
         SBtoCreate.Chassis_Serial__c='6789';
         update SBtoCreate;
  } 
  Private static testMethod void createRAForSB()
  {
         Opportunity Opp1= TestDataFactoryforInternal.createOpportunity();
         Account acc=TestDataFactoryforInternal.createAccount();
         Case cs=TestDataFactoryforInternal.createCase(Opp1,acc);
         Opp1.Smartbox_Leased__c= true;
         
         update Opp1;
      
         SmartBox__c SBtoCreate=TestDataFactoryforInternal.createSmartBox(Opp1);
         SBtoCreate.Chassis_Serial__c='6789';
         SBtoCreate.Action__c='Send Trip / HIT';
         SBtoCreate.CaseID__c=cs.id;
         SBtoCreate.Smartbox_Leased2__c=true;
          
         update SBtoCreate;
  }
  Private static testMethod void StatusTest()
  {
        Opportunity Opp1= TestDataFactoryforInternal.createOpportunity();
        SmartBox__c SBtoCreate=TestDataFactoryforInternal.SmartBoxStateTesting(Opp1);
        SBtoCreate.Verizon_ServiceState__c='Deactivate';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='Activate';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='Suspend';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='Restore';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='SMSUp120';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='CreateAccount';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='SMSUp';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='SMSDown';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='SMSUP120-4G';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='SMSUP4G';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='SMSDown-4G';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='Activate-4G';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='Deactivate-4G';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='Suspend-4G';
        Update SBtoCreate;
        SBtoCreate.Verizon_ServiceState__c='Restore-4G';
        Update SBtoCreate;
        SBtoCreate.Verizon_Do_It_Action__c=false;
        SBtoCreate.Verizon_Do_It_Login__c=false;
        SBtoCreate.Verizon_Do_It_Get_M2M_Token__c=true;
        Update SBtoCreate;
        SBtoCreate.Verizon_Do_It_Login__c=true;
        SBtoCreate.Verizon_Do_It_Get_M2M_Token__c=false;
        SBtoCreate.Verizon_Do_It_Action__c=false;
        Update SBtoCreate;
         
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
       
        Test.stopTest();
  }
  
}