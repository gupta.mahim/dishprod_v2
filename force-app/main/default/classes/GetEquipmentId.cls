public class GetEquipmentId
{

public class FlowInput
{
  @Invocablevariable
 public string id;
 
}
public class FlowOutput
{
  @Invocablevariable
  public List<string> Teid = new list <string>();
 
  @Invocablevariable
  public List<string> Heid = new list <string>();
 
  @Invocablevariable
  public List<string> sbid = new list <string>();
     
  @Invocablevariable
  public List<string> TeidAddress = new list <string>();
    @Invocablevariable
    public boolean IsAddressInvalid =false ;
     @Invocablevariable
    public boolean isDupeExist=false ;
    
 
 
}

  @InvocableMethod(Label='GetIds')
 Public static List<Flowoutput> getIds(List<FlowInput> req)
 {
     List<FlowOutput> result= new list<FlowOutput>();
     FlowOutput output = new FlowOutput();
  
     List<Tenant_Equipment__c> TEInvalidAdd= new list <Tenant_Equipment__c>([Select id from Tenant_Equipment__c where opportunity__c =: req[0].id AND 	Valid_Address__c= false AND Override_NetQual__c = false]);
     List<Tenant_Equipment__c> TElist= new list <Tenant_Equipment__c>( [Select id from Tenant_Equipment__c where opportunity__c =: req[0].id AND Is_active_elsewhere__c=TRUE and Where_it_s_active_elsewhere__c!=null]);
     List<Equipment__c> hElist= new list <Equipment__c>( [Select id from Equipment__c where opportunity__c =: req[0].id AND Is_active_elsewhere__c=TRUE and Where_it_s_active_elsewhere__c!=null]);
     List<Smartbox__c> Sblist= new list <Smartbox__c>( [Select id from Smartbox__c where opportunity__c =: req[0].id AND Is_active_elsewhere__c=TRUE and Where_it_s_active_elsewhere__c!=null]);
    
     for(Tenant_Equipment__c TE:TElist)
     {
         if(TElist.size()>0)
          {
            output.Teid.add(TE.id);
          
          }
      }
      // for Invalid Address
      for(Tenant_Equipment__c TE:TEInvalidAdd)
     {
         if(TEInvalidAdd.size()>0)
          {
            output.TeidAddress.add(TE.id);
            
          }
      }
     for(Equipment__c HE:hElist)
     {
        if(HElist.size()>0) 
        {
          
           output.Heid.add(HE.id);
         }
     }
      for(Smartbox__c SB:Sblist)
      {
         if(Sblist.size()>0)
         {
           output.Sbid.add(SB.id);
         }
      }
     
     if(Sblist.size()>0 ||TElist.size()>0 ||Helist.size()>0)
     {
         output.isDupeExist=true;
     }
    
       result.add(Output); 
     if(TEInvalidAdd.size()>0)
     {
        output.IsAddressInvalid= true;
     }

       return result;
 
 
 
 }

}