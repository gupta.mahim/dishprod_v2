@isTest public class TenantProdSelectorCtrlTest {

    
    
@isTest    public static void initProdSelector(){      
        Opportunity opp = TestDataFactoryforInternal.createOpportunity();
        Tenant_Account__c TA= TestDataFactoryforInternaL.createTenantAccount(opp); 
       
         ta.Type__c='Individual';
       
       update ta;
       
        Tenant_Pricebook__c pbook = new Tenant_Pricebook__c();
        pbook.Name = 'TestCtrl';
        pbook.Pricebook_Name__c='Incremental';
               insert  pbook;
     
        Tenant_Product__c prod = new Tenant_Product__c();
        
        prod.name ='HBO';
        prod.Tenant_Product_Type__c = 'Individual';
        prod.Family__c= 'Core';
        prod.Tenant_Product_Type__c ='Core';
        prod.Individual_Price__c =60;
        prod.Incremental_Price__c = 10;
        
        
        insert prod;
        
        Tenant_Product__c prod2 = new Tenant_Product__c();
        
        prod2.name ='HBO';
        prod2.Tenant_Product_Type__c = 'Individual';
        prod2.Family__c= 'Add-On';
        prod2.Tenant_Product_Type__c ='Add-On';
        prod2.Individual_Price__c =60;
        prod.Incremental_Price__c = 10;
    
        insert prod2;
  
    Tenant_ProductEntry__c tpe = new Tenant_ProductEntry__c (Tenant_Pricebook__c=pbook.id, Tenant_Product__c=prod.id,Grandfathered__c=true);
        insert tpe;
        
    Tenant_ProductEntry__c tpe1 = new Tenant_ProductEntry__c (Tenant_Pricebook__c=pbook.id, Tenant_Product__c=prod2.id);
        insert tpe1;    
    Tenant_Product_Line_Item__c tpl = new  Tenant_Product_Line_Item__c(Name='Test', Tenant_ProductEntry__c =tpe.Id,Tenant_Account__c = TA.Id,Action__c='Active');
        insert tpl;  
        
         Map<String,List<String>> gfProducts=new Map<String,List<String>>();
        List<String> gfProd=new List<String>();
        
        gfProd.add(tpe.id);
        gfProducts.put('Core',gfProd);
        
        String strgfProducts=JSON.serialize(gfProducts);
        System.debug(strgfProducts);
    
        TenantProdSelectorCtrl.OppProductsData data = TenantProdSelectorCtrl.initProdSelector(TA.id);
        TenantProdSelectorCtrl.loadProductData(TA.id, TA.Type__c, pbook.id, 'Core', 'abc', 'English',strgfProducts);
    
    List<String> newProdData=new List<String>();
    List<Tenant_Product_Line_Item__c> removeProdData=new List<Tenant_Product_Line_Item__c>();
        
     
        newProdData.add(tpe1.Id);
        removeProdData.add(tpl);
        
        TenantProdSelectorCtrl.saveProducts(TA.id, '', '');
      TenantProdSelectorCtrl.performSave(TA.id, newProdData, removeProdData);
   // TenantProdSelectorCtrl.saveProducts(TA.id, strNewProdData, strRemoveProdData);
      
            
    }
}