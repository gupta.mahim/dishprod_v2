public class Site_TradeShowDish001{

private final Lead L;
 public string error {get; set;}

   public Site_TradeShowDish001(ApexPages.StandardController stdcontroller) {
   L = (Lead)stdController.getRecord();
    }

  public PageReference onsave() {
    if (L.Company=='') { error='Missing Property Name. Please Complete All Fields'; return null; }
    else IF ( !Pattern.matches('[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+.[a-zA-Z]{2,4}', L.Email)) { error ='Email Address is not formatted correctly.'; return null; }
    else IF ( L.Email == Null ) { error='Missing Contact Email address. Please Complete All Fields.'; return null; }
    else IF ( L.Number_of_Units__c == Null ) { error='Missing Units passed. Please Complete All Fields.'; return null; }
    else IF ( L.Phone == '') { error='Missing Property Phone. Please Complete All Fields.';          return null;        }
    else IF ( !L.Phone.isNumeric() ) { error='Phone must be numeric.';      return null;    }
    else IF ( L.Postalcode == '') { error='Missing Property Zip Code. Please Complete All Fields.';          return null;        }
    else IF ( !L.Postalcode.isNumeric() ) { error='Property Zip Code must be numeric.';      return null;    }
    else IF ( L.Street == '') {  error='Missing Property Address. Please Complete All Fields';          return null;        }
    else IF ( L.City == '') {  error='Missing Property City. Please Complete All Fields.';          return null;        }
    else IF ( L.State2__c == '') {  error='Missing State. Please Complete All Fields.';          return null;        }
    else IF ( L.Number_of_Portfolio_Units__c == '')  {  error='Missing Number of Portfolio Units/Rooms. Please Complete All Fields.'; return null; } 
    else IF ( L.FirstName == '') { error='Missing Contact First Name. Please Complete All Fields.'; return null; }
    else IF ( L.LastName == '') { error='Missing Contact Last Name. Please Complete All Fields.'; return null; }
       
      else{   L.OwnerId='00G60000002PXna'; 
           L.RecordTypeId='012f2000000QLcv';
           L.LeadSource='Events';
           L.Lead_Source_External__c='Test Event Name';
           insert L;
           return new PageReference('http://dish.force.com/tradewshow/DISH001_Thank_You');
   }
 }
}