@isTest
public class TestDataFactoryforInternal {
    
    public static Opportunity createOpportunity(){
        Opportunity o = New Opportunity();
        
        o.Name = 'Test';
        o.Property_Type__c='MDU Property (1300)';
        o.StageName='Deal Re-Work';
        o.Property_Status__c ='Inactive';
        o.Phased_Construction__c = 'No';
        o.CloseDate = system.today();
        o.RecordTypeID = '012600000005Bu3';
        o.Address__c='18F Enginners and Architect';
        o.city__c='Lahore';
        o.State__c='MI';
        o.Zip__c='48001';
        o.AmDocs_SiteID__c='1234567';
        o.AmDocs_BarID__c='1234567';
        
        insert o; 
        return o;
    }
    
    public static Opportunity createOpportunityIncludingReferenceValues(){
        Opportunity o = New Opportunity();
        
        o.Name = 'Test';
        o.Property_Type__c='MDU Property (1300)';
        o.StageName='Deal Re-Work';
        o.Property_Status__c ='Inactive';
        o.Phased_Construction__c = 'No';
        o.CloseDate = system.today();
        o.RecordTypeID = '012600000005Bu3';
        o.Address__c='18F Enginners and Architect';
        o.city__c='Lahore';
        o.State__c='MI';
        o.Zip__c='48001';
        o.AmDocs_SiteID__c = '22222';
        o.AmDocs_BarID__c = '0123456789';
        o.AmDocs_tenantType__c = 'Individual';
        
        
        insert o; 
        return o;
    }
    
    public static Opportunity createOpportunity(Integer recType){
        Opportunity o = New Opportunity();
        
        o.Name = 'Test';
        o.Property_Type__c='MDU Property (1300)';
        o.StageName='Deal Re-Work';
        o.Property_Status__c ='Inactive';
        o.Phased_Construction__c = 'No';
        o.CloseDate = system.today();
        o.RecordTypeID = '012600000005Bu3';
        if(recType == 1){
            o.Property_Type__c = 'Private';
        }
        else if(recType == 2){
            o.Property_Type__c = 'Public';
            o.EVO__c = '51-100';
            o.EVO2__c =1;
            
        }
        
        
        insert o; 
        return o;
    }
    
    public static Front_Office_Request__c createFrontOfficeRequest(Opportunity opp)
    {
        Front_Office_Request__c fo = New Front_Office_Request__c ();
        fo.Opportunity__c = opp!=null?opp.id:createOpportunity().id;
        fo.Front_Office_Decision__c = 'In Progress';
        fo.RecordTypeId = '0126000000017rc';
        fo.Phase_Build__c = 'No';
        fo.Property_Address__c = '1011 Collie Path';
        fo.Property_City__c = 'Round Rock';
        fo.Property_State__c = 'TX';
        fo.Property_Zip_Code__c = '78664';
        fo.Lead_Source__c = 'Test';
        fo.Lead_Source_External__c = 'Testing';
        insert fo;
        return fo;
    }
    
    public static Executive_Summary__c createExecutiveSummary()
    {
        
        Executive_Summary__c ex = New  Executive_Summary__c ();
        
        ex.Front_Office_Request__c = createFrontOfficeRequest(null).id;
        ex.Status__c = 'Approved';
        insert ex;
        return ex;
    }
    
    public static Lead createLead() {
        Lead L = new Lead();
        L.Company='Sadia is doing some testing';
        L.LeadSource='Direct Call';
        L.Lead_Source_External__c='Dish Network Web Site';
        L.Phone='512-295-5581';
        L.Type_of_Business__c='Apartment/Condo/Townhome';
        L.Building_Stories__c='1 - 2';
        L.FirstName='Sadia';
        L.LastName='Asif';
        L.Number_of_Locations__c=1;
        L.Number_of_Units__c=1;
        L.Number_of_TVs__c='1 - 2';
        L.Sales_Stage__c='Ready to Activate';
        
        insert L;
        return L;
    }
    
    public static Tenant_Equipment__c  createTenantEquipment(Opportunity opp){
        Tenant_Equipment__c TE = New Tenant_Equipment__c ();
        TE.Name='R0001234';
        TE.Address__c='18F Enginners and Architect';
        TE.Opportunity__c = opp.id;
        TE.city__c='Lahore'; 
        TE.State__c='MI';
        TE.Zip_Code__c='48001';
        
        insert TE;
        
        return TE;
    }
    
    public static List<Tenant_Equipment__c>  createTenantEquipmentBulk(Opportunity opp){
        List<Tenant_Equipment__c> TEBulk = new List<Tenant_Equipment__c>();
        for(Integer i=0; i<10;i++)
        {
            Tenant_Equipment__c TE = New Tenant_Equipment__c ();
            TE.Name='R0001234';
            TE.Address__c='18F Enginners and Architect';
            TE.Opportunity__c = opp.id;
            TE.city__c='Lahore'; 
            TE.State__c='MI';
            TE.Zip_Code__c='48001';
            TE.Is_active_elsewhere__c=false;
            TEBulk.add(TE);
        }
        insert TEBulk;
        return TEBulk;
    }
    
    public static Tenant_Account__c createTenantAccount(Opportunity opp){
        
        Tenant_Account__c TA = New Tenant_Account__c();
        
        TA.Address__c='18F Enginners and Architect';
        TA.city__c='Lahore';
        TA.Opportunity__c = opp!=null?opp.id:createOpportunity().id;
        TA.State__c='MI';
        TA.Zip__c='48001';
        TA.Phone_Number__c='5123835201';
        
        insert TA; 
        return TA;
    }
    public static Equipment__c  createHeEquipment(Opportunity opp){
        Equipment__c HE = New Equipment__c ();
        
        
        HE.Opportunity__c = opp!=null?opp.id:createOpportunity().id;
        HE.Name='12345';
        
        insert HE; 
        return HE;
    }
    public static List<Equipment__c> createHeEquipmentBulk(Opportunity opp){
        List<Equipment__c> HEBulk = new List<Equipment__c>();
        for(Integer i=0;i<10;i++)
        {
            Equipment__c HE = New Equipment__c ();
            
            HE.Opportunity__c = opp!=null?opp.id:createOpportunity().id;
            HE.Name='12345-'+i;
            HEBulk.add(HE);
        }
        insert HEBulk;
        return HEBulk;
    }
    public static Smartbox__c  createSBEquipment(Opportunity opp){
        Smartbox__c SB = New Smartbox__c ();
        
        
        SB.Opportunity__c = opp!=null?opp.id:createOpportunity().id;
        Sb.CAID__c='1234TEST';
        SB.Serial_Number__c='123456';
        SB.Chassis_Serial__c='123ABC';
        
        insert SB; 
        return SB;
        
    }
    public static List<Smartbox__c>  createSBEquipmentBulk(Opportunity opp){
        List<Smartbox__c> SbBulk = New List<Smartbox__c>();
        for(Integer i=0;i<10;i++)
        {
            Smartbox__c SB = New Smartbox__c ();
            SB.Opportunity__c = opp!=null?opp.id:createOpportunity().id;
            Sb.CAID__c='1234TEST';
            SB.Serial_Number__c='123456';
            SB.Chassis_Serial__c='123ABC';
            SB.Is_active_elsewhere__c=false;
            SbBulk.add(SB);
        }
        insert SbBulk; 
        return SbBulk;
    }
    Public static Account CreateAccount()
    {
         Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;
        return acct1; 
        
        
    }
  Public static Opportunity createOppforCallout(Account acct1)
  {
      Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '786664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.LeadSource='Test Place';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Digital Bulk';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
        insert opp1;
      return opp1;
  
  }
}