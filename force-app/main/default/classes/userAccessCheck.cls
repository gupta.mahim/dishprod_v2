/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 25 June 2020
Created for: Milestone# MS-001252
Description: To check logged in User Access
-------------------------------------------------------------*/

public class userAccessCheck {
    
    public class FlowInput{
        /*@Invocablevariable
        public string sObjectTypeName;
        
        @Invocablevariable
        public string fieldName;*/
        
        @Invocablevariable
        public string buttonName;
    }
    
    public class FlowOutput{
        @Invocablevariable
        public Boolean userHaveAccess = true;
        
        @Invocablevariable
        public String noAccessMessage;
    }
    
    @InvocableMethod (label='UserAccessCheck')
    public static List<Flowoutput> partnerUserAccessCheck(List<FlowInput> inputValue)
    {
        userAccessCheckResponse response = new userAccessCheckResponse();
        FlowOutput output = new FlowOutput();
        List<FlowOutput> result= new list<FlowOutput>();
        result.add(output);
        
        list<Custom_Button_Access_Detail__mdt> objectFieldDetail = [SELECT Object_Name__c, Field_Name__c FROM Custom_Button_Access_Detail__mdt 
                                                          WHERE Custom_Button_Name__c=:inputValue[0].buttonName Limit 1];
        
        if(objectFieldDetail.size()>0)
        	response = getCurrentUserAccess(objectFieldDetail[0].Object_Name__c, objectFieldDetail[0].Field_Name__c, inputValue[0].buttonName);
         else{
            response.currentUserHaveAccess = false;
            response.noAccessMsg = System.Label.CustomButtonNoAccessMsg;
        }
        output.userHaveAccess = response.currentUserHaveAccess;
        output.noAccessMessage = response.noAccessMsg;
                
        return result;
    }
    
    @AuraEnabled
    public static userAccessCheckResponse getCurrentUserAccess(string sObjectTypeName, string fieldName, string buttonName){
        userAccessCheckResponse Resp = new userAccessCheckResponse();
        string fullFieldName = sObjectTypeName + '.' + fieldName;
        list<PermissionSetAssignment> userObjAccess = new list<PermissionSetAssignment>();
        list<PermissionSetAssignment> userFieldAccess = new list<PermissionSetAssignment>();
        
        userFieldAccess = [SELECT Id FROM PermissionSetAssignment WHERE Assignee.Id= :userinfo.getUserId() AND PermissionSetId in 
                           (SELECT ParentId FROM FieldPermissions WHERE SobjectType = :sObjectTypeName AND Field = :fullFieldName 
                            AND PermissionsEdit = true)];
        
        //for Button Add / Remove Products - Object is OpportunityLineItem which is a detail object. Hence checking the Master object access
        //if(sObjectTypeName == 'OpportunityLineItem'){
        if(buttonName == 'Add / Remove Products'){
            list<PermissionSetAssignment> userObjAccessOpp = [SELECT Id FROM PermissionSetAssignment WHERE Assignee.Id= :userinfo.getUserId() AND PermissionSetId in
                                                              (SELECT ParentId FROM ObjectPermissions WHERE PermissionsCreate = TRUE AND SObjectType = 'Opportunity')];
            list<PermissionSetAssignment> userObjAccessProd = [SELECT Id FROM PermissionSetAssignment WHERE Assignee.Id= :userinfo.getUserId() AND PermissionSetId in
                                                               (SELECT ParentId FROM ObjectPermissions WHERE PermissionsRead = TRUE AND SObjectType = 'Product2')];
            list<PermissionSetAssignment> userObjAccessPricebook = [SELECT Id FROM PermissionSetAssignment WHERE Assignee.Id= :userinfo.getUserId() AND PermissionSetId in
                                                                    (SELECT ParentId FROM ObjectPermissions WHERE PermissionsRead = TRUE AND SObjectType = 'Pricebook2')];
            
            if(userObjAccessOpp.size()>0 && userObjAccessProd.size()>0 && userObjAccessPricebook.size()>0 && userFieldAccess.size()>0)
                Resp.currentUserHaveAccess=true;
            else
                Resp.currentUserHaveAccess=false;
        }
        else{
            if(buttonName == 'Upload PURCHASED Smartbox'|| buttonName == 'Upload Leased Smartbox' || buttonName == 'Upload Equipment'
               || buttonName == 'Upload Tenant Equipment' || buttonName == 'New Tenant Equipment' || buttonName == 'New Case' 
               || buttonName == 'Create New Property' || buttonName == 'Create New Tenant Account' || buttonName == 'Tenant Add / Remove Products'){
                userObjAccess = [SELECT Id FROM PermissionSetAssignment WHERE Assignee.Id= :userinfo.getUserId() AND PermissionSetId in
                                 (SELECT ParentId FROM ObjectPermissions WHERE PermissionsCreate = TRUE AND 
                                  SObjectType = :sObjectTypeName)];
            }
            if(buttonName == 'Edit Smartbox'|| buttonName == 'Edit Equipment' || buttonName == 'Edit Tenant Equipment' 
               || buttonName == 'Trip All Digital Bulk Equipment'){
                userObjAccess = [SELECT Id FROM PermissionSetAssignment WHERE Assignee.Id= :userinfo.getUserId() AND PermissionSetId in
                                 (SELECT ParentId FROM ObjectPermissions WHERE PermissionsEdit = TRUE AND 
                                  SObjectType = :sObjectTypeName)];
            }
            
            if(userObjAccess.size()>0 && userFieldAccess.size()>0)
                Resp.currentUserHaveAccess=true;
            else
                Resp.currentUserHaveAccess=false;
        }
        
        Resp.noAccessMsg = System.Label.CustomButtonNoAccessMsg;
        system.debug('Resp123-'+Resp);
        return Resp;        
    }
    
    
    public class userAccessCheckResponse{
        @AuraEnabled
        public boolean currentUserHaveAccess;
        
        @AuraEnabled
        public string noAccessMsg;
    }
}