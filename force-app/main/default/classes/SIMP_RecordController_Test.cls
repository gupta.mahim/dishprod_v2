@isTest
//test class for Fiber_Building_Detail__c and Fiber_IDF_Detail__c Apex controller for
//Fiber_Building and Fiber_IDF lightning components
//Christian Comia - 20190719 - initial version 
public class SIMP_RecordController_Test {
    @isTest static void getRecords(){
        List<String> fieldsList = new List<String>(); 
        fieldsList.add('Name');
        List<sObject> recordList = SIMP_RecordController.getExistingRelatedRecordList('a5F540000004astEAA','Quote__c','Fiber_Building_Detail__c',fieldsList);
        
        SBQQ__Quote__c sbqC = new SBQQ__Quote__c();
        sbqC.Contract_Term__c = 1;
        sbqC.No_of_Ports_per_Unit__c = 1;
        insert sbqC;
        
        sbqC = [SELECT ID, Name FROM SBQQ__Quote__c WHERE ID =: sbqC.ID];
        
        Fiber_Building_Detail__c fbc = new Fiber_Building_Detail__c();
        fbc.Name = sbqC.Name;  
        fbc.Quote__c = sbqC.ID;  
        insert fbc;
        
        Fiber_IDF_Detail__c fic = new Fiber_IDF_Detail__c();
        fic.No_of_Units_Connected_to_IDF__c = 1;
        fic.No_of_Ports_per_Unit__c = 1;
        fic.Install_RF__c = 'No';
        fic.IDF_Location__c = 'Indoor';  
        fic.Quote__c = sbqC.ID;    
        fic.Name = sbqC.Name;
        insert fic;
        
        String recordString = SIMP_RecordController.fetchQuoteName(sbqC.ID,'SBQQ__Quote__c','Name');
        String recordString1 = SIMP_RecordController.fetchQuoteName(sbqC.ID,'SBQQ__Quote__c','Reason');
        String recordString2 = SIMP_RecordController.fetchQuoteName(sbqC.ID,'SBQQ__Quote__c','');
        
        
        SIMP_RecordController.createClones(sbqC.ID,3);
        
        SIMP_RecordController.cloneWithRelatedAll(sbqC.ID,'SBQQ__Quote__c');
        
        SIMP_RecordController.deleteAllRelatedRecords(sbqC.ID,'Quote__c','Fiber_Building_Detail__c');
        
    }
    
    @isTest(SeeAllData=true)
    static void handleQuoteLinesTest(){
        Product2 sbqpc = [SELECT Id, Name FROM Product2 WHERE Name = 'DISH Fiber Solution'];
        List<SBQQ__Quote__c> newQuote = new List<SBQQ__Quote__c>();
        for(Integer x = 0;x<1;x++){
            SBQQ__Quote__c q = new SBQQ__Quote__c();
            q.Contract_Term__c = 50;
            q.Annual_ARPU_Increase__c = 0.04;
            q.Monthly_Cash_Flow__c = 0;
            q.Upfront_Door_Fees__c = 1;
            q.Monthly_Door_Fees__c = 1;
            q.No_of_Units__c = 76;
            q.Year_1__c = 21000;
            q.Year_2__c = 21313;
            q.Year_3__c = 21313;
            q.Year_4__c = 21313;
            q.Year_5__c = 3813;
            q.Year_6__c = 0;
            q.Year_7__c = 0;
            q.Year_8__c = 0;
            q.Year_9__c = 0;
            q.Year_10__c = 0;
            q.Door_Fees_of_Months__c = 0;
            newQuote.add(q);
        }
        insert newQuote;
        
        List<SBQQ__QuoteLine__c> sbql = new List<SBQQ__QuoteLine__c>();
        List<SBQQ__QuoteLine__c> sbqli = new List<SBQQ__QuoteLine__c>();
        for(Integer i = 0;i<1;i++){
            SBQQ__Quote__c newQuote1 = newQuote[i];
            
            for(Integer y = 0;y<1;y++){
                sbqli.add(new SBQQ__QuoteLine__c(
                                                 SBQQ__OptionLevel__c = null,
                                                 ARPU__c = 1,
                                                 SBQQ__ComponentCost__c = 70338.94,
                                                 SBQQ__ComponentListTotal__c = 156088.94,
                                                 SBQQ__ComponentTotal__c = 156088.94,
                                                 SBQQ__SubscriptionBase__c = 'List',
                                                 SBQQ__SubscriptionScope__c = 'Quote',
                                                 SBQQ__Product__c = sbqpc.id,
                                                 SBQQ__Quote__c = newQuote1.id)
                                               );
            }
            
            
        }
        
        insert sbqli;
        system.debug('TEST'+ newQuote[0]);
        system.debug('TESTX'+ sbqli[0]);
        SIMP_RecordController.handleBundleQuoteLine(newQuote[0],sbqli[0]);
    }
    
    @isTest(SeeAllData=true)
    static void handleQuoteLinesTestUpd(){
        Product2 sbqpc = [SELECT Id, Name FROM Product2 WHERE Name = 'DISH Fiber Solution'];
        List<SBQQ__Quote__c> newQuote = new List<SBQQ__Quote__c>();
        for(Integer x = 0;x<1;x++){
            SBQQ__Quote__c q = new SBQQ__Quote__c();
            q.Contract_Term__c = 50;
            q.Annual_ARPU_Increase__c = 0.04;
            q.Monthly_Cash_Flow__c = 0;
            q.Upfront_Door_Fees__c = 1;
            q.Monthly_Door_Fees__c = 1;
            q.No_of_Units__c = 76;
            q.Year_1__c = 21000;
            q.Year_2__c = 21313;
            q.Year_3__c = 21313;
            q.Year_4__c = 21313;
            q.Year_5__c = 3813;
            q.Year_6__c = 0;
            q.Year_7__c = 0;
            q.Year_8__c = 0;
            q.Year_9__c = 0;
            q.Year_10__c = 0;
            q.Door_Fees_of_Months__c = 0;
            newQuote.add(q);
        }
        insert newQuote;
        
        List<SBQQ__QuoteLine__c> sbql = new List<SBQQ__QuoteLine__c>();
        List<SBQQ__QuoteLine__c> sbqli = new List<SBQQ__QuoteLine__c>();
        for(Integer i = 0;i<1;i++){
            SBQQ__Quote__c newQuote1 = newQuote[i];
            
            for(Integer y = 0;y<1;y++){
                sbqli.add(new SBQQ__QuoteLine__c(
                                                 SBQQ__OptionLevel__c = null,
                                                 ARPU__c = 1,
                                                 SBQQ__ComponentCost__c = 70338.94,
                                                 SBQQ__ComponentListTotal__c = 156088.94,
                                                 SBQQ__ComponentTotal__c = 156088.94,
                                                 SBQQ__SubscriptionBase__c = 'List',
                                                 SBQQ__SubscriptionScope__c = 'Quote',
                                                 SBQQ__Product__c = sbqpc.id,
                                                 SBQQ__Quote__c = newQuote1.id)
                                               );
            }
            
            
        }
        
        insert sbqli;
        
        SBQQ__QuoteLine__c cc = sbqli[0];
        cc.ARPU__c = 50;
        update cc;
        
        system.debug('TEST1'+ newQuote[0]);
        system.debug('TESTX'+ cc);
        SIMP_RecordController.handleBundleQuoteLine(newQuote[0],cc);
    }
}