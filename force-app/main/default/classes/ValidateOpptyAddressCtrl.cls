Public class ValidateOpptyAddressCtrl
{
    public Id currentOppId{get;set;}
    public boolean inProgress{get;set;}
    public boolean callNetQual{get;set;}
    public boolean callBulk{get;set;}
    public boolean validation_msg_exist{get;set;}
    
    public ValidateOpptyAddressCtrl(ApexPages.StandardController sc)
    {
        inProgress=true;
        currentOppId=sc.getRecord().Id;
        
        Opportunity currentOpp=[select id,Property_Status__c,Amdocs_CustomerID__c,AmDocs_tenantType__c,AmDocs_ServiceID__c,AmDocs_SiteID__c,Invoke_NetQual__c,Override_NetQual__c,Valid_Address__c, Address_Validation_Message__c from Opportunity where id=:currentOppId];
        boolean invokeNetQual=currentOpp.Invoke_NetQual__c;
        boolean overrideNetQual=currentOpp.Override_NetQual__c;
        boolean validAddress=currentOpp.Valid_Address__c;
        System.debug('invokeNetQual='+invokeNetQual+', overrideNetQual='+overrideNetQual+', validAddress='+validAddress);
        
        if(overrideNetQual==false && validAddress==false && invokeNetQual==true)
        {
            callNetQual=true;
        }
        else
        {
            callNetQual=false;
            validateOppForBulk(currentOpp);
        }
        
        String validation_msg = currentOpp.Address_Validation_Message__c;
        if (validation_msg != null && validation_msg != ''){
            validation_msg_exist = true;
        }else{
            validation_msg_exist = false;
        }
        
    }
    
    public PageReference callBulkAPI()
    {  
        System.debug(callBulk+'Calling Get Tenant by Bulk on opp page load...');
        if(callBulk)
        {
            GetTenantByBulkCtrl.UpdateDirectOppApiStatus(currentOppId);
        }
        return null;
    }
    public PageReference invokeNetQual()
    {  
        if(callNetQual)
        {
            ValidateAdressCtrl.validateOppty(currentOppId);
        }
        return null;
    }
    public void validateOppForBulk(Opportunity opp)
    {
        String oppStatus=opp.Property_Status__c;
        String oppBulkerviceId=opp.AmDocs_ServiceID__c;
        Boolean validAddress=opp.Valid_Address__c;
        String oppSiteId=opp.AmDocs_SiteID__c;
        String oppCustomerId=opp.Amdocs_CustomerID__c;
        String oppTenantType=opp.AmDocs_tenantType__c;
        boolean overrideNetQual=opp.Override_NetQual__c;
        
        System.debug('oppStatus=['+oppStatus+'],oppBulkerviceId=[='+oppBulkerviceId+'],validAddress=['+validAddress+'], callNetQual=['+callNetQual+']');
        System.debug('oppSiteId=['+oppSiteId+'], oppCustomerId='+oppCustomerId+'],oppTenantType=['+oppTenantType+']');

        
        if(!(String.isEmpty(oppStatus) && String.isEmpty(oppBulkerviceId)) && (validAddress==true ||overrideNetQual==true ))
        {
            List<Tenant_Equipment__c> oppEquip=[select Id from Tenant_Equipment__c where Opportunity__c=:opp.id and Action__c='Active' and Amdocs_Tenant_ID__c=null];
            
            System.debug('Active with missing Service Id equip=['+oppEquip.size()+']');
            if(oppEquip.size()>0)
            {
                 Datetime dt1 = System.Now();
                 List<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateGetTenantByBulk__c from API_LOG__c where ServiceID__c = :opp.Amdocs_ServiceID__c AND NextAvailableActivateGetTenantByBulk__c >= :dt1 Order By NextAvailableActivateGetTenantByBulk__c  DESC Limit 1];
                 System.debug('Active timer=['+timer.size()+']');
                 if(timer.size() < 1)
                 {
                     callBulk=true;
                     System.debug('Opp qualified for Get Tenant by Bulk');
                 }
            }
        }

    }
    public PageReference removeValidationMsg()
    {
        System.debug('Inside "removeValidationMsg" function');
        System.debug('validation_msg_exist : ' + validation_msg_exist + ' && callNetQual : ' + callNetQual);
        if(validation_msg_exist == true && callNetQual == false)
        {
            Opportunity currentOpp=[Select Id, Address_Validation_Message__c from Opportunity where Id=:currentOppId];
            currentOpp.Address_Validation_Message__c = null;
            try{
                update currentOpp;
                }catch(Exception e){
                System.debug('Exception message is : ' + e.getMessage());
            }
        }
        return null;
    }
    
}