public with sharing class CaseRecallButton2 {
    
    String CaseId = '';
   
    public CaseRecallButton2(ApexPages.StandardController controller){
        CaseId = ApexPages.currentPage().getParameters().get('id');
    }
    
    
    public Pagereference ReCallIt() {
        Case ThisCase = [Select ClosedDate, OwnerID, Status from Case where ID = :CaseID];
        if( ThisCase.ClosedDate == Null && (ThisCase.Status != 'Request Completed' || ThisCase.Status != 'Request Recalled')){
            ThisCase.status = 'Request Recalled';
            ThisCase.OwnerID = UserInfo.getUserId();
            update ThisCase;
            return  new Pagereference('/'+CaseID);}
            return  new Pagereference('/'+CaseID);
        
    }
    

}