public class Evolve_GETCMDHistory {

public class person {
    public cls_commands[] commands;
}

class cls_commands{
    public String requestId;
    public String lastUpdate;
    public String status;
    public String type;
}

    @future(callout=true)

    public static void makeCallout(String Id) {
    
    list<Evolve__c> E = [select id, MAC_Address__c, Location__c, Smartbox__r.Opportunity__r.AmDocs_ServiceID__c, Smartbox__r.UUID_Opp__c from Evolve__c where Id =:Id AND Location__c = Null];
        System.debug('RESULTS of the LIST lookup to the Evolve object' +E);   

    HttpRequest request = new HttpRequest();
        String endpoint = 'https://wytp2gunxk.execute-api.us-east-1.amazonaws.com/dev/evolve/'+E[0].Smartbox__r.UUID_Opp__c+'/'+E[0].MAC_Address__c+'/commands';
            request.setEndPoint(endpoint);
            request.setMethod('GET');
            request.setHeader('Content-Type', 'application/json'); 
            request.setTimeout(101000);
            request.setHeader('Accept', 'application/json'); 
            request.setHeader('User-Agent', 'SFDC-Callout/45.0');    
        
        HttpResponse response = new HTTP().send(request); 
            if (response.getStatusCode() == 200) { 
                String strjson = response.getbody();
                String shortStrJson = strjson.abbreviate(8000);
                    System.debug('DEBUG 1 === RESPONSE json string: '   + strjson);
                 JSONParser parser = JSON.createParser(strjson);
                     parser.nextToken(); 
                     person obj = (person)parser.readValueAs( person.class);

        List<Evolve_Command_History__c> evCMDs = new List<Evolve_Command_History__c>();
        for(Integer i = 0; i < obj.commands.size(); i++){
            if( obj.commands.size() >0){
                if( obj.commands[0] != Null ) {
                    Evolve_Command_History__c sbc = new Evolve_Command_History__c();
                        sbc.Evolve__c=Id;
                        sbc.Request_ID__c=obj.commands[i].requestId;
                        sbc.Evolve_Request_Time__c = obj.commands[i].lastUpdate;
                        sbc.Status__c=obj.commands[i].status;
                        sbc.Type__c = obj.commands[i].type;
                      evCMDs.add(sbc);
                          System.debug('DEBUG 2 === evList: '   + evCMDs);
                }
            }
        }
        upsert evCMDs evCMDs.Request_ID__c;

        Evolve__c ev = new Evolve__c(); {
            ev.Id=Id;
            ev.Transaction_Description__c = 'SUCCESS: Get Command History: ';
            ev.Transaction_Code__c = 'SUCCESS';
        update ev; 
        }

        API_Log__c apil2 = new API_Log__c();{
            apil2.Record__c=id;       
            apil2.Object__c='Evolve';
            apil2.Status__c='SUCCESS';       
            apil2.Results__c=shortStrJson;
            apil2.API__c='Get Evolve Command History';       
            apil2.ServiceID__c=E[0].Smartbox__r.Opportunity__r.AmDocs_ServiceID__c;       
            apil2.User__c=UserInfo.getUsername();   
        insert apil2;  
        }

          System.debug(response.toString());
          System.debug('DEBUG REQUEST: ' + request);
          System.debug('Full JSON response payload: ' +response.getBody());
          System.debug('Full Status response payload: ' +response.getStatus());
          System.debug('Full Status Code response payload: ' +response.getStatusCode());
          System.debug('DEBUG RESPONSE GET HEADER KEYS: ' +response.getHeaderKeys());
          System.debug('STATUS:'+response.getStatus());
          System.debug('STATUS_CODE:'+response.getStatusCode());
          System.debug(response.getBody());
        }
 
        if (response.getStatusCode() != 200) { 
            String strjson = response.getbody();
            String shortStrJson = strjson.abbreviate(8000);
                System.debug('DEBUG 1 === RESPONSE json string: '   + strjson);

        Evolve__c ev = new Evolve__c(); {
            ev.Id=Id;
            ev.Transaction_Description__c = 'ERROR: Get Command History: ' +shortStrJson;
            ev.Transaction_Code__c = 'ERROR';
        update ev; 
        }

        API_Log__c apil2 = new API_Log__c();{
            apil2.Record__c=id;       
            apil2.Object__c='Evolve';
            apil2.Status__c='ERROR';
            apil2.Results__c=shortStrJson;
            apil2.API__c='Get Evolve Command History';       
            apil2.ServiceID__c=E[0].Smartbox__r.Opportunity__r.AmDocs_ServiceID__c;       
            apil2.User__c=UserInfo.getUsername();   
        insert apil2;  
        }

          System.debug(response.toString());
          System.debug('DEBUG REQUEST: ' + request);
          System.debug('Full JSON response payload: ' +response.getBody());
          System.debug('Full Status response payload: ' +response.getStatus());
          System.debug('Full Status Code response payload: ' +response.getStatusCode());
          System.debug('DEBUG RESPONSE GET HEADER KEYS: ' +response.getHeaderKeys());
          System.debug('STATUS:'+response.getStatus());
          System.debug('STATUS_CODE:'+response.getStatusCode());
          System.debug(response.getBody());
        }
    }
}