@isTest public class ValidateAdressCtrlTest{
	
    @isTEst public static void TestValidateOppty(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        Opportunity opp = TestDataFactoryforInternal.createOpportunity();
        
        Test.startTest();
        ValidateAdressCtrl.validateOppty(opp.id);
        Test.stopTest();
    }
    
    @isTEst public static void TestValidateTenantEquip(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        Opportunity opp = TestDataFactoryforInternal.createOpportunity();
        Tenant_Equipment__c TA = TestDataFactoryforInternal.createTenantEquipment(opp);
        
        Test.startTest();
        ValidateAdressCtrl.validateTenantEquip(TA.id);
        Test.stopTest();
    }
    
    @isTEst public static void TestValidateTenantEquips(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        Opportunity opp = TestDataFactoryforInternal.createOpportunity();
        List<Tenant_Equipment__c> TABulk = TestDataFactoryforInternal.createTenantEquipmentBulk(opp);
        List<id> lstID = new list<id>();
        for(Tenant_Equipment__c TE: TABulk)
        	lstID.add(TE.Id);
        
        Test.startTest();
        ValidateAdressCtrl.validateTenantEquips(lstID);
        Test.stopTest();
    }
    
    @isTEst public static void TestValidateTenantEquipBulk(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        Opportunity opp = TestDataFactoryforInternal.createOpportunity();
        List<Tenant_Equipment__c> TABulk = TestDataFactoryforInternal.createTenantEquipmentBulk(opp);
        
        Test.startTest();
        ValidateAdressCtrl.validateTenantEquipBulk(TABulk);
        Test.stopTest();
    }
    
    @isTEst public static void TestValidateTenantAccount(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        Opportunity opp = TestDataFactoryforInternal.createOpportunity();
        Tenant_Account__c TA = TestDataFactoryforInternal.createTenantAccount(opp);
        
        Test.startTest();
        ValidateAdressCtrl.validateTenantAccount(TA.Id);
        Test.stopTest();
    }
    
    @isTEst public static void TestValidateAddressFlow(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        List<ValidateAdressCtrl.FlowInputs> inputs=new List<ValidateAdressCtrl.FlowInputs>();
        for(Integer i=0; i<20; i++)
        {
            ValidateAdressCtrl.FlowInputs input = new ValidateAdressCtrl.FlowInputs();
            input.street = 'Street '+i;
            input.city = 'city '+i;
            input.state = 'state '+i;
            input.zip = 'zip '+i;
            input.country = 'country '+i;
            inputs.add(input);
        }
        
        Test.startTest();
        ValidateAdressCtrl.ValidateAddressFlow(inputs);
        Test.stopTest();
    }
}