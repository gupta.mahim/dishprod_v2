@IsTest
public class GetBulkEquipmentTest
{
    public static testmethod void testBulkEquip()
    {
        Opportunity opp = TestDataFactoryforInternal.createOpportunity();
        Tenant_Equipment__c TE = TestDataFactoryforInternal.createTenantEquipment(opp);
        String oppId = String.valueOf(opp.Id);
        TE.Smart_Card__c = 'S1234567890';
        TE.Amdocs_Tenant_ID__c = 'TestTenantID';
        TE.Customer_First_Name__c = 'TestFirstName';
        TE.Customer_Last_Name__c = 'TestLastName';
        TE.Phone_Number__c = '1111111111';
        TE.Unit__c = '111';
        TE.Action__c = 'Active';
        Update TE;
        GetBulkEquipment.initBulkTenantEquipment(oppId);
        GetBulkEquipment.searchBulkTenantEquipment(oppId,'Test');
        
        GetBulkEquipment.BulkEquipData rec=new GetBulkEquipment.BulkEquipData(TE);
        
    }
}