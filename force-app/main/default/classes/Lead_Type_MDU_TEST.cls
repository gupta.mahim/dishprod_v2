@IsTest
private class Lead_Type_MDU_TEST {
    static testMethod void validateLead_Type_S_MDU() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Apartment/Condo/Townhome',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

       insert L;
        
       L = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L.Id];
       System.debug('Lead Type  after trigger fired: ' + L.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L.sundog_deprm2__Lead_Type__c);
    }
     static testMethod void validateLead_Type_S_MDU2() {
        Lead LL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Planned Unit Development',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LL;
        
       LL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LL.Id];
       System.debug('Lead Type  after trigger fired: ' + LL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_S_MDU3() {
        Lead LLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='RV Park',
        Location_of_Service__c='Connection',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLL;
        
       LLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLL.Id];
       System.debug('Lead Type  after trigger fired: ' + LLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_S_MDU4() {
        Lead LLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='University',
        Location_of_Service__c='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLL;
        
       LLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLL.Id];
       System.debug('Lead Type  after trigger fired: ' + LLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_S_MDU5() {
        Lead LLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='School',
        Location_of_Service__c='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLL;
        
       LLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLL.Id];
       System.debug('Lead Type  after trigger fired: ' + LLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_S_MDU6() {
        Lead LLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Marinas',
        Location_of_Service__c='Dock Hookup',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLL;
        
       LLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLL.Id];
       System.debug('Lead Type  after trigger fired: ' + LLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_S_MDU7() {
        Lead L7= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Assisted Living',
        Location_of_Service__c ='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert L7;
        
       L7= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L7.Id];
       System.debug('Lead Type  after trigger fired: ' + L7.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L7.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_S_MDU8() {
        Lead L8= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Assisted Living',
        Location_of_Service__c ='Chewbacca',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert L8;
        
       L8= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L8.Id];
       System.debug('Lead Type  after trigger fired: ' + L8.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L8.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_S_MDU9() {
        Lead L9 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retirement Community',
        Location_of_Service__c ='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert L9;
        
       L9= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L9.Id];
       System.debug('Lead Type  after trigger fired: ' + L9.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L9.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_S_MDU10() {
        Lead L10 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retirement Community',
        Location_of_Service__c ='Chewbacca',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert L10;
        
       L10= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L10.Id];
       System.debug('Lead Type  after trigger fired: ' + L10.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L10.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_P_MDU() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Apartment/Condo/Townhome',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

       insert L;
        
       L = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L.Id];
       System.debug('Lead Type  after trigger fired: ' + L.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L.sundog_deprm2__Lead_Type__c);
    }
     static testMethod void validateLead_Type_P_MDU2() {
        Lead LL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Planned Unit Development',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert LL;
        
       LL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LL.Id];
       System.debug('Lead Type  after trigger fired: ' + LL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_P_MDU3() {
        Lead LLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='RV Park',
        Location_of_Service__c='Connection',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert LLL;
        
       LLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLL.Id];
       System.debug('Lead Type  after trigger fired: ' + LLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_P_MDU4() {
        Lead LLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='University',
        Location_of_Service__c='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert LLLL;
        
       LLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLL.Id];
       System.debug('Lead Type  after trigger fired: ' + LLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_P_MDU5() {
        Lead LLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='School',
        Location_of_Service__c='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert LLLLL;
        
       LLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLL.Id];
       System.debug('Lead Type  after trigger fired: ' + LLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_P_MDU6() {
        Lead LLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Marinas',
        Location_of_Service__c='Dock Hookup',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert LLLLLL;
        
       LLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLL.Id];
       System.debug('Lead Type  after trigger fired: ' + LLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', LLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_P_MDU7() {
        Lead L7= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Assisted Living',
        Location_of_Service__c ='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert L7;
        
       L7= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L7.Id];
       System.debug('Lead Type  after trigger fired: ' + L7.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L7.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_P_MDU8() {
        Lead L8= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Assisted Living',
        Location_of_Service__c ='Chewbacca',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert L8;
        
       L8= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L8.Id];
       System.debug('Lead Type  after trigger fired: ' + L8.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L8.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_P_MDU9() {
        Lead L9 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retirement Community',
        Location_of_Service__c ='Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert L9;
        
       L9= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L9.Id];
       System.debug('Lead Type  after trigger fired: ' + L9.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L9.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLead_Type_P_MDU10() {
        Lead L10 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retirement Community',
        Location_of_Service__c ='Chewbacca',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert L10;
        
       L10= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L10.Id];
       System.debug('Lead Type  after trigger fired: ' + L10.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('MDU', L10.sundog_deprm2__Lead_Type__c);
}
}