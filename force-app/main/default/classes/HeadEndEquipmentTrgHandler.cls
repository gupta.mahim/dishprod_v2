/*
* Author: M Fazal Ur Rehan
* Main handler class for Head-end Equipment trigger, to hold the logic for equipment dupes, dupe equipment case creation, AmDocs BulkEQ Callout,
* hidden field populate on Opportunity, Equipment Recall Reject Delete handling
* Invokes the appropriate TenantEquipmentTrgHandler mehtods for event handling
*/
public class HeadEndEquipmentTrgHandler
{
    public static void doBeforeInsert(List<Equipment__c> newHEData)
    {
        // Inovkes the process to locate Head-end Equipment dupe
        EquipDupeHandler.handleHEDupes(newHEData,false); 
    }
    public static void doAfterInsert(Map<Id,Equipment__c> newHEData,Map<Id,Equipment__c> oldHEData)
    {
        //Invoke the case creation process for Active elsewhere head-end equipment
        EquipDupeCaseHandler.createCaseForHE(oldHEData,newHEData);
    }
    public static void doBeforeUpdate(Map<Id,Equipment__c> newHEData,Map<Id,Equipment__c> oldHEData)
    {
        //Inovke the Amdocs bulk callouts 
        doAmDocsBulkEQCallout(newHEData.values());
    }
    public static void doAfterUpdate(Map<Id,Equipment__c> newHEData,Map<Id,Equipment__c> oldHEData)
    {
        //Invoke the Recall / Reject / Delete process
        doEquipRecallRejectDelete(newHEData.values());
        doMoveEquipment(newHEData.values(),oldHEData.values());
    }
    public static void doMoveEquipment(List<Equipment__c> newTEData,List<Equipment__c> oldTEData)
    {
        Equipment__c newRec=newTEData[0];// The address validation is a manual process and should happen on individual record.
        Equipment__c oldRec=Trigger.isUpdate?oldTEData[0]:null;
        
        boolean doMoveReceiver=newRec.Move_Receiver_To__c!=null && (oldRec!=null && oldRec.Move_Receiver_To__c!=newRec.Move_Receiver_To__c);
        if(doMoveReceiver){AmdocsMoveReceiverCtrl.doHEMoveReceiver(newRec.id);}
    }
    //Copied from AmDocs_BulkEQCalloutTrigger
    // Performs AmDocs callouts when Send Trip is requested, Smartcard Swap is requested
    private static void doAmDocsBulkEQCallout(List<Equipment__c> heData)
    {
        // Processes for SF->AmDocs to call @Future API callout triigers.
        Equipment__c S = heData[0];
        system.debug('he'+s);
        
        if(S.Action__c == 'Send Trip / HIT')
        { S.AmDocs_FullString_Return__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_Transaction_Code__c = ''; S.AmDocs_Order_ID__c = ''; S.API_Status__c = ''; AmDocs_CalloutClass_EQReSend.AmDocsMakeCalloutEQResend(S.Id); System.debug('IF 2' +S);}
        else
        if(S.Action__c == 'SWAP SMARTCARD')
        { S.Action__c = 'SWAP PENDING'; S.AmDocs_FullString_Return__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_Transaction_Code__c = ''; S.AmDocs_Order_ID__c = ''; S.API_Status__c = ''; AmDocs_CalloutClass_BulkHESCSwap.AmDocsMakeCalloutBulkHESCSwap(S.Id); System.debug('IF 2' +S);}
        // else
    }
    //Copied from EquipmentRecallRejectDelete trigger
    // Deletes the equipment records when Flag__c is set to 'Delete'
    private static void doEquipRecallRejectDelete(List<Equipment__c> heData)
    {
        Equipment__c eqa = heData[0];
        if (eqa.Opportunity__c != null && eqa.Flags__c == 'Delete' ){Set<Id> eqaIds = new Set<Id>();Set<Id> opIds = new Set<Id>();for (Equipment__c eqa2: heData){opIds.add(eqa2.Opportunity__c);eqaIds.add(eqa2.Id);}list<equipment__c> eqa3 = [select id, Flags__c, Statis__c, Opportunity__c from equipment__c where ID in :eqaIds AND Flags__c = 'Delete' ];Database.delete(eqa3);}


    }
}