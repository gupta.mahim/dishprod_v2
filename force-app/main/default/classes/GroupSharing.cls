public with sharing class GroupSharing {
/*    
    public static Boolean ACCOUNT_ASSOC_PROCESS_FLAG = false;
    
    public static boolean isReciprocal()
    {
        // Static variables are static per thread in SFDC not app or session
        if (ACCOUNT_ASSOC_PROCESS_FLAG)
        {
            ACCOUNT_ASSOC_PROCESS_FLAG = false;
            return true;
        }
        else
        {
            ACCOUNT_ASSOC_PROCESS_FLAG = true;
            return false;
        }
    }
*/    
    public static Boolean run = true; public static Boolean runOnce() { if (run) { run = false; return true; } else { return false; } }

    @future    
    public static void createShares(List<Id> assocList, Map<Id,Id> accountIdUserRoleIdMap, Map<Id,Id> gMap)
    {   //Map<String,String> groupIdMap, Map<String,String> acctIdParentIdMap
    
        //moved from the trigger to make the sharing method future
        List<AccountShare> newShares = new AccountShare[]{};
        List<LeadShare> newLeadShares = new List<LeadShare>();

        System.debug('assocList = ' + assocList);       
        System.debug('accountIdUserRoleIdMap = ' + accountIdUserRoleIdMap);         
        System.debug('gMap = ' + gMap);

        Map<Id, Id> userAccountMap = new Map<Id, Id>();
        Map<Id, User> users = new Map<Id, User>([Select Id, Contact.AccountId from user where Contact.AccountId In :assocList]);
        for (User u : users.values()) {
            userAccountMap.put(u.Id, u.Contact.AccountId);
        }
        List<Lead> leads = [Select Id, OwnerId from Lead where OwnerId In :users.keySet()];
    
        for (Distributor_Retailer_Association__c assoc : [select id, Distributor__c, Retailer__c from Distributor_Retailer_Association__c where Retailer__c in :assocList]) {  
            System.debug('assoc = ' + assoc);       
            if (assoc.Distributor__c != null && accountIdUserRoleIdMap.containsKey(assoc.Distributor__c))
            {
                Id roleId = accountIdUserRoleIdMap.get(assoc.Distributor__c);
                Id groupId = gMap.get(roleId);
                newShares.add(new AccountShare(
                    AccountId = assoc.Retailer__c,
                    userOrGroupId = groupId,
                    AccountAccessLevel = 'Edit',
                    OpportunityAccessLevel = 'None'));

                for (Lead l : leads) {
                    Id retailerId = userAccountMap.get(l.OwnerId);
                    if (retailerId == assoc.Retailer__c) {
                        newLeadShares.add(new LeadShare(
                            LeadId = l.Id,
                            userOrGroupId = groupId,
                            LeadAccessLevel = 'Read'));
                    }
                }

            }

        }
        
        if (newShares.size() > 0)
            insert newShares;
        if(newLeadShares.size() > 0)
            insert newLeadShares;
        
        System.debug('newShares => '+newShares);        
    }
    
    @future
    public static void removeShares(List<Id> retailerIds, List<Id> distributorIds, Map<Id,Id> accountIdUserRoleIdMap, Map<Id,Id> gMap) {

        //moved from the trigger to make the sharing method future
        List<AccountShare> sharesToRemove = new AccountShare[]{};
        List<LeadShare> leadSharesToRemove = new List<LeadShare>();

        System.debug('retailerIds = ' + retailerIds); 
        System.debug('distributorIds = ' + distributorIds);     
        System.debug('accountIdUserRoleIdMap = ' + accountIdUserRoleIdMap);         
        System.debug('gMap = ' + gMap);
        
        Map<Id, Set<Id>> retailerDistiMap = new Map<Id, Set<Id>>();
        for (Integer i = 0; i < retailerIds.size(); i++) {
            Id retailerId = retailerIds.get(0);
            Id distributorId = distributorIds.get(0);
            Set<Id> distiList = retailerDistiMap.get(retailerId);
            if (distiList == null) {
                distiList = new Set<Id>();
                retailerDistiMap.put(retailerId, distiList);
            }
            distiList.add(distributorId);
        }
        System.debug('retailerDistiMap = ' + retailerDistiMap);
        
        for (AccountShare share : [Select Id, userOrGroupId, AccountId from AccountShare where AccountId In :retailerIds And userOrGroupId In :gMap.values()])  { System.debug('share = ' + share); Set<Id> distiIds = retailerDistiMap.get(share.AccountId); System.debug('distiIds = ' + distiIds); if (distiIds == null) continue;  for (Id distiId : distiIds) { Id roleId = accountIdUserRoleIdMap.get(distiId); Id groupId = gMap.get(roleId); if (share.userOrGroupId == groupId) {  sharesToRemove.add(share);
                }
            }       
                    
        }
        
        //get all leads that were owned by that account
        Map<Id, User> partnerUsers = new Map<Id, User>([Select Id, Contact.AccountId 
            from User where ContactId != null And Contact.AccountId In :retailerIds]);
        Map<Id, Id> leadRetailerMap = new Map<id, Id>();
        List<Lead> leads = [Select Id, OwnerId from Lead where OwnerId In :partnerUsers.keySet()];
        for (Lead l : leads) { leadRetailerMap.put(l.Id, partnerUsers.get(l.OwnerId).COntact.AccountId);
        }
        
        
        //query the lead shares to remove
        for(LeadShare share : [Select Id, LeadId, UserOrGroupId from LeadShare where UserOrGroupId In :gMap.values()]) { Id retailerId = leadRetailerMap.get(share.LeadId); if (retailerId == null) continue; Set<Id> distiIds = retailerDistiMap.get(retailerId); for (Id distiId : distiIds) { Id roleId = accountIdUserRoleIdMap.get(distiId); Id groupId = gMap.get(roleId); if (share.userOrGroupId == groupId) { leadSharesToRemove.add(share);
                }
            }       
            
            
        }
        
        System.debug('sharesToRemove = ' + sharesToRemove);
        if (! sharesToRemove.isEmpty()) { delete sharesToRemove;
        }
        System.debug('leadSharesToRemove = ' + leadSharesToRemove);
        if (! leadSharesToRemove.isEmpty()) {delete leadSharesToRemove;
        }
        
    }

    //@future public static void createLeadShares(Map<Id,String> leadAccMap) { List<LeadShare> newLeadShares = new List<LeadShare>(); List<Id> distiAccountIds = new List<Id>(); List<Id> retailerAccountIds = new List<Id>(); Map<Id, Set<Id>> retailerDistiMap = new Map<Id, Set<Id>>(); for (Distributor_Retailer_Association__c assoc : [select id, Distributor__c, Retailer__c from Distributor_Retailer_Association__c where Retailer__c in :leadAccMap.values()]) {  distiAccountIds.add(assoc.Distributor__c); Set<Id> distiList = retailerDistiMap.get(assoc.Retailer__c); if (distiList == null) { distiList = new Set<Id>(); retailerDistiMap.put(assoc.Retailer__c, distiList); } distiList.add(assoc.Distributor__c); } Map<Id, Id> accountIdUserRoleIdMap = GroupSharingHelper.getRoles(distiAccountIds); Map<Id,Id> gMap = GroupSharingHelper.getGroups(accountIdUserRoleIdMap.values()); for (Id leadId : leadAccMap.keySet()) { Id retailerAccountId = leadAccMap.get(leadId); if (retailerAccountId == null) continue; Set<Id> distiList = retailerDistiMap.get(retailerAccountId); if (distiList == null) continue; for (Id distiId : distiList) { Id roleId = accountIdUserRoleIdMap.get(distiId); Id groupId = gMap.get(roleId); newLeadShares.add(new LeadShare( LeadId = leadId, userOrGroupId = groupId,LeadAccessLevel = 'Read')); } } if(newLeadShares.size() > 0) insert newLeadShares;   }
                
                
/*
    @future
    public static void removeLeadShares(Map<Id,String> leadAccMap) { //Map - LeadId - RetailerId
        
        List<LeadShare> sharesToRemove = new List<LeadShare>();
        
        List<Id> distiAccountIds = new List<Id>();
        Map<Id, Set<Id>> retailerDistiMap = new Map<Id, Set<Id>>();
        for (Distributor_Retailer_Association__c assoc : [select id, Distributor__c, Retailer__c from Distributor_Retailer_Association__c where Retailer__c in :leadAccMap.values()])
        {
            distiAccountIds.add(assoc.Distributor__c);
            Set<Id> distiList = retailerDistiMap.get(assoc.Retailer__c);
            if (distiList == null) {
                distiList = new Set<Id>();
                retailerDistiMap.put(assoc.Retailer__c, distiList);
            }
            distiList.add(assoc.Distributor__c);
        }
        
        Map<Id, Id> accountIdUserRoleIdMap = GroupSharingHelper.getRoles(distiAccountIds);
        Map<Id,Id> gMap = GroupSharingHelper.getGroups(accountIdUserRoleIdMap.values());
        List<LeadShare> leadShares = [Select Id, userOrGroupId from LeadShare where LeadId In :leadAccMap.keySet() And userOrGroupId In :gMap.values()];
        
        for (LeadShare ls : leadShares) {
            //get the associated retailer
            Id retailerId = leadAccMap.get(ls.LeadId);
            Set<Id> distiIds =  retailerDistiMap.get(retailerId);
            for (Id distiId : distiIds) {
                Id roleId = accountIdUserRoleIdMap.get(distiId);
                Id groupId = gMap.get(roleId);
                if (ls.userOrGroupId == groupId) {
                    sharesToRemove.add(ls);
                }
            }       
        }
        
        if (! sharesToRemove.isEmpty()) {
            delete sharesToRemove;
        }
        
    }*/
}
    
/* 

    
    public GroupSharing (List<Distributor_Retailer_Association__c> associations) {
        
        Set<Id> accountIds = new Set<Id>();
        for (Distributor_Retailer_Association__c assoc : associations) {
            accountIds.add(assoc.Retailer__c);
            if (assoc.Distributor__c != null && String.valueOf(assoc.Distributor__c) != '')
                accountIds.add(assoc.Distributor__c);
        }
    
        
        Map<String, String> roleIdToAcctIdMap = new Map<String, String>();   
        for (UserRole role : [select Id, Name, PortalAccountId from UserRole where portalaccountid in 
            :accountIds and Name like '%Partner Executive']) { 
            roleIdToAcctIdMap.put(role.id, role.PortalAccountId);
        }
        
        Map<String, String> acctIdToGrpIdMap = new Map<String, String>();
        for (Group g : [select RelatedId, Id from Group where RelatedId in :roleIdToAcctIdMap.keySet() 
            and Type = 'RoleAndSubordinates']) {
            acctIdToGrpIdMap.put(roleIdToAcctIdMap.get(g.relatedid), g.id);
        }           
       
        for (Distributor_Retailer_Association__c assoc : associations) {
            if (acctIdToGrpIdMap.get(assoc.Retailer__c) != null)
                grpIdMap.put('Account'+assoc.Retailer__c+assoc.Distributor__c, 
                    acctIdToGrpIdMap.get(assoc.Retailer__c));
                
                    
            if (acctIdToGrpIdMap.get(assoc.Distributor__c) != null)
                grpIdMap.put('AssocAccount'+assoc.Retailer__c+assoc.Distributor__c, 
                    acctIdToGrpIdMap.get(assoc.Distributor__c));
                    
        }
        
        System.debug('grpIdMap = ' + grpIdMap);         
    }
     
    public Map<String,String> getGroupIds() {
        return grpIdMap;
    }
    

    
    @future
    public static void removeShares(Map<string,Id> acctIds, Map<string,Id> assocIds, Map<String,String> groupIds, Set<string> userGroupIds, Set<String> acctIdToInverseRoleMapKeys ){

        List<AccountShare> accSharesToDelete = new List<AccountShare>();
        for (List<AccountShare> existingShares : [select id, accountid, userorgroupid from AccountShare where (accountid in :acctIds.values() or 
            accountid in :assocIds.values()) and userOrGroupId in :groupIds.values() and rowCause = 'Manual']) {
                
            List<AccountShare> sharesToDelete = new List<AccountShare>();   
            for (AccountShare share : existingShares) {
                if (userGroupIds.contains(share.accountid+''+share.userOrGroupId))
                    sharesToDelete.add(share);
            }
            accSharesToDelete.addAll(sharesToDelete);
        }
    
        if (accSharesToDelete.size() > 0)
            delete accSharesToDelete;
        
    }
   
    @future
    public static void createLeadShares(Map<Id,String> leadAccMap, Map<Id,Id> accountIdUserRoleIdMap, Map<Id,Id> gMap) {
        //get the highest roles for these accounts
        List<LeadShare> leadShares = new List<LeadShare>();
//      getRoles(leadAccMap.values());
        for (Id Id : leadAccMap.keySet()) {
            List<Id> parentIds = leadAccMap.get(Id).split(',');
            for(Id pid : parentIds){
                Id roleId = accountIdUserRoleIdMap.get(pId);
                if (roleId == null) {
                    System.debug('Id for null = ' + Id);
                } else {
                    System.debug('roleId = ' + roleId);
                    Id relatedGroupId = gMap.get(roleId);   //getGroup(roleId, gList);
                    System.debug('relatedGroupId = ' + relatedGroupId);
                    if (relatedGroupId != null) {
                        leadShares.add(new LeadShare( LeadAccessLevel = 'Edit', LeadId = Id, UserOrGroupId = relatedGroupId));
                    //  groupMembers.add(new GroupMember(groupId = dynamicGroup, UserOrGroupId = relatedGroupId));
                    } else {
                        System.debug('roleId for null = ' + roleId);
                    }
                }
            }
            
        }
        insert leadShares;
    }
}


        for (Distributor_Retailer_Association__c assoc : [select id, Distributor__c, Retailer__c from Distributor_Retailer_Association__c where Retailer__c in :assocList]) {  
            System.debug('assoc id = ' + assoc.id);     
            if (assoc.Distributor__c != null ) 
            {  
                if (groupIdMap.containsKey('AssocAccount'+assoc.Retailer__c+assoc.Distributor__c))
                {
                    newShares.add(new AccountShare(
                    AccountId = assoc.Retailer__c,
                    userOrGroupId = groupIdMap.get('AssocAccount'+assoc.Retailer__c+assoc.Distributor__c),
                    AccountAccessLevel = 'Read',
                    OpportunityAccessLevel = 'None'));
                }
               
        }
*/

 /*else if(assoc.Distributor__c != null ){
            if(assoc.LeadId__c != null){
                Id roleId = accountIdUserRoleIdMap.get(assoc.Distributor__c);
                if (roleId == null) {
                    //System.debug('Id for null = ' + assoc.LeadId__c);
                } else {
                    System.debug('roleId = ' + roleId);
                    Id relatedGroupId = gMap.get(roleId);   //getGroup(roleId, gList);
                    System.debug('relatedGroupId = ' + relatedGroupId);
                    if (relatedGroupId != null) {
                        //newLeadShares.add(new LeadShare( LeadAccessLevel = 'Edit', LeadId = assoc.LeadId__c, UserOrGroupId = relatedGroupId));
                    //  groupMembers.add(new GroupMember(groupId = dynamicGroup, UserOrGroupId = relatedGroupId));
                    } else {
                        System.debug('roleId for null = ' + roleId);
                    }
                }
            } 
        }*/
/*        
public class TestGroupSharing {
    
    testMethod static void testCreateShares() {
        Account disti = new Account(Name = 'Disti');
        Account retailer = new Account(Name = 'Retailer');
        insert disti;
        insert retailer;
        
        //now create an association
        Distributor_Retailer_Association__c assoc = new Distributor_Retailer_Association__c(Distributor__c = disti.Id, 
            Retailer__c = retailer.Id);
        Test.startTest();
        insert assoc;
        Test.stopTest(); 
    } 
}
*/