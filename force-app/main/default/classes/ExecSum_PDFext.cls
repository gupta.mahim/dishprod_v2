public with sharing class ExecSum_PDFext {
public Executive_Summary__c  es {get;set;}
private List<opportunityLineItem> Prods;
private List<ProcessInstance> apps;



private Executive_Summary__c ex;
    public ExecSum_PDFext(ApexPages.StandardController controller) {
this.ex=(Executive_Summary__c)controller.getRecord();
    }

{
es = [select Id, Name, Property_Name__r.id, Property_Name_text__c, Address__c, Anticipated_Penetration__c, ARPU__c,  Capex__c, Operations_Manager_Approval_Date__c,
Churn_Rate__c, Date_Submitted__c, Deal_Overview__c, Discounted_Payback_months__c, FO_Addition__c, Property_Name__c, Total_Capex__c, Property_Contribution__c,
Front_Office_Manager_Recommendation__c, Front_Office_Request__c, Gross_Margin__c, Install_Work__c, Front_Office_Name_Text__c, Operations_Manager_Recommendation2__c,
Internet_Exclusive__c, Monthly_Free_Cash_Flow__c, NPV__c, Number_of_Units__c, Operations_Manager_Recommendation__c, Net_Margin__c,Internet_Equipment__c,Internet_Labor__c, 
Overall_Risk__c, Overall_Risk_Comments__c, Prin__c,  Property_Type__c, Requestor__c, Required_Approval_Date__c, State__c, Status__c, Front_Office_Recommendation__c, IRR__c,
Total_EBUs__c, Total_Subs__c, Variance_from_Standard_Deal__c, Video_Exclusive__c, Financial_Risk__c, Financial_Risk_Comments__c, Total_Equip_Freight_Costs__c,Contract_Terms__c,
Operations_SVP_Approval__c, Operations_SVP_Comments__c, Operations_Director_Comments__c, Operations_Director_Approval__c, Labor_Contractor_Travel_Costs__c, city__c,
Sales_SVP_Approval__c, Sales_SVP_Comments__c, Sales_VP_Approval__c, Sales_VP_Comments__c, Account__c, Front_Office_Project_Manager__c,  Door_Fees__c, Estimated_Quarterly_Revenue_Share__c,
Assigned_Project_Manger__c, Business_Unit__c, Ops_Director_Approval_Date__c, Operations_SVP_Approval_Date__c, Sales_SVP_Approval_Date__c,Revenue_Share__c, Revenue_Share_Percent__c,    Incremental_Fee__c,
Sales_VP_Approval_Date__c
 from Executive_Summary__C where Id = :ApexPages.currentPage().getParameters().get('Id')];


}
  public List<opportunityLineItem> getprods() {
       Executive_Summary__c exs = [SELECT ID, Property_Name__r.id, Property_Name__c from Executive_Summary__c where id = :es.id];
       if (exs.Property_Name__c == null)
       return null;

       prods = [select Id, Quantity, TotalPrice, UnitPrice, Description, PriceBookEntryId, PriceBookEntry.Name, Status__c
       FROM opportunityLineItem WHERE OpportunityId = :exs.Property_Name__r.id ];
         


        return prods;
        }
        
          public List<ProcessInstance> getapps() {
       apps = [SELECT ID, TARGETOBJECTID, Status, LASTMODIFIEDBYID, LASTMODIFIEDDATE from ProcessInstance where TARGETOBJECTID = :es.id 
       ORDER BY LASTMODIFIEDDATE DESC];
       
       return apps;
       }

 

       
        
}