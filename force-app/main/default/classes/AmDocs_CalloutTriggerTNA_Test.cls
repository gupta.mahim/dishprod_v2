@IsTest
private class AmDocs_CalloutTriggerTNA_Test{
static testMethod void TestTNACallToAmdocs() {

        Account acct1 = new Account( 
            Name = 'Test Account 1',
            ToP__c = 'Integrator',
            Pyscical_Address__c = '1011 Collie Path',
            City__c = 'Round Rock',
            State__c = 'TX',
            Zip__c = '78664',
            Amdocs_CreateCustomer__c = false,
            Distributor__c = 'PACE',
            OE_AR__c = '12345',
            Amdocs_CustomerID__c = '123',
            Amdocs_BarID__c = '456',
            Amdocs_FAID__c = '789');
        insert acct1;

        Opportunity opp1 = New Opportunity(
            Name = 'Test Opp 123456789 Testing opp1',
            First_Name_of_Property_Representative__c = 'Jerry',
            Name_of_Property_Representative__c = 'Clifft',
            Business_Address__c = '1011 Collie Path',
            Business_City__c = 'Round Rock',
            Business_State__c = 'TX',
            Business_Zip_Code__c = '78664',
            Billing_Contact_Phone__c = '512-383-5201',
            Billing_Contact_Email__c = 'jerry.clifft@dish.com',
            CloseDate=system.Today(),
            StageName='Closed Won',

            AmDocs_SiteID__c = '22222',
            AccountId = acct1.Id,
            AmDocs_tenantType__c = 'Incremental',
            AmDocs_ServiceID__c='012',
            Amdocs_CustomerID__c = '123',
            Amdocs_BarID__c = '456',
            Amdocs_FAID__c = '789',
            System_Type__c='QAM',
            Category__c='FTG',
            Address__c='address',
            City__c='Austin',
            State__c='TX',
            Zip__c='78664',
            Phone__c='5122965581',
            AmDocs_Property_Sub_Type__c='Hotel/Motel',
            Number_of_Units__c=100,
            Smartbox_Leased__c=false,
            RecordTypeId='012600000005ChE');
        insert opp1;

        Tenant_Account__c ta = new Tenant_Account__c(
            Address__c ='1011 Coliie Path',
            City__c = 'Round Rock',
            State__c = 'TX',
            Zip__c = '78664',
            Unit__c = '1A',
            Phone_Number__c = '5123835201',
            Email_Address__c = 'jerry.clifft1111111111111@dish.com',
            First_Name__c = 'Jerry',
            Last_Name__c = 'Clifft',
            Type__c = 'Incremental',
            Opportunity__c = opp1.id,
            LastActivateUpdate__c = Null,
            AmDocs_CreateUnit__c=True,
            AmDocs_UpdateUnit__c =False,
            Ignore__c = False,
            Action__c = '');
        insert ta;
        
            ta.AmDocs_CreateUnit__c=True;
            update ta;
       
        Tenant_Account__c ta2 = new Tenant_Account__c(
            Address__c ='1011 Coliie Path',
            City__c = 'Round Rock',
            State__c = 'TX',
            Zip__c = '78664',
            Unit__c = '2A',
            Phone_Number__c = '5123835201',
            Email_Address__c = 'jerry.clifft1111111111111@dish.com',
            First_Name__c = 'Jerry',
            Last_Name__c = 'Clifft',
            Type__c = 'Incremental',
            LastActivateUpdate__c = Null,
            Ignore__c = False,
            Opportunity__c = opp1.id,
            AmDocs_CreateUnit__c=False,
            AmDocs_UpdateUnit__c =True,
            Action__c = '');
        insert ta2;
        
            ta2.AmDocs_UpdateUnit__c =True;
            update ta2;
 
        Tenant_Account__c ta3 = new Tenant_Account__c(
            Address__c ='1011 Coliie Path',
            City__c = 'Round Rock',
            State__c = 'TX',
            Zip__c = '78664',
            Unit__c = '3A',
            Phone_Number__c = '5123835201',
            Email_Address__c = 'jerry.clifft1111111111111@dish.com',
            First_Name__c = 'Jerry',
            Last_Name__c = 'Clifft',
            Type__c = 'Incremental',
            LastActivateUpdate__c = Null,
            Ignore__c = False,
            Opportunity__c = opp1.id,
            AmDocs_CreateUnit__c=False,
            AmDocs_UpdateUnit__c =False,
            Action__c = 'UnMute');
        insert ta3;
        
        ta3.Action__c = 'Mute';
        update ta3;       
       
       ta = [SELECT Id,Action__c, AmDocs_CreateUnit__c, AmDocs_UpdateUnit__c, Mute_Disconnect__c  FROM Tenant_Account__c WHERE Id =:ta.Id];
       System.debug('Category after trigger fired: ' + ta.Id);
//       System.assertEquals(False, ta.AmDocs_CreateUnit__c);
//       System.assertEquals(False, ta.AmDocs_UpdateUnit__c);
//       System.assertEquals('Mute', ta.Mute_Disconnect__c);
}

     
  }