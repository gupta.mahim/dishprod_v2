@isTest
Public class Equipment_EditTests{
    public static testMethod void testEquipment_Edit(){
//Test converage for the myPage visualforce page
PageReference pageRef = Page.equipment_edit_smatv;
Test.setCurrentPageReference(pageRef);
Opportunity newOpp = new Opportunity (name='XYZ Organization', StageName='Closed Won', CloseDate=Date.Today());
insert newOpp;
//create first contact
Equipment__c myEq = new Equipment__c (Name='Joe',
 Receiver_S__c='SJoe', Receiver_Model__c='311', Statis__c='Activation Requested', Programming__c='HBO',
Opportunity__c=newopp.id);
insert myEq;
ApexPages.StandardController sc = new ApexPages.standardController(newOpp);
// create an instance of the controller
Equipment_Edit myPageCon = new Equipment_Edit(sc);
//try calling methods/properties of the controller in all possible scenarios
// to get the best coverage.
myPageCon.GetEquip();
myPageCon.onsave();
myPageCon.onclose();
myPageCon.onadd();
}
}