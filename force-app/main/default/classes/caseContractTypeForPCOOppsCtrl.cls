//Created this new class for Milestone# MS-000218

public class caseContractTypeForPCOOppsCtrl {
    
    @AuraEnabled
    public static string getTenantType(string OppId)
    {
        system.debug('OppId-'+OppId);
        String tenantTypeValue;
        List<Opportunity> OppLst = New List<Opportunity>();
        OppLst = [SELECT AmDocs_tenantType__c FROM Opportunity WHERE Id = : OppId Limit 1];
        if(OppLst.size()>0){
        	tenantTypeValue = String.valueof(OppLst[0].AmDocs_tenantType__c);
        }
        system.debug('tenantTypeValue-'+tenantTypeValue);
        return tenantTypeValue;
    }

}