@IsTest
public class AmDocs_CalloutClass_TNAGMeISActvElsWTest {
    
    @testSetup static void testSetupdata(){
        AmDocs_Login__c aml = TestDataFactoryForGuideMe.createAmDocsLogin();
        Account acc = TestDataFactoryForGuideMe.createAccount();
        Opportunity opp = TestDataFactoryForGuideMe.createOpportunity(acc.Id);
        Tenant_Account__c ta = TestDataFactoryForGuideMe.createTenantAccount(opp.Id);
        Tenant_Account__c ta2 = TestDataFactoryForGuideMe.createTenantAccount2(opp.Id);
        Tenant_Account__c ta3 = TestDataFactoryForGuideMe.createTenantAccount3(opp.Id);
        Tenant_Equipment__c te4 = TestDataFactoryForGuideMe.createTenantEquipment4(opp.Id, ta2.Id);
    }
    
    @isTest
    static void testTEDisconnect1()
    {
        list<Tenant_Account__c> TA = [select id, Name, Mute_Disconnect__c, Reset__c, Action__c, AmDocs_ServiceID__c from Tenant_Account__c 
                                         where First_Name__c = 'Mahim' ];
        
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete',
        	'{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
			null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        AmDocs_CalloutClass_TNAGMeISActvElsWhrDC.AmDocsMakeCalloutTNAMuteDC(TA[0].Id);
        Test.stopTest();    
    }
    
    @isTest
    static void testTEDisconnect2()
    {
        list<Tenant_Account__c> TA = [select id, Name, Mute_Disconnect__c, Reset__c, Action__c, AmDocs_ServiceID__c from Tenant_Account__c 
                                         where First_Name__c = 'Mahim2' ];
        
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete',
        	'{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"OrderId":"1335840"}}',
			null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        AmDocs_CalloutClass_TNAGMeISActvElsWhrDC.AmDocsMakeCalloutTNAMuteDC(TA[0].Id);
        Test.stopTest();    
    }
    
    @isTest
    static void testTEDisconnect3()
    {
        list<Tenant_Account__c> TA = [select id, Name, Mute_Disconnect__c, Reset__c, Action__c, AmDocs_ServiceID__c from Tenant_Account__c 
                                         where First_Name__c = 'Mahim2' ];
        
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete',
        	'{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}]}}',
			null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        AmDocs_CalloutClass_TNAGMeISActvElsWhrDC.AmDocsMakeCalloutTNAMuteDC(TA[0].Id);
        Test.stopTest();    
    }
    
    @isTest
    static void testTEDisconnect4()
    {
        list<Tenant_Account__c> TA = [select id, Name, Mute_Disconnect__c, Reset__c, Action__c, AmDocs_ServiceID__c from Tenant_Account__c 
                                         where First_Name__c = 'Mahim3' ];
        
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete',
        	'{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}]}}',
			null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        AmDocs_CalloutClass_TNAGMeISActvElsWhrDC.AmDocsMakeCalloutTNAMuteDC(TA[0].Id);
        Test.stopTest();    
    }
    
    @isTest
    static void testTEDisconnect5()
    {
        list<Tenant_Account__c> TA = [select id, Name, Mute_Disconnect__c, Reset__c, Action__c, AmDocs_ServiceID__c from Tenant_Account__c 
                                         where First_Name__c = 'Mahim2' ];
        
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(100, 'Complete',
        	'{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
			null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        AmDocs_CalloutClass_TNAGMeISActvElsWhrDC.AmDocsMakeCalloutTNAMuteDC(TA[0].Id);
        Test.stopTest();    
    }
}