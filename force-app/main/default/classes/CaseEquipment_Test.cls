@isTEST
private class CaseEquipment_Test{

    private static testmethod void testTriggerForCase(){
        Account a = new Account();
            a.Name = 'Test Account';
  //          a.Programming__c = 'Starter';
  //          a.phone = '(303) 555-5555';
        insert a;
        
        Opportunity o = new Opportunity();
            o.Name = 'Test';
            o.AccountId = a.Id;
    //        o.BTVNA_Request_Type__c = 'Site Survey';
    //        o.LeadSource='BTVNA Portal';
    //        o.Property_Type__c='Private';
    //        o.misc_code_siu__c='N999';
            o.CloseDate=system.Today();
            o.StageName='Closed Won';
    //        o.Restricted_Programming__c = 'True';
        insert o;
       
       Equipment__c e = new Equipment__c();
           e.Name = 'R1234567890';
           e.Statis__c = 'Add Requested';
           e.Opportunity__c = o.id;
      insert e;
    
      Case c = new Case();
           c.Opportunity__c = o.id;
           c.AccountId = a.id;
           c.Status = 'Request Completed';
           c.Origin = 'PRM';
//           c.RequestedAction__c = 'Activation';
           c.RequestedAction__c = 'General Inquiry';
           c.Description = 'Test';
           c.RecordTypeId = '012600000001EeC';
//           c.RecordTypeId = '0126000000017LQ';
//           c.Number_of_Accounts__c = 2;
//           c.CSG_Account_Number__c = '82551234567890';
           c.Requested_Actvation_Date_Time__c = system.Today();
     insert c;
     update c;
     }
}