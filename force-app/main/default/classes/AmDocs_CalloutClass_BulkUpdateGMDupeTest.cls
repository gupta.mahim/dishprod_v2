@IsTest
public class AmDocs_CalloutClass_BulkUpdateGMDupeTest {
    //Implement mock callout tests here
    
    @testSetup static void testSetupdata(){
        
        AmDocs_Login__c aml = TestDataFactoryForGuideMe.createAmDocsLogin();
        Account acc = TestDataFactoryForGuideMe.createAccount();
        Opportunity opp = TestDataFactoryForGuideMe.createOpportunity(acc.Id);
        Tenant_Account__c ta = TestDataFactoryForGuideMe.createTenantAccount(opp.Id);
        Tenant_Equipment__c te = TestDataFactoryForGuideMe.createTenantEquipment(opp.Id, ta.Id);
        Equipment__c et = TestDataFactoryForGuideMe.createHeadEndEquipment(opp.Id);
        Smartbox__c sb = TestDataFactoryForGuideMe.createSmartBoxEquipment(opp.Id);
    }
    
    @isTest
    static void testHESBDisconnect1()
    {
        List<Opportunity> opp=[Select Id,AmDocs_ServiceID__c from Opportunity where Name='Test Opp 123456789 Testing opp1'];
        
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete',
        	'{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
			null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        AmDocs_CalloutClass_BulkUpdateGMDupeRmv.TellAmdocstoRemoveThisEquipmentFromServiceID(opp[0].AmDocs_ServiceID__c);
        Test.stopTest();    
    }
    
    @isTest
    static void testHESBDisconnect2()
    {
        List<Opportunity> opp=[Select Id,AmDocs_ServiceID__c from Opportunity where Name='Test Opp 123456789 Testing opp1'];
        
        Test.startTest();        
        SingleRequestMock fakeResponse = new SingleRequestMock(100, 'Complete',
        	'{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037993","responseStatus":"Error"}}',
			null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        AmDocs_CalloutClass_BulkUpdateGMDupeRmv.TellAmdocstoRemoveThisEquipmentFromServiceID(opp[0].AmDocs_ServiceID__c);
        Test.stopTest();    
    }
    
}