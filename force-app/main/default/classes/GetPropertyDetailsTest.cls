@isTest
public class GetPropertyDetailsTest {
    
    public static testMethod void testActiveEquip(){
        
        Opportunity o = New Opportunity();
        o.Name = 'Test';
        o.Property_Type__c='MDU Property (1300)';
        o.StageName='Deal Re-Work';
        o.Property_Status__c ='Inactive';
        o.Phased_Construction__c = 'No';
        o.CloseDate = system.today();
        o.Address__c='18F Enginners and Architect';
        o.city__c='Lahore';
        o.State__c='MI';
        o.Zip__c='48001';
        
        insert o;
        
        Tenant_Account__c TA = New Tenant_Account__c();
        TA.Address__c='18F Enginners and Architect';
        TA.city__c='Lahore';
        TA.Opportunity__c = o.id;
        TA.State__c='MI';
        TA.Zip__c='48001';
        
        insert TA; 
        
        List <Tenant_Equipment__c> myEq = new List <Tenant_Equipment__c>();
        for (integer i=1; i<=10; i++){
            Tenant_Equipment__c anEquip=new Tenant_Equipment__c();   
            anEquip.Name='Test';
            //anEquip.Receiver_S__c='ATest';
            //anEquip.Receiver_Model__c='311';
            anEquip.Opportunity__c = o.Id;
            myEq.add(anEquip);
        }
        insert myEq;
        Set<Id> myEqIds = (new Map<Id,Tenant_Equipment__c>(myEq)).keySet();
        
        List<Id> EquipId=new List<Id>();
        EquipId.addAll(myEqIds);
        String allstring = string.join(EquipId,',');
        List<GetPropertyDetails.EquipData> testEquipData = GetPropertyDetails.getActiveEquips(Equipid,o.Id, ta.Id);
        
        
    }
}