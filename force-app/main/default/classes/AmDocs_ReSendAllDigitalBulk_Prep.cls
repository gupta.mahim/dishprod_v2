public class AmDocs_ReSendAllDigitalBulk_Prep {

    @future(callout=true)

    public static void AmDocsMakeReSendAll(String Id) {
    
    list<Opportunity> O = [select id, Name, AmDocs_ServiceID__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Reset__c, Category__c, AmDocs_Site_Id__c from Opportunity where Id = :id Limit 1];
        System.debug('DEBUG LOG - RESULTS of the LIST lookup to the Opp object' +O);

    list<Tenant_Equipment__c> TE = [select id, Name, Amdocs_Tenant_ID__c, Action__c, Mass_Trip_Requested__c, Tenant_Account__c from Tenant_Equipment__c where Opportunity__c = :id AND (Tenant_Account__c = '' OR Tenant_Account__c = NULL ) AND Amdocs_Tenant_ID__c != '' AND Amdocs_Tenant_ID__c != NULL AND Action__c = 'Active' AND Mass_Trip_Requested__c = FALSE Limit 4500];
//        list<Tenant_Equipment__c> TE = [select id, Name, Amdocs_Tenant_ID__c, Action__c, Mass_Trip_Requested__c, Tenant_Account__c from Tenant_Equipment__c where Opportunity__c = :id AND Action__c = 'Active' AND Tenant_Account__c = Null AND Mass_Trip_Requested__c = FALSE Limit 4500];
        System.debug('DEBUG LOG - RESULTS of the LIST lookup to the Tenant Equipment object' +TE);

 if(O[0].AmDocs_Site_Id__c == '' || O[0].AmDocs_Site_Id__c == Null ){
  
      Opportunity sbc = new Opportunity(); {
          sbc.id=id;
          sbc.API_Status__c='Error';
          sbc.Amdocs_Transaction_Code__c='Error';
          sbc.Amdocs_Transaction_Description__c = 'Action requies a Site ID. You are missing: Site ID.';
      update sbc;
      }
      
      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='Opportunity';
          apil2.Status__c='ERROR';
          apil2.Results__c='Action requies a Site ID. You are missing: Site ID.';
          apil2.API__c='Trip / Resend ALL Digital BULK';
          apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
          apil2.User__c=UserInfo.getUsername();
          apil2.User_Error__c=TRUE;
      insert apil2;
      }
  }
  
 if(TE.size() < 1){

       Opportunity sbc = new Opportunity(); {
           sbc.id=id; sbc.API_Status__c='Error'; 
           sbc.Amdocs_Transaction_Code__c='Error'; 
           sbc.Amdocs_Transaction_Description__c='No eligible Tenant Equipment found for your Resend/Trip All Digital Bulk request.'; 
            update sbc;
       }
       API_Log__c apil2 = new API_Log__c();{ 
           apil2.Record__c=id; 
           apil2.Object__c='Opportunity';
           apil2.Status__c='ERROR'; 
           apil2.Results__c='No eligible Tenant Equipment found for your Resend/Trip All Digital Bulk request.'; 
           apil2.API__c='Trip / Resend ALL Digital Bulk'; 
           apil2.ServiceID__c=O[0].AmDocs_ServiceID__c; 
           apil2.User__c=UserInfo.getUsername();
           apil2.User_Error__c=TRUE;           
           insert apil2; 
     }
   }   

   else if(TE.size() > 0) {    

    list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
        System.debug('RESULTS of the LIST lookup to the Amdocs Login object' +A);  
     
     Opportunity sbc = new Opportunity(); {
         sbc.id=id; 
         sbc.AmDocs_Transaction_Description__c='Request Successful: Resend All / Trip All Digital Bulk. All Digital Bulk Equipment has been queued for a trip.'; 
         sbc.AmDocs_Transaction_Code__c='SUCCESS'; 
     update sbc;
         System.debug('sbc'+sbc); 
     }

    List<Tenant_Equipment__c> TEList = new List<Tenant_Equipment__c>();
    
    for(Integer i = 0; i < TE.size(); i++){
        Tenant_Equipment__c teu = new Tenant_Equipment__c();
            teu.id=TE[i].id;
            teu.Mass_Trip_Requested__c = TRUE;
         TEList.add(teu);
             System.debug('DEBUG List of Updated Tenant Equipment === TEList: '   + TEList);
   }
   update TEList;
   }
  }
}