/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 22 July 2020
Created for: Milestone# MS-003294
Description: Test class for guideMeAccountAccessCheck.apxc
-------------------------------------------------------------*/

@IsTest
public class guideMeAccountAccessCheckTest {
    public static testMethod void testMethod1() {
        List<guideMeAccountAccessCheck.FlowInput> ListOfFlowInputs = new List<guideMeAccountAccessCheck.FlowInput>();
        guideMeAccountAccessCheck.FlowInput FlowInpObj = new guideMeAccountAccessCheck.FlowInput();
        FlowInpObj.objectName = 'Test';
        FlowInpObj.fieldName = 'Test';
        ListOfFlowInputs.add(FlowInpObj);
        
        List<guideMeAccountAccessCheck.Flowoutput> checkAccess = guideMeAccountAccessCheck.guideMeUserAccessForAccount(ListOfFlowInputs);
        system.assertEquals(checkAccess[0].userHaveAccess , false);
    }
}