public class Equipment_Edit_lex{
    
    public Equipment__c[] equip = new Equipment__c[0];
    Id oppId=null;
    public Equipment_Edit_lex(ApexPages.StandardController controller) {
        oppId=controller.getRecord().Id;
    }
    
    public PageReference onsave() {
     update equip;
     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.info,'Records saved successfully.');
            ApexPages.addMessage(errormsg);
     return null;
    }
    
    
public PageReference onclose() {
     return null;
    } 
    
    public PageReference onadd() {
     return new PageReference('/apex/equipment_add_smatv?Id=' + ApexPages.currentPage().getParameters().get('Id') + '&st=' + + ApexPages.currentPage().getParameters().get('st'));
    } 


  public Equipment__c[] getEquip() {
               
       equip = [SELECT ID, Name, Remove_Equipment__c, Receiver_S__c, Receiver_Model__c, Channel__c, Programming__c,  Statis__c FROM Equipment__c WHERE Opportunity__c = :oppId/*ApexPages.currentPage().getParameters().get('Id')*/ ORDER BY Equipment__c.Name ];

        return equip;
        
    }

    
}