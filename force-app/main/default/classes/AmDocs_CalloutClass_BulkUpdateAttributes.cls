public class AmDocs_CalloutClass_BulkUpdateAttributes {

public class person {
    public String serviceID;
    public String ownerServiceId;
    public String orderID;
    public String responseStatus;
    public cls_attributeList[] attributeList;
    public cls_informationMessages[] informationMessages;
}

class cls_attributeList {
    public String catalogCode;
    public String selectedValue;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}


    @future(callout=true)

    public static void AmDocsMakeCalloutBulkUpdateAttributes(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

 list<Opportunity> O = [select id, AmDocs_ServiceID__c, Name, Number_of_Units__c, NLOS_Locals__c, No_Sports__c, AmDocs_Site_Id__c from Opportunity where Id = :id Limit 1];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);

list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
    jsonObj.writeFieldName('ImplUpdateBulkRestInput');

        jsonObj.writeStartObject();
        jsonObj.writeStringField('reasonCode', 'CREQ');
        jsonObj.writeStringField('orderActionType', 'CH');
        
        jsonObj.writeFieldName('attributeList');
 
 String CC = 'catalogCode"';
 String SC = ' : ';
 String RL = '"RLC_Number of units",';
 String SV = '"selectedValue';
 String NL = '"NLOS Locals",';
 String NS = '"No_Sports",';
     
 String RL2 = CC + SC + RL + SV;
//      System.debug('RESULTS of the RL2 String ' +RL2 );
 String NL2 = CC + SC + NL + SV;
 String NS2 = CC + SC + NS + SV;
     

 
        jsonObj.writeStartArray();
            for(Integer i = 0; i < O.size(); i++){
                jsonObj.writeStartObject();
//                    jsonObj.writeNumberField('catalogCode' + ':' + 'RLC_Number of units' + ':' + 'selectedValue', O[i].Number_of_Units__c);
//                    jsonObj.writeNumberField(CC + SC + RL + SC + SV, O[i].Number_of_Units__c);
                      if ( O[i].Number_of_Units__c != null ) {
                          jsonObj.writeNumberField(RL2, O[i].Number_of_Units__c);
                      }
//                    jsonObj.writeBoolenField('catalogCode', 'NLOS Locals', 'selectedValue', O[i].NLOS_Locals__c);
                      if ( O[i].NLOS_Locals__c == 'Yes' ) { jsonObj.writeStringField(NL2, 'Yes'); }
                     if ( O[i].NLOS_Locals__c == 'No' ) { jsonObj.writeStringField(NL2, 'No'); }
//                    jsonObj.writeBooleanField('catalogCode', 'No Sports', 'selectedValue', O[i].No_Sports__c);
                      if ( O[i].NLOS_Locals__c == 'Yes' ) { jsonObj.writeStringField(NS2, 'Yes'); }
                     if ( O[i].NLOS_Locals__c == 'No' ) { jsonObj.writeStringField(NS2, 'No'); }
                jsonObj.writeEndObject();
            }
                
        jsonObj.writeEndArray();
        jsonObj.writeEndObject();

    jsonObj.writeEndObject();
    
 String finalJSON = jsonObj.getAsString();
//    System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
    
 String finalJsonStr = finalJSON.replace('\\"', '"');
//    System.debug('DEBUG Full Request finalJSONstr : ' +(finalJsonStr));

   
if (!Test.isRunningTest()){ HttpRequest request = new HttpRequest(); String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/updateBulk?sc=SS&lo=EN&ca=SF'; request.setEndPoint(endpoint); request.setBody(finalJsonSTR); request.setHeader('Content-Type', 'application/json'); request.setMethod('POST'); String authorizationHeader = A[0].UXF_Token__c; request.setHeader('Authorization', authorizationHeader); HttpResponse response = new HTTP().send(request); if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { String strjson = response.getbody(); JSONParser parser = JSON.createParser(strjson); parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class);
        System.debug('DEBUG Full Body : ' +request.getBody());                 
        System.debug(response.toString());
        System.debug('STATUS:'+response.getStatus());
        System.debug('STATUS_CODE:'+response.getStatusCode());
        System.debug(response.getBody());
        System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
        System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
        System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
        System.debug('DEBUG 3 ======== obj.attributeList: ' + obj.attributeList);
        System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
        System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
        
        Opportunity sbc = new Opportunity(); sbc.id=id; sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); sbc.AmDocs_Order_ID__c=obj.orderID; sbc.API_Status__c=String.valueOf(response.getStatusCode()); if(obj.informationMessages != Null) { sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription; sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; } else if(obj.informationMessages == Null) { sbc.AmDocs_Transaction_Description__c=obj.responseStatus; sbc.AmDocs_Transaction_Code__c=obj.responseStatus; } else if(obj.responseStatus == Null) { sbc.AmDocs_Transaction_Description__c='Request Rejected'; sbc.AmDocs_Transaction_Code__c='ERROR'; } update sbc;
	        System.debug('sbc'+sbc);
        }
        else if (response.getStatusCode() <= 200 && response.getStatusCode() >= 600) { Opportunity sbc = new Opportunity(); sbc.id=id; sbc.API_Status__c=String.valueOf(response.getStatusCode()); update sbc; }
        }
    }
}