@isTest
private class AmDocs_CreateCustomer_Test {
  //Implement mock callout tests here
  
   @testSetup static void testSetupdata(){

    // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.LeadSource='Test Place';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AccountId = acct1.Id;
        insert opp1;
        
//        acct1.Amdocs_CreateCustomer__c = true;
//        update acct1;

   // Create Account 2
        Account acct2 = new Account();
            acct2.Name = 'Test Account 2';
            acct2.ToP__c = 'PCO';
            acct2.Pyscical_Address__c = '1011 Collie Path';
            acct2.City__c = 'Round Rock';
            acct2.State__c = 'TX';
            acct2.Zip__c = '78664';
            acct2.OE_AR__c = '12345';
            acct2.Amdocs_CreateCustomer__c = false;
        insert acct2;

        Opportunity opp2 = New Opportunity();
            opp2.Name = 'Test Opp 2';
            opp2.First_Name_of_Property_Representative__c = 'Jerry';
            opp2.Name_of_Property_Representative__c = 'Clifft';
            opp2.Business_Address__c = '1011 Collie Path';
            opp2.Business_City__c = 'Round Rock';
            opp2.Business_State__c = 'TX';
            opp2.Business_Zip_Code__c = '78664';
            opp2.Billing_Contact_Phone__c = '512-383-5201';
            opp2.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp2.LeadSource='Test Place';
            opp2.CloseDate=system.Today();
            opp2.StageName='Closed Won';
            opp2.Smartbox_Leased__c = false;
            opp2.AmDocs_ServiceID__c = '1111111111';
            opp2.AccountId = acct2.Id;
        insert opp2;
        
   // Create Account 3
        Account acct3 = new Account();
            acct3.Name = 'Test Account 3';
            acct3.ToP__c = 'Direct';
            acct3.Pyscical_Address__c = '1011 Collie Path';
            acct3.City__c = 'Round Rock';
            acct3.State__c = 'TX';
            acct3.Zip__c = '78664';
            acct3.Amdocs_CreateCustomer__c = false;
            acct3.Distributor__c = 'SMS';
            acct3.OE_AR__c = '12345';
        insert acct3;

        Opportunity opp3 = New Opportunity();
            opp3.Name = 'Test Opp 3';
            opp3.First_Name_of_Property_Representative__c = 'Jerry';
            opp3.Name_of_Property_Representative__c = 'Clifft';
            opp3.Business_Address__c = '1011 Collie Path';
            opp3.Business_City__c = 'Round Rock';
            opp3.Business_State__c = 'TX';
            opp3.Business_Zip_Code__c = '78664';
            opp3.Billing_Contact_Phone__c = '512-383-5201';
            opp3.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp3.LeadSource='Test Place';
            opp3.CloseDate=system.Today();
            opp3.StageName='Closed Won';
            opp3.Smartbox_Leased__c = false;
            opp3.AmDocs_ServiceID__c = '1111111111';
            opp3.AccountId = acct3.Id;
        insert opp3;


   // Create Account 4
        Account acct4 = new Account();
            acct4.Name = 'Test Account 4';
            acct4.ToP__c = 'Retailer';
            acct4.Pyscical_Address__c = '1011 Collie Path';
            acct4.City__c = 'Round Rock';
            acct4.State__c = 'TX';
            acct4.Zip__c = '78664';
            acct4.Amdocs_CreateCustomer__c = false;
            acct4.Distributor__c = '4COM';
            acct4.OE_AR__c = '12345';
        insert acct4;

        Opportunity opp4 = New Opportunity();
            opp4.Name = 'Test Opp 4';
            opp4.First_Name_of_Property_Representative__c = 'Jerry';
            opp4.Name_of_Property_Representative__c = 'Clifft';
            opp4.Business_Address__c = '1011 Collie Path';
            opp4.Business_City__c = 'Round Rock';
            opp4.Business_State__c = 'TX';
            opp4.Business_Zip_Code__c = '78664';
            opp4.Billing_Contact_Phone__c = '512-383-5201';
            opp4.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp4.LeadSource='Test Place';
            opp4.CloseDate=system.Today();
            opp4.StageName='Closed Won';
            opp4.Smartbox_Leased__c = false;
            opp4.AmDocs_ServiceID__c = '1111111111';
            opp4.AccountId = acct4.Id;
        insert opp4;
    }
  
  
  @isTest
  static void Acct1(){
    Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 1' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp); System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
        String sendThisID = opp.Id;
        Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());       // Set mock callout class
        Test.startTest();    // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
            AmDocs_CalloutClass_CreateCustomer.AmDocsMakeCalloutCreateCustomer(sendThisID);
        Test.stopTest();    
      
        opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Opportunity where id =: opp.Id];       // Verify that the response received contains fake values        
            // System.assertEquals('0123456789',opp.AmDocs_CustomerID__c); System.assertEquals(null,opp.AmDocs_CustomerID__c); System.assertEquals('0123456789',opp.AmDocs_FAID__c); System.assertEquals('0123456789',opp.AmDocs_BarID__c);     
  }
    @isTest
  static void Acct2(){
      Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 2' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp);  
        System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
  
        String sendThisID = opp.Id;
        System.Debug('DEBUG 2 sendThisID : ' +sendThisID);
  
      // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
      // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClass_CreateCustomer.AmDocsMakeCalloutCreateCustomer(sendThisID);
      Test.stopTest();    
  
          // Verify that the response received contains fake values        
      opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Opportunity where id =: opp.Id];
          // System.assertEquals('0123456789',opp.AmDocs_CustomerID__c); System.assertEquals(null,opp.AmDocs_CustomerID__c); System.assertEquals('0123456789',opp.AmDocs_FAID__c); System.assertEquals('0123456789',opp.AmDocs_BarID__c);       
  }
  
  @isTest
  static void Acct3(){
      Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 3' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp);  
        System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
  
        String sendThisID = opp.Id;
        System.Debug('DEBUG 2 sendThisID : ' +sendThisID);
  
          // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
          // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClass_CreateCustomer.AmDocsMakeCalloutCreateCustomer(sendThisID);
      Test.stopTest();    
  
          // Verify that the response received contains fake values        
      opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Opportunity where id =: opp.Id];
        // System.assertEquals('0123456789',opp.AmDocs_CustomerID__c); System.assertEquals(null,opp.AmDocs_CustomerID__c); System.assertEquals('0123456789',opp.AmDocs_FAID__c); System.assertEquals('0123456789',opp.AmDocs_BarID__c);     
  }  
   
  @isTest
  static void Acct4(){
      Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 4' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp);  
        System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
  
        String sendThisID = opp.Id;
        System.Debug('DEBUG 2 sendThisID : ' +sendThisID);
  
          // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
          // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClass_CreateCustomer.AmDocsMakeCalloutCreateCustomer(sendThisID);
      Test.stopTest();    
  
          // Verify that the response received contains fake values        
      opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Opportunity where id =: opp.Id];
        // System.assertEquals('0123456789',opp.AmDocs_CustomerID__c); System.assertEquals(null,opp.AmDocs_CustomerID__c); System.assertEquals('0123456789',opp.AmDocs_FAID__c); System.assertEquals('0123456789',opp.AmDocs_BarID__c);     
  }  
}