@isTest
//test class for Fiber_Building_Detail__c and Fiber_IDF_Detail__c Triggers
//calls SIMP_TestDataFactory
//Christian Comia - 20190719 - initial version 
private class SIMP_IDFANDBuildingTestClass {
    
    //TEST METHODS FOR INSERT START
    @isTest static void createIDFBLDG(){
        SIMP_TestDataFactory.createQuote(1,'BLDG','',1);
        SIMP_TestDataFactory.createQuote(1,'IDF','',1);
    }
    @isTest static void createBLDGChangeQuoteNametoQ9999(){
        SIMP_TestDataFactory.createQuote(1,'BLDG','Q-99999',0);       
    }
    @isTest static void createIDFChangeQuoteNametoQ9999(){
        SIMP_TestDataFactory.createQuote(1,'IDF','Q-99999',0);
    }
    @isTest static void createBLDGChangeQuoteNametoPatternOnly(){
        SIMP_TestDataFactory.createQuote(1,'BLDG','-BLDG#',0);
    }
    @isTest static void createIDFChangeQuoteNametoPatternOnly(){
        SIMP_TestDataFactory.createQuote(1,'IDF','-IDF#',0);
    }
    @isTest static void createBLDGChangeQuoteNametoPatternOnlywNum(){
        SIMP_TestDataFactory.createQuote(1,'BLDG','Q-99999-BLDG#',0); 
    }
    @isTest static void createIDFChangeQuoteNametoPatternOnlywNum(){
        SIMP_TestDataFactory.createQuote(1,'IDF','Q-99999-IDF#',0);
    }
    @isTest static void createBLDGChangeQuoteNametoFROMCLONE(){
        SIMP_TestDataFactory.createQuote(1,'BLDG','Q-99999FROMCLONE',0);
    }
    @isTest static void createIDFChangeQuoteNametoFROMCLONE(){
        SIMP_TestDataFactory.createQuote(1,'IDF','Q-99999FROMCLONE',0);
    }
    @isTest static void createBLDGChangeQuoteNametodummy(){
        SIMP_TestDataFactory.createQuote(3,'BLDG','ChangeQuoteName1',0);
    }
    @isTest static void createIDFChangeQuoteNametodummy(){
        SIMP_TestDataFactory.createQuote(3,'IDF','ChangeQuoteName1',0);
    }
    @isTest static void createBLDGChangeQuoteNametodummyClone(){
        SIMP_TestDataFactory.createQuote(1,'BLDG','',1);
    }
    @isTest static void createIDFChangeQuoteNametodummyClone(){
        SIMP_TestDataFactory.createQuote(1,'IDF','',1);
    }
    @isTest static void createBLDGMult(){
        SIMP_TestDataFactory.createQuote(3,'BLDG','',1);
        SIMP_TestDataFactory.createQuote(3,'IDF','',1);
    }
    @isTest static void createBLDGChangeQuoteName(){
        SIMP_TestDataFactory.createQuote(1,'BLDG','ChangeQuoteName',0);
    }
    @isTest static void createIDFChangeQuoteName(){
        SIMP_TestDataFactory.createQuote(1,'IDF','ChangeQuoteName',0);
    }
    //TEST METHODS FOR INSERT END
    
    //TEST METHODS FOR UPDATE START
    @isTest static void updateIDFBLDG(){
        SIMP_TestDataFactory.updateQuote(1,'BLDG','',0);
        SIMP_TestDataFactory.updateQuote(1,'IDF','',0);
    }
    @isTest static void updateBLDGChangeQuoteName(){
        SIMP_TestDataFactory.updateQuote(1,'BLDG','-BLDG#',0);
    }
    @isTest static void updateIDFChangeQuoteName(){
        SIMP_TestDataFactory.updateQuote(1,'IDF','-IDF#',0);
    }    
    @isTest static void updateBLDGChangeQuoteNameWithNum(){
        SIMP_TestDataFactory.updateQuote(1,'BLDG','-BLDG#32',0);
    }
    @isTest static void updateIDFChangeQuoteNameWithNum(){
        SIMP_TestDataFactory.updateQuote(1,'IDF','-IDF#32',0);
    }    
    @isTest static void updateBLDGChangeQuoteNameWithNumFull(){
        SIMP_TestDataFactory.updateQuote(1,'BLDG','Q-999999-BLDG#',0);
    }
    @isTest static void updateIDFChangeQuoteNameWithNumFull(){
        SIMP_TestDataFactory.updateQuote(1,'IDF','Q-999999-IDF#',0);
    }    
    @isTest static void updateBLDGChangeQuoteNameWithNum0(){
        SIMP_TestDataFactory.updateQuote(1,'BLDG','BLDG#0',0);
    }
    @isTest static void updateIDFChangeQuoteNameWithNum0(){
        SIMP_TestDataFactory.updateQuote(1,'IDF','IDF#0',0);
    }   
    @isTest static void updateBLDGChangeQuoteNameWithNum2(){
        SIMP_TestDataFactory.updateQuote(1,'BLDG','BLDG#2',0);
    }
    @isTest static void updateIDFChangeQuoteNameWithNum2(){
        SIMP_TestDataFactory.updateQuote(1,'IDF','IDF#2',0);
    }   
    @isTest static void updateBLDGChangeQuoteNameWithQ99999(){
        SIMP_TestDataFactory.updateQuote(1,'BLDG','Q-99999',0);
    }
    @isTest static void updateIDFChangeQuoteNameWithQ99999(){
        SIMP_TestDataFactory.updateQuote(1,'IDF','Q-99999',0);
    }   
    @isTest static void updateBLDGChangeQuoteNameWithless(){
        SIMP_TestDataFactory.updateQuote(1,'BLDG','less',0);
    }
    @isTest static void updateIDFChangeQuoteNameWithless(){
        SIMP_TestDataFactory.updateQuote(1,'IDF','less',0);
    }
    @isTest static void updateBLDGChangeQuoteNameWithmore(){
        SIMP_TestDataFactory.updateQuote(1,'BLDG','more',0);
    }
    @isTest static void updateIDFChangeQuoteNameWithmore(){
        SIMP_TestDataFactory.updateQuote(1,'IDF','more',0);
    }
     @isTest static void updateBLDGChangeQuoteNameWithnotmatch(){
        SIMP_TestDataFactory.updateQuote(1,'BLDG','nomatch',0);
    }
    @isTest static void updateIDFChangeQuoteNameWithnotmatch(){
        SIMP_TestDataFactory.updateQuote(1,'IDF','nomatch',0);
    }   
    //TEST METHODS FOR UPDATE END  
}