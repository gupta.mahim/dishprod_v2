/*
* Author: M Fazal
* Description: 
* Invokes the Amdocs Callout class for Move Receiver for Head-End Equipment and Tenant Equipment
* If API is UNSUCCESSFUL on Target Service ID:
*    Clear the new 'Move Equipment To' field
*    Update Transaction Status and Transaction Description
*    Append value from new field
*    example: Move Equipment to Service ID (New Field) FAILED + JSON Error Message
* If API request = successful:
*    Mark existing Record as 'Removed'
*    Create New Record on Target Service ID, UNLESS the receiver is already on the new Service ID
*    If create: match the address and user information
*    Update status to match JSON response on Target Service ID
* 
*/
public class AmdocsMoveReceiverCtrl
{
    @future(callout=true)
    public static void doTEMoveReceiver(Id currentEquipId)
    {
        Tenant_Equipment__c currentEquip = [select id,Move_Receiver_To__c, Smart_Card__c, Name, Address__c
        , City__c, State__c, Zip_Code__c, Unit__c, Phone_Number__c, Email_Address__c, Customer_First_Name__c
        , Customer_Last_Name__c, Location__c,Tenant_Account__c, Action__c,Opportunity__r.AmDocs_ServiceID__c,Opportunity__r.AmDocs_tenantType__c, Opportunity__c from Tenant_Equipment__c where Id=:currentEquipId];
        
        if(currentEquip.Action__c=='Active')
        {
            String sourceTA=currentEquip.Tenant_Account__c;
            String targetTA='';
            
            //Lookup the target Tenant Account record based on Move Equip To service Id
            List<Tenant_Account__c> targetTAs=[select id,Opportunity__c,Email_Address__c,Phone_Number__c,Address__c, City__c, First_Name__c, Last_Name__c, State__c, Unit__c, Zip__c from Tenant_Account__c where AmDocs_ServiceID__c=:currentEquip.Move_Receiver_To__c];
            List<Opportunity> oppList=[select id,Address__c,City__c,State__c,Zip__c,Phone__c,Contact_Email__c,Contact_Name__c from Opportunity where AmDocs_ServiceID__c= :currentEquip.Move_Receiver_To__c];
            List<Tenant_Equipment__c> teList = [select id,Smart_Card__c, Name,Action__c from Tenant_Equipment__c where Amdocs_Tenant_ID__c= :currentEquip.Move_Receiver_To__c];
            
                        
            if(targetTAs.size()>0){targetTA=targetTAs[0].id;}
            
            // Error when no Opportunity and Tenant Account is found against provided Move Equip To service Id
            if(String.isEmpty(targetTA) && (oppList==null || oppList.size()==0) && (teList==null || teList.size()==0)){logAPIResult(currentEquip,'Error','Target service id record not found.','TE');}
            else
            {
                AmDocs_CalloutClass_MoveRvr.MoveReceiverResp response=AmDocs_CalloutClass_MoveRvr.doTEMove(currentEquip); 
                if(response!=null)
                {
                    String responseStatus=response.ImplMoveReceiverRestOutput.responseStatus;
                    
                    if(responseStatus.equalsIgnoreCase('Error')){AmDocs_CalloutClass_MoveRvr.cls_informationMessages infoMessage=response.ImplMoveReceiverRestOutput.informationMessages[0];logAPIResult(currentEquip,responseStatus,infoMessage.errorDescription,'TE');createAPILog(currentEquip.id,currentEquip.Move_Receiver_To__c,'ERROR',response.tostring(),'Tenant Equipment');}
                    else if(responseStatus.equalsIgnoreCase('Success')){
                        If(!String.isEmpty(targetTA)){createTE(currentEquip.Name,currentEquip.Smart_Card__c,targetTAs[0]);}else if(oppList!=null && oppList.size()>0){createTEFromOppty(currentEquip.Name,currentEquip.Smart_Card__c,oppList[0]);}else{createTEfromTE(currentEquip.Name,currentEquip.Smart_Card__c,teList[0]);}
                        currentEquip.Action__c='Removed';
                        logAPIResult(currentEquip,'Success','Receiver moved successfully.','TE');
                        createAPILog(currentEquip.id,currentEquip.Move_Receiver_To__c,'SUCCESS',response.tostring(),'Tenant Equipment');
                    }
                }
           }
        }
        else{logAPIResult(currentEquip,'Error','Remove Receiver is allowed only an Active equipment.','TE');}
    }
    @future(callout=true)
    public static void doHEMoveReceiver(Id currentEquipId)
    {
        Equipment__c currentEquip = [select id, Receiver_S__c, Name,Opportunity__r.AmDocs_ServiceID__c, Move_Receiver_To__c, Location__c, Action__c, Opportunity__c from Equipment__c where Id= :currentEquipId  AND Location__c = 'C'];
        
        if(currentEquip.Action__c=='Active')
        {
            String targetTA='';
            
            //Lookup the target Tenant Account record based on Move Equip To service Id
            List<Tenant_Account__c> targetTAs=[select id,Opportunity__c,Email_Address__c,Phone_Number__c,Address__c, City__c, First_Name__c, Last_Name__c, State__c, Unit__c, Zip__c from Tenant_Account__c where AmDocs_ServiceID__c=:currentEquip.Move_Receiver_To__c];
            List<Opportunity> oppList=[select id,Address__c,City__c,State__c,Zip__c,Phone__c,Contact_Email__c,Contact_Name__c from Opportunity where AmDocs_ServiceID__c= :currentEquip.Move_Receiver_To__c];
            List<Tenant_Equipment__c> teList = [select id,Smart_Card__c, Name,Action__c from Tenant_Equipment__c where Amdocs_Tenant_ID__c= :currentEquip.Move_Receiver_To__c];
            
            if(targetTAs.size()>0){targetTA=targetTAs[0].id;}
            
            // Error when no Opportunity and Tenant Account is found against provided Move Equip To service Id
            if(String.isEmpty(targetTA) && (oppList==null || oppList.size()==0) && (teList==null || teList.size()==0)){logAPIResult(currentEquip,'Error','Target service id record not found.','HE');}
            else
            {
                AmDocs_CalloutClass_MoveRvr.MoveReceiverResp response=AmDocs_CalloutClass_MoveRvr.doHEMove(currentEquip); 
                if(response!=null)
                {
                    String responseStatus=response.ImplMoveReceiverRestOutput.responseStatus;
                    
                    if(responseStatus.equalsIgnoreCase('Error')){AmDocs_CalloutClass_MoveRvr.cls_informationMessages infoMessage=response.ImplMoveReceiverRestOutput.informationMessages[0];logAPIResult(currentEquip,responseStatus,infoMessage.errorDescription,'HE');createAPILog(currentEquip.id,currentEquip.Move_Receiver_To__c,'ERROR',response.tostring(),'Headend Equipment');}
                    else if(responseStatus.equalsIgnoreCase('Success'))
                    {
                        If(!String.isEmpty(targetTA)){createTE(currentEquip.Name,currentEquip.Receiver_S__c,targetTAs[0]);}else if(oppList!=null && oppList.size()>0){createTEFromOppty(currentEquip.Name,currentEquip.Receiver_S__c,oppList[0]);}else{createTEfromTE(currentEquip.Name,currentEquip.Receiver_S__c,teList[0]);}
                        //createTE(currentEquip.Name,currentEquip.Receiver_S__c,targetTAs[0]);
                        
                        currentEquip.Action__c='Removed';
                        logAPIResult(currentEquip,'Success','Receiver moved successfully.','HE');
                        createAPILog(currentEquip.id,currentEquip.Move_Receiver_To__c,'SUCCESS',response.tostring(),'Headend Equipment');
                    }
                }
            }
        }
        else{logAPIResult(currentEquip,'Error','Remove Receiver is allowed only an Active equipment.','HE');}
    }
    private static void createTE(String receiverName,String smartCard,Tenant_Account__c ta)
    {
        List<Tenant_Equipment__c> targetExistingTE=[select Id, Action__c from Tenant_Equipment__c where Tenant_Account__c=:ta.id and Name=:receiverName];
        if(targetExistingTE.size()>0){Tenant_Equipment__c existingTE=targetExistingTE[0];if(!existingTE.Action__c.equalsIgnoreCase('Active')){existingTE.Action__c='Active';update existingTE;}}
        else
        {
            Tenant_Equipment__c newEquip=new Tenant_Equipment__c
            (
                Action__c='Active'
               ,Smart_Card__c=smartCard
               ,Name=receiverName
               ,Address__c=ta.Address__c
               ,City__c=ta.City__c
               ,State__c=ta.State__c
               ,Zip_Code__c=ta.Zip__c
               ,Unit__c=ta.Unit__c
               ,Phone_Number__c=ta.Phone_Number__c
               ,Email_Address__c=ta.Email_Address__c
               ,Customer_First_Name__c=ta.First_Name__c
               ,Customer_Last_Name__c=ta.Last_Name__c
               ,Tenant_Account__c=ta.id
               ,Opportunity__c=ta.Opportunity__c
               
            );    
            insert newEquip;
        }
    }
    public static void createTEfromTE(String receiverName,String smartCard,Tenant_Equipment__c te)
    {
        List<Tenant_Equipment__c> targetExistingTE=[select Id, Action__c from Tenant_Equipment__c where Opportunity__c=:te.Opportunity__c and Name=:receiverName];
        if(targetExistingTE.size()>0){Tenant_Equipment__c existingTE=targetExistingTE[0];if(!existingTE.Action__c.equalsIgnoreCase('Active')){existingTE.Action__c='Active';update existingTE;}}
        else
        {
            Tenant_Equipment__c newEquip=new Tenant_Equipment__c
            (
                Action__c='Active'
               ,Smart_Card__c=smartCard
               ,Name=receiverName
               ,Address__c=te.Address__c
               ,City__c=te.City__c
               ,State__c=te.State__c
               ,Zip_Code__c=te.Zip_Code__c
               ,Unit__c=te.Unit__c
               ,Phone_Number__c=te.Phone_Number__c
               ,Email_Address__c=te.Email_Address__c
               ,Customer_First_Name__c=te.Customer_First_Name__c
               ,Customer_Last_Name__c=te.Customer_Last_Name__c
               ,Opportunity__c=te.Opportunity__c
               
            );    
            insert newEquip;
        }
    }
    public static void createTEFromOppty(String receiverName,String smartCard,Opportunity opp)
    {
        List<Tenant_Equipment__c> targetExistingTE=[select Id, Action__c from Tenant_Equipment__c where Opportunity__c=:opp.id and Name=:receiverName];
        if(targetExistingTE.size()>0){Tenant_Equipment__c existingTE=targetExistingTE[0];if(!existingTE.Action__c.equalsIgnoreCase('Active')){existingTE.Action__c='Active';update existingTE;}}
        else
        {
            Tenant_Equipment__c newEquip=new Tenant_Equipment__c
            (
                Action__c='Active'
               ,Smart_Card__c=smartCard
               ,Name=receiverName
               ,Address__c=opp.Address__c
               ,City__c=opp.City__c
               ,State__c=opp.State__c
               ,Zip_Code__c=opp.Zip__c
               //,Unit__c=opp.Unit__c
               ,Phone_Number__c=opp.Phone__c
               ,Email_Address__c=opp.Contact_Email__c
               //,Customer_First_Name__c=opp.First_Name__c
               ,Customer_Last_Name__c=opp.Contact_Name__c
               ,Opportunity__c=opp.id
               
            );    
            insert newEquip;
        }
    }
    public static void createAPILog(Id currentEquipId,String serviceId,String status,String response,String equipType)
    {
        API_Log__c apil2 = new API_Log__c();{
        apil2.Record__c=currentEquipId;
        apil2.Object__c=equipType;
        apil2.Status__c=status;
        apil2.Results__c=response;
        apil2.API__c='MoveReceiver';
        apil2.ServiceID__c=serviceId;
        apil2.User__c=UserInfo.getUsername();
        Insert apil2;
    }
    }
    private static void logAPIResult(sObject currentRec,String status, string message,String equipType)
    {
        if(equipType=='TE')
        {
            Tenant_Equipment__c currentEquip=(Tenant_Equipment__c)currentRec;
            currentEquip.API_Status__c=status; 
            currentEquip.Amdocs_Transaction_Code__c=status; 
            currentEquip.Amdocs_Transaction_Description__c=message;
            update currentEquip;
        }
        else if(equipType=='HE')
        {
            Equipment__c currentEquip=(Equipment__c)currentRec;
            currentEquip.API_Status__c=status; 
            currentEquip.Amdocs_Transaction_Code__c=status; 
            currentEquip.Amdocs_Transaction_Description__c=message;
            update currentEquip;
        }
    }
}