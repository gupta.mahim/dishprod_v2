@isTest(SeeAllData=true)
Public class multiAccountInsert_lex_Tests{
    public static testMethod void testmultiAccountInsert_lex(){

PageReference pageRef = Page.equipment_add_smatv_lex;
Test.setCurrentPageReference(pageRef);

 Opportunity newOpp = new Opportunity (name='XYZ Organization', StageName='Closed Won', CloseDate=Date.Today());
 insert newOpp;

 Equipment__c myEq = new Equipment__c (Name='Joe',
 Receiver_S__c='SJoe', Receiver_Model__c='311', Statis__c='Activation Requested', Programming__c='HBO', Opportunity__c= newOpp.id);
 insert myEq;

 Case mycas = new Case (Status='Form Submitted', Origin='PRM', Opportunity__c=newOpp.id);
 insert mycas;

ApexPages.currentPage().getParameters().put('oppId', newOpp.id);

ApexPages.currentPage().getParameters().put('Id', newOpp.id);
multiAccountInsert_lex controller = new multiAccountInsert_lex(new ApexPages.StandardController(newOpp));
multiAccountInsert_lex controller2 = new multiAccountInsert_lex();

controller.addrow();
controller.save();
controller.onclose();
}
}