/* MockResponse class implementing the HttpCalloutMock interface */


@isTest
global class AmDocs_CallOutMockResponses implements HttpCalloutMock {

   // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('http://api.salesforce.com/endpoint', request.getEndpoint());
        System.assertEquals('POST', request.getMethod());
  
       // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setHeader('Uxfauthorization', 'UXFToken');
        response.setBody('{"foo":"bar"}');
        response.setStatusCode(200);
        return response;
    }
}