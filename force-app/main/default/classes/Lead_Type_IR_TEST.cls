@IsTest
private class Lead_Type_IR_TEST {
    static testMethod void validateLeadCategorySelectionIR() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');
        
        insert L;
        
       L = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L.Id];
       System.debug('Category after trigger fired: ' + L.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('P/P', L.sundog_deprm2__Lead_Type__c);
    }
}