@IsTest
private class Lead_Type_FTG_TEST {

static testMethod void validateLeadSubTypeS_FTG2() {
        Lead LL= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c ='Chewbacca',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LL;
        
       LL= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LL.Id];
       System.debug('Category after trigger fired: ' + LL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LL.sundog_deprm2__Lead_Type__c);
}

static testMethod void validateLeadSubTypeS_FTG3() {
        Lead LLL= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hospitals',
        Location_of_Service__c ='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLL;
        
       LLL= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLL.Id];
       System.debug('Category after trigger fired: ' + LLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadSubTypeS_FTG4() {
        Lead LLLL= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c ='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLL;
        
       LLLL= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLL.Id];
       System.debug('Category after trigger fired: ' + LLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadSubTypeS_FTG7() {
        Lead LLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Casinos',
        Location_of_Service__c ='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLL;
        
       LLLLLLL = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadSubTypeS_FTG8() {
        Lead LLLLLLLL= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Prisons',
        Location_of_Service__c ='Cells',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLL;
        
       LLLLLLLL= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LLLLLLLL.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadSubTypeS_FTG9() {
        Lead LLLLLLLLL= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLL;
        
       LLLLLLLLL= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:LLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLL.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', LLLLLLLLL.sundog_deprm2__Lead_Type__c);
}

static testMethod void validateLeadCategorySubType_P_FTG2() {
        Lead MM= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c ='Chewbacca',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert MM;
        
       MM= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:MM.Id];
       System.debug('Category after trigger fired: ' + MM.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', MM.sundog_deprm2__Lead_Type__c);
}

static testMethod void validateLeadCategorySubType_P_FTG3() {
        Lead MML= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hospitals',
        Location_of_Service__c ='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert MML;
        
       MML= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:MML.Id];
       System.debug('Category after trigger fired: ' + MML.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', MML.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadCategorySubType_P_FTG4() {
        Lead MMMM= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c ='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert MMMM;
        
       MMMM= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:MMMM.Id];
       System.debug('Category after trigger fired: ' + MMMM.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', MMMM.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadCategorySubType_P_FTG7() {
        Lead MMMMMML = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Casinos',
        Location_of_Service__c ='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert MMMMMML;
        
       MMMMMML = [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:MMMMMML.Id];
       System.debug('Category after trigger fired: ' + MMMMMML.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', MMMMMML.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadCategorySubType_P_FTG8() {
        Lead MMMMMMMM= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Prisons',
        Location_of_Service__c ='Cells',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert MMMMMMMM;
        
       MMMMMMMM= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:MMMMMMMM.Id];
       System.debug('Category after trigger fired: ' + MMMMMMMM.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', MMMMMMMM.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadCategorySubType_P_FTG9() {
        Lead MMMMMMMML= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=150,
        Number_of_TVs__c='1 - 2');

        insert MMMMMMMML;
        
       MMMMMMMML= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:MMMMMMMML.Id];
       System.debug('Category after trigger fired: ' + MMMMMMMML.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', MMMMMMMML.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadCategorySubType_P_FTG010() {
        Lead L010= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Prisons',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_TVs__c='1 - 2');

        insert L010;
        
       L010= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L010.Id];
       System.debug('Category after trigger fired: ' + L010.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', L010.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadCategorySubType_P_FTG011() {
        Lead L011= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        State = 'TX',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_TVs__c='1 - 2');

        insert L011;
        
       L011= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L011.Id];
       System.debug('Category after trigger fired: ' + L011.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', L011.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadCategorySubType_P_FTG012() {
        Lead L012= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c = 'Rooms',
        Number_of_Units__c = 119,
        State = 'PA',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_TVs__c='1 - 2');

        insert L012;
        
       L012= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L012.Id];
       System.debug('Category after trigger fired: ' + L012.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', L012.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadCategorySubType_P_FTG013() {
        Lead L013= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c = 'Rooms',
        Number_of_Units__c = 1119,
        State = 'PA',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_TVs__c='1 - 2');

        insert L013;
        
       L013= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L013.Id];
       System.debug('Category after trigger fired: ' + L013.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', L013.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadCategorySubType_P_FTG014() {
        Lead L014= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c = 'Rooms',
        Number_of_Units__c = 1119,
        State = 'CA',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_TVs__c='1 - 2');

        insert L014;
        
       L014= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L014.Id];
       System.debug('Category after trigger fired: ' + L014.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', L014.sundog_deprm2__Lead_Type__c);
}
static testMethod void validateLeadCategorySubType_P_FTG015() {
        Lead L015= new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct CaMM',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c = 'Rooms',
        Number_of_Units__c = 19,
        State = 'CA',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_TVs__c='1 - 2');

        insert L015;
        
       L015= [SELECT sundog_deprm2__Lead_Type__c FROM Lead WHERE Id =:L015.Id];
       System.debug('Category after trigger fired: ' + L015.sundog_deprm2__Lead_Type__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('FTG', L015.sundog_deprm2__Lead_Type__c);
}

}