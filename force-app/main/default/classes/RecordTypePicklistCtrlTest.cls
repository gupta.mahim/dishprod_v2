@isTest
public class RecordTypePicklistCtrlTest {

    @isTest Public static void getRecordTypeValues()
    {
        Opportunity opp = TestDataFactoryforInternal.createOpportunity();
        
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                               'Complete',
                                                               '{"controllerValues":{"FTG":0,"MDU":1},"defaultValue":null,"eTag":"1d78c50c97bda4112db066162498f22a","url":"/services/data/v44.0/ui-api/object-info/Opportunity/picklist-values/0126000000053vzAAA/AmDocs_Property_Sub_Type__c","values":[{"attributes":null,"label":"Apartment","validFor":[1],"value":"Apartment"}]}',
                                                               null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        RecordTypePicklistCtrl.getPicklistValues(Opp.RecordTypeId,'Opportunity', 'test');
        
    }
}