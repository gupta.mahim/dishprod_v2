@isTest  (seeAllData=true)  public class OppProdSelectorCtrlTest {
    
    
    @isTest public static void initProdSelector(){
   
         Pricebook2 pbook = new pricebook2();
        pbook.Name = 'TestCtrl';
                   insert  pbook;
        product2 prod = new product2();
        prod.name ='HBO';
        prod.Product_Selector_Family__c='Core';
        prod.Grandfathered__c=true;
        insert prod;
        
        product2 prod2 = new product2();
        prod2.name ='HBO All';
        prod.Product_Selector_Family__c='Add-On';
        insert prod2;
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        PricebookEntry pbe = new PricebookEntry (Pricebook2Id=standardPB.id, Product2Id=prod.id, IsActive=true, UnitPrice=100.0,  UseStandardPrice = false);
        insert pbe;
        
        PricebookEntry pbe1 = new PricebookEntry (Pricebook2Id=standardPB.id, Product2Id=prod2.id, IsActive=true, UnitPrice=100.0,  UseStandardPrice = false);
        insert pbe1;
        
        PricebookEntry pbes = new PricebookEntry (Pricebook2Id=pbook.id, Product2Id=prod.id, IsActive=true, UnitPrice=100.0,  UseStandardPrice = false);
        insert pbes;
        
        PricebookEntry pbes2 = new PricebookEntry (Pricebook2Id=pbook.id, Product2Id=prod2.id, IsActive=true, UnitPrice=100.0,  UseStandardPrice = false);
        insert pbes2;
        
        Account acc = New Account();
        acc.Name = 'TestAccount';
        Insert acc;
        
        Opportunity o = New Opportunity();
        o.AccountId = acc.Id;
        o.Name = 'TestCtrl';
        o.Property_Type__c='MDU Property (1300)';
        o.StageName='Deal Re-Work';
        o.Property_Status__c ='Inactive';
        o.Phased_Construction__c = 'No';
        o.CloseDate = system.today();
        o.RecordTypeID = '012600000005Bu3';
        o.Address__c='18F Enginners and Architect';
        o.city__c='Lahore'; 
        o.State__c='MI';
        o.Zip__c='48001';
        o.weighted_average__c=4;
        o.Seaonsal_Property__c = true;
        o.pricebook2ID= pbook.Id;
        o.Number_of_Units__c=11;   
        insert o;  
        OpportunityLineItem ol = new OpportunityLineItem(pricebookentryid=pbes.Id,Quantity = 1,UnitPrice = 1000.00,OpportunityID = o.Id);
        insert ol;
        
        Map<String,List<String>> gfProducts=new Map<String,List<String>>();
        List<String> gfProd=new List<String>();
        
        gfProd.add(pbe.id);
        gfProducts.put('Core',gfProd);
        
        String strgfProducts=JSON.serialize(gfProducts);
        System.debug(strgfProducts);
        OppProdSelectorCtrl.OppProductsData data = OppProdSelectorCtrl.initProdSelector(o.id);
        OppProdSelectorCtrl.OppProductsData abc = OppProdSelectorCtrl.loadProductData(o.Id,pbook.id, 'core','abc','english',strgfProducts);
        
        List<PriceBookEntry> newProdData=new List<PriceBookEntry>();
        List<OpportunityLineItem> removeProdData=new List<OpportunityLineItem>();
        
        newProdData.add(pbes2);
        
        removeProdData.add(ol);
        
        oppProdSelectorCtrl.saveProducts(o.id, '', '');
       oppProdSelectorCtrl.performSave(o.id, newProdData, removeProdData);
    }
   }