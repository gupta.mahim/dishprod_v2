@isTest
private class AmDocs_CalloutClass_BulkUpdtCovid19_Test { 
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

Pricebook2 pb2 = new Pricebook2();
            pb2.Name='Test PB2';
            pb2.IsActive=false;
        insert pb2;
        
        pb2.IsActive=true;
        update pb2;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.LeadSource='Test Place';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Digital Bulk';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Pricebook2Id=pb2.id;
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
        insert opp1;
        
        Product2 p2 = new Product2();
            p2.Name='AT120 Test';
            p2.IsActive=true;
            p2.Family='Core';
            p2.Category__c='Core';
            p2.Amdocs_ProductID__c = '123456789';
        insert p2;
        
        Product2 p3 = new Product2();
            p3.Name='SPS Test';
            p3.IsActive=true;
            p3.Family='Add-On';
            p3.Category__c='Add-On';
            p3.Amdocs_Caption2__c = 'ABC123';
            p3.Amdocs_ProductID__c = '123456789';
        insert p3;
        
        Product2 p4 = new Product2();
            p4.Name='Component Test';
            p4.IsActive=true;
            p4.Family='Promotion';
            p4.Category__c='Promotion';
            p4.Amdocs_Caption2__c = 'XYZ789';
            p4.Amdocs_ProductID__c = '123456789';
        insert p4;
        
        //add to standrd pricebook
        Pricebookentry pbe1 = new pricebookentry();
            pbe1.Product2Id=p2.Id;
            pbe1.Pricebook2Id='01s60000000EIELAA4';
            pbe1.UnitPrice=100;
            pbe1.IsActive=true;
        insert pbe1;

        //add to standrd pricebook
        Pricebookentry pbe2 = new pricebookentry();
            pbe2.Product2Id=p3.Id;
            pbe2.Pricebook2Id='01s60000000EIELAA4';
            pbe2.UnitPrice=100;
            pbe2.IsActive=true;
        insert pbe2;

        //add to standrd pricebook        
        Pricebookentry pbe3 = new pricebookentry();
            pbe3.Product2Id=p4.Id;
            pbe3.Pricebook2Id='01s60000000EIELAA4';
            pbe3.UnitPrice=100;
            pbe3.IsActive=true;
        insert pbe3;


        // Add product to new pricebook
        Pricebookentry pbe = new pricebookentry();
            pbe.Product2Id=p2.Id;
            pbe.Pricebook2Id=pb2.Id;
            pbe.UnitPrice=100;
            pbe.IsActive=true;
        insert pbe;
        
        // Add product to new pricebook
        Pricebookentry pbe2a = new pricebookentry();
            pbe2a.Product2Id=p3.Id;
            pbe2a.Pricebook2Id=pb2.Id;
            pbe2a.UnitPrice=100;
            pbe2a.IsActive=true;
        insert pbe2a;
        
        // Add product to new pricebook
        Pricebookentry pbe3a = new pricebookentry();
            pbe3a.Product2Id=p4.Id;
            pbe3a.Pricebook2Id=pb2.Id;
            pbe3a.UnitPrice=100;
            pbe3a.IsActive=true;
        insert pbe3a;
        
        OpportunityLineItem oli1 = New OpportunityLineItem();
            oli1.OpportunityId = opp1.id;
            oli1.PriceBookEntryId=pbe.id;
            oli1.Quantity=1;
            oli1.UnitPrice=1;
            oli1.Action__c='Active';
        insert oli1;
        
        OpportunityLineItem oli2 = New OpportunityLineItem();
            oli2.OpportunityId = opp1.id;
            oli2.PriceBookEntryId=pbe3a.id;
            oli2.Quantity=1;
            oli2.UnitPrice=1;
            oli2.Action__c='Covid19NOWReducingRate';
        insert oli2;
        
/*        Case cs = New Case();
            cs.OpportunityId = opp1.id;
            cs.RecordTypeId = ''; */
  }
  
  
 @isTest
  static void Acct1(){
//    Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 123456789 Testing opp1' Limit 1];
    OpportunityLineItem opp = [Select Id FROM OpportunityLineItem WHERE Action__c = 'Covid19NOWReducingRate' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp); System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
        String sendThisID = opp.Id;
        Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());       // Set mock callout class
        Test.startTest();    // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
            AmDocs_CalloutClass_BulkUpdtCoronavirus.ReduceRateNOW(sendThisID);
        Test.stopTest();    
      
//        opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Opportunity where id =: opp.Id];       // Verify that the response received contains fake values        
//             System.assertEquals('123',opp.AmDocs_CustomerID__c); 
             
//        opr = [select Id, PriceBookEntryId from OpportunityLineItem where OpportunityId =: opp.Id];       // Verify that the response received contains fake values        
//             System.assertEquals('01t600000039UwJ',oli.PriceBookEntryId); 
     
  }
}