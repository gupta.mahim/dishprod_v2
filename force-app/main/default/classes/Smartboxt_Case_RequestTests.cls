@isTest
Public class Smartboxt_Case_RequestTests{
    public static testMethod void testSmartbox_Case_Request(){

PageReference pageRef = Page.case_request;
Test.setCurrentPageReference(pageRef);

Opportunity newOpp = new Opportunity (name='XYZ Organization', StageName='Closed Won', CloseDate=Date.Today());
insert newOpp;

Smartbox__c myEq = new Smartbox__c (
Chassis_Serial__c='A1234',
Part_Number__c='DN004343',
Serial_Number__c='L23PR6789ABC',
CAID__c='R1234567',
Status__c='Activation Requested',
SmartCard__c='S1234567',
Opportunity__c=newopp.id);
insert myEq;

Case mycas = new Case (Status='Form Submitted', Origin='PRM', Opportunity__c=newopp.id);
insert mycas;

ApexPages.StandardController sc = new ApexPages.standardController(mycas);

Smartbox_Case_Request myPageCon = new Smartbox_Case_Request(sc);

myPageCon.getequip();
}
}