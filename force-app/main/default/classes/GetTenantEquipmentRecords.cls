public class GetTenantEquipmentRecords {
 
    @AuraEnabled
    public static List<Tenant_Equipment__c> getTenantEquipmentRecords(id oppId)
    {  
       return new List<Tenant_Equipment__c>([Select id,Name,Address__c,city__c,EquipmentType__c,State__c,Zip_Code__c ,opportunity__c ,Override_NetQual__c,Address_Validation_Message__c,Account_Name__c,Dupe_Oppty_Name__c,Dupe_Account_Name__c ,IsOverrideCaseCreated__c from Tenant_Equipment__c where Opportunity__c = :oppId AND Valid_Address__c=false AND Override_NetQual__c=false]);
    }
    @AuraEnabled
    public static List<Tenant_Equipment__c> saveRecords(String strEquipData) {
        system.debug('Jsonstring'+strEquipData);
       
        List<Tenant_Equipment__c> overrideRecords=new List<Tenant_Equipment__c>();
        List<Tenant_Equipment__c> validateAddressRecords=new List<Tenant_Equipment__c>();
         List<Tenant_Equipment__c>notoverrideRecords =new List<Tenant_Equipment__c>();
       
         
        List<Tenant_Equipment__c> equipData=(List<Tenant_Equipment__c>)JSON.deserialize(strEquipData, List<Tenant_Equipment__c>.class);
      
        for(Tenant_Equipment__c anEquip:equipData)
        {  
            if(anEquip.Override_NetQual__c==true && anEquip.IsOverrideCaseCreated__c==false )
            {   
                anEquip.IsOverrideCaseCreated__c=true;
                overrideRecords.add(anEquip);
            }
          else
            {
                validateAddressRecords.add(anEquip);
            }
             
             
                }
         
         //Validate address
         if(validateAddressRecords.size()>0)
         {
             ValidateAdressCtrl.validateTenantEquipBulk(validateAddressRecords);
             update validateAddressRecords;
              notoverrideRecords= [ Select id,Name,Address__c,city__c,EquipmentType__c,State__c,Zip_Code__c ,opportunity__c ,Override_NetQual__c,Address_Validation_Message__c from Tenant_Equipment__c where id in:validateAddressRecords AND Valid_Address__c=false];
        
         }
         if(overrideRecords.size()>0)
         { 
             //Create ops case for override address
             List<EquipDupeCaseHandler.DupeData>DupeList=new List<EquipDupeCaseHandler.DupeData>();
             
             for(Tenant_Equipment__c TE:overrideRecords)
             {
               EquipDupeCaseHandler.DupeData DupeData= new  EquipDupeCaseHandler.DupeData();
               DupeData.opptyId=TE.Opportunity__c;
               DupeData.recordName=TE.Name;
               DupeList.add(DupeData);
               
             }
              EquipDupeCaseHandler.createAddressOverrideCase(DupeList);
               update overrideRecords;
             
         }       
          
        if(equipData.size()>0)
        {
            update equipData;
        }
        
        
        return notoverrideRecords ;
    }
}