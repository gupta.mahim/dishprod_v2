public class EquipDupeHandler
{
    public static void handleTEDupes(List<Tenant_Equipment__c> teData,boolean performDML)
    {
        Set<String> newReceiverNumbers=new Set<String>();
        List<Tenant_Equipment__c> teToUpdate=new List<Tenant_Equipment__c>();
        for(Tenant_Equipment__c aTE: teData)
        {
            newReceiverNumbers.add(aTE.Name);    
        }
        Map<String,Id> dupeReceiverNumbers=new Map<String,Id>();
        
        checkAllDupes(dupeReceiverNumbers,newReceiverNumbers);
        for(Tenant_Equipment__c aTE: teData)
        {
            if(dupeReceiverNumbers.containsKey(aTE.Name)){aTE.Is_active_elsewhere__c=true;aTE.Where_it_s_active_elsewhere__c=dupeReceiverNumbers.get(aTE.Name);teToUpdate.add(aTE);}
        }
        if(performDML && teToUpdate.size()>0){update teToUpdate;}
    }
    public static void handleHEDupes(List<Equipment__c> heData,boolean performDML)
    {
        Set<String> newReceiverNumbers=new Set<String>();
        List<Equipment__c> heToUpdate=new List<Equipment__c>();
        for(Equipment__c anHE: heData)
        {
            newReceiverNumbers.add(anHE.Name);    
        }
        Map<String,Id> dupeReceiverNumbers=new Map<String,Id>();
        
        checkAllDupes(dupeReceiverNumbers,newReceiverNumbers);
        for(Equipment__c anHE: heData)
        {
            if(dupeReceiverNumbers.containsKey(anHE.Name)){anHE.Is_active_elsewhere__c=true;anHE.Where_it_s_active_elsewhere__c=dupeReceiverNumbers.get(anHE.Name);heToUpdate.add(anHE);}
        }
        if(performDML && heToUpdate.size()>0){update heToUpdate;}
    }
    public static void handleSBDupes(List<Smartbox__c> sbData,boolean performDML)
    {
        Set<String> newReceiverNumbers=new Set<String>();
        List<Smartbox__c> sbToUpdate=new List<Smartbox__c>();
        for(Smartbox__c anSB: sbData)
        {
            if(!String.IsEmpty(anSB.CAID__c))newReceiverNumbers.add(anSB.CAID__c);    
        }
        Map<String,Id> dupeReceiverNumbers=new Map<String,Id>();
        
        checkAllDupes(dupeReceiverNumbers,newReceiverNumbers);
        for(Smartbox__c anSB: sbData)
        {
            if(dupeReceiverNumbers.containsKey(anSB.CAID__c)){anSB.Is_active_elsewhere__c=true;anSB.Where_it_s_active_elsewhere__c=dupeReceiverNumbers.get(anSB.CAID__c);sbToUpdate.add(anSB);}
        }
        if(performDML && sbToUpdate.size()>0){update sbToUpdate;}
    }
    private static void checkAllDupes(Map<String,Id> dupeReceiverNumbers,Set<String> newReceiverNumbers)
    {
        for(Tenant_Equipment__c dupeTE:[select Id,Name, Opportunity__c from Tenant_Equipment__c where Name in:newReceiverNumbers and Action__c='Active'])
        {
            dupeReceiverNumbers.put(dupeTE.Name,dupeTE.Opportunity__c);
            newReceiverNumbers.remove(dupeTE.Name);
        }
        for(Equipment__c dupeTE:[select Id,Name, Opportunity__c from Equipment__c where Name in:newReceiverNumbers and Action__c='Active'])
        {
            dupeReceiverNumbers.put(dupeTE.Name,dupeTE.Opportunity__c);
            newReceiverNumbers.remove(dupeTE.Name);
        }
        for(Smartbox__c dupeTE:[select Id,CAID__c, Opportunity__c from Smartbox__c where CAID__c in:newReceiverNumbers and CAID__c!=null and Action__c='Active'])
        {
            dupeReceiverNumbers.put(dupeTE.CAID__c,dupeTE.Opportunity__c);
        }
    }
}