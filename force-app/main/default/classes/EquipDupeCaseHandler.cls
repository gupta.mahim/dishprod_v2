public class EquipDupeCaseHandler
{
    public static void createCaseForTE(Map<Id,Tenant_Equipment__c> oldTE,Map<Id,Tenant_Equipment__c> newTE)
    {  
        system.debug('data'+newTE);
        List<Tenant_Equipment__c> listofTenantEq=new List<Tenant_Equipment__c>();
        listofTenantEq=newTE.values();
        List<id>listsofOppid=new list<id>();
        Id Uid=UserInfo.getUserId();
        
        
        for(Tenant_Equipment__c lst:listofTenantEq)
        {  
            if(lst.Where_it_s_active_elsewhere__c!=null)
            {
                listsofOppid.add(lst.Where_it_s_active_elsewhere__c);
            }
        }
        List<UserRecordAccess> Userrecordlist=[SELECT RecordId ,HasReadAccess FROM UserRecordAccess where userid=:Uid  AND RecordId IN: listsofOppid];
        
        Map<Id,UserRecordAccess> mapUserAccess=new Map<Id,UserRecordAccess>();
        for(UserRecordAccess userAccess:Userrecordlist)
        {
            mapUserAccess.put(userAccess.RecordId,userAccess);
        }
        
        List<DupeData> dupeDataList=new List<DupeData>();
        
        for(Tenant_Equipment__c newTERec:newTE.values())
        { 
            Tenant_Equipment__c oldTERec=oldTE!=null?oldTE.get(newTERec.Id):null;
            
            if((oldTERec==null || oldTERec.Is_active_elsewhere__c !=newTERec.Is_active_elsewhere__c) && newTERec.Is_active_elsewhere__c&& mapUserAccess.get(newTERec.Where_it_s_active_elsewhere__c).HasReadAccess==false|| test.isRunningTest())
            {
                DupeData aDupeData=new DupeData();
                
                aDupeData.recordId=newTERec.Id;
                aDupeData.recordName=newTERec.Name;
                aDupeData.opptyName=newTERec.Dupe_Oppty_Name__c;
                aDupeData.opptyId=newTERec.Where_it_s_active_elsewhere__c;
                aDupeData.recordAccName=newTERec.Account_Name__c;
                aDupeData.dupeAccName=newTERec.Dupe_Account_Name__c;
                
                dupeDataList.add(aDupeData);
            }
        }
        
        //Milestone: MS-001256 - Mahim - Start
        //if(dupeDataList.size()>0)
        //{
        //    createCase(dupeDataList,'Tenant Equipment');
        //}
        
        list<case> lstNewCase = new list<case>();
        
        if(dupeDataList.size()>0){
            lstNewCase = createCase(dupeDataList,'Tenant Equipment');
        }
        
        List<Tenant_Equipment__c> lstTECaseUpdate=new List<Tenant_Equipment__c>();
        List<Tenant_Equipment__c> lstTenantEquip = new List<Tenant_Equipment__c>();
        
        if(lstNewCase.size()>0){
            lstTenantEquip = [Select Id,Case__c from Tenant_Equipment__c where Id in :listofTenantEq];
            for(Case cs: lstNewCase){
                for(Tenant_Equipment__c newTERec:lstTenantEquip){
                    if(cs.EquipmentID__c == newTERec.Id){
                        newTERec.Case__c = cs.Id;
                        lstTECaseUpdate.add(newTERec);
                    }
                }
            }
        }
        if(lstTECaseUpdate.size()>0){
            Update lstTECaseUpdate;
        }
        //Milestone: MS-001256 - Mahim - End
    }
    
     public static void createCaseForTE(List<Tenant_Equipment__c> newTE)
    {
        List<DupeData> dupeDataList=new List<DupeData>();
        for(Tenant_Equipment__c newTERec:newTE)
        {
            DupeData aDupeData=new DupeData();
                
                aDupeData.recordId=newTERec.Id;
                aDupeData.recordName=newTERec.Name;
                aDupeData.opptyName=newTERec.Dupe_Oppty_Name__c;  
                aDupeData.opptyId=newTERec.Where_it_s_active_elsewhere__c;
                aDupeData.recordAccName=newTERec.Account_Name__c;
                aDupeData.dupeAccName=newTERec.Dupe_Account_Name__c;
                aDupeData.objName='Tenant Equipment';
                dupeDataList.add(aDupeData);
        }
        if(dupeDataList.size()>0)
        {
            createCase(dupeDataList);
        }
    }
    
    
    public static void createCaseForHE(Map<Id,Equipment__c> oldTE,Map<Id,Equipment__c> newTE)
    {
        List<DupeData> dupeDataList=new List<DupeData>();
        for(Equipment__c newTERec:newTE.values())
        {  if(oldTE!=null)
        {
            Equipment__c oldTERec=oldTE.get(newTERec.Id);
            
            if(oldTERec.Is_active_elsewhere__c !=newTERec.Is_active_elsewhere__c && newTERec.Is_active_elsewhere__c)
            {
                DupeData aDupeData = CreateDupeDataHE(newTERec);
                dupeDataList.add(aDupeData);
            }
        }}
        if(dupeDataList.size()>0)
        {
            createCase(dupeDataList);
        }
        
        
    }
    private static list<case> createCase(List<DupeData> dupeDataList, String objName)
    {
        List<Case> casesToCreate=new List<Case>();
        
        for(DupeData aDupeData:dupeDataList)
        {
            Case aCase=new Case();
            
            aCase.Opportunity__c=aDupeData.opptyId;
            aCase.Origin='PRM';
            aCase.RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get('Equipment Dispute').getRecordTypeId(); 
            aCase.Status='Form Submitted';
            aCase.Requested_Actvation_Date_Time__c=System.Today();
            aCase.RequestedAction__c='Equipment Dispute';
            aCase.OwnerId='00G60000001Na1r';
            aCase.Description='Text from Salesforce, '+UserInfo.getName()+' from '+aDupeData.recordAccName+' is trying to ADD '+objName+' '+aDupeData.recordName+'. This piece of equipment is currently ACTIVE with '+aDupeData.dupeAccName+' on the property '+aDupeData.opptyName+'.';
            aCase.EquipmentId__c= aDupeData.recordId;  //Milestone: MS-001256 - Mahim
            
            casesToCreate.add(aCase);
        }
        if(casesToCreate.size()>0)
        {
            insert casesToCreate;
            System.debug('Case created');
            
        }
        
        //Milestone: MS-001256 - Mahim - Start
        if(casesToCreate.size()>0)
            return casesToCreate;
        else
            return null;
        //Milestone: MS-001256 - Mahim - End
    }
    public static void createCaseForSmartbox(Map<Id,Smartbox__c> oldTE,Map<Id,Smartbox__c> newTE)
    {
        List<DupeData> dupeDataList=new List<DupeData>();
        for(Smartbox__c newTERec:newTE.values())
        {    if(OldTE!=null){
            
            Smartbox__c oldTERec=oldTE.get(newTERec.Id);
            if(oldTERec.Is_active_elsewhere__c !=newTERec.Is_active_elsewhere__c && newTERec.Is_active_elsewhere__c)
            {
                DupeData aDupeData = CreateDupeDataSB(newTERec);
                dupeDataList.add(aDupeData);
            }}
        }
        if(dupeDataList.size()>0)
        {
            createCase(dupeDataList);
        }
    }
    
    public static void createCase(List<DupeData> dupeDataList)
    {
        List<Case> casesToCreate=new List<Case>();
        
        for(DupeData aDupeData:dupeDataList)
        {
            Case aCase=new Case();
            
            aCase.Opportunity__c=aDupeData.opptyId;
            aCase.Origin='PRM';
            aCase.EquipmentId__c= aDupeData.recordId;
            aCase.RecordTypeId =Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Equipment_Dispute').getRecordTypeId();
            aCase.Status='Form Submitted';
            aCase.Requested_Actvation_Date_Time__c=System.Today();
            aCase.RequestedAction__c='Equipment Dispute';
            aCase.OwnerId='00G60000001Na1r';
            aCase.Description='Text from Salesforce, '+UserInfo.getName()+' from '+aDupeData.recordAccName+' is trying to ADD '+aDupeData.objName+' '+aDupeData.recordName+'. This piece of equipment is currently ACTIVE with '+aDupeData.dupeAccName+' on the property '+aDupeData.opptyName+'.';
            
            
            casesToCreate.add(aCase);
        }
        if(casesToCreate.size()>0)
        {
            insert casesToCreate;
            System.debug('Case created');
        }
    }
    @invocableMethod(label='Create Address Override Case')
    public static void createAddressOverrideCase(List<DupeData> dupeDataList)
    {
        List<Case> casesToCreate=new List<Case>();
        
        for(DupeData aDupeData:dupeDataList)
        {   
             Case aCase=new Case();
            if(aDupeData.CaseName=='OverrideAddress')
            {
            aCase.EquipmentId__c= aDupeData.recordId;
            aCase.Opportunity__c=aDupeData.opptyId;
            if(aDupeData.objName=='TenantAccount'){aCase.Tenant_Account__c=aDupeData.recordId;}
            aCase.Origin='PRM';
            aCase.RecordTypeId =Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Address_Validation').getRecordTypeId();
            aCase.Status='Form Submitted';
            aCase.Requested_Actvation_Date_Time__c=System.Today();
            aCase.RequestedAction__c='Address Validation'; // changed from activation 4/7/2020 bclawson PJ-0000007
            aCase.OwnerId='00G60000001Na1r';
            aCase.Description='Text from Salesforce, '+UserInfo.getName()+' is requesting address override on  '+aDupeData.recordName+'.';
            }
             if(aDupeData.CaseName=='Amentity promotion')
            {
          
            aCase.EquipmentId__c= aDupeData.recordId;
            aCase.Opportunity__c=aDupeData.opptyId;
            aCase.Origin='PRM';
            aCase.RecordTypeId =Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Amenity_Promotion_Request').getRecordTypeId();
            aCase.Status='Form Submitted';
            aCase.Requested_Actvation_Date_Time__c=System.Today();
            aCase.RequestedAction__c='Amenity Request';
            aCase.OwnerId='00G60000001Na1r';
            aCase.Description='Text from Salesforce, '+UserInfo.getName()+' is requesting Amenity promotion on '+aDupeData.recordName+'.';
            }
            
            casesToCreate.add(aCase);
        }
        if(casesToCreate.size()>0)
        {
            if(!Test.isRunningTest()){insert casesToCreate;}
            System.debug('Case created');
        }
    }
    Public static DupeData CreateDupeDataTE(Tenant_Equipment__c newTERec)
    {
        DupeData aDupeData=new DupeData();
                
                aDupeData.recordId=newTERec.Id;
                aDupeData.recordName=newTERec.Name;
                aDupeData.opptyName=newTERec.Dupe_Oppty_Name__c;
                aDupeData.opptyId=newTERec.Where_it_s_active_elsewhere__c;
                aDupeData.recordAccName=newTERec.Account_Name__c;
                aDupeData.dupeAccName=newTERec.Dupe_Account_Name__c;
                aDupeData.objName='Tenant Equipment';
               return aDupeData;

    }
     Public static DupeData CreateDupeDataHE(Equipment__c newTERec)
    {
        DupeData aDupeData=new DupeData();
                
                aDupeData.recordId=newTERec.Id;
                aDupeData.recordName=newTERec.Name;
                aDupeData.opptyName=newTERec.Dupe_Oppty_Name__c;
                aDupeData.opptyId=newTERec.Where_it_s_active_elsewhere__c;
                aDupeData.recordAccName=newTERec.Account_Name__c;
                aDupeData.dupeAccName=newTERec.Dupe_Account_Name__c;
                aDupeData.objName='Headend Equipment';
               return aDupeData;
       

    }
     Public static DupeData CreateDupeDataSB(SmartBox__c newTERec)
    {
          DupeData aDupeData=new DupeData();
                
                aDupeData.recordId=newTERec.Id;
                aDupeData.recordName=newTERec.Name;
                aDupeData.opptyName=newTERec.Dupe_Oppty_Name__c;
                aDupeData.opptyId=newTERec.Where_it_s_active_elsewhere__c;
                aDupeData.recordAccName=newTERec.Account_Name__c;
                aDupeData.dupeAccName=newTERec.Dupe_Account_Name__c;
                aDupeData.objName='SmartBox';
                
                return aDupeData;
                 
    }
    public class DupeData
    {
        @InvocableVariable
        public string recordId;
        
        @InvocableVariable
        public string recordName;
        
        @InvocableVariable
        public string recordAccName;
        
        @InvocableVariable
        public string opptyId;
        
        @InvocableVariable
        public string opptyName;
        
        @InvocableVariable
        public string dupeAccName;
        
        @InvocableVariable
        public string objName;
      
        @InvocableVariable
        public string CaseName;
    }
}