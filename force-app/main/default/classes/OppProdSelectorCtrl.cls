public class OppProdSelectorCtrl
{
    
    static boolean multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
    @AuraEnabled
    public static OppProductsData initProdSelector(String oppId)
    {
        OppProductsData prodData=new OppProductsData();
        Opportunity theOpp=null;
        Boolean forcePricebookSelection = false;
        Pricebook2 theBook;
        Map<String,List<String>> existingGFEntries=new Map<String,List<String>>();
        
        
        List<ProdFamilyData> prodFamilies=initProductFamilies();
        prodData.prodFamilies=prodFamilies;
        prodData.languageList=initLanguages();
        if(multipleCurrencies)            
            theOpp = database.query('select Id, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode from Opportunity where Id = \'' + oppId + '\' limit 1');
        else
            theOpp = [select Id, Pricebook2Id, PriceBook2.Name, weighted_average__c from Opportunity where Id = :oppId limit 1];
        
        opportunityLineItem[] shoppingCart  = [select Id,PriceBookEntry.Product2.Grandfathered__c,PriceBookEntry.Product2.Name,PriceBookEntry.Sales_Price2__c,Action__c, PriceBookEntryId,PriceBookEntry.Product2.Product_Selector_Family__c, Sales_Price2__c from opportunityLineItem where PriceBookEntry.Product2.Maintenance__c != true AND OpportunityId=:theOpp.Id and (Action__c='ADD' OR Action__c='Active') and PriceBookEntry.Product2.Product_Selector_Family__c!='Fee' order by PriceBookEntry.Product2.Name];       
        
        List<ExistingProdData> existingProdData=new List<ExistingProdData>();
        Map<String,List<ExistingProdDataEntry>> mapOppData=new Map<String,List<ExistingProdDataEntry>>();
        Map<String,Schema.SObjectField> m = Schema.SObjectType.opportunityLineItem.fields.getMap();
        for(OpportunityLineItem oppLI:shoppingCart)
        {
            List<ExistingProdDataEntry> oppLIs=mapOppData.get(oppLI.PriceBookEntry.Product2.Product_Selector_Family__c);
            if(oppLIs==null){oppLIs=new List<ExistingProdDataEntry>();mapOppData.put(oppLI.PriceBookEntry.Product2.Product_Selector_Family__c,oppLIs);}
            if(oppLI.PriceBookEntry.Product2.Grandfathered__c==true)
            {
                List<String> gfEntries=existingGFEntries.get(oppLI.PriceBookEntry.Product2.Product_Selector_Family__c);
                if(gfEntries==null){gfEntries=new List<String>();}
                gfEntries.add(oppLI.PriceBookEntryId);
                existingGFEntries.put(oppLI.PriceBookEntry.Product2.Product_Selector_Family__c,gfEntries);
            }
            ExistingProdDataEntry entry=new ExistingProdDataEntry();
            
            entry.name=oppLI.PriceBookEntry.Product2.Name;
            entry.UnitPrice=oppLI.PriceBookEntry.Sales_Price2__c;
            oppLIs.add(entry);
        }
        Decimal GrandTotal=0.00;
        for(String prodFamily:mapOppData.keySet())
        {
            
            ExistingProdData existingProdDataRec=new ExistingProdData();
            
            existingProdDataRec.family =prodFamily;
            
            List<ExistingProdDataEntry> eProds=mapOppData.get(prodFamily);
            
            existingProdDataRec.prods=eProds;
            
            Decimal TotalPrice=0.00;
            for(ExistingProdDataEntry pEntry:eProds)
            {
                
                if(pEntry.UnitPrice!=null)TotalPrice+=pEntry.UnitPrice;
                if(prodFamily!='Authorization Code(s)' && pEntry.UnitPrice!=null){GrandTotal+=pEntry.UnitPrice;}
                
            }
            existingProdDataRec.TotalPrice=TotalPrice;
            existingProdData.add(existingProdDataRec);
        }
        prodData.existingProducts=shoppingCart;
        prodData.existingProdData=existingProdData;
        prodData.GrandTotal=GrandTotal;
        if(theOpp.Pricebook2Id == null)
        {
            Pricebook2[] activepbs = [select Id, Name from Pricebook2 where Id = '01s60000000dMje'];
            if(activepbs.size() == 2){ forcePricebookSelection = true;    theBook = new Pricebook2();}
            else{theBook = activepbs[0];}
        }
        else{
            theBook = theOpp.Pricebook2;
            prodData.priceBookId=theBook.id; 
        }
        
        if (Schema.sObjectType.PriceBookEntry.fields.Sales_Price2__c.isAccessible()){
            prodData.isVisible = true; 
        }
        prodData.existingGFEntries=existingGFEntries;
        return prodData;
    }
    
    @AuraEnabled
    public static OppProductsData loadProductData(String oppId,String theBookId,String family,String searchText,String prodLanguage,String strExistingGFProd)
    {
        OppProductsData prodData=new OppProductsData();
        Opportunity theOpp=null;
        List<ProdData> prodDataList=new List<ProdData>();
        
        Map<String,List<String>> existingGFProds=(Map<String,List<String>>)JSON.deserialize(strExistingGFProd, Map<String,List<String>>.class);
        
        
        if (Schema.sObjectType.PriceBookEntry.fields.Sales_Price2__c.isAccessible()){
            prodData.isVisible = true; 
        }
        
        if(multipleCurrencies)            
            theOpp = database.query('select Id, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode from Opportunity where Id = \'' + oppId + '\' limit 1');
        else
            theOpp = [select Id, Pricebook2Id, PriceBook2.Name, weighted_average__c from Opportunity where Id = :oppId limit 1];
        
        PriceBookEntry[] availableProducts=loadProductEntries(existingGFProds,theOpp,theBookId,family,searchText,prodLanguage);
        
        for(PriceBookEntry pbEntry:availableProducts)
        {
            ProdData aProdData=new ProdData();
            
            aProdData.productEntry=pbEntry;
            prodDataList.add(aProdData);
        }
        
        prodData.availableProducts=prodDataList;
        return prodData;
    }
    private static PriceBookEntry[] loadProductEntries(Map<String,List<String>> existingGFProds,Opportunity theOpp,String theBookId,String family,String searchText,String prodLanguage)
    {
        PriceBookEntry[] availableProducts=null;
        PriceBookEntry[] existingGFProducts=null;
        
        System.debug('Book Id=['+theBookId+']');
        String qString = 'select Id,Pricebook2Id, Product2.Name, Product2.Family,Product2.Product_Selector_Family__c,'
            +' Product2.One_Time_Price__c,Product2.Description, Sales_Price2__c from PricebookEntry where '
            +'  Product2.Product_Selector_Family__c!=null and Pricebook2Id = \'' + theBookId + '\'';
        
        qString +=' and Grandfathered__c = False and IsActive=true ';
        
        if(existingGFProds!=null && existingGFProds.size()>0 && existingGFProds.get(family)!=null && existingGFProds.get(family).size()>0)
        {
            List<String> gfProds=existingGFProds.get(family);
            
            existingGFProducts=[select Id,Pricebook2Id, Product2.Name, Product2.Family,Product2.Product_Selector_Family__c,Product2.One_Time_Price__c,Product2.Description, Sales_Price2__c from PricebookEntry where Id in:gfProds];
            
            //qString +=' and ((Grandfathered__c = False and IsActive=true) or (Id in '+gfProds+')';
        }
        //else
        //{
        //qString +=' and Grandfathered__c = False and IsActive=true ';
        //}
        System.debug('ProdSelector Query='+qString);
        if(multipleCurrencies)  
            qString += ' and CurrencyIsoCode = \'' + theOpp.get('currencyIsoCode') + '\'';
        
        if(!String.IsEmpty(searchText))
            qString+= ' and Product2.Name like\'%'+searchText+'%\'';
        
        qString+= ' and Product2.Product_Selector_Family__c=\''+family+'\'';
        
        if(!String.IsEmpty(prodLanguage))
            qString+= ' and Product2.Language__c =\''+prodLanguage+'\'';
        
        qString+= ' order by Product2.Name';
        
        availableProducts= database.query(qString);
        if(existingGFProducts!=null && existingGFProducts.size()>0)
        {
            availableProducts.addAll(existingGFProducts);
        }
        return availableProducts;    
    }
    @AuraEnabled
    public static void saveProducts(String oppId,String strNewProdData,String strRemoveProdData)
    {
        if(String.isBlank(strNewProdData) || String.isBlank(strRemoveProdData)){return;}
        List<PriceBookEntry> newProdData=(List<PriceBookEntry>)JSON.deserialize(strNewProdData, List<PriceBookEntry>.class);
        List<OpportunityLineItem> removeProdData=(List<OpportunityLineItem>)JSON.deserialize(strRemoveProdData, List<OpportunityLineItem>.class);
        
        performSave(oppId,newProdData,removeProdData);
    }
    @AuraEnabled
    public static void performSave(String oppId,List<PriceBookEntry> newProdData,List<OpportunityLineItem> removeProdData)
    {
        List<OpportunityLineItem> oppList=new List<OpportunityLineItem>();
        
        OpportunityLineItem[] existingProducts  = [select Id,PriceBookEntry.Product2.Name, PriceBookEntry.Sales_Price2__c ,Action__c, PriceBookEntryId,PriceBookEntry.Product2.Product_Selector_Family__c, Sales_Price2__c from opportunityLineItem where PriceBookEntry.Product2.Maintenance__c != true AND OpportunityId=:oppId];       
        Map<Id,OpportunityLineItem> mapExistingProducts=new Map<Id,OpportunityLineItem>();
        
        Opportunity theOpp=[Select Id, Number_of_Units__c,weighted_average__c from Opportunity where Id=:oppId];
        
        for(OpportunityLineItem oppLI:existingProducts){mapExistingProducts.put(oppLI.PriceBookEntryId,oppLI);}
        
        boolean prodExist=true;
        for(PriceBookEntry pbE:newProdData)
        {
            OpportunityLineItem oppLI=mapExistingProducts.get(pbE.Id);
            
            if(oppLI==null){oppLI=new OpportunityLineItem();prodExist=false;}
            
            if(prodExist==false){oppLI.PriceBookEntryId=pbE.Id;oppLI.OpportunityId=oppId;setQuantity(oppLI,pbE,theOpp.Number_of_Units__c,theOpp.weighted_average__c);}
            oppLI.Action__c='ADD';
            
            oppList.add(oppLI);
        }
        for(OpportunityLineItem existoppLi:removeProdData)
        {
            String nextAction=existoppLi.Action__c=='Active'?'REMOVE':'Removed';
            existoppLi.Action__c=nextAction;
            
            oppList.add(existoppLi);
        }
        upsert oppList;
        System.debug('Prod Saved');
    }
    private static void setQuantity(OpportunityLineItem oppLI,PriceBookEntry pbe,Decimal numberOfUnits,Decimal weightedAvg)
    {
        Set<String> fixedPriceProds=new Set<String>{'Transport','Authorization Code(s)'};
            
            String prodName=pbe.Product2.Name;
        String prodFamily=pbe.Product2.Product_Selector_Family__c;
        
        prodName=prodName==null?'':prodName;
        prodFamily=prodFamily==null?'':prodFamily;
        
        Boolean isFixedPriceProd=false;
        
        for(String fixedPriceProd:fixedPriceProds)
        {
            if(prodName.indexOf(fixedPriceProd)!=-1 || prodFamily.indexOf(fixedPriceProd)!=-1){isFixedPriceProd=true;}
        }
        if(isFixedPriceProd){oppLI.Quantity=1;}
        
        else if(weightedAvg!=null){oppLI.Quantity=weightedAvg;}
        else if(numberOfUnits!=null){oppLI.Quantity=numberOfUnits;}
        else{oppLI.Quantity=0;}
    }
    @AuraEnabled
    public static List<ProdFamilyData> initProductFamilies()
    {
        List<ProdFamilyData> prodData=new List<ProdFamilyData>();
        
        Schema.DescribeFieldResult fieldResult = Product2.Product_Selector_Family__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple)
        {
            String label=pickListVal.getLabel();
            Boolean isCore=label.indexof('Core')!=-1;
            prodData.add(new ProdFamilyData(label,isCore));
        }
        
        return prodData;
    }
    @AuraEnabled
    public static List<String> initLanguages()
    {
        List<String> languageData=new List<String>();
        
        Schema.DescribeFieldResult fieldResult = Product2.Language__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple)
        {
            languageData.add(pickListVal.getLabel());
        }
        
        return languageData;
    }
    public class OppProductsData
    {
        @AuraEnabled
        public String priceBookId {get;set;}
        
        @AuraEnabled
        public String gfEntryId {get;set;}
        
        @AuraEnabled
        public Map<String,List<String>> existingGFEntries {get;set;}
        
        @AuraEnabled
        public List<ProdFamilyData> prodFamilies{get;set;}
        
        @AuraEnabled
        public List<String> languageList{get;set;}
        
        @AuraEnabled
        public ProdData[] availableProducts;
        
        @AuraEnabled
        public OpportunityLineItem[] existingProducts;
        
        @AuraEnabled
        public ExistingProdData[] existingProdData;
        
        @AuraEnabled
        public Decimal GrandTotal{get;set;}
        
        @AuraEnabled
        public Boolean isVisible{get;set;}
        
    }
    public class ProdData
    {
        @AuraEnabled
        public boolean isSelected {get;set;}
        
        @AuraEnabled
        public boolean isExisting {get;set;}
        
        @AuraEnabled
        public PriceBookEntry productEntry{get;set;}
    }
    public class ExistingProdDataEntry
    {
        @AuraEnabled
        public String Name {get;set;}
        
        @AuraEnabled
        public Decimal UnitPrice{get;set;}
    }
    public class ExistingProdData
    {
        @AuraEnabled
        public String family {get;set;}
        
        @AuraEnabled
        public List<ExistingProdDataEntry> prods{get;set;}
        
        @AuraEnabled
        public Decimal TotalPrice{get;set;}
    }
    public class ProdFamilyData
    {
        ProdFamilyData(String inCategoryName,boolean inIsCore)
        {
            categoryName=inCategoryName;
            isCore=inIsCore;
        }
        @AuraEnabled
        public String categoryName{get;set;}
        
        @AuraEnabled
        public boolean isCore{get;set;}
    }
}