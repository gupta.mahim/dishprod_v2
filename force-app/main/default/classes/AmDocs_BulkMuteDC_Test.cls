@isTest
private class AmDocs_BulkMuteDC_Test {
  //Implement mock callout tests here
  
   @testSetup static void testSetupdata(){

    // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'PR';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

        // Create a U.S. based contact
        Contact ctc1 = New Contact();
            ctc1.FirstName = 'Jerry';
            ctc1.LastName = 'Clifft';
            ctc1.Phone = '512-383-5201';
            ctc1.Email = 'jerry.clifft@dish.com';
            ctc1.Role__c = 'Billing Contact (PR)';
            ctc1.AccountId = acct1.Id;
        insert ctc1;
        
        Opportunity o = new Opportunity();
            o.Name = 'Test Opp 1';
            o.AccountId = acct1.Id;
            o.LeadSource='Test Place';
            o.CloseDate=system.Today();
            o.StageName='Closed Won';
            o.Smartbox_Leased__c = false;
            o.AmDocs_ServiceID__c = '1111111111';
            o.Mute_Disconnect__c = 'Non-Pay Disconnect';
        insert o;

        Smartbox__c s = new Smartbox__c();
            s.Chassis_Serial__c='A1234';
            s.Part_Number__c='DN004343';
            s.Serial_Number__c='L23PR6789ABC';
            s.CAID__c='R1234567';
            s.SmartCard__c='S1234567';
            s.Opportunity__c=o.id;
            s.Smartbox_Leased2__c=false;
            s.RA_Number__c = 'RA123';

            insert s;

        Opportunity o2 = new Opportunity();
            o2.Name = 'Test Opp 2';
            o2.AccountId = acct1.Id;
            o2.LeadSource='Test Place';
            o2.CloseDate=system.Today();
            o2.StageName='Closed Won';
            o2.Smartbox_Leased__c = false;
            o2.AmDocs_ServiceID__c = '1111111111';
            o2.Mute_Disconnect__c = 'Non-Pay Resume';
        insert o2;
        
        Opportunity o3 = new Opportunity();
            o3.Name = 'Test Opp 3';
            o3.AccountId = acct1.Id;
            o3.LeadSource='Test Place';
            o3.CloseDate=system.Today();
            o3.StageName='Closed Won';
            o3.Smartbox_Leased__c = false;
            o3.AmDocs_ServiceID__c = '1111111111';
            o3.Mute_Disconnect__c = 'Mute';
        insert o3;
        
        Opportunity o4 = new Opportunity();
            o4.Name = 'Test Opp 4';
            o4.AccountId = acct1.Id;
            o4.LeadSource='Test Place';
            o4.CloseDate=system.Today();
            o4.StageName='Closed Won';
            o4.Smartbox_Leased__c = false;
            o4.AmDocs_ServiceID__c = '1111111111';
            o4.Mute_Disconnect__c = 'UnMute';
        insert o4;
        
        Opportunity o5 = new Opportunity();
            o5.Name = 'Test Opp 5';
            o5.AccountId = acct1.Id;
            o5.LeadSource='Test Place';
            o5.CloseDate=system.Today();
            o5.StageName='Closed Won';
            o5.Smartbox_Leased__c = false;
            o5.AmDocs_ServiceID__c = '1111111111';
            o5.Mute_Disconnect__c = 'Disconnect';
        insert o5;
        
        Opportunity o6 = new Opportunity();
            o6.Name = 'Test Opp 6';
            o6.AccountId = acct1.Id;
            o6.LeadSource='Test Place';
            o6.CloseDate=system.Today();
            o6.StageName='Closed Won';
            o6.Smartbox_Leased__c = false;
            o6.AmDocs_ServiceID__c = '1111111111';
            o6.Mute_Disconnect__c = 'UnMute';
        insert o6;
    }
  
  
  @isTest
  static void Acct1(){
    Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 1' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        String sendThisID = opp.Id;
  
          // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
          // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(sendThisID);
      Test.stopTest();    
  
        // Verify that the response received contains fake values        
    opp = [select AmDocs_ServiceID__c from Opportunity where id =: opp.Id];
        // System.assertEquals(null,opp.AmDocs_ServiceID__c); System.assertEquals('0123456789',opp.AmDocs_ServiceID__c);     
  }
   @isTest
  static void Acct2(){
    Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 2' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        String sendThisID = opp.Id;
  
          // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
          // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(sendThisID);
      Test.stopTest();    
  
        // Verify that the response received contains fake values        
    opp = [select AmDocs_ServiceID__c from Opportunity where id =: opp.Id];
        // System.assertEquals(null,opp.AmDocs_ServiceID__c); System.assertEquals('0123456789',opp.AmDocs_ServiceID__c);     
  } 
    @isTest
  static void Acct3(){
    Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 3' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        String sendThisID = opp.Id;
  
          // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
          // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(sendThisID);
      Test.stopTest();    
  
        // Verify that the response received contains fake values        
    opp = [select AmDocs_ServiceID__c from Opportunity where id =: opp.Id];
        // System.assertEquals(null,opp.AmDocs_ServiceID__c); System.assertEquals('0123456789',opp.AmDocs_ServiceID__c);     
  }
    @isTest
  static void Acct4(){
    Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 4' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        String sendThisID = opp.Id;
  
          // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
          // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(sendThisID);
      Test.stopTest();    
  
        // Verify that the response received contains fake values        
    opp = [select AmDocs_ServiceID__c from Opportunity where id =: opp.Id];
        // System.assertEquals(null,opp.AmDocs_ServiceID__c); System.assertEquals('0123456789',opp.AmDocs_ServiceID__c);     
  }
  @isTest
  static void Acct5(){
    Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 5' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        String sendThisID = opp.Id;
  
          // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
          // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(sendThisID);
      Test.stopTest();    
  
        // Verify that the response received contains fake values        
    opp = [select AmDocs_ServiceID__c from Opportunity where id =: opp.Id];
        // System.assertEquals(null,opp.AmDocs_ServiceID__c); System.assertEquals('0123456789',opp.AmDocs_ServiceID__c);     
  }
 @isTest
  static void Acct6(){
    Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 6' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        String sendThisID = opp.Id;
  
          // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
          // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(sendThisID);
      Test.stopTest();    
  
        // Verify that the response received contains fake values        
    opp = [select AmDocs_ServiceID__c from Opportunity where id =: opp.Id];
        // System.assertEquals(null,opp.AmDocs_ServiceID__c); System.assertEquals('0123456789',opp.AmDocs_ServiceID__c);     
  } 
  
}