@isTEST

public class SBoxDupeLocatorTests{
    static testMethod void testLeadDupPreventer() {
        
// First make sure there are no Unique_Opp_SN__c already in the system
// that have the Unique_Opp_SN__c  used for testing
      Set<String> testUnique_Opp_SN = new Set<String>();
      testUnique_Opp_SN.add('8255123456');
      testUnique_Opp_SN.add('8255234567');
      testUnique_Opp_SN.add('8255345678');
      testUnique_Opp_SN.add('8255456789');
      testUnique_Opp_SN.add('8255567890');
      System.assert([SELECT count() FROM Smartbox__c
                     WHERE Unique_Opp_SN__c  IN :testUnique_Opp_SN ] == 0);
                     
// Seed the database with some records, and make sure they can
// be bulk inserted successfully.
    Account a = new Account();
        a.Name = 'Test Account';
        a.phone = '(303) 555-5555';
            insert a;

    Opportunity o = new Opportunity();
        o.Name = 'Test';
        o.AccountId = a.Id;
        o.LeadSource='Test Method';
        o.misc_code_siu__c='T999';
        o.CloseDate=system.Today();
        o.StageName='Closed Won';
            insert o;
            
    Smartbox__c s01 = new Smartbox__c();
        s01.Chassis_Serial__c='LALPFY00096K';
        s01.Serial_Number__c='LALPRZ04961A';
        s01.Opportunity__c=o.id;

    Smartbox__c s02 = new Smartbox__c();
        s02.Chassis_Serial__c='LALPFY00096K';
        s02.Serial_Number__c='LALPRZ04962A';
        s02.Opportunity__c=o.id;
        
    Smartbox__c s03 = new Smartbox__c();
        s03.Chassis_Serial__c='LALPFY00096K';
        s03.Serial_Number__c='LALPRZ04963A';
        s03.Opportunity__c=o.id;
        
    Smartbox__c[] SBoxes = new Smartbox__c[] {s01, s02, s03};
        insert SBoxes;                     

// The above gives 100% coverage, the rest is flair..and actual testing

// Now make sure that some of these leads can be changed and
// then bulk updated successfully. Note that lead1 is not
// being changed, but is still being passed to the update
// call. This should be OK.
      s02.Serial_Number__c='LALPRZ04964A';
      s03.Serial_Number__c='LALPRZ04965A';
      update SBoxes;


// Unmodded
// Make sure that single row lead duplication prevention works
// on insert.
//      Lead dup1 = new Lead(LastName='Test1Dup',
//                           Company='Test1Dup Inc.',
//                           Email='test1@duptest.com');
//      try {
//         insert dup1;
//         System.assert(false);
//      } catch (DmlException e) {
//         System.assert(e.getNumDml() == 1);
//         System.assert(e.getDmlIndex(0) == 0);
//         System.assert(e.getDmlFields(0).size() == 1);
//         System.assert(e.getDmlFields(0)[0] == 'Email');
//         System.assert(e.getDmlMessage(0).indexOf(
//            'A lead with this email address already exists.') > -1);
//      }
        
// Make sure that single row lead duplication prevention works
// on update.
//      dup1 = new Lead(Id = lead1.Id, LastName='Test1Dup',
//                      Company='Test1Dup Inc.',
//                      Email='test2@duptest.com');
//      try {
//         update dup1;
//         System.assert(false);
//      } catch (DmlException e) {
//         System.assert(e.getNumDml() == 1);
//         System.assert(e.getDmlIndex(0) == 0);
//         System.assert(e.getDmlFields(0).size() == 1);
//         System.assert(e.getDmlFields(0)[0] == 'Email');
//         System.assert(e.getDmlMessage(0).indexOf(
//            'A lead with this email address already exists.') > -1);
//        }
//    
// Make sure that bulk lead duplication prevention works on
// insert. Note that the first item being inserted is fine,
// but the second and third items are duplicates. Note also
// that since at least one record insert fails, the entire
// transaction will be rolled back.
//      dup1 = new Lead(LastName='Test1Dup', Company='Test1Dup Inc.',
//                      Email='test4@duptest.com');
//      Lead dup2 = new Lead(LastName='Test2Dup',
//                           Company='Test2Dup Inc.',
//                           Email='test2@duptest.com');
//      Lead dup3 = new Lead(LastName='Test3Dup',
//                           Company='Test3Dup Inc.',
//                           Email='test3@duptest.com');
//      Lead[] dups = new Lead[] {dup1, dup2, dup3};
//      try {
//         insert dups;
//         System.assert(false);
//      } catch (DmlException e) {
//         System.assert(e.getNumDml() == 2);
//         System.assert(e.getDmlIndex(0) == 1);
//         System.assert(e.getDmlFields(0).size() == 1);
//         System.assert(e.getDmlFields(0)[0] == 'Email');
//         System.assert(e.getDmlMessage(0).indexOf(
//            'A lead with this email address already exists.') > -1);
//         System.assert(e.getDmlIndex(1) == 2);
//         System.assert(e.getDmlFields(1).size() == 1);
//         System.assert(e.getDmlFields(1)[0] == 'Email');
//        System.assert(e.getDmlMessage(1).indexOf(
//            'A lead with this email address already exists.') > -1);
//      }
    
// Make sure that bulk lead duplication prevention works on
// update. Note that the first item being updated is fine,
// because the email address is new, and the second item is
// also fine, but in this case it's because the email
// address doesn't change. The third case is flagged as an
// error because it is a duplicate of the email address of the
// first lead's value in the database, even though that value
// is changing in this same update call. It would be an
// interesting exercise to rewrite the trigger to allow this
// case. Note also that since at least one record update
// fails, the entire transaction will be rolled back.
//      dup1 = new Lead(Id=lead1.Id, Email='test4@duptest.com');
//      dup2 = new Lead(Id=lead2.Id, Email='test2@duptest.com');
//      dup3 = new Lead(Id=lead3.Id, Email='test1@duptest.com');
//      dups = new Lead[] {dup1, dup2, dup3};
//      try {
//         update dups;
//         System.assert(false);
//      } catch (DmlException e) {
//         System.debug(e.getNumDml());
//         System.debug(e.getDmlMessage(0));
//         System.assert(e.getNumDml() == 1);
//         System.assert(e.getDmlIndex(0) == 2);
//         System.assert(e.getDmlFields(0).size() == 1);
//         System.assert(e.getDmlFields(0)[0] == 'Email');
//         System.assert(e.getDmlMessage(0).indexOf(
//            'A lead with this email address already exists.') > -1);
//        }
        
// Make sure that duplicates in the submission are caught when
// inserting leads. Note that this test also catches an
// attempt to insert a lead where there is an existing
// duplicate.
//      dup1 = new Lead(LastName='Test1Dup', Company='Test1Dup Inc.',
//                      Email='test4@duptest.com');
//      dup2 = new Lead(LastName='Test2Dup', Company='Test2Dup Inc.',
//                      Email='test4@duptest.com');
//      dup3 = new Lead(LastName='Test3Dup', Company='Test3Dup Inc.',
//                      Email='test3@duptest.com');
//      dups = new Lead[] {dup1, dup2, dup3};
//      try {
//         insert dups;
//         System.assert(false);
//      } catch (DmlException e) {
//         System.assert(e.getNumDml() == 2);
//         System.assert(e.getDmlIndex(0) == 1);
//         System.assert(e.getDmlFields(0).size() == 1);
//         System.assert(e.getDmlFields(0)[0] == 'Email');
//         System.assert(e.getDmlMessage(0).indexOf(
//            'Another new lead has the same email address.') > -1);
//         System.assert(e.getDmlIndex(1) == 2);
//         System.assert(e.getDmlFields(1).size() == 1);
//         System.assert(e.getDmlFields(1)[0] == 'Email');
//         System.assert(e.getDmlMessage(1).indexOf(
//            'A lead with this email address already exists.') > -1);
//      }
        
// Make sure that duplicates in the submission are caught when
// updating leads. Note that this test also catches an attempt
// to update a lead where there is an existing duplicate.
//      dup1 = new Lead(Id=lead1.Id, Email='test4@duptest.com');
//      dup2 = new Lead(Id=lead2.Id, Email='test4@duptest.com');
//      dup3 = new Lead(Id=lead3.Id, Email='test2@duptest.com');
//      dups = new Lead[] {dup1, dup2, dup3};
//      try {
//         update dups;
//         System.assert(false);
//      } catch (DmlException e) {
//         System.assert(e.getNumDml() == 2);
//         System.assert(e.getDmlIndex(0) == 1);
//         System.assert(e.getDmlFields(0).size() == 1);
//         System.assert(e.getDmlFields(0)[0] == 'Email');
//         System.assert(e.getDmlMessage(0).indexOf(
//            'Another new lead has the same email address.') > -1);
//         System.assert(e.getDmlIndex(1) == 2);
//         System.assert(e.getDmlFields(1).size() == 1);
//         System.assert(e.getDmlFields(1)[0] == 'Email');
//         System.assert(e.getDmlMessage(1).indexOf(
//            'A lead with this email address already exists.') > -1);
//      }
//   }
//}
                    
}
}