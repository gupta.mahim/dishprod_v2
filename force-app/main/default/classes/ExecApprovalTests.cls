@isTest (seeAllData=true)
Public class ExecApprovalTests{
    public static testMethod void testExecApproval1(){
    PageReference pageRef = Page.ExecApproval;
Test.setCurrentPageReference(pageRef);


Opportunity o = New Opportunity();

                o.Name = 'Test';
                o.Property_Type__c='MDU Property (1300)';
                o.StageName='Deal Re-Work';
                o.Property_Status__c ='Inactive';
                o.CloseDate = system.today();
                o.Phased_Construction__c = 'No';
                o.RecordTypeID = '012600000005Bu3';
            insert o;

Front_Office_Request__c fo = New Front_Office_Request__c ();
          fo.Opportunity__c = o.id;
          fo.Front_Office_Decision__c = 'In Progress';
          fo.Phase_Build__c = 'No';
          fo.Property_Address__c = '1011 Collie Path';
          fo.Property_City__c = 'Round Rock';
          fo.Property_State__c = 'TX';
          fo.Property_Zip_Code__c = '78664';
          fo.RecordTypeId = '0126000000017rc';
          insert fo;
          
Executive_Summary__c ex = New  Executive_Summary__c ();
           ex.Front_Office_Request__c = fo.id;
           ex.Status__c = 'In Progress';
           insert ex;

ApexPages.StandardController sc = new ApexPages.standardController(ex);
System.currentPagereference().getParameters().put('uid','00560000001S9tG');
System.currentPagereference().getParameters().put('dec','Approved');
ExecApproval oPEE = new ExecApproval(sc);
   oPEE.onSave();
   }
   
    public static testMethod void testExecApproval2(){
    PageReference pageRef = Page.ExecApproval;
Test.setCurrentPageReference(pageRef);


Opportunity o = New Opportunity();

                o.Name = 'Test';
                o.Property_Type__c='MDU Property (1300)';
                o.StageName='Deal Re-Work';
                o.Property_Status__c ='Inactive';
                o.CloseDate = system.today();
                o.Phased_Construction__c = 'No';
                o.RecordTypeID = '012600000005Bu3';
            insert o;

Front_Office_Request__c fo = New Front_Office_Request__c ();
          fo.Opportunity__c = o.id;
          fo.Front_Office_Decision__c = 'In Progress';
          fo.Phase_Build__c = 'No';
          fo.Property_Address__c = '1011 Collie Path';
          fo.Property_City__c = 'Round Rock';
          fo.Property_State__c = 'TX';
          fo.Property_Zip_Code__c = '78664';
          fo.RecordTypeId = '0126000000017rc';
          insert fo;
          
Executive_Summary__c ex = New  Executive_Summary__c ();
           ex.Front_Office_Request__c = fo.id;
           ex.Status__c = 'In Progress';
           insert ex;

ApexPages.StandardController sc = new ApexPages.standardController(ex);
System.currentPagereference().getParameters().put('uid','00560000001ClYw');
System.currentPagereference().getParameters().put('dec','Approved');
ExecApproval oPEE = new ExecApproval(sc);
   oPEE.onSave();
   }
   
      
    public static testMethod void testExecApproval3(){
    PageReference pageRef = Page.ExecApproval;
Test.setCurrentPageReference(pageRef);


Opportunity o = New Opportunity();

                o.Name = 'Test';
                o.Property_Type__c='MDU Property (1300)';
                o.StageName='Deal Re-Work';
                o.Property_Status__c ='Inactive';
                o.CloseDate = system.today();
                o.Phased_Construction__c = 'No';
                o.RecordTypeID = '012600000005Bu3';
            insert o;

Front_Office_Request__c fo = New Front_Office_Request__c ();
          fo.Opportunity__c = o.id;
          fo.Front_Office_Decision__c = 'In Progress';
          fo.Phase_Build__c = 'No';
          fo.Property_Address__c = '1011 Collie Path';
          fo.Property_City__c = 'Round Rock';
          fo.Property_State__c = 'TX';
          fo.Property_Zip_Code__c = '78664';
          fo.RecordTypeId = '0126000000017rc';
          insert fo;
          
Executive_Summary__c ex = New  Executive_Summary__c ();
           ex.Front_Office_Request__c = fo.id;
           ex.Status__c = 'In Progress';
           insert ex;

ApexPages.StandardController sc = new ApexPages.standardController(ex);
System.currentPagereference().getParameters().put('uid','00560000001TXaU');
System.currentPagereference().getParameters().put('dec','Approved');
ExecApproval oPEE = new ExecApproval(sc);
   oPEE.onSave();
   }
   
       public static testMethod void testExecApproval4(){
    PageReference pageRef = Page.ExecApproval;
Test.setCurrentPageReference(pageRef);


Opportunity o = New Opportunity();

                o.Name = 'Test';
                o.Property_Type__c='MDU Property (1300)';
                o.StageName='Deal Re-Work';
                o.Property_Status__c ='Inactive';
                o.CloseDate = system.today();
                o.Phased_Construction__c = 'No';
                o.RecordTypeID = '012600000005Bu3';
            insert o;

Front_Office_Request__c fo = New Front_Office_Request__c ();
          fo.Opportunity__c = o.id;
          fo.Front_Office_Decision__c = 'In Progress';
          fo.Property_Address__c = '1011 Collie Path';
          fo.Property_City__c = 'Round Rock';
          fo.Property_State__c = 'TX';
          fo.Property_Zip_Code__c = '78664';
          fo.Phase_Build__c = 'No';
          fo.RecordTypeId = '0126000000017rc';
          insert fo;
          
Executive_Summary__c ex = New  Executive_Summary__c ();
           ex.Front_Office_Request__c = fo.id;
           ex.Status__c = 'In Progress';
           insert ex;

ApexPages.StandardController sc = new ApexPages.standardController(ex);
System.currentPagereference().getParameters().put('uid','00560000001ClZ1');
System.currentPagereference().getParameters().put('dec','Approved');
ExecApproval oPEE = new ExecApproval(sc);
   oPEE.onSave();
   }

    public static testMethod void testExecApproval5(){
    PageReference pageRef = Page.ExecApproval;
Test.setCurrentPageReference(pageRef);


Opportunity o = New Opportunity();

                o.Name = 'Test';
                o.Property_Type__c='MDU Property (1300)';
                o.StageName='Deal Re-Work';
                o.Property_Status__c ='Inactive';
                o.CloseDate = system.today();
                o.Phased_Construction__c = 'No';
                o.RecordTypeID = '012600000005Bu3';
            insert o;

Front_Office_Request__c fo = New Front_Office_Request__c ();
          fo.Opportunity__c = o.id;
          fo.Front_Office_Decision__c = 'In Progress';
          fo.Phase_Build__c = 'No';
          fo.Property_Address__c = '1011 Collie Path';
          fo.Property_City__c = 'Round Rock';
          fo.Property_State__c = 'TX';
          fo.Property_Zip_Code__c = '78664';
          fo.RecordTypeId = '0126000000017rc';
          insert fo;
          
Executive_Summary__c ex = New  Executive_Summary__c ();
           ex.Front_Office_Request__c = fo.id;
           ex.Status__c = 'In Progress';
           insert ex;

ApexPages.StandardController sc = new ApexPages.standardController(ex);
System.currentPagereference().getParameters().put('uid','00560000001S9tG');
System.currentPagereference().getParameters().put('dec','Declined');
ExecApproval oPEE = new ExecApproval(sc);
   oPEE.onSave();
   }
   
    public static testMethod void testExecApproval6(){
    PageReference pageRef = Page.ExecApproval;
Test.setCurrentPageReference(pageRef);


Opportunity o = New Opportunity();

                o.Name = 'Test';
                o.Property_Type__c='MDU Property (1300)';
                o.StageName='Deal Re-Work';
                o.Property_Status__c ='Inactive';
                o.CloseDate = system.today();
                o.Phased_Construction__c = 'No';
                o.RecordTypeID = '012600000005Bu3';
            insert o;

Front_Office_Request__c fo = New Front_Office_Request__c ();
          fo.Opportunity__c = o.id;
          fo.Front_Office_Decision__c = 'In Progress';
          fo.Phase_Build__c = 'No';
          fo.Property_Address__c = '1011 Collie Path';
          fo.Property_City__c = 'Round Rock';
          fo.Property_State__c = 'TX';
          fo.Property_Zip_Code__c = '78664';
          fo.RecordTypeId = '0126000000017rc';
          insert fo;
          
Executive_Summary__c ex = New  Executive_Summary__c ();
           ex.Front_Office_Request__c = fo.id;
           ex.Status__c = 'In Progress';
           insert ex;

ApexPages.StandardController sc = new ApexPages.standardController(ex);
System.currentPagereference().getParameters().put('uid','00560000001ClYw');
System.currentPagereference().getParameters().put('dec','Declined');
ExecApproval oPEE = new ExecApproval(sc);
   oPEE.onSave();
   }
   
      
    public static testMethod void testExecApproval7(){
    PageReference pageRef = Page.ExecApproval;
Test.setCurrentPageReference(pageRef);


Opportunity o = New Opportunity();

                o.Name = 'Test';
                o.Property_Type__c='MDU Property (1300)';
                o.StageName='Deal Re-Work';
                o.Property_Status__c ='Inactive';
                o.CloseDate = system.today();
                o.Phased_Construction__c = 'No';
                o.RecordTypeID = '012600000005Bu3';
            insert o;

Front_Office_Request__c fo = New Front_Office_Request__c ();
          fo.Opportunity__c = o.id;
          fo.Phase_Build__c = 'No';
          fo.Property_Address__c = '1011 Collie Path';
          fo.Property_City__c = 'Round Rock';
          fo.Property_State__c = 'TX';
          fo.Property_Zip_Code__c = '78664';
          fo.Front_Office_Decision__c = 'In Progress';
          fo.RecordTypeId = '0126000000017rc';
          insert fo;
          
Executive_Summary__c ex = New  Executive_Summary__c ();
           ex.Front_Office_Request__c = fo.id;
           ex.Status__c = 'In Progress';
           insert ex;

ApexPages.StandardController sc = new ApexPages.standardController(ex);
System.currentPagereference().getParameters().put('uid','00560000001TXaU');
System.currentPagereference().getParameters().put('dec','Declined');
ExecApproval oPEE = new ExecApproval(sc);
   oPEE.onSave();
   }
   
       public static testMethod void testExecApproval8(){
    PageReference pageRef = Page.ExecApproval;
Test.setCurrentPageReference(pageRef);


Opportunity o = New Opportunity();

                o.Name = 'Test';
                o.Property_Type__c='MDU Property (1300)';
                o.StageName='Deal Re-Work';
                o.Property_Status__c ='Inactive';
                o.CloseDate = system.today();
                o.Phased_Construction__c = 'No';
                o.RecordTypeID = '012600000005Bu3';
            insert o;

Front_Office_Request__c fo = New Front_Office_Request__c ();
          fo.Opportunity__c = o.id;
          fo.Phase_Build__c = 'No';
          fo.Property_Address__c = '1011 Collie Path';
          fo.Property_City__c = 'Round Rock';
          fo.Property_State__c = 'TX';
          fo.Property_Zip_Code__c = '78664';
          fo.Front_Office_Decision__c = 'In Progress';
          fo.RecordTypeId = '0126000000017rc';
          insert fo;
          
Executive_Summary__c ex = New  Executive_Summary__c ();
           ex.Front_Office_Request__c = fo.id;
           ex.Status__c = 'In Progress';
           insert ex;

ApexPages.StandardController sc = new ApexPages.standardController(ex);
System.currentPagereference().getParameters().put('uid','00560000001ClZ1');
System.currentPagereference().getParameters().put('dec','Declined');
ExecApproval oPEE = new ExecApproval(sc);
   oPEE.onSave();
   }}