/*-------------------------------------------------------------
Author: Jerry Clifft
Created on: 5th March 2020
Created for: Milestone# MS-000022
Description: To DISCONNECT an entire Tenant Account ID / Tenant Service where Tenant Account = NULL
-------------------------------------------------------------*/


public class AmDocs_CalloutClass_TNEGuideMeDupeDC {
    
    public class person {
        public String serviceID;
        public String orderID;
        public String responseStatus;
        public String status;
        public cls_informationMessages[] informationMessages;
    }
    class cls_informationMessages {
        public String errorCode;
        public String errorDescription;
    }
    
    @AuraEnabled   
    public static string TellAmdocsToDISCONNECT(String Id) {
        // Added by Mahim - Start
        String returnStatus = 'Success';
        list<String> receiversForActiveElseWhereUpdate = new list<String>();
        list<Tenant_Equipment__c> teForActiveElseWhereUpdate = new list<Tenant_Equipment__c>();
        list<Tenant_Equipment__c> teWithActiveElseWhereTrue = new list<Tenant_Equipment__c>();
        // Added by Mahim - End
        
        try{
            // conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];
            
            // Updated by Mahim - Start
            //list<Tenant_Equipment__c> O = [select id, Name, Amdocs_Tenant_ID__c, Reset__c, Action__c from Tenant_Equipment__c where Id = :id AND (Action__c = 'DupeDisconnect' OR Action__c = 'TEST') ];
            list<Tenant_Equipment__c> O = [select id, Name, Amdocs_Tenant_ID__c, Reset__c, Action__c from Tenant_Equipment__c where 
                                           Amdocs_Tenant_ID__c = :id AND (Action__c = 'DupeDisconnect' OR Action__c = 'TEST') ];
            // Updated by Mahim - End
            System.debug('RESULTS of the LIST lookup to the Opp object' +O);
            
            
            if(O[0].Amdocs_Tenant_ID__c == '' || O[0].Amdocs_Tenant_ID__c == Null ){
                
                Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
                    sbc.id=O[0].id;
                    sbc.API_Status__c='Error';
                    sbc.Amdocs_Transaction_Code__c='Error';
                    if( (O[0].Amdocs_Tenant_ID__c == '' || O[0].Amdocs_Tenant_ID__c == Null )) {
                        sbc.Amdocs_Transaction_Description__c= O[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
                    }
                    update sbc;
                }
                API_Log__c apil2 = new API_Log__c();{
                    apil2.Record__c=id;
                    apil2.Object__c='Tenant Equipment';
                    apil2.Status__c='ERROR';
                    apil2.Results__c=O[0].Action__c + ' requies a Tenant Service ID. You are missing: Tenant Service ID.';
                    apil2.API__c='Update Unit';
                    apil2.ServiceID__c=O[0].Amdocs_Tenant_ID__c;
                    apil2.User__c=UserInfo.getUsername();
                    insert apil2;
                } 
            }
            
            if(O[0].Amdocs_Tenant_ID__c != '' && O[0].Amdocs_Tenant_ID__c != Null ){        
                Datetime dt1 = System.Now();
                list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateUpdateTime__c from API_LOG__c where ServiceID__c = :O[0].Amdocs_Tenant_ID__c AND NextAvailableActivateUpdateTime__c >= :dt1 Order By NextAvailableActivateUpdateTime__c  DESC Limit 1];
                
                if( timer.size() > 0 || O[0].Reset__c == 'ChewbaccaIsTesting' ) {
                    system.debug('DEBUG API LOG Timer size : ' +timer.size());
                    system.debug('DEBUG API LOG Info' +timer);
                    system.debug('DEBUG timer 1 : ' +dt1);
                    
                    Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
                        sbc.id=O[0].id; 
                        sbc.API_Status__c='Error';  
                        sbc.Amdocs_Transaction_Code__c='Error'; 
                        sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions'; 
                        update sbc;
                    }
                    API_Log__c apil2 = new API_Log__c();
                    { 
                        apil2.Record__c=O[0].id; 
                        apil2.Object__c='Tenant Equipment'; 
                        apil2.Status__c='ERROR'; 
                        apil2.Results__c='You must wait atleast (2) two minutes between transactions'; 
                        apil2.API__c='Update Unit'; 
                        apil2.ServiceID__c=O[0].AmDocs_Tenant_ID__c; 
                        apil2.User__c=UserInfo.getUsername(); 
                        insert apil2; 
                        
                    }
                }   
                
                else if(timer.size() < 1) {
                    
                    list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
                    System.debug('RESULTS of the LIST lookup to the Amdocs Login object' +A);
                    System.debug('DEBUG UXF_Token' +A[0].UXF_Token__c);
                    
                    JSONGenerator jsonObj = JSON.createGenerator(true);
                    jsonObj.writeStartObject();
                    jsonObj.writeFieldName('ImplUpdateProductStatusInput');
                    //    jsonObj.writeStartArray();
                    jsonObj.writeStartObject();
                    
                    for(Integer i = 0; i < O.size(); i++){
                        // Regular Disconnect
                        if( O[i].Action__c == 'DupeDisconnect') { jsonObj.writeStringField('type', 'CE'); }            
                    }
                    
                    jsonObj.writeEndObject();
                    //    jsonObj.writeEndArray();
                    jsonObj.writeEndObject();
                    
                    String finalJSON = jsonObj.getAsString();
                    System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
                    
                    // if (!Test.isRunningTest()){
                    HttpRequest request = new HttpRequest();  
                    String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].Amdocs_Tenant_ID__c+'/updateProductStatus?sc=SS&lo=EN&ca=SF'; 
                    request.setEndPoint(endpoint); 
                    request.setBody(jsonObj.getAsString()); 
                    request.setHeader('Content-Type', 'application/json'); 
                    request.setMethod('POST'); 
                    String authorizationHeader = A[0].UXF_Token__c; 
                    request.setHeader('Authorization', authorizationHeader); 
                    request.setTimeout(101000); 
                    
                    HttpResponse response = new HTTP().send(request); 
                    system.debug('O[0].Reset__c - '+O[0].Reset__c);
                    if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) {  
                        String strjson = response.getbody(); 
                        JSONParser parser = JSON.createParser(strjson); 
                        parser.nextToken(); parser.nextToken(); parser.nextToken(); 
                        person obj = (person)parser.readValueAs( person.class);
                        System.debug('DEBUG Authorization Header :' +A[0].UXF_Token__c);
                        System.debug(response.toString());
                        System.debug('STATUS_CODE:'+response.getStatusCode());
                        System.debug(response.getBody());
                        System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                        System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
                        System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
                        System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
                        System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
                        System.debug('DEBUG 7 ======== obj.Status: ' + obj.status);
                        
                        //Updated by Mahim - Start --------------------------------------------
                        /*Tenant_Equipment__c sbc = new Tenant_Equipment__c();
                        sbc.id=id; 
                        sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
                        sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
                        
                        if(obj.orderID != null) { 
                        sbc.AmDocs_Order_ID__c=obj.orderID; 
                        sbc.Action__c = O[0].Action__c+ ' ' + 'Successful'; 
                        sbc.AmDocs_Transaction_Code__c='SUCCESS'; 
                        sbc.AmDocs_Transaction_Description__c='Disconncet SUCCESSFUL'; 
                        }
                        else if(obj.orderID == null) {
                        sbc.Action__c = O[0].Action__c+ ' ' + 'Failed'; 
                        if(obj.informationMessages != Null) { 
                        sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; 
                        sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription; 
                        }
                        else if(obj.informationMessages == Null) {
                        sbc.Action__c = O[0].Action__c+ ' ' + 'Failed'; 
                        sbc.AmDocs_Transaction_Code__c='ERROR'; 
                        sbc.AmDocs_Transaction_Description__c=O[0].Action__c + ' ERROR something went wrong, please contact your Salesforce Admin';             
                        }
                        }
                        else{} update sbc;*/
                        List<Tenant_Equipment__c> lstUpdateTERecs = new List<Tenant_Equipment__c>();
                        for(Tenant_Equipment__c TE :O){
                            Tenant_Equipment__c sbc = new Tenant_Equipment__c();
                            sbc.id=TE.id; 
                            sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
                            sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
                            
                            if(obj.orderID != null) { 
                                sbc.AmDocs_Order_ID__c=obj.orderID; 
                                //Updated by Mahim - Start
                                //sbc.Action__c = O[0].Action__c+ ' ' + 'Successful'; 
                                sbc.Action__c = 'Disconnected'; 
                                //Updated by Mahim - End
                                sbc.AmDocs_Transaction_Code__c='SUCCESS'; 
                                sbc.AmDocs_Transaction_Description__c='Amdocs Tenant ID: '+ TE.Amdocs_Tenant_ID__c +' - Disconnect SUCCESSFUL';
                                receiversForActiveElseWhereUpdate.add(TE.Name);
                            }
                            
                            if(obj.orderID == null) {
                                //Updated by Mahim - Start
                                //sbc.Action__c = O[0].Action__c+ ' ' + 'Failed'; 
                                sbc.Action__c = 'Active'; 
                                //Updated by Mahim - End
                                if(obj.informationMessages != Null) { 
                                    sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; 
                                    sbc.AmDocs_Transaction_Description__c='Amdocs Tenant ID: '+ TE.Amdocs_Tenant_ID__c +' - '+ obj.informationMessages[0].errorDescription; 
                                }
                                else if(obj.informationMessages == Null) {
                                    //Updated by Mahim - Start
                                    //sbc.Action__c = O[0].Action__c+ ' ' + 'Failed'; 
                                    sbc.Action__c = 'Active'; 
                                    //Updated by Mahim - End 
                                    sbc.AmDocs_Transaction_Code__c='ERROR'; 
                                    sbc.AmDocs_Transaction_Description__c='Amdocs Tenant ID: '+ TE.Amdocs_Tenant_ID__c +' - '+ O[0].Action__c + ' ERROR something went wrong, please contact your Salesforce Admin';             
                                }
                            }
                            lstUpdateTERecs.add(sbc);
                        }
                        if(lstUpdateTERecs.size()>0){
                            try {
                                Update lstUpdateTERecs;
                            } catch(DmlException e) {
                                System.debug('The following exception has occurred: ' + e.getMessage());
                            }
                        }
                        
                        //Milestone #MS-001326 - Mahim - Start
                        //Update the ActiveElseWhere field of new equipment to false if the disconnect/remove is successful
                        if(receiversForActiveElseWhereUpdate.size()>0){
                            teWithActiveElseWhereTrue = [Select Id,Is_active_elsewhere__c,Where_it_s_active_elsewhere__c From Tenant_Equipment__c 
                                                         Where name in: receiversForActiveElseWhereUpdate And Is_active_elsewhere__c = true And Action__c='Add'];
                            for(Tenant_Equipment__c TE: teWithActiveElseWhereTrue){
                                TE.Is_active_elsewhere__c = false;
                                TE.Where_it_s_active_elsewhere__c = null;
                                teForActiveElseWhereUpdate.add(TE);
                            }
                            if(teForActiveElseWhereUpdate.size()>0){
                                Update teForActiveElseWhereUpdate;
                            }
                        }
                        //Milestone #MS-001326 - End
                        
                        //Updated by Mahim - End --------------------------------------------
                        
                        API_Log__c apil2 = new API_Log__c();{ 
                            apil2.Record__c=id; 
                            apil2.Object__c='Tenant Equipment'; 
                            apil2.Status__c=obj.responseStatus; 
                            apil2.Results__c=strjson; 
                            apil2.API__c=O[0].Action__c; 
                            apil2.ServiceID__c=O[0].AmDocs_Tenant_ID__c; 
                            apil2.User__c=UserInfo.getUsername(); 
                            insert apil2; 
                        }
                    } 
                    //Updated by Mahim - Start
                    //else if (response.getStatusCode() == Null || O[0].Reset__c == 'ChewbaccaIsTesting' {
                    else if (response.getStatusCode() == Null || O[0].Reset__c == 'ChewbaccaIsTesting' ||
                             response.getStatusCode() <= 200 || response.getStatusCode() >= 600) { 
                                 //Updated by Mahim - End
                                 
                                 //Added by Mahim - Incase the Action is not updated, reverting the Action back to Active - Start
                                 list<Tenant_Equipment__c> RevertTEAction = new list<Tenant_Equipment__c>();
                                 list<Tenant_Equipment__c> checkTEData = [select id, Action__c from Tenant_Equipment__c 
                                                                          where Amdocs_Tenant_ID__c = :id  AND Action__c = 'DupeDisconnect'];
                                 for(Tenant_Equipment__c TE: checkTEData){
                                     TE.Action__c = 'Active';
                                     RevertTEAction.add(TE);
                                 }
                                 if(RevertTEAction.size()>0)
                                     Update RevertTEAction;
                                 //Added by Mahim - End
                                 
                                 Tenant_Equipment__c sbc = new Tenant_Equipment__c(); {
                                     sbc.id=id;  
                                     sbc.Action__c = O[0].Action__c+ ' ' + 'Time Out'; 
                                     sbc.API_Status__c=String.valueOf(response.getStatusCode());
                                     sbc.AmDocs_Transaction_Description__c=O[0].Action__c + ' ERROR - Missing Response Code';
                                     update sbc; }
                                 
                                 API_Log__c apil2 = new API_Log__c();{ 
                                     apil2.Record__c=id; 
                                     apil2.Object__c='Tenant Equipment'; 
                                     apil2.Status__c='Error'; 
                                     apil2.Results__c=(response.getBody()); 
                                     apil2.API__c=O[0].Action__c; 
                                     apil2.ServiceID__c=O[0].AmDocs_Tenant_ID__c; 
                                     apil2.User__c=UserInfo.getUsername(); 
                                     insert apil2; 
                                 }
                             }
                }
            }
            return returnStatus;
        }
        catch(Exception e) {
            returnStatus = 'The following exception has occurred: ' + e.getMessage();
            return returnStatus;
        }
    }
}