@IsTest
private class LeadReturnToDish_TEST {
    static testMethod void validateLeadReturnToDish_TEST() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_service__c='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Sales_Stage__c ='Returned to Dish - Invalid Lead',
        OwnerID='00G60000001ORIJEA4',        
        Number_of_TVs__c='1 - 2');
        
       Database.DMLOptions dlo1 = new Database.DMLOptions();
       dlo1.EmailHeader.triggerAutoResponseEmail = false;
       dlo1.EmailHeader.triggerOtherEmail = false;
       dlo1.EmailHeader.triggerUserEmail = false;
       dlo1.assignmentRuleHeader.useDefaultRule= false;

      database.insert (L, dlo1);
        
       L = [SELECT OwnerId FROM Lead WHERE Id =:L.Id];
       System.debug('Lead Owner after trigger fired: ' + L.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G60000001ORIJEA4', L.OwnerId);
    }
    static testMethod void validateLeadReturnToDish_TEST2() {
        Lead L2 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Apartment/Condo/Townhome',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Sales_Stage__c ='Returned to Dish - Invalid Lead',
        OwnerID='00G60000001ORIJEA4',        
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo2 = new Database.DMLOptions();
       dlo2.EmailHeader.triggerAutoResponseEmail = false;
       dlo2.EmailHeader.triggerOtherEmail = false;
       dlo2.EmailHeader.triggerUserEmail = false;
       dlo2.assignmentRuleHeader.useDefaultRule= false;

       database.insert (L2, dlo2);
        
       L2 = [SELECT OwnerId FROM Lead WHERE Id =:L2.Id];
       System.debug('Lead Owner after trigger fired: ' + L2.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G60000001ORIJEA4', L2.OwnerId);  
    }
       static testMethod void validateLeadReturnToDish_TEST3() {
        Lead L3 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_service__c='Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Sales_Stage__c ='Returned to Dish - Duplicate',        
        OwnerID='00G60000001ORIJEA4',
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo3 = new Database.DMLOptions();
       dlo3.EmailHeader.triggerAutoResponseEmail = false;
       dlo3.EmailHeader.triggerOtherEmail = false;
       dlo3.EmailHeader.triggerUserEmail = false;
       dlo3.assignmentRuleHeader.useDefaultRule= false;

       database.insert (L3, dlo3);
        
       L3 = [SELECT OwnerId FROM Lead WHERE Id =:L3.Id];
       System.debug('Lead Owner after trigger fired: ' + L3.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G60000001ORIJEA4', L3.OwnerId);
    }
    static testMethod void validateLeadReturnToDish_TEST4() {
        Lead L4 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Apartment/Condo/Townhome',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Sales_Stage__c ='Returned to Dish - Duplicate',        
        OwnerID='00G60000001ORIJEA4',
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo4 = new Database.DMLOptions();
       dlo4.EmailHeader.triggerAutoResponseEmail = false;
       dlo4.EmailHeader.triggerOtherEmail = false;
       dlo4.EmailHeader.triggerUserEmail = false;
       dlo4.assignmentRuleHeader.useDefaultRule= false;
       
       database.insert (L4, dlo4);
        
       L4 = [SELECT OwnerId FROM Lead WHERE Id =:L4.Id];
       System.debug('Lead Owner after trigger fired: ' + L4.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G60000001ORIJEA4', L4.OwnerId);  
    }
    static testMethod void validateLeadReturnToDish_TEST5() {
        Lead L5 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Airports',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Sales_Stage__c ='Returned to Dish - Invalid Lead',  
        OwnerID='00G600000016h6A',      
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo5 = new Database.DMLOptions();
       dlo5.EmailHeader.triggerAutoResponseEmail = false;
       dlo5.EmailHeader.triggerOtherEmail = false;
       dlo5.EmailHeader.triggerUserEmail = false;
       dlo5.assignmentRuleHeader.useDefaultRule= false;
    
        database.insert (L5, dlo5);
        
       L5 = [SELECT OwnerId FROM Lead WHERE Id =:L5.Id];
       System.debug('Lead Owner after trigger fired: ' + L5.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G600000016h6A', L5.OwnerId); 
    }
        static testMethod void validateLeadReturnToDish_TEST6() {
        Lead L6 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Sales_Stage__c ='Returned to Dish - Invalid Lead',
        Lead_Sold__c=False,
        Status='Open',
        Sales_Status__c='Open',
        OwnerID='00G600000016h6A',          
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo6 = new Database.DMLOptions();
       dlo6.EmailHeader.triggerAutoResponseEmail = false;
       dlo6.EmailHeader.triggerOtherEmail = false;
       dlo6.EmailHeader.triggerUserEmail = false;
       dlo6.assignmentRuleHeader.useDefaultRule= false;    
    
        database.insert (L6, dlo6);
        
       L6 = [SELECT OwnerId FROM Lead WHERE Id =:L6.Id];
       System.debug('Lead Owner after trigger fired: ' + L6.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G600000016h6A', L6.OwnerId); 
    }
        static testMethod void validateLeadReturnToDish_TEST7() {
        Lead L7 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='MDU Residential',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        OwnerID='00G600000016h6A',      
        Sales_Stage__c ='Returned to Dish - Invalid Lead',        
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo7 = new Database.DMLOptions();
       dlo7.EmailHeader.triggerAutoResponseEmail = false;
       dlo7.EmailHeader.triggerOtherEmail = false;
       dlo7.EmailHeader.triggerUserEmail = false;
       dlo7.assignmentRuleHeader.useDefaultRule= false;
    
        database.insert (L7, dlo7);
        
       L7 = [SELECT OwnerId FROM Lead WHERE Id =:L7.Id];
       System.debug('Lead Owner after trigger fired: ' + L7.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G600000016h6A', L7.OwnerId); 
    }
        static testMethod void validateLeadReturnToDish_TEST8() {
        Lead L8 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='National Account',
        Category__c='National Account',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Sales_Stage__c ='Returned to Dish - Invalid Lead',        
        OwnerID='00G600000016h6A',      
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo8 = new Database.DMLOptions();
       dlo8.EmailHeader.triggerAutoResponseEmail = false;
       dlo8.EmailHeader.triggerOtherEmail = false;
       dlo8.EmailHeader.triggerUserEmail = false;
       dlo8.assignmentRuleHeader.useDefaultRule= false;
       
        database.insert (L8, dlo8);
        
       L8 = [SELECT OwnerId FROM Lead WHERE Id =:L8.Id];
       System.debug('Lead Owner after trigger fired: ' + L8.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G600000016h6A', L8.OwnerId); 
    }
        static testMethod void validateLeadReturnToDish_TEST9() {
        Lead L9 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Banks',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Sales_Stage__c ='Returned to Dish - Invalid Lead',        
        OwnerID='00G600000016h6A',      
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo9 = new Database.DMLOptions();
       dlo9.EmailHeader.triggerAutoResponseEmail = false;
       dlo9.EmailHeader.triggerOtherEmail = false;
       dlo9.EmailHeader.triggerUserEmail = false;
       dlo9.assignmentRuleHeader.useDefaultRule= false;
       
        database.insert (L9, dlo9);
        
       L9 = [SELECT OwnerId FROM Lead WHERE Id =:L9.Id];
       System.debug('Lead Owner after trigger fired: ' + L9.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G600000016h6A', L9.OwnerId); 
    }
    static testMethod void validateLeadReturnToDish_TEST10() {
        Lead L10 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Airports',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        OwnerID='00G600000016h6A',      
        Sales_Stage__c ='Returned to Dish - Duplicate',        
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo10 = new Database.DMLOptions();
       dlo10.EmailHeader.triggerAutoResponseEmail = false;
       dlo10.EmailHeader.triggerOtherEmail = false;
       dlo10.EmailHeader.triggerUserEmail = false;
       dlo10.assignmentRuleHeader.useDefaultRule= false;
       
        database.insert (L10, dlo10);
        
       L10 = [SELECT OwnerId FROM Lead WHERE Id =:L10.Id];
       System.debug('Lead Owner after trigger fired: ' + L10.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G600000016h6A', L10.OwnerId); 
    }
        static testMethod void validateLeadReturnToDish_TEST11() {
        Lead L11 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        OwnerID='00G600000016h6A',      
        Sales_Stage__c ='Returned to Dish - Duplicate',        
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo11 = new Database.DMLOptions();
       dlo11.EmailHeader.triggerAutoResponseEmail = false;
       dlo11.EmailHeader.triggerOtherEmail = false;
       dlo11.EmailHeader.triggerUserEmail = false;
       dlo11.assignmentRuleHeader.useDefaultRule= false;
       
database.insert (L11, dlo11);
        
       L11 = [SELECT OwnerId FROM Lead WHERE Id =:L11.Id];
       System.debug('Lead Owner after trigger fired: ' + L11.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G600000016h6A', L11.OwnerId); 
    }
        static testMethod void validateLeadReturnToDish_TEST12() {
        Lead L12 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='MDU Residential',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        OwnerID='00G600000016h6A',      
        Sales_Stage__c ='Returned to Dish - Duplicate',        
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo12 = new Database.DMLOptions();
       dlo12.EmailHeader.triggerAutoResponseEmail = false;
       dlo12.EmailHeader.triggerOtherEmail = false;
       dlo12.EmailHeader.triggerUserEmail = false;
       dlo12.assignmentRuleHeader.useDefaultRule= false;
       
       database.insert (L12, dlo12);
        
       L12 = [SELECT OwnerId FROM Lead WHERE Id =:L12.Id];
       System.debug('Lead Owner after trigger fired: ' + L12.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G600000016h6A', L12.OwnerId); 
    }
        static testMethod void validateLeadReturnToDish_TEST13() {
        Lead L13 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='National Account',
        Category__c='National Account',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        OwnerID='00G600000016h6A',      
        Sales_Stage__c ='Returned to Dish - Duplicate',        
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo13 = new Database.DMLOptions();
       dlo13.EmailHeader.triggerAutoResponseEmail = false;
       dlo13.EmailHeader.triggerOtherEmail = false;
       dlo13.EmailHeader.triggerUserEmail = false;
       dlo13.assignmentRuleHeader.useDefaultRule= false;
       
        database.insert (L13, dlo13);
        
       L13 = [SELECT OwnerId FROM Lead WHERE Id =:L13.Id];
       System.debug('Lead Owner after trigger fired: ' + L13.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G600000016h6A', L13.OwnerId); 
    }
        static testMethod void validateLeadReturnToDish_TEST14() {
        Lead L14 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Banks',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        OwnerID='00G600000016h6A',      
        Number_of_Units__c=1,
        Sales_Stage__c ='Returned to Dish - Duplicate',        
        Number_of_TVs__c='1 - 2');

       Database.DMLOptions dlo14 = new Database.DMLOptions();
       dlo14.EmailHeader.triggerAutoResponseEmail = false;
       dlo14.EmailHeader.triggerOtherEmail = false;
       dlo14.EmailHeader.triggerUserEmail = false;
       dlo14.assignmentRuleHeader.useDefaultRule= false;
       
        database.insert (L14, dlo14);
        
       L14 = [SELECT OwnerId FROM Lead WHERE Id =:L14.Id];
       System.debug('Lead Owner after trigger fired: ' + L14.OwnerId);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('00G600000016h6A', L14.OwnerId); 
    }
    
}