public without sharing class GroupSharingHelper {

	public static Map<Id,String> parentAccMap = new Map<Id,String>();
  	public static List<Id> parentIds = new List<Id>();
  	public static Map<Id,String> oldParentAccMap = new Map<Id,String>();
  	public static List<Id> oldParentIds = new List<Id>();
  
  	public static Map<Id, Id> getRoles(List<Id> parIds){
    	Map<Id, Id> accountIdUserRoleIdMap = new Map<Id, Id>();
    	try{
      		for (UserRole uRole : [Select  u.PortalType, u.PortalAccountId, u.Name, u.Id From UserRole u where Name like '%Executive' and PortalType = 'Partner' and PortalAccountId In :parIds]) {
        		accountIdUserRoleIdMap.put(uRole.PortalAccountId, uRole.Id);
      		}
		}catch(Exception e){
	      	System.debug('Exception occured : ' + e.getMessage());
	    }
    	System.debug('accountIdUserRoleIdMap = ' + accountIdUserRoleIdMap);
    	return accountIdUserRoleIdMap;
  	}
  
  	public static Map<Id,Id> getGroups(List<Id> roleIds){
    	//get the group for the above roles
    	Map<Id, Id> gMap = new Map<Id, Id>();
    	List<Group> gList = [Select g.Name, g.Id, g.RelatedId From Group g where RelatedId In :roleIds and Type = 'RoleAndSubordinates'];
    	for (Group g : gList) {
      		gMap.put(g.RelatedId, g.Id);
    	}
    	System.debug('gMap = ' + gMap);
    	return gMap;
  	} 
/*
  	public static List<Id> getRelatedGroupIds(Map<Id,String> accParentMap, Map<Id,Id> accountIdUserRoleIdMap, Map<Id,Id> gMap){
    	List<Id> relatedGroupIds = new List<Id>();
    	List<String> pIds = new List<String>();
    	for(String s : accParentMap.values()){
      		pIds.addAll(s.split(','));
    	}
    	for (Id accId : pIds) {
      		Id roleId = accountIdUserRoleIdMap.get(accId);
      		Id relatedGroupId = gMap.get(roleId);  //getGroup(roleId, gList);
      		relatedGroupIds.add(relatedGroupId);
    	}

    	return relatedGroupIds;    
  	} */
}