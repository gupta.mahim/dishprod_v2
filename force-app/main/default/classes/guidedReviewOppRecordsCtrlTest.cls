/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 16 Sept 2020
Created for: Milestone# MS-000971, MS-001092, MS-003351
Description: Test class for guidedReviewOppRecordsCtrl.apxc
-------------------------------------------------------------*/

@IsTest
public class guidedReviewOppRecordsCtrlTest {
    public static testMethod void testMethod1() {
        
        Account acct1 = new Account();
        acct1.Name = 'Test Account 1';
        acct1.ToP__c = 'Integrator';
        acct1.Pyscical_Address__c = '1011 Collie Path';
        acct1.City__c = 'Round Rock';
        acct1.State__c = 'PR';
        acct1.Zip__c = '78664';
        acct1.Amdocs_CreateCustomer__c = false;
        acct1.Distributor__c = 'PACE';
        acct1.OE_AR__c = '12345';
        insert acct1;
        
        Opportunity o1 = new Opportunity();
        o1.Name = 'Test Opp 1';
        o1.AccountId = acct1.Id;
        o1.LeadSource='Test Place';
        o1.CloseDate=system.Today();
        o1.StageName='Closed Won';
        o1.AmDocs_tenantType__c = 'Incremental';
        o1.Zip__c='78664';
        o1.Smartbox_Leased__c = false;
        o1.Mute_Disconnect__c = '';
        o1.AmDocs_ServiceID__c = '1111111111';
        insert o1;
        
        Hotel_Portfolio_Owner__c HPO = new Hotel_Portfolio_Owner__c();
        HPO.Name = 'Test';
        insert HPO;
        
        Brands__c brand = new Brands__c();
        brand.name = 'Test';
        insert brand;
        
        Sub_Brand__c subBrand = new Sub_Brand__c();
        subBrand.name = 'Test';
        subBrand.Brand__c = brand.Id;
        insert subBrand;
        
        guidedReviewOppRecordsCtrl.getInitDetails(HPO.Id, 'test', brand.Id, subBrand.Id);
    }
}