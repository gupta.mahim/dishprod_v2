public class Sites_Controller_SFLAction {


public String nameQuery {get; set;}
public String nameQuery2 {get; set;}
public string error {get; set;}


    public Sites_Controller_SFLAction (ApexPages.StandardController controller) {

    }
    
            public String getNameQuery() {

        return null;
    }
    
        public String getNameQuery2() {         return null;    }

        public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('-Select-','-Select-'));
            options.add(new SelectOption('Account Change','Existing Account Change'));
            options.add(new SelectOption('Account Pre-Build','Account Pre-Build'));
            options.add(new SelectOption('Service Call','Existing Account Service Call'));
            options.add(new SelectOption('Installation/Activation','Installation / Activation'));
            options.add(new SelectOption('Site Survey','Site Survey'));
            return options;
        }
    
            public List<SelectOption> getProg() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('-Select-','-Select-'));
            options.add(new SelectOption('Public','Public'));
            options.add(new SelectOption('Private','Private'));
            return options;
        }
    
 public PageReference Next(){
 if(nameQuery == '-Select-' || nameQuery2 == '-Select-'){ error='Please select a request type and account type.';  return null;}
else
    {
        if(nameQuery2 == 'Public' && (nameQuery == 'Account Pre-Build' || nameQuery == 'Installation/Activation' || nameQuery == 'Site Survey' ))
            {
             return new PageReference('/Salesforcelite/Sites_SFLCreateOpp?acctId=' + ApexPages.currentPage().getParameters().get('acctId') + 
             '&siu=' + ApexPages.currentPage().getParameters().get('siu') + 
             '&acp=' + ApexPages.currentPage().getParameters().get('acp') + 
             '&type=' + nameQuery + 
             '&cat=' + nameQuery2);
            }
else
    {
        if(nameQuery2 == 'Private' && (nameQuery == 'Account Pre-Build' || nameQuery == 'Installation/Activation' || nameQuery == 'Site Survey'))
            {
             return new PageReference('/Salesforcelite/Sites_SFLCreateOpp?acctId=' + ApexPages.currentPage().getParameters().get('acctId') + 
             '&siu=' + ApexPages.currentPage().getParameters().get('siu') + 
             '&acp=' + ApexPages.currentPage().getParameters().get('acp') + 
             '&type=' + nameQuery + 
             '&cat=' + nameQuery2);
            }



else
{
 return new PageReference('/Salesforcelite/Sites_SFLCase?acctId=' + ApexPages.currentPage().getParameters().get('acctId') + 
 '&siu=' + ApexPages.currentPage().getParameters().get('siu') + 
 '&type=' + nameQuery + 
 '&cat=' + nameQuery2);
}

}
}}}