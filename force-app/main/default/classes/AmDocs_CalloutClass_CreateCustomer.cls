public class AmDocs_CalloutClass_CreateCustomer {

public class person {
    public String customerID;
    public String faID;
    public String barID;
    public String errorCode;
    public String backendErrorMessage;

    }
    Public Class FlowInputs
    {
      
      @Invocablevariable
      Public string id;
    }
     Public Class Flowoutputs
    {
      
        @Invocablevariable
        Public string AmDocs_Transaction_Code;
      
        @Invocablevariable
        Public string AmDocs_Transaction_Description;
       
    }


    @future(callout=true)

    public static void AmDocsMakeCalloutCreateCustomer(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

list<Opportunity> O = [select id, Name, AmDocs_ServiceID__c, Account.Name, Account.Distributor__c, Name_of_Property_Representative__c, First_Name_of_Property_Representative__c, Billing_Contact_Phone__c, Billing_Contact_Email__c, Account.OE_AR__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, AmDocs_Site_Id__c, Address__c, City__c, State__c, Zip__c, Business_Address__c, Business_City__c, Business_State__c, Business_Legal_Name__c, Business_Zip_Code__c, Business_Phone_Number__c, Property_Representative_s_email__c from Opportunity where Id = :id AND Account.OE_AR__c != NULL];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);
     
list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        jsonObj.writeFieldName('ImplCreateFullCustomerSetUpInput');
            jsonObj.writeStartObject();
                 jsonObj.writeFieldName('customerDetails');
                     for(Integer i = 0; i < O.size(); i++){
                         jsonObj.writeStartObject();
                             jsonObj.writeStringField('name', O[i].Name);
                             System.debug('Jerry Debug 0 : '   + O[i].Name);
                             System.debug('Jerry Debug 1 : '   + O[i].AmDocs_Customer_Type__c);
                             if(O[i].AmDocs_Customer_Type__c == 'Integrator'){ jsonObj.writeStringField('type', 'I');
                            }
                            else
                            if(O[i].AmDocs_Customer_Type__c == 'PCO'){ jsonObj.writeStringField('type', 'P');
                            }
                            else
                            if(O[i].AmDocs_Customer_Type__c == 'Direct'){ jsonObj.writeStringField('type', 'D');
                            }
                            else
                            if(O[i].AmDocs_Customer_Type__c == 'Retailer'){ jsonObj.writeStringField('type', 'T');
                            }
                            jsonObj.writeStringField('subType', O[i].AmDocs_Customer_SubType__c);
                                System.debug('Jerry Debug 3 : '   + O[i].AmDocs_Customer_SubType__c);
                                System.debug('Jerry Debug 4 : '   + O[i].Account.OE_AR__c);
//                            if(O[i].Account.OE_AR__c != '' && O[i].Account.OE_AR__c != null ){
//                                jsonObj.writeStringField('dealerCode', O[i].Account.OE_AR__c);
////                            }
//                               System.debug('Jerry Debug 5 : '   + O[i].Account.Distributor__c);
                           if(O[i].Account.Distributor__c != '' && O[i].Account.Distributor__c != null ){ if(O[i].Account.Distributor__c == 'PACE' ){jsonObj.writeStringField('distributorCode', '21493339'); } else if(O[i].Account.Distributor__c == 'SMS' ){ jsonObj.writeStringField('distributorCode', '9325'); } else if(O[i].Account.Distributor__c == '4COM' ){jsonObj.writeStringField('distributorCode', '71701'); } else if(O[i].Account.Distributor__c == 'CVS Systems Incorporated' ){jsonObj.writeStringField('distributorCode', '1001'); } else if(O[i].Account.Distributor__c == 'All Systems Satellite' ){jsonObj.writeStringField('distributorCode', '21844'); } else if(O[i].Account.Distributor__c == 'DOW Electronics' ){jsonObj.writeStringField('distributorCode', '1007'); } else if(O[i].Account.Distributor__c == 'RS&I' ){jsonObj.writeStringField('distributorCode', '1024'); } else if(O[i].Account.Distributor__c == 'Mid-State Distributing' ){jsonObj.writeStringField('distributorCode', '23439'); }
                           }
                           jsonObj.writeEndObject();
                        }
                
                   jsonObj.writeFieldName('customerAddress');
                       for(Integer i = 0; i < O.size(); i++){
                           System.debug('Jerry Debug 6 : '   + O[i].Business_Address__c);
                           System.debug('Jerry Debug 7 : '   + O[i].Business_City__c);
                           System.debug('Jerry Debug 8 : '   + O[i].Business_State__c);
                           System.debug('Jerry Debug 9 : '   + O[i].Business_Zip_Code__c);
                           jsonObj.writeStartObject();
                               jsonObj.writeStringField('state', O[i].Business_State__c);
                               jsonObj.writeStringField('country', 'US');
                               jsonObj.writeStringField('city', O[i].Business_City__c);            
                               jsonObj.writeStringField('addressLine1', O[i].Business_Address__c);            
                               // address 2
                               // address format
                               // geocode
                               jsonObj.writeStringField('zipCode', O[i].Business_Zip_Code__c);
                               // zip +4
                           jsonObj.writeEndObject();
                       }
            
                   jsonObj.writeFieldName('customerAccount');
                       for(Integer i = 0; i < O.size(); i++){
                           System.debug('Jerry Debug 10 : '   + O[i].Account.Name);
                           System.debug('Jerry Debug 10.5 : '   + O[i].Name);
                           jsonObj.writeStartObject();
                           if(O[i].AmDocs_Customer_Type__c == 'Direct' || O[i].AmDocs_Customer_Type__c == 'Retailer') {
                                   jsonObj.writeStringField('name', O[i].Name);
                           }
                           else if(O[i].AmDocs_Customer_Type__c == 'Integrator' || O[i].AmDocs_Customer_Type__c == 'PCO') { 
                                 jsonObj.writeStringField('name', O[i].Account.Name);
                           }
                           jsonObj.writeEndObject();
                       }

                  jsonObj.writeFieldName('customerBillingData');
                       for(Integer i = 0; i < O.size(); i++){
                           System.debug('Jerry Debug 11 : '   + O[i].Name);
                           jsonObj.writeStartObject();
                               jsonObj.writeStringField('faName', O[i].Name + ' Finanical Account');
                               jsonObj.writeStringField('barName', O[i].Name + ' Billing Arrangement');
                               jsonObj.writeStringField('billFormat', 'CSV');
                           jsonObj.writeEndObject();
                       }

                   jsonObj.writeFieldName('customerContact');
                       for(Integer i = 0; i < O.size(); i++){
                           System.debug('Jerry Debug 12 : '   + O[i].First_Name_of_Property_Representative__c);
                           System.debug('Jerry Debug 13 : '   + O[i].Name_of_Property_Representative__c);
                           System.debug('Jerry Debug 14 : '   + O[i].Billing_Contact_Phone__c);
                           System.debug('Jerry Debug 15 : '   + O[i].Billing_Contact_Email__c);
                           jsonObj.writeStartObject();
                               // Salutation
                               jsonObj.writeStringField('firstName', O[i].First_Name_of_Property_Representative__c);
                               // Middle Name
                               jsonObj.writeStringField('lastName', O[i].Name_of_Property_Representative__c);
                               jsonObj.writeStringField('phone', O[i].Billing_Contact_Phone__c);
//                               jsonObj.writeStringField('phone', '5122965584');
                               jsonObj.writeStringField('email', O[i].Billing_Contact_Email__c);
                           jsonObj.writeEndObject();
                   }
           jsonObj.writeEndObject();
    

String finalJSON = jsonObj.getAsString();
System.debug('Jerry Debug 16 === json string: '   + finalJSON);
   
// if (!Test.isRunningTest()){       
        HttpRequest request = new HttpRequest();
              String endpoint = A[0].End_Point_Environment__c+'/commerce/createCustomer?lo=EN';
                 request.setEndPoint(endpoint);        
                 request.setBody(jsonObj.getAsString());
                 request.setTimeout(120000);
                 request.setHeader('Content-Type', 'application/json');
                 request.setHeader('Accept', 'application/json'); 
                 request.setHeader('User-Agent', 'SFDC-Callout/45.0');
                 request.setMethod('POST');
                 String authorizationHeader = A[0].UXF_Token__c ;
                 request.setHeader('Authorization', authorizationHeader);

         HttpResponse response = new HTTP().send(request);
                 System.debug(response.toString());
                 System.debug('STATUS:'+response.getStatus());
                 System.debug('STATUS_CODE:'+response.getStatusCode());
                 System.debug(response.getBody());
                 request.setTimeout(101000);
                
                 if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) {
                     String strjson = response.getbody();
                     JSONParser parser = JSON.createParser(strjson);
                     // Make calls to nextToken()  // to point to the second // start object marker.
                     parser.nextToken(); parser.nextToken(); parser.nextToken();

                     // Retrieve the Person object from the JSON string.
                     person obj = (person)parser.readValueAs( person.class);
                         System.debug('DEBUG 17 ======== 1st STRING: ' + strjson);
                         System.debug('DEBUG 18 ======== obj.siteID: ' + obj.customerID);
                         System.debug('DEBUG 19 ======== obj.faID: ' + obj.faID);
                         System.debug('DEBUG 20 ======== obj.barID: ' + obj.barID);
                    
                               Opportunity sbc = new Opportunity();
                                   sbc.id=id;
                                   sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
                                   sbc.AmDocs_CustomerID__c=obj.customerID;
                                   sbc.AmDocs_faID__c=obj.faID;
                                   sbc.AmDocs_barID__c=obj.barID;
                                   sbc.API_Status__c=String.valueOf(response.getStatusCode());
                                   if(obj.customerID != null){
                                       sbc.AmDocs_Transaction_Code__c='SUCCESS';
                                       sbc.AmDocs_Transaction_Description__c='CUSTOMER ID, FINANCIAL ACCOUNT ID, and  BILLING ARRANGEMENT ID CREATED';
                                   }
                                   if(obj.backendErrorMessage != null){ sbc.AmDocs_Transaction_Code__c='FAILED'; sbc.AmDocs_Transaction_Description__c=obj.backendErrorMessage; }
                                    
                                    update sbc;
                                    System.debug('sbc'+sbc);
                }
                else if (response.getStatusCode() <= 200 && response.getStatusCode() >= 600) { Opportunity sbc = new Opportunity(); sbc.id=id; sbc.API_Status__c=String.valueOf(response.getStatusCode()); sbc.AmDocs_FullString_Return__c=String.valueOf(response.getBody()) + ' ' + system.now();    update sbc;
             }
     //  }
    }
     @InvocableMethod(label='Create Customer')
      public static List<Flowoutputs> AmDocsMakeCalloutCreateCustomerfromflow(List<FlowInputs>req) 
      {
          List<FlowOutputs> result= new List<Flowoutputs>();
          flowoutputs output = new flowoutputs();
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

list<Opportunity> O = [select id, Name, AmDocs_ServiceID__c, Account.Name, Account.Distributor__c, Name_of_Property_Representative__c, First_Name_of_Property_Representative__c, Billing_Contact_Phone__c, Billing_Contact_Email__c, Account.OE_AR__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, AmDocs_Site_Id__c, Address__c, City__c, State__c, Zip__c, Business_Address__c, Business_City__c, Business_State__c, Business_Legal_Name__c, Business_Zip_Code__c, Business_Phone_Number__c, Property_Representative_s_email__c from Opportunity where Id = :req[0].id AND Account.OE_AR__c != NULL];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);
     
list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        jsonObj.writeFieldName('ImplCreateFullCustomerSetUpInput');
            jsonObj.writeStartObject();
                 jsonObj.writeFieldName('customerDetails');
                     for(Integer i = 0; i < O.size(); i++){
                         jsonObj.writeStartObject();
                             jsonObj.writeStringField('name', O[i].Name);
                             System.debug('Jerry Debug 0 : '   + O[i].Name);
                             System.debug('Jerry Debug 1 : '   + O[i].AmDocs_Customer_Type__c);
                             if(O[i].AmDocs_Customer_Type__c == 'Integrator'){ jsonObj.writeStringField('type', 'I');
                            }
                            else
                            if(O[i].AmDocs_Customer_Type__c == 'PCO'){ jsonObj.writeStringField('type', 'P');
                            }
                            else
                            if(O[i].AmDocs_Customer_Type__c == 'Direct'){ jsonObj.writeStringField('type', 'D');
                            }
                            else
                            if(O[i].AmDocs_Customer_Type__c == 'Retailer'){ jsonObj.writeStringField('type', 'T');
                            }
                            jsonObj.writeStringField('subType', O[i].AmDocs_Customer_SubType__c);
                                System.debug('Jerry Debug 3 : '   + O[i].AmDocs_Customer_SubType__c);
                                System.debug('Jerry Debug 4 : '   + O[i].Account.OE_AR__c);
//                            if(O[i].Account.OE_AR__c != '' && O[i].Account.OE_AR__c != null ){
//                                jsonObj.writeStringField('dealerCode', O[i].Account.OE_AR__c);
////                            }
//                               System.debug('Jerry Debug 5 : '   + O[i].Account.Distributor__c);
                           if(O[i].Account.Distributor__c != '' && O[i].Account.Distributor__c != null ){ if(O[i].Account.Distributor__c == 'PACE' ){jsonObj.writeStringField('distributorCode', '21493339'); } else if(O[i].Account.Distributor__c == 'SMS' ){ jsonObj.writeStringField('distributorCode', '9325'); } else if(O[i].Account.Distributor__c == '4COM' ){jsonObj.writeStringField('distributorCode', '71701'); } else if(O[i].Account.Distributor__c == 'CVS Systems Incorporated' ){jsonObj.writeStringField('distributorCode', '1001'); } else if(O[i].Account.Distributor__c == 'All Systems Satellite' ){jsonObj.writeStringField('distributorCode', '21844'); } else if(O[i].Account.Distributor__c == 'DOW Electronics' ){jsonObj.writeStringField('distributorCode', '1007'); } else if(O[i].Account.Distributor__c == 'RS&I' ){jsonObj.writeStringField('distributorCode', '1024'); } else if(O[i].Account.Distributor__c == 'Mid-State Distributing' ){jsonObj.writeStringField('distributorCode', '23439'); }
                           }
                           jsonObj.writeEndObject();
                        }
                
                   jsonObj.writeFieldName('customerAddress');
                       for(Integer i = 0; i < O.size(); i++){
                           System.debug('Jerry Debug 6 : '   + O[i].Business_Address__c);
                           System.debug('Jerry Debug 7 : '   + O[i].Business_City__c);
                           System.debug('Jerry Debug 8 : '   + O[i].Business_State__c);
                           System.debug('Jerry Debug 9 : '   + O[i].Business_Zip_Code__c);
                           jsonObj.writeStartObject();
                               jsonObj.writeStringField('state', O[i].Business_State__c);
                               jsonObj.writeStringField('country', 'US');
                               jsonObj.writeStringField('city', O[i].Business_City__c);            
                               jsonObj.writeStringField('addressLine1', O[i].Business_Address__c);            
                               // address 2
                               // address format
                               // geocode
                               jsonObj.writeStringField('zipCode', O[i].Business_Zip_Code__c);
                               // zip +4
                           jsonObj.writeEndObject();
                       }
            
                   jsonObj.writeFieldName('customerAccount');
                       for(Integer i = 0; i < O.size(); i++){
                           System.debug('Jerry Debug 10 : '   + O[i].Account.Name);
                           System.debug('Jerry Debug 10.5 : '   + O[i].Name);
                           jsonObj.writeStartObject();
                           if(O[i].AmDocs_Customer_Type__c == 'Direct' || O[i].AmDocs_Customer_Type__c == 'Retailer') {
                                   jsonObj.writeStringField('name', O[i].Name);
                           }
                           else if(O[i].AmDocs_Customer_Type__c == 'Integrator' || O[i].AmDocs_Customer_Type__c == 'PCO') { 
                                 jsonObj.writeStringField('name', O[i].Account.Name);
                           }
                           jsonObj.writeEndObject();
                       }

                   jsonObj.writeFieldName('customerBillingData');
                       for(Integer i = 0; i < O.size(); i++){
                           System.debug('Jerry Debug 11 : '   + O[i].Name);
                           jsonObj.writeStartObject();
                               jsonObj.writeStringField('faName', O[i].Name + ' Finanical Account');
                               jsonObj.writeStringField('barName', O[i].Name + ' Billing Arrangement');
                               jsonObj.writeStringField('billFormat', 'CSV');
                           jsonObj.writeEndObject();
                       }

                  jsonObj.writeFieldName('customerContact');
                       for(Integer i = 0; i < O.size(); i++){
                           System.debug('Jerry Debug 12 : '   + O[i].First_Name_of_Property_Representative__c);
                           System.debug('Jerry Debug 13 : '   + O[i].Name_of_Property_Representative__c);
                           System.debug('Jerry Debug 14 : '   + O[i].Billing_Contact_Phone__c);
                           System.debug('Jerry Debug 15 : '   + O[i].Billing_Contact_Email__c);
                           jsonObj.writeStartObject();
                               // Salutation
                               jsonObj.writeStringField('firstName', O[i].First_Name_of_Property_Representative__c);
                               // Middle Name
                               jsonObj.writeStringField('lastName', O[i].Name_of_Property_Representative__c);
                               jsonObj.writeStringField('phone', O[i].Billing_Contact_Phone__c);
//                               jsonObj.writeStringField('phone', '5122965584');
                               jsonObj.writeStringField('email', O[i].Billing_Contact_Email__c);
                           jsonObj.writeEndObject();
                   }
           jsonObj.writeEndObject();
    

String finalJSON = jsonObj.getAsString();
System.debug('Jerry Debug 16 === json string: '   + finalJSON);
   
// if (!Test.isRunningTest()){       
        HttpRequest request = new HttpRequest();
              String endpoint = A[0].End_Point_Environment__c+'/commerce/createCustomer?lo=EN';
                 request.setEndPoint(endpoint);        
                 request.setBody(jsonObj.getAsString());
                 request.setTimeout(120000);
                 request.setHeader('Content-Type', 'application/json');
                 request.setHeader('Accept', 'application/json'); 
                 request.setHeader('User-Agent', 'SFDC-Callout/45.0');
                 request.setMethod('POST');
                 String authorizationHeader = A[0].UXF_Token__c ;
                 request.setHeader('Authorization', authorizationHeader);

         HttpResponse response = new HTTP().send(request);
                 System.debug(response.toString());
                 System.debug('STATUS:'+response.getStatus());
                 System.debug('STATUS_CODE:'+response.getStatusCode());
                 System.debug(response.getBody());
                 request.setTimeout(101000);
                
                 if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) {
                     String strjson = response.getbody();
                     JSONParser parser = JSON.createParser(strjson);
                     // Make calls to nextToken()  // to point to the second // start object marker.
                     parser.nextToken(); parser.nextToken(); parser.nextToken();

                     // Retrieve the Person object from the JSON string.
                     person obj = (person)parser.readValueAs( person.class);
                         System.debug('DEBUG 17 ======== 1st STRING: ' + strjson);
                         System.debug('DEBUG 18 ======== obj.siteID: ' + obj.customerID);
                         System.debug('DEBUG 19 ======== obj.faID: ' + obj.faID);
                         System.debug('DEBUG 20 ======== obj.barID: ' + obj.barID);
                    
                               Opportunity sbc = new Opportunity();
                                   sbc.id=req[0].id;
                                   sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
                                   sbc.AmDocs_CustomerID__c=obj.customerID;
                                   sbc.AmDocs_faID__c=obj.faID;
                                   sbc.AmDocs_barID__c=obj.barID;
                                   sbc.API_Status__c=String.valueOf(response.getStatusCode());
                                   if(obj.customerID != null){
                                       sbc.AmDocs_Transaction_Code__c='SUCCESS';
                                       output.AmDocs_Transaction_Code='SUCCESS';
                                       sbc.AmDocs_Transaction_Description__c='CUSTOMER ID, FINANCIAL ACCOUNT ID, and  BILLING ARRANGEMENT ID CREATED';
                                       output.AmDocs_Transaction_Description='CUSTOMER ID, FINANCIAL ACCOUNT ID, and  BILLING ARRANGEMENT ID CREATED';                                
                                   }
                                   if(obj.backendErrorMessage != null){
                                      sbc.AmDocs_Transaction_Code__c='FAILED';
                                      output.AmDocs_Transaction_Code='FAILED';
                                      sbc.AmDocs_Transaction_Description__c=obj.backendErrorMessage;
                                     output.AmDocs_Transaction_Description=obj.backendErrorMessage;
                                   }
                                    
                                    update sbc;
                                    System.debug('sbc'+sbc);
                }
                else if (response.getStatusCode() <= 200 && response.getStatusCode() >= 600) { Opportunity sbc = new Opportunity(); sbc.id=req[0].id; sbc.API_Status__c=String.valueOf(response.getStatusCode()); sbc.AmDocs_FullString_Return__c=String.valueOf(response.getBody()) + ' ' + system.now();    update sbc;
             }
     //  }
     result.add(output);
     return result;
    }
}