/*
* @author  Utkarsh Gupta <utkarsh.gupta@dish.com>
* It is a controller class of oppValidateAddressPartner Lightning Component
* It contains the methods which has logic to call API Services and do the address validation based on certain criterias
*/

Public class ValidateOpptyAddressCtrlComp
{
    @AuraEnabled
    public static Id currentOppId{get;set;}
    @AuraEnabled
    public static boolean callNetQual{get;set;}
    @AuraEnabled
    public static boolean callBulk{get;set;}
    @AuraEnabled
    public static boolean validation_msg_exist{get;set;}
    
    /* 
* This method checks the criteria on the Opportunity record to set the boolean values in callNetQual and callBulk variables 
* and then further do the API calls to CallNetQual and callBulk functionalty.
* The callBulk and callNetQual variables on page sets the value on the lightning component which renders the message on the page.
*/
    
    @AuraEnabled
    public static NetQualData getValidateOpptyAddress(String recId)
    {
        NetQualData NetQualDataObj = new NetQualData ();
        // inProgress=true;
        callBulk = false;
        callNetQual = false;
        currentOppId=recId;
        
        Opportunity currentOpp=[select id,Property_Status__c,Amdocs_CustomerID__c,AmDocs_tenantType__c,AmDocs_ServiceID__c,AmDocs_SiteID__c,Invoke_NetQual__c,Override_NetQual__c,Valid_Address__c, Address_Validation_Message__c from Opportunity where id=:currentOppId];
        
        boolean invokeNetQual=currentOpp.Invoke_NetQual__c;
        boolean overrideNetQual=currentOpp.Override_NetQual__c;
        boolean validAddress=currentOpp.Valid_Address__c;
        System.debug('invokeNetQual='+invokeNetQual+', overrideNetQual='+overrideNetQual+', validAddress='+validAddress);
        //Milestone: MS-001253 - Utkarsh - Start
        if(overrideNetQual==false && validAddress==false && invokeNetQual==true && currentOpp.AmDocs_ServiceID__c == Null)
        //Milestone: MS-001253 - Utkarsh - End
        {
            callNetQual=true;
        }
        else
        {
            callNetQual=false;
            callBulk = validateOppForBulk(currentOpp);
        }
        
        /*      String validation_msg = currentOpp.Address_Validation_Message__c;
if (validation_msg != null && validation_msg != ''){
validation_msg_exist = true;
}
else
{
validation_msg_exist = false;
}*/
        if(callNetQual ==  true){
            ValidateAdressCtrl.validateOppty(currentOppId);
        }       
        if(callBulk ==  true){
            callBulkAPI();
        }
        validation_msg_exist = false;
        Address_Valid_Msg__c Addrs1 = Address_Valid_Msg__c.getValues(recId);
        system.debug('Addrs1'+Addrs1);
        if(Addrs1 != null){
            if (Addrs1.ValidMessage__c != null && Addrs1.ValidMessage__c != ''){
                validation_msg_exist = true;
                NetQualDataObj.validationMsgValueWr = Addrs1.ValidMessage__c;
            }else{
                validation_msg_exist = false;
            }
        }
        
        // NetQualDataObj.vMsgWr = validation_msg_exist;
        system.debug('callNetQual'+callNetQual);
        NetQualDataObj.overRideNetQualWr = overrideNetQual;
        NetQualDataObj.callNetQWr = callNetQual;
        NetQualDataObj.callBWr = callBulk;
        NetQualDataObj.validationMsgWr = validation_msg_exist;
        //    NetQualDataObj.validationMsgValueWr = validation_msg;        
        return NetQualDataObj;
    }
    
    /* 
* This method calls the Get Tenant By Bulk on page load whenever callBulk variable is set to true
*/    
    @auraEnabled
    public static void callBulkAPI()
    {  
        System.debug(callBulk+'Calling Get Tenant by Bulk on opp page load...');
        if(callBulk)
        {
            GetTenantByBulkCtrl.UpdateDirectOppApiStatus(currentOppId);
        }
        
    }   
    
    @AuraEnabled
    public static boolean validateOppForBulk(Opportunity opp)
    {
        String oppStatus=opp.Property_Status__c;
        String oppBulkerviceId=opp.AmDocs_ServiceID__c;
        Boolean validAddress=opp.Valid_Address__c;
        String oppSiteId=opp.AmDocs_SiteID__c;
        String oppCustomerId=opp.Amdocs_CustomerID__c;
        String oppTenantType=opp.AmDocs_tenantType__c;
        boolean overrideNetQual=opp.Override_NetQual__c;
        
        System.debug('oppStatus=['+oppStatus+'],oppBulkerviceId=[='+oppBulkerviceId+'],validAddress=['+validAddress+'], callNetQual=['+callNetQual+']');
        System.debug('oppSiteId=['+oppSiteId+'], oppCustomerId='+oppCustomerId+'],oppTenantType=['+oppTenantType+']');
        
        
        if(!(String.isEmpty(oppStatus) && String.isEmpty(oppBulkerviceId)) && (validAddress==true ||overrideNetQual==true ))
        {
            System.debug('opp'+opp.id);
            List<Tenant_Equipment__c> oppEquip=[select Id from Tenant_Equipment__c where Opportunity__c=:opp.id and Action__c='Active' and Amdocs_Tenant_ID__c=null];
            
            System.debug('Active with missing Service Id equip=['+oppEquip.size()+']');
            if(oppEquip.size()>0)
            {
                Datetime dt1 = System.Now();
                List<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateGetTenantByBulk__c from API_LOG__c where ServiceID__c = :opp.Amdocs_ServiceID__c AND NextAvailableActivateGetTenantByBulk__c >= :dt1 Order By NextAvailableActivateGetTenantByBulk__c  DESC Limit 1];
                System.debug('Active timer=['+timer.size()+']');
                if(timer.size() < 1)
                {
                    callBulk=true;
                    System.debug('Opp qualified for Get Tenant by Bulk');
                }
            }
        }
        return callBulk;
    }
    
    /* 
* This method removes the validation message from the opportunity record and sets the value to the custom Setting 'Address_Valid_Msg__c'.
*/ 
    
    @AuraEnabled
    public static void removeValidationMsg(String recId, Boolean callNQl, Boolean  vMsgExist)
    {
        currentOppId = recId;
        System.debug('Inside "removeValidationMsg" function');
        System.debug('validation_msg_exist : ' + vMsgExist + ' && callNetQual : ' + callNQl);
        String validationMsgRemove;
        List<Address_Valid_Msg__c> addList = new List<Address_Valid_Msg__c>(); 
        if(callNQl == true)
        {
            Opportunity currentOpp=[Select Id, Address_Validation_Message__c from Opportunity where Id=:currentOppId];
            
            Address_Valid_Msg__c Addrs2 = Address_Valid_Msg__c.getValues(recId);
            system.debug('Addrs2'+Addrs2);
            if(Addrs2 == null){
                Address_Valid_Msg__c Addrs = new Address_Valid_Msg__c();
                Addrs.Name = recId;    
                Addrs.ValidMessage__c = currentOpp.Address_Validation_Message__c;
                addList.add(Addrs);
            }
            
            currentOpp.Address_Validation_Message__c = null;
            try{
                Insert addList;
                update currentOpp;
            }catch(Exception e){
                System.debug('Exception message is : ' + e.getMessage());
            }
        }
       
    }
    
    /* 
* This method deleted the custom setting record if the validation message is present on the record.
*/    
    @AuraEnabled
    public static String updateValidationMsg(String recId, Boolean callNQl, Boolean  vMsgExist)
    {
        currentOppId = recId;
        System.debug('Inside "removeValidationMsg" function');
        System.debug('validation_msg_exist : ' + vMsgExist + ' && callNetQual : ' + callNQl);
        String validationMsgRemove;
        if(vMsgExist == true && callNQl == false)
        {
            Address_Valid_Msg__c AddrsMsg = Address_Valid_Msg__c.getValues(recId);
            //  system.debug('AddrsMsg'+AddrsMsg);
            //AddrsMsg.ValidMessage__c = null;
            if(AddrsMsg != null){
                try{
                    delete AddrsMsg;
                    //    validationMsgRemove = AddrsMsg.ValidMessage__c;
                }catch(Exception e){
                    System.debug('Exception message is : ' + e.getMessage());
                }                
            }
        }
        return null;
    }
    /* 
* It is a wrapper class to store the variables of getValidateOpptyAddress.
*/     
    public class NetQualData
    {
        @AuraEnabled
        public boolean callNetQWr {get;set;}
        
        @AuraEnabled
        public boolean callBWr {get;set;}
        
        @AuraEnabled
        public boolean validationMsgWr{get;set;}
        
        @AuraEnabled
        public boolean overRideNetQualWr{get;set;}
        
        @AuraEnabled
        public String validationMsgValueWr{get;set;}
        
    }
    
    
}