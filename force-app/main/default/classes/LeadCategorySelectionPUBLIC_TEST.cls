@IsTest
private class LeadCategorySelectionPUBLIC_TEST {
    static testMethod void validateLeadCategorySelectionPUBLIC() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Airports',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

       insert L;
        
       L = [SELECT Category__c FROM Lead WHERE Id =:L.Id];
       System.debug('Category after trigger fired: ' + L.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', L.Category__c);
    }
     static testMethod void validateLeadCategorySelectionPUBLIC2() {
        Lead LL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Lobbies',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LL;
        
       LL = [SELECT Category__c FROM Lead WHERE Id =:LL.Id];
       System.debug('Category after trigger fired: ' + LL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC3() {
        Lead LLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Beauty Salon',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLL;
        
       LLL = [SELECT Category__c FROM Lead WHERE Id =:LLL.Id];
       System.debug('Category after trigger fired: ' + LLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC4() {
        Lead LLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Golf Course',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLL;
        
       LLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLL.Id];
       System.debug('Category after trigger fired: ' + LLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC5() {
        Lead LLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Bar/Restaurant',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLL;
        
       LLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC6() {
        Lead LLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retirement Community',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLL;
        
       LLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC7() {
        Lead LLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Assisted Living',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLL;
        
       LLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC9() {
        Lead LLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLL;
        
       LLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC10() {
        Lead LLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='RV Park',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLL;
        
       LLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC11() {
        Lead LLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Marinas',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLL;
        
       LLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC12() {
        Lead LLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Casinos',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLL;
        
       LLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC13() {
        Lead LLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hotel/Motel',
        Location_of_Service__c ='Food Service',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLL;
        
       LLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC14() {
        Lead LLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='RV Park',
        Location_of_Service__c ='Food Service',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC15() {
        Lead LLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Marinas',
        Location_of_Service__c ='Food Service',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC16() {
        Lead LLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='RV Park',
        Location_of_Service__c ='Other',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC17() {
        Lead LLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Marinas',
        Location_of_Service__c ='Other',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPUBLIC18() {
        Lead LLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='University',
        Location_of_Service__c ='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Public', LLLLLLLLLLLLLLLLLL.Category__c);
}
}