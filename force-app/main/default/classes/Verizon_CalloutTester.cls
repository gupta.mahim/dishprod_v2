@IsTest(seeAllData=true)

private class Verizon_CalloutTester{
static testmethod void testSuspend(){

// Create some fake data for testing with
    Account a = new Account();
            a.Name = 'Test Account';
     insert a;
     System.debug('DEBUG Account INFO' +a);
             
    Opportunity o = new Opportunity();
                o.Name = 'Test';
                o.AccountId = a.Id;
                o.Zip__c = '78664';
                o.Address__c = '1011 Collie Path';
                o.City__c = 'Round Rock';
                o.State__c = 'TX';
                o.CSG_Account_Number__c = '925570701234';
                o.CloseDate=system.Today();
                o.StageName='Closed Won';
         insert o;
         System.debug('DEBUG Verizon Suspend Opportunity INFO' +o);
       
    Smartbox__c e = new Smartbox__c();
                e.Chassis_Serial__c = '12345';
                e.Serial_Number__c = 'A100001574C254';
                e.kind__c = 'meid';
                e.ModemID__c = '12349812312312';
                e.Opportunity__c = o.id;
                e.Verizon_ServiceState__c = 'Suspend';
                e.ICCID__c = 'ICCID';
                e.ID_4G__c = 'B123456789';
         insert e;
         System.debug('DEBUG Verizon Suspend  Smartbox INFO' +e);
         
     Verizon__c v1 = new Verizon__c();
                v1.Name = '(010d93096921f3ddee58ef103bc479e9, am_application_scope default, Bearer, 3600)';
                v1.IS_M2M_Token__c=true;
                v1.VID__c=e.Id;
                v1.Smartbox__c = e.id;
         insert v1;
         System.debug('DEBUG Verizon Suspend M2M INFO' +v1);
         
     Verizon__c v2 = new Verizon__c();
                v2.Name = '(38cd68a7-76d6-4c6c-99c5-1cbc10beb922)';
                v2.IS_VZ_Token__c=true;
                v2.VID__c=e.Id;
                v2.Smartbox__c = e.id;
         insert v2;
         System.debug('DEBUG Verizon Suspend VZT INFO' +v2);
     
    list<Smartbox__c> S = [select id, Verizon_ServiceState__c, kind__c, ModemID__c from Smartbox__c where Id = :e.id AND Serial_Number__c = 'A100001574C254' LIMIT 1 ];
    System.debug('Debug Verizon Suspend RESULTS of the LIST lookup to the Verizon Suspend object' +S);

//                S[0].Verizon_Do_It_Action__c = true;         
//                System.debug('Debug Verizon Suspend RESULTS of the LIST lookup to the Verizon Suspend object' +S);
//         update S;

            Set<Id> sIds = new Set<Id>();
               sIds.add(S[0].Id);
               Verizon_CalloutClass_Suspend.makeCalloutSuspend(S[0].Id);
               Verizon_CalloutClass_Restore.makeCalloutRestore(S[0].Id);
               Verizon_CalloutClass_SMS_Up120.makeCalloutSMS_Up120(S[0].Id);
               Verizon_CalloutClass_SMS_Up.makeCalloutSMS_Up(S[0].Id);
               Verizon_CalloutClass_SMS_Down.makeCalloutSMS_Down(S[0].Id);
               Verizon_CalloutClass_Deactivate.makeCalloutDeactivate(S[0].Id);               
               Verizon_CalloutClass_Activate.makeCalloutActivate(S[0].Id);   
               Verizon_CalloutClass_CreateAccount.makeCalloutCreateAccount(S[0].Id); 
               Verizon_CalloutClass_GetM2MToken.makeCalloutM2M(S[0].Id); 
               Verizon_CalloutClass_GetVZToken.makeCallout(S[0].Id); 
               
               
               Verizon_CalloutClass_Suspend_4G.makeCalloutSuspend(S[0].Id);
               Verizon_CalloutClass_Restore_4G.makeCalloutRestore(S[0].Id);
               Verizon_CalloutClass_SMS_Up120_4G.makeCalloutSMS_Up120(S[0].Id);
               Verizon_CalloutClass_SMS_Up_4G.makeCalloutSMS_Up(S[0].Id);
               Verizon_CalloutClass_SMS_Down_4G.makeCalloutSMS_Down(S[0].Id);
               Verizon_CalloutClass_Deactivate_4G.makeCalloutDeactivate(S[0].Id);               
               Verizon_CalloutClass_Activate_4G.makeCalloutActivate(S[0].Id);                  
}

}