public class AmDocs_CalloutClass_MoveRvr {

   /*
* Process the Move Receiver request for Tenant Equipment and Head-end Equipment
* Checks if a timer is already active, generates the API Log object as well as upate API fields on relevant object record
* Prepare the request json using service id and receiver number, send the request to Amdocs API
* Parse the JSON response and populate wrapper classes
*/ 
   public static MoveReceiverResp doTEMove(Tenant_Equipment__c equipRec)
   {
       MoveReceiverResp response;
       
       //Tenant_Equipment__c equipRec = [select id, Smart_Card__c, Name, Address__c, City__c, State__c, Zip_Code__c, Unit__c, Phone_Number__c, Email_Address__c, Customer_First_Name__c, Customer_Last_Name__c, Location__c, Action__c, Opportunity__c from Tenant_Equipment__c where Opportunity__c = :id AND Location__c = 'D'];
       
       String serviceId=equipRec.Opportunity__r.AmDocs_ServiceID__c;
       String targetServiceId=equipRec.Move_Receiver_To__c;
       
       response=moveReceiver(equipRec.id,equipRec.Name,serviceId, targetServiceId,'Tenant Equipment');
        
       return response;
   }
   public static MoveReceiverResp doHEMove(Equipment__c equipRec)
   {
       MoveReceiverResp response;
       
       //Equipment__c equipRec = [select id, Receiver_S__c, Name,Opportunity__r.AmDocs_ServiceID__c, Move_Receiver_To__c, Location__c, Action__c, Opportunity__c from Equipment__c where Id= :equipId  AND Location__c = 'C'];
        
       String serviceId=equipRec.Opportunity__r.AmDocs_ServiceID__c;
       String targetServiceId=equipRec.Move_Receiver_To__c;
       
       response=moveReceiver(equipRec.id,equipRec.Name,serviceId, targetServiceId,'Headend Equipment');
        
       return response;
   }
   private static MoveReceiverResp moveReceiver(Id equipId,String recName,String serviceId, String targetServiceId,String objectName)
   {
       MoveReceiverResp response;
       
       Datetime dt1 = System.Now();
       
       list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateUpdateTime__c from API_LOG__c where ServiceID__c = :serviceId  AND NextAvailableActivateUpdateTime__c >= :dt1 Order By NextAvailableActivateUpdateTime__c DESC Limit 1];
        
        if( (timer.size() > 0 )) {writeTimerError(equipId,serviceId,objectName);}
        else if(timer.size() < 1)
        {
            String finalJSON = prepareRequestJSON(recName);
            System.debug('DEBUG Full Request finalJSON : ' +finalJSON );
            
            response=sendRequest(equipId,finalJSON,targetServiceId,objectName);
        }
       
       return response;
   }
   public static void writeTimerError(Id equipId,String serviceId,String objectName)
   {
       if(objectName=='Tenant Equipment')
       {
           Tenant_Equipment__c sbc = new Tenant_Equipment__c(); { 
            sbc.id=equipId; sbc.API_Status__c='Error'; 
            sbc.Amdocs_Transaction_Code__c='Error'; 
            sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions'; 
            update sbc;
            }
            
       }
       else if(objectName=='Headend Equipment')
       {
            Equipment__c sbc = new Equipment__c(); { 
            sbc.id=equipId; sbc.API_Status__c='Error'; 
            sbc.Amdocs_Transaction_Code__c='Error'; 
            sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions'; 
            update sbc;
            }
       } 
         API_Log__c apil2 = new API_Log__c(); {
                        apil2.Record__c=equipId;
                        apil2.Object__c=objectName;
                        apil2.Status__c='ERROR';
                        apil2.Results__c='You must wait atleast (2) two minutes between transactions';
                        apil2.API__c='MoveReceiver';
                        apil2.ServiceID__c=serviceId;
                        apil2.User__c=UserInfo.getUsername();
                        insert apil2; 
                    }         
   }
   private static string prepareRequestJSON(String receiverNum)
   {
        String finalJSON ='';
        JSONGenerator jsonObj = JSON.createGenerator(true);
        jsonObj.writeStartObject();
        jsonObj.writeFieldName('ImplMoveReceiverRestInput');
        jsonObj.writeStartObject();
    
        // This is EQUIPMENT - Head End ONLY
        jsonObj.writeFieldName('receiverList');
        jsonObj.writeStartArray();
    
        jsonObj.writeStartObject();
        jsonObj.writeStringField('receiverID', receiverNum);
        jsonObj.writeEndObject();
        
        
        jsonObj.writeEndArray();
        jsonObj.writeEndObject();
        jsonObj.writeEndObject();
        
        finalJSON = jsonObj.getAsString();
        
        return finalJSON;
   }
   private static MoveReceiverResp sendRequest(String recId,String reqJSON,String serviceId,String objType)
   {
        MoveReceiverResp obj=null;
        list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
        System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);  
        
        HttpRequest request = new HttpRequest();
        String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+serviceId+'/moveReceiver?lo=EN&sc=SS&ca=SF';
        request.setEndPoint(endpoint);
        request.setBody(reqJSON);
        request.setTimeout(101000);
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('POST'); 
        String authorizationHeader = A[0].UXF_Token__c; 
        request.setHeader('Authorization', authorizationHeader); 
    
        HttpResponse response = new HTTP().send(request);
        
        if (response.getStatusCode() >= 200 && response.getStatusCode() <= 299) { 
         String strjson = response.getbody();
         //strjson='{"ImplMoveReceiverRestOutput":{"orderID":"783216","responseStatus":"SUCCESS"}}';
         System.debug('Move Receiver JSON Response::'+strjson);
         JSONParser parser = JSON.createParser(strjson);
            
            //parser.nextToken();
            //parser.nextToken();
            //parser.nextToken();
            obj = (MoveReceiverResp)parser.readValueAs( MoveReceiverResp.class);
            
            System.debug(obj.toString());
        }else{logAPIResult(recId,'Error',response.toString(),objType);}
        return obj;
        
        
        
   }
   public static void logAPIResult(Id currentRec,String status, string message,String equipType)
    {
        if(equipType=='TE')
        {
            Tenant_Equipment__c currentEquip=new Tenant_Equipment__c();
            currentEquip.id=currentRec;
            currentEquip.API_Status__c=status; 
            currentEquip.Amdocs_Transaction_Code__c=status; 
            currentEquip.Amdocs_Transaction_Description__c=message;
            update currentEquip;
        }
        else if(equipType=='HE')
        {
            Equipment__c currentEquip=new Equipment__c();
            currentEquip.id=currentRec;
            currentEquip.API_Status__c=status; 
            currentEquip.Amdocs_Transaction_Code__c=status; 
            currentEquip.Amdocs_Transaction_Description__c=message;
            update currentEquip;
        }
        }
   public class MoveReceiverResp
{
    public person ImplMoveReceiverRestOutput;
}
public class person {
    public String serviceID;
    public String ownerServiceId;
    public String orderID;
    public String responseStatus;
    public cls_equipmentList[] equipmentList;
    public cls_componentList[] componentList;
    public cls_spsList[] spsList;
    public cls_informationMessages[] informationMessages;
    public cls_ImplUpdateBulkRestOutput [] ImplUpdateBulkRestOutput;
}

class cls_ImplUpdateBulkRestOutput{
    public String serviceID;
    public String orderID;
    public String responseStatus;
}

class cls_equipmentList {
    public String action;
    public String receiverID;
    public String location;
    public String smartCardID;
    public String leaseInd;
    public String chassisSerialNumber;
    public String serialNumber;
}

class cls_componentList {
    public String action;
    public String caption;
}

class cls_spsList {
    public String action;
    public String caption;
}

public class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}
  
 }