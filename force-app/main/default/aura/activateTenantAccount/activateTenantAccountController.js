({
    //Milestone# MS-001732 - 22June2020 - Start
    /*activateTenant : function(component, event, helper) 
    {
        component.set("v.isLoading", true);
        var taId=component.get("v.targetTAId");
        var loadDelay=20*1000;
        var recLoadAction = component.get("c.loadTenantAccount");
        var updateUnitAction = component.get("c.loadTenantAccount");
        var initAPICallsAction = component.get("c.initAPICalls");
        
        initAPICallsAction.setParams({taId: taId});
        recLoadAction.setParams({taId: taId});
        updateUnitAction.setParams({taId: taId});
        
        updateUnitAction.setCallback(this, function (response){
            if (component.isValid() && response.getState() === "SUCCESS"){
                var updateUnitResp=response.getReturnValue();console.info(updateUnitResp);
                alert(' updateUnitResp- '+updateUnitResp);
                var oldMsg=component.get("v.modalMsg");
                var seperator="";
                if(oldMsg && oldMsg!=null && oldMsg!=""){seperator="<br/>";}else{oldMsg=""}
                var newMsg=oldMsg+seperator+"Update unit completed successfully.";
                component.set("v.modalMsg",newMsg);
                component.set("v.activationStatus",newMsg);
            }
        });
        
        initAPICallsAction.setCallback(this, function (response) {
            console.info("API Calls "+response.getState());
            component.set("v.isLoading", false);
            if (component.isValid() && response.getState() === "SUCCESS")
            {
                var apiCallsResp=response.getReturnValue();console.info(apiCallsResp);
                alert('apiCallsResp - '+apiCallsResp);
                if(apiCallsResp && apiCallsResp.isError)
                {
                    var  resMessage=apiCallsResp.resultMessage;
                    component.set("v.modalMsg",resMessage);
                    component.set("v.activationStatus",resMessage);
                }
                else
                {
                    //console.info("Inovking record load...");
                    //setTimeout(function(){ $A.enqueueAction(recLoadAction);}, loadDelay);
                }
            }
        });
        var reLoadCounter=1;
        
        recLoadAction.setCallback(this, function (response) {
            console.info("Load "+response.getState());
            if (component.isValid() && response.getState() === "SUCCESS")
            {
                var activationResp=response.getReturnValue();
                alert('activationResp - '+activationResp);
                console.info(activationResp);
                if(activationResp.apiStatus)
                {
                    var resMessage=activationResp.apiStatus+"<br/>"+activationResp.apiDescription;
                    component.set("v.modalMsg",resMessage); 
                    component.set("v.activationStatus",resMessage);
                }
                else 
                {
                    if(reLoadCounter<=3)
                    {
                        console.info("Inovking record load again...");
                        $A.enqueueAction(recLoadAction);
                        reLoadCounter++;
                    }
                    else
                    {
                        component.set("v.modalMsg",resMessage);
                        component.set("v.activationStatus",resMessage);
                    }
                }
            }
        });
        $A.enqueueAction(initAPICallsAction);
    }*/
    
    activateTenant : function(component, event, helper) {
        component.set("v.isLoading", true);
        var taId=component.get("v.targetTAId");
        var loadDelay=101*1000;
        var unexpectedErrorMsg = "An unexpected error has occurred. Please contact your Salesforce Administrator.";
        var buttonNext = component.find('btnNext');
        
        var initAPICallsAction = component.get("c.initAPICalls");
        initAPICallsAction.setParams({taId: taId});      
        initAPICallsAction.setCallback(this, function (response) {
            console.log("API status "+JSON.stringify(response.getState()));
            
            if (component.isValid() && response.getState() === "SUCCESS"){
                var apiCallsResp=response.getReturnValue();
                console.log("API response "+JSON.stringify(apiCallsResp));
                
                if(apiCallsResp && apiCallsResp.isResponseNull == false){
                    var  resMessage=apiCallsResp.resultMessage;
                    component.set("v.modalMsg",resMessage);
                    component.set("v.activationStatus",resMessage);
                    buttonNext.set('v.disabled',false);
                    component.set("v.isLoading", false);
                }
                else{
                    setTimeout(function(){ 
                        var recLoadAction = component.get("c.loadTenantAccount");
                        recLoadAction.setParams({taId: taId});
                        recLoadAction.setCallback(this, function (response) {
                            if (component.isValid() && response.getState() === "SUCCESS"){
                                var activationResp=response.getReturnValue();
                                console.log("activationResp "+JSON.stringify(activationResp));
                                
                                if(activationResp.apiStatus != null && activationResp.apiStatus != ''){
                                    var resMessage;
                                    if(activationResp.apiDescription.includes("ERROR") || activationResp.apiStatus.includes("ERROR"))
                                        resMessage = unexpectedErrorMsg;
                                    else 
                                    	resMessage=activationResp.apiStatus+"<br/>"+activationResp.apiDescription;
                                    component.set("v.modalMsg",resMessage); 
                                    component.set("v.activationStatus",resMessage);
                                }
                                else{
                                    //var resMessage="The system is currently experiencing connectivity issues. Please try again later or contact CommercialOperations@dish.com.";
                                    component.set("v.modalMsg",unexpectedErrorMsg);
                                    component.set("v.activationStatus",unexpectedErrorMsg);
                                } 
                            }
                            else{
                                component.set("v.modalMsg",unexpectedErrorMsg);
                                component.set("v.activationStatus",unexpectedErrorMsg);
                                console.log("Error Details - "+JSON.stringify(response.getError()));
                            }
                            buttonNext.set('v.disabled',false);
                            component.set("v.isLoading", false);
                        });
                        $A.enqueueAction(recLoadAction);
                    }, loadDelay);
                }
            }
            else{
                component.set("v.modalMsg",unexpectedErrorMsg);
                component.set("v.activationStatus",unexpectedErrorMsg);
                console.log("Error Details - "+JSON.stringify(response.getError()));
                component.set("v.isLoading", false);
                buttonNext.set('v.disabled',false);
            }
        });
        $A.enqueueAction(initAPICallsAction);
    },
    
    btnNextClick : function(component, event, helper) {       
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    },
    // Milestone# MS-001732 - End
})