({   
/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 10 September 2020
Created for: Milestone# MS-000971, MS-001092, MS-003351
Description: To get details of Opportunity related fields for GuideMe Create Property Review screen
-------------------------------------------------------------*/
    
    doInit : function(component, event, helper) {        
        var OwnerId = component.get('v.HotelPortfolioOwnerId'); 
        var selectedDistributorAPI = component.get('v.selectedDistributor');
        var selectedBrandId = component.get('v.selectedBrandId');
        var selectedSubBrandId = component.get('v.selectedSubBrandId');
        
        var action=component.get("c.getInitDetails");
        action.setParams({
            HotelPFOwnerId : OwnerId,
            selDistAPI : selectedDistributorAPI,
            BrandId: selectedBrandId,
            SubBrandId: selectedSubBrandId
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                var resp = response.getReturnValue();                
                component.set("v.HotelPortfolioOwnerName",resp.HotelPortfolioOwnerName);
                component.set("v.selectedDistributorValue",resp.SelectedDistributorValue);
                component.set("v.selectedBrandName",resp.BrandName);
                component.set("v.selectedSubBrandName",resp.SubBrandName);
            }
            else{
                if(state==="ERROR")
                    console.log('Error Details: '+JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);   
    },    
})