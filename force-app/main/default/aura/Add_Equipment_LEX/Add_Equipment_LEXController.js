({ doInit: function(component, event, helper){
    
     var emptyArr=[{Name:'',Receiver_S__c:'',Receiver_Model__c:''}];
            component.set("v.addEquipment", emptyArr);
},
    saveFile:function (component, jsonstr) {
            component.set("v.isLoading", true);
            var equipmentdata = component.get("v.addEquipment");
         	var jsonData=JSON.stringify(equipmentdata);
            console.info(jsonData);
        	console.info(opporId);
        	var opporId=component.get("v.recordId");
            var action= component.get("c.save");
            action.setParams({
           "lexAddEquip": jsonData,
                "oppId":opporId

       });
           console.info(action);
            action.setCallback(this, function(a) {
            component.set("v.isLoading", false);
            var state = a.getState();
            console.info(state);
            if (state === "SUCCESS") {  
           
                            component.set("v.isLoading", false);
               				component.set("v.isStatusMessage", "SaveSuccess");
                    		component.set("{!v.recordStatusMessage}", "Your record has been saved successfully");
                            // show toast message
                             
                            //$A.get("e.force:closeQuickAction").fire();
                            $A.get('e.force:refreshView').fire();
                			 var emptyArr=[{Name:'',Receiver_S__c:'',Receiver_Model__c:''}];
            				component.set("v.addEquipment", emptyArr); 
                var name = a.getReturnValue();
            //    $A.get('e.force:refreshView').fire();
            }else if (res.getState() === "ERROR") {
     			component.set("v.isStatusMessage", "errorStatus");
                component.set("{!v.recordStatusMessage}", "Their is an error while saving record");
            }
            /*alert("Records saved Succesfully");
            var emptyArr=[{Name:'',Receiver_S__c:'',Receiver_Model__c:''}];
           component.set("v.addEquipment", emptyArr);
            }
            else if (state === "ERROR") {
                var errors = a.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                     alert('Unknown');
                }              
            }*/
            });
             var ID= component.get("v.recordId");
             window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");  
            $A.enqueueAction(action);
     
} ,
   onCancel : function(component, event, helper) {
        
       var ID= component.get("v.recordId");
      
       window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");
           
} ,
    addRow : function(component, event, helper){
        var RowItemList = component.get("v.addEquipment");
        RowItemList.push({
            'sobjectType': 'Equipment__c',
            'Name': '',
            'Receiver_S__c': '',
            'Receiver_Model__c': ''
        });   
        component.set("v.addEquipment", RowItemList);
        //component.getEvent("AddRowEvent").fire();     
    },
  hideStatusMesssage : function(component, event, helper) {
        
        component.set("v.isStatusMessage", "hideMessage");
    }
 
  
})