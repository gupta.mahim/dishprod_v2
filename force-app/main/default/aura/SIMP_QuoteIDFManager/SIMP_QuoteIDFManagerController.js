({
    doInit : function(component, event, helper) {
       	helper.getIDFCount(component, event);
    },
    
    createIDF : function(component, event, helper){
          helper.createRecord(component, event);
    },
    
    deleteIDFs : function(component, event, helper){
        if(confirm("Are you sure you wanna delete all IDF Records?")){
          helper.deleteAllRecord(component, event);
        }
    },
    
    updateCopyCount : function(component, event, helper){
        var count = component.find("numberOfIDFCopies").get("v.value").substring(0,1) == 0 ? '' : component.find("numberOfIDFCopies").get("v.value");
        component.set("v.idfCount", count);
    },
    
    handleShowToast : function(component, event, helper){
        //$A.get('e.force:refreshView').fire();
        helper.callCreateIDFCopies(component, event);
    },
    
    checkStatus : function(component, event, helper){
        //$A.get('e.force:refreshView').fire();
        helper.getRecordType(component, event);
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
})