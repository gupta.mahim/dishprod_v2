({
    //Milestone# MS-000026 -- Mahim -- Start
    init: function (component, event, helper) 
    {
        //Milestone# MS-001252 - 30Jun20 - Start
        component.set("v.isLoading", true);
        var action = component.get("c.getUserAccess");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.userHaveAccess", response.getReturnValue().currentUserHaveAccess);
                component.set("v.noAccessMsg", response.getReturnValue().noAccessMsg);               
                component.set("v.isLoading", false);
            }
            else{
                console.log('Error Details - '+ JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
        //Milestone# MS-001252 - End
        
        console.log('recordId - '+component.get("v.recordId"));
        console.log('isCommunity - '+component.get("v.isCommunity"));
        console.log('doRedirect - '+component.get("v.doRedirect"));
        console.log('displaySave - '+component.get("v.displaySave"));
        /*var oppId=component.get("v.OppId");
        
        var action = component.get("c.initBulkTenantEquipment");
        action.setParams({ "OpptyId" : oppId});
        action.setCallback(this, function (response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS")
            {
                var prodResp=response.getReturnValue();
                component.set("v.existingEquips",prodResp);
                if(prodResp.length>0)
                    component.set("v.displayExistingEquipTab",true);
                else
                    component.set("v.displayExistingEquipTab",false);
                    
            }});        
        $A.enqueueAction(action);
        var isFromBack = component.get("v.IsBackFromNextScr");
        
        if(isFromBack!='yes')
        {*/
            
            helper.addTERecords(component, event);
            $A.enqueueAction(component.get('c.ValdiateInp'));
        //}
        
        var action=component.get("c.getStateOptions");
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                console.log('response.getReturnValue-'+response.getReturnValue());
                component.set("v.stateOptions",response.getReturnValue());
            }
            else{
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
        
        
    },
    
    addRow: function(component, event, helper) {
        helper.addTERecords(component, event);
        //this.ValdiateInp(component, event, helper);
        $A.enqueueAction(component.get('c.ValdiateInp'));
    },
    
    ValdiateInpSC : function(component, event, helper) {
        
        var inpCmp= component.get('v.addedEquipList');
        for (var indexVar = 0; indexVar < inpCmp.length; indexVar++) {
            if (inpCmp[indexVar].Smart_Card__c.length < 11) 
            {
                
                component.set("v.Invalidinp",false);
            }
            else
            {
                component.set("v.Invalidinp",true);
            }
        }
    },
    
    ValdiateInp : function(component, event, helper) {
        
        var inpCmp= component.get('v.addedEquipList');
        for (var indexVar = 0; indexVar < inpCmp.length; indexVar++) {
            if (inpCmp[indexVar].Name.length < 11) 
            {
                
                component.set("v.Invalidinp",false);
            }
            else
            {
                component.set("v.Invalidinp",true);
            }
        }
    },
    
    removeRow: function(component, event, helper) {
        //Get the account list
        var addedEquipList = component.get("v.addedEquipList");
        //Get the target object
        var selectedItem = event.currentTarget;
        //Get the selected item index
        var index = selectedItem.dataset.record;
        addedEquipList.splice(index, 1);
        component.set("v.addedEquipList", addedEquipList);
    },
    //Milestone# MS-000026 -- Mahim -- End
    
    uploadFile: function (component, event, helper)
    {
        var fileTypes = ['csv', 'xls', 'xlsx'];
        var fileInput = component.find("file").getElement();
        component.set("v.isLoading", "true");
        var oppId=component.get("v.recordId");
        var file = fileInput.files[0];
        if (file) 
        { 
            
            
            var extension = file.name.split('.').pop().toLowerCase(),  //file extension from input file
                isSuccess = fileTypes.indexOf(extension) > -1;
            if(isSuccess) {
                component.set("v.isError", "false");
                
                //console.log("File");
                var file_name = file.name;
                var file_extension = file_name.substring(file_name.lastIndexOf(".") + 1);
                //console.log("File Extension : " + file_extension);
                
                var reader = new FileReader();
                
                var file_data = "";
                reader.onload = $A.getCallback(function() {
                    if (file_extension == "csv"){
                        file_data = reader.result;
                        //console.log('@@@ csv file contains : '+ file_data);
                    }
                    else if (file_extension == "xlsx" || file_extension == "xls")
                    {
                        // This is used for Excel file Support..
                        var X = XLSX;
                        
                        var data = reader.result;
                        var arr = fixdata(data);
                        var wb =  X.read(btoa(arr), {type: 'base64'});
                        file_data = process_wb(wb);
                        // Start of Excel support functions...
                        function fixdata(data) {
                            var o = "", l = 0, w = 10240;
                            for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint8Array(data.slice(l*w,l*w+w)));
                            o+=String.fromCharCode.apply(null, new Uint8Array(data.slice(l*w)));
                            return o;
                        }
                        
                        function to_csv(workbook) {
                            var result = [];
                            workbook.SheetNames.forEach(function(sheetName) {
                                var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
                                if(csv.length > 0){
                                    result.push(csv);
                                }
                            });
                            return result.join("\n");
                        }
                        
                        function process_wb(wb) {
                            var output = "";
                            output = to_csv(wb);
                            return output;
                        }
                        // End of Excel support functions...
                    }
                    
                    // Calling Apex function...
                    
                    
                    if (file_data != "" && file_data != null &&file_data.length>2)
                    { 
                        var action= component.get("c.ReadFile");
                        action.setParams({
                            "strNameFile":file_data,
                            "opportunityId": oppId
                        });
                        action.setCallback(this,function(a){
                            var state = a.getState();
                            //console.log(a.getReturnValue())
                            component.set("v.uploadedEquipment", a.getReturnValue());
                            
                            
                            component.set("v.isLoading", "false");
                        });
                        $A.enqueueAction(action);
                    }else
                    {
                        component.set("v.isError", "true");
                        component.set("{!v.ErrorMessage}", "The uploaded file does not contain any data. "); 
                        component.set("v.isLoading", "false"); 
                    }
                });
                
                reader.onerror = function (evt) {
                    component.set("v.isStatusMessage", "errorStatus");
                    component.set("{!v.recordStatusMessage}", "Their is an error while uploading file");
                    //console.log("error reading file");
                }
                
                if (file_extension != "" && file_extension != null && file_extension == "csv")
                {
                    reader.readAsText(file, "UTF-8");
                }else if (file_extension != "" && file_extension != null && (file_extension == "xlsx" || file_extension == "xls") )
                {
                    reader.readAsArrayBuffer(file);
                }
            }
            else
            {
                component.set("v.isLoading","false");
                component.set("v.isError", "true");
                component.set("{!v.ErrorMessage}", "Invalid file format, please select appropriate file format(csv,xls and xlsx) and Try Again. ");
                
                
                
            }
        }
        else
        {   
            component.set("v.isError", "true");
            component.set("{!v.ErrorMessage}", "Please upload file and then press upload file button. "); 
            component.set("v.isLoading", false);
            
            
        }
    },
    
    
    removeComponent:function(component, event, helper){
        //get event and set the parameter of Aura:component type, as defined in the event above.
        var compEvent = component.getEvent("RemoveComponent");
        compEvent.setParams({
            "comp" : component
        });
        compEvent.fire();
        component.set("v.isStatusMessage", "RecordRemoved");
        component.set("{!v.recordStatusMessage}", "Your record(s) has been saved successfully.");
        component.set("v.uploadedEquipment", null);
    },
    
    
    saveFile:function (component, jsonstr){        
        component.set("v.isLoading", true);
        var equipmentdata = component.get("v.uploadedEquipment");
        if(equipmentdata!='')
        {
            //MS-003464 - 28 Oct 20 - Start
            var stateValues = component.get("v.stateOptions");
            
            for(var i=0; i<equipmentdata.length; i++){
                if(equipmentdata[i].Name == '' || equipmentdata[i].Name == null || 
                   equipmentdata[i].Smart_Card__c == '' || equipmentdata[i].Smart_Card__c == null )
                {
                    //component.set("v.recordStatusMessage", "Please provide Receiver # and SmartCard # for all uploaded Tenant Equipments.\nReceiver# and SmartCard # length must be 11 characters.");
                    component.set("v.recordStatusMessage", "Please provide RECEIVER # and SMART CARD # for all uploaded Tenant Equipments.");
                    component.set("v.isStatusMessage", "errorStatus");
                    component.set("v.isLoading", false);
                    return;
                }
                
                if(equipmentdata[i].Name.length != 11 || equipmentdata[i].Smart_Card__c.length != 11)
                {
                    component.set("v.recordStatusMessage", "Please provide 11 character RECEIVER # and SMART CARD # for all uploaded Tenant Equipments.");
                    component.set("v.isStatusMessage", "errorStatus");
                    component.set("v.isLoading", false);
                    return;
                }
                if(equipmentdata[i].Name.length>0 && equipmentdata[i].Name.charAt(0) != 'R' && equipmentdata[i].Name.charAt(0) != 'r'){
                    component.set("v.recordStatusMessage", 'RECEIVER # must start with letter "R" for all uploaded Tenant Equipments.');
                    component.set("v.isStatusMessage", "errorStatus");
                    component.set("v.isLoading", false);
                    return;
                }
                if(equipmentdata[i].Smart_Card__c.length>0 && equipmentdata[i].Smart_Card__c.charAt(0) != 'S' && equipmentdata[i].Smart_Card__c.charAt(0) != 's'){
                    component.set("v.recordStatusMessage", 'SMART CARD # must start with letter "S" for all uploaded Tenant Equipments.');
                    component.set("v.isStatusMessage", "errorStatus");
                    component.set("v.isLoading", false);
                    return;
                }
                if(equipmentdata[i].Address__c == '' || equipmentdata[i].Address__c == null || 
                   equipmentdata[i].City__c == '' || equipmentdata[i].City__c == null ||
                   equipmentdata[i].State__c == '' || equipmentdata[i].State__c == null || 
                   equipmentdata[i].Zip_Code__c == '' || equipmentdata[i].Zip_Code__c == null ||
                   equipmentdata[i].Unit__c == '' || equipmentdata[i].Unit__c == null)
                {
                    component.set("v.recordStatusMessage", "Please provide UNIT, ADDRESS, CITY, STATE & ZIP CODE for all uploaded Tenant Equipments.");
                    component.set("v.isStatusMessage", "errorStatus");
                    component.set("v.isLoading", false);
                    return;
                }
                
                //MS-003474 - 29 Oct 20 - Start
                var stateValueValid = false;
                console.log('12'+equipmentdata[i].State__c);
                for(var j=0; j<stateValues.length; j++){
                    console.log('2'+stateValues[j]);
                    if(equipmentdata[i].State__c.toUpperCase() == stateValues[j].toUpperCase())
                        stateValueValid = true;
                }
                if(stateValueValid != true){
                    component.set("v.recordStatusMessage", "Please provide a valid STATE value for all uploaded Tenant Equipments.");
                    component.set("v.isStatusMessage", "errorStatus");
                    component.set("v.isLoading", false);
                    return;
                }
                //MS-003474 - End
            }
            //MS-003464 - End
            
            var jsonData=JSON.stringify(equipmentdata);
            
            var action= component.get("c.save");
            action.setParams({
                "lexAccstoupload": jsonData
            });
            action.setCallback(this, function(a){
                var state = a.getState();
                if (state === "SUCCESS"){
                    
                    
                    var ID= component.get("v.recordId");
                    var isCommunity=component.get("v.isCommunity");
                    var doRedirect=component.get("v.doRedirect");
                    if(!isCommunity && !doRedirect)
                    {
                        window.setTimeout($A.getCallback(function() 
                                                         {
                                                             component.set("v.isStatusMessage","SaveSuccess");
                                                             component.set("v.isLoading", false);
                                                         }),15000); 
                    }
                    
                    
                    
                    if(!(!isCommunity && !doRedirect))
                    {
                        component.set("v.isStatusMessage","SaveSuccess");
                    }
                    component.set("{!v.recordStatusMessage}", "Your record(s) has been saved successfully.");
                    component.set("v.uploadedEquipment", null);
                    var ID= component.get("v.recordId");
                    var isCommunity=component.get("v.isCommunity");
                    var doRedirect=component.get("v.doRedirect");
                    if(!isCommunity && doRedirect)
                    {
                        window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");
                    }
                    
                    else if(isCommunity && doRedirect)
                    {
                        window.open('/lex/s/opportunity/'+ID+'/view',"_self");
                    }
                    
                    
                    var name = a.getReturnValue();
                }
                
                else if (state === "ERROR"){
                    var errors = a.getError();
                    var message = 'Unknown error'; // Default error message
                    
                    // Retrieve the error message sent by the server
                    if (errors && Array.isArray(errors) && errors.length > 0){
                        message = errors[0].message;
                    }
                    
                    // Display the message
                    console.error(message);
                    console.error(errors);
                    component.set("v.isStatusMessage", "errorStatus");
                    component.set("{!v.recordStatusMessage}", "There is an error while saving record: "+message);
                }
                
                if(!(!isCommunity && !doRedirect))
                {
                    component.set("v.isLoading", false);
                }
                
                
            });
            $A.enqueueAction(action);
        }else{
            component.set("v.isError", "true");
            
            component.set("{!v.ErrorMessage}", "You did not upload the file, or file is empty. Please try again. "); 
            component.set("v.isLoading", false);
            
        }
        
    },
    
    
    onCancel : function(component, event, helper){
        var ID= component.get("v.recordId");
        window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");         
    },
    
    
    hideStatusMesssage : function(component, event, helper){
        component.set("v.isStatusMessage", "hideMessage");
    },    
    
    print : function(component, event, helper){
        
        window.print();
        },
})