({
	getPicklistValues1 : function(component,event){
        var fieldApiName=component.get("v.fieldApiName")
        var action=component.get("c.getPicklistValues");
		action.setParams({
            recordTypeId : component.get("v.selectedRecordTypeId"),
            ObjectApiName : component.get("v.objectApiName"),
            fieldApiName : fieldApiName
        });
        
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                component.set("v.picklistValues",response.getReturnValue());
                var optionalValue=component.get("v.optionalValue");
                if(optionalValue && optionalValue!='')
                {
                	//component.set("v.selectedPicklistValue",optionalValue);    
                }
                else
                {
                    //component.set("v.selectedPicklistValue",response.getReturnValue()[0].value);
                }
            }
            else{
                console.log('State: '+state);
                if(state==="ERROR")
                    console.log(response.getError());
            }
        });
        
       $A.enqueueAction(action); 
    },handleChange: function (cmp, event) {
        // This will contain an array of the "value" attribute of the selected options
        //console.info("handleChange called.");
        var selectedOptionValue = cmp.find('picklistId').get('v.value');
        //var selectedOptionValue1 = selectedOptionValue.toString().replace(/,/g, ';');
        var convertedValue=selectedOptionValue.replace("&amp;","&");//.replace("&gt;"/>/g, ).replace(/</g, "&lt;");
        cmp.set("v.selectedPicklistValue",convertedValue);
    }
})