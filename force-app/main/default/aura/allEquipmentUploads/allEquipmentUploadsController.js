({
    onButtonPressed: function(component, event, helper)
    {
        var actionClicked = event.getSource().getLocalId();
        var navigate = component.get('v.navigateFlow');
        
        if(actionClicked=='NEXT'){
            helper.saveChanges(component, event,helper);
        }
        
        //Milestone# MS-001273 - Mahim - to handle Data Insert lag issue - 22 March 2020 - Start
        //if(component.get('v.isStatusMessage') != "errorStatus")	//Milestone# MS-000026 - Added by Mahim
        //	navigate(actionClicked);
        if(actionClicked=='BACK'){
            navigate(actionClicked);
        }
        //Milestone# MS-001273 - End
    },
    
    hideStatusMesssage : function(component, event, helper) {
        
        component.set("v.isStatusMessage", "hideMessage");
    }
})