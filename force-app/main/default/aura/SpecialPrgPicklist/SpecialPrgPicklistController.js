({
	getPicklistValues1 : function(component,event){
        var fieldApiName=component.get("v.fieldApiName")
        var action=component.get("c.getPicklistValues");
		action.setParams({
            recordTypeId : component.get("v.selectedRecordTypeId"),
            ObjectApiName : component.get("v.objectApiName"),
            fieldApiName : fieldApiName
        });
        
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                component.set("v.picklistValues",response.getReturnValue());
            }
            else{
                console.log('State: '+state);
                if(state==="ERROR")
                    console.log(response.getError());
            }
        });
        
       $A.enqueueAction(action); 
    },handleChange: function (cmp, event) {
        // This will contain an array of the "value" attribute of the selected options
        var selectedOptionValue = event.getParam("value");
        var selectedOptionValue1 = selectedOptionValue.toString().replace(/,/g, ';');
        cmp.set("v.selectedPicklistValue",selectedOptionValue1);
        
    }
})