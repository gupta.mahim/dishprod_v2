({
    updateDISHContractLDS: function(component, event, helper) {
        console.log("Testing.....");
        //debugger;
        component.set("v.recObjFields.Activated_By__c", $A.get('$SObjectType.CurrentUser.Id'));
        component.set("v.recObjFields.Status__c", "Activated");
        component.set("v.recObjFields.Locked__c", true);
        // Jerry Clifft 12.11.2019 - component.set("v.recObjFields.RecordTypeId", "01260000000YLR7");
        var now = new Date();
        //var today = $A.localizationService.formatDateTime(now, "MMM DD YYYY, hh:mm:ss a");
        component.set("v.recObjFields.Activated_Date__c", now);

        component.find("recordEditor").saveRecord(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                
                // record is saved successfully
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Saved",
                    "message": "The record was processed."
                });
                resultsToast.fire();
                
            } else if (saveResult.state === "INCOMPLETE") {
                // handle the incomplete state
                console.log("User is offline, device doesn't support drafts.");
            } else if (saveResult.state === "ERROR") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "mode": 'sticky',
                    "title": "Not Saved",
                    "message": JSON.stringify(saveResult.error[0].message)
                });
                resultsToast.fire();
                // handle the error state
                console.log('Problem saving record, error: ' + 
                            JSON.stringify(saveResult.error));
            } else {
                console.log('Unknown problem, state: ' + saveResult.state +
                            ', error: ' + JSON.stringify(saveResult.error));
            }
        });
        

        // Close the quick action
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
        
    }
})