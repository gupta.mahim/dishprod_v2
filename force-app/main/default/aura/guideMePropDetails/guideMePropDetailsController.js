({
	doInit : function(component, event, helper) {
		var action = component.get("c.loadPropDetails");
        var oppIdText=component.get("v.recordId");
        action.setParams({oppId: oppIdText});
        
        action.setCallback(this, function (response) {
      var state = response.getState();
      console.info(state);
      if (component.isValid() && state === "SUCCESS")
      {
          console.info(response.getReturnValue());
          //Milestone #MS-001604 - Added by Mahim - Start
          //component.set("v.oppObj",response.getReturnValue());
          component.set("v.oppObj",response.getReturnValue().oppDetails);
          component.set("v.oppProdDetails",response.getReturnValue().oppProdDetails);
          //Milestone #MS-001604 - End
      }});
        var requestInitiatedTime = new Date().getTime();
      $A.enqueueAction(action);
	}
})