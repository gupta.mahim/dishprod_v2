({
	cloneWithRelated : function(component, event, helper) {
        
		var action = component.get("c.cloneWithRelatedAll");
        //alert(component.get("v.recordId"));
        action.setParams({ 
            "parentRecId": component.get("v.recordId"),
            "parentApiName" : "SBQQ__Quote__c",
        });
        action.setCallback(this, function(response) {
            //alert(response.getState());
            console.log(response.getState());
            if(response.getState() == "SUCCESS"){ 
                $A.get("e.force:closeQuickAction").fire();  
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Cloned this quote",
                    "type" : "success"
                });
                toastEvent.fire();
                var navEvt = $A.get("e.force:navigateToSObject");
                  navEvt.setParams({
                  "recordId": response.getReturnValue(),
                });
                navEvt.fire();
            }
        }); 
        $A.enqueueAction(action);
	},
   
})