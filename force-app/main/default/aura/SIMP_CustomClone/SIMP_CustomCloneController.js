({
    CustomClone : function(component, event, helper) {
        var rsp = confirm("Clone this quote?");
        if (rsp === true) {       
            //alert('test');  
            
            helper.cloneWithRelated(component, event, helper);
        } 
        else{
            //alert('test');
            $A.get("e.force:closeQuickAction").fire();
            
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Canceled!",
                "message": "Cloning quote cancelled",
                "type" : "warning"
            });
            toastEvent.fire();
        }  
    },
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    CloseModal : function(component, event, helper) {
        
    }
})