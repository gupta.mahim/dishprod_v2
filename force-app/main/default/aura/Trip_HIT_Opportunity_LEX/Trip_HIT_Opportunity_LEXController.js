({
    doSendHitTrip: function(component, event, helper) 
    {
        var action = component.get("c.doSendTripHitAction");
        action.setParams({
            oppId: component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    },
    updateOppLDS: function(component, event, helper) {
        console.log("Testing.....");
        //debugger;

        var rec = component.get("v.recOppFields.Name");

        console.log("rec="+rec);
        console.log("recordid="+component.get("v.recordId"));
        console.log("recoOpp="+component.get("v.recoOpp"));
        console.log("recOppFields="+component.get("v.recOppFields"));
        console.log("Name="+component.get("v.recOppFields.Name"));
        console.log("Subscriber Id="+component.get("v.recOppFields.AmDocs_ServiceID__c"));
        console.log("AmDocs_ResendAll__c="+component.get("v.recOppFields.AmDocs_ResendAll__c"));
        
        //component.set("v.recOppFields.Id", component.get("v.recordId"));
        
        //component.set("v.recOppFields.AmDocs_ServiceID__c", "1234567");
        console.log("Subscriber Id="+component.get("v.recOppFields.AmDocs_ServiceID__c"));
        console.log("AmDocs_ResendAll__c="+component.get("v.recOppFields.AmDocs_ResendAll__c"));
        console.log("End Testing.....");
        
        var SID =component.get("v.recOppFields.AmDocs_ServiceID__c");
        //var num = Math.floor(Math.random() * 100);
        //var name = component.get("v.recOppFields.Name") + num
        component.set("v.recOppFields.AmDocs_ResendAll__c", true);
        component.set("v.recOppFields.Name", name);
        console.log("SID="+SID);
        if (SID!=null && SID!=""){
            
            component.find("recordEditor").saveRecord(function(saveResult) {
                if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                    
                    // record is saved successfully
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Saved",
                        "message": "The record was processed."
                    });
                    resultsToast.fire();
                    
                } else if (saveResult.state === "INCOMPLETE") {
                    // handle the incomplete state
                    console.log("User is offline, device doesn't support drafts.");
                } else if (saveResult.state === "ERROR") {
                    // handle the error state
                    console.log('Problem saving contact, error: ' + 
                                JSON.stringify(saveResult.error));
                } else {
                    console.log('Unknown problem, state: ' + saveResult.state +
                                ', error: ' + JSON.stringify(saveResult.error));
                }
            });
            
        }else{
            
            // record is saved successfully
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                "title": "Not Processed",
                "message": "Subscriber ID is null"
            });
            resultsToast.fire();
            
        }
        // Close the quick action
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        $A.get('e.force:refreshView').fire();
        
    }
})