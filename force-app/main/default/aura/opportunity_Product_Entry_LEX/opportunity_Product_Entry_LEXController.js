({
    initFlow : function (component) {
        // Find the component whose aura:id is "flowData"
        var flow = component.find("flowData");
        var varRecordId=component.get("v.recordId");
         var inputVariables = [
         { name : "recordId", type : "String", value: varRecordId }
        ,{ name : "isCommunity", type : "Boolean", value: true }
         ];
        
        // In that component, start your flow. Reference the flow's API Name.
        flow.startFlow("AddRemoveProducts",inputVariables);
    },


    oppprodentry : function(component, event, helper) {
        var action = component.get("c.updateAvailableList");
        var recIdSD = component.get("v.recordId");
        console.log('SA:: '+ recIdSD);
        component.set("v.isLoading", true);
        action.setParams({
            oppId: component.get("v.recordId")
        });
        action.setCallback(this,function(a){
            component.set("v.oppprodentryItems", a.getReturnValue());
            component.set("{!v.isLoading}", false);
            
        });
        $A.enqueueAction(action);
    },
    
    getRecordType : function(component, event, helper) {
        var action = component.get("c.getNoPriceStage");
        component.set("v.isLoading", true);
        action.setParams({
            opporId: component.get("v.recordId")
        });
        console.log("KA:: " +  action);
        action.setCallback(this,function(a){
            console.log(a.getReturnValue())
            component.set("v.recordType", a.getReturnValue());
            component.set("{!v.isLoading}", false);
            
        });
        $A.enqueueAction(action);
    },
    oppLineItemData : function(component, event, helper) {
        var action = component.get("c.getOppLineItems");
        component.set("v.isLoading", true);
        action.setParams({
            oppId: component.get("v.recordId")
        });
        console.log("KA:: " +  action);
        action.setCallback(this,function(a){
            console.log(a.getReturnValue())
            component.set("v.OpportunityLineItemList", a.getReturnValue());
            component.set("{!v.isLoading}", false);
            
        });
        $A.enqueueAction(action);
    },
    addSelected : function(component, event, helper){
        
        var self = this;  // safe reference
        component.set("v.isLoading", true); 
        var recordID = event.target.getAttribute("data-recId");
        console.info('OPP Rec ID: '+ recordID); 
        var priceBookEntry=component.get("v.oppprodentryItems");
        var jsonData=JSON.stringify(priceBookEntry);
        var OpportunityLineItemLst=component.get("v.OpportunityLineItemList");
        var jsonDataOPLIT=JSON.stringify(OpportunityLineItemLst);
        var oppRecrodID = component.get("v.recordId");
        console.log('oppRecrodID');
        var action = component.get("c.createOppLineItem");
        action.setParams({
            "strEquipData" : jsonData, 
            "recordToAdd" : recordID,
            "theOppId" : oppRecrodID,
            "OPLITData" : jsonDataOPLIT
        });
       
        action.setCallback(this, function(a) {
            component.set("v.isLoading", false);
            var state = a.getState();
            if (state === "SUCCESS") {
                component.set("v.isStatusMessage", "recordSelected");
                component.set("{!v.recordStatusMessage}", "You have selected a record");
                component.set("v.OpportunityLineItemList", a.getReturnValue());
                component.set("v.isLoading", false);  
            }
        });
        $A.enqueueAction(action);  
    },
     reInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    removeSelected : function(component, event, helper){
        var index = event.target.getAttribute("data-recId_ind");
        //var index = event.target.getAttribute("data-recId_index");
        var indexvar =event.target.getAttribute("data-row-index");
         //console.log('KA:: '+index);
        //alert(index+' '+indexvar); 
        var oppLineItems = component.get("v.OpportunityLineItemList");
       
        console.log('KA: To remove: '+oppLineItemsToRemove);
       
        var recordID = event.target.getAttribute("data-recId");
        var jsonData=JSON.stringify(oppLineItems);
        
        var oppLineItemsToRemove = component.get("v.OpportunityLineItemListToDelete");
        var jsonDataToRemove=JSON.stringify(oppLineItemsToRemove);
        var action = component.get("c.removeFromShoppingCart");
        component.set("v.isLoading", true);
        action.setParams({
            "strOppLineItemData" : jsonData, 
            "recordToRemove" : index,
            "itemToRemove" : jsonDataToRemove,
            "oppID" : recordID
            
        });
        action.setCallback(this, function(a) {
            component.set("v.isLoading", true);
            var state = a.getState();
            if (state === "SUCCESS") {
                component.set("v.isStatusMessage", "RecordRemoved");
                component.set("{!v.recordStatusMessage}", "Your record has been removed from selected list");
                component.set("v.OpportunityLineItemListToDelete",  a.getReturnValue());
                oppLineItems.splice(indexvar, 1);
                component.set("v.OpportunityLineItemList",oppLineItems);
                component.set("v.isLoading", false);  
            }
        });
        $A.enqueueAction(action); 
       
    },
    SaveOppLnItm : function(component, event, helper){
        
        component.set("v.isLoading", true);
        var equipData=component.get("v.OpportunityLineItemList")
        var jsonData=JSON.stringify(equipData);
        
        var oppLineItemsToRemove = component.get("v.OpportunityLineItemListToDelete");
        var jsonDataToRemove=JSON.stringify(oppLineItemsToRemove);
        console.info(oppLineItemsToRemove);
        
        var action = component.get("c.saveRecords");
        action.setParams({
            "strOppLineItemData": jsonData,
            "itemToRemove" : jsonDataToRemove
        });
        console.info(jsonData);
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                	component.set("v.isStatusMessage", "SaveSuccess");
                    component.set("{!v.recordStatusMessage}", "Your record has been saved successfully");
                    // show toast message
                    
                    component.set("v.OpportunityLineItemListToDelete",  a.getReturnValue());
                    var ID= component.get("v.recordId");
                    var isCommunity=component.get("v.isCommunity");
                    var doRedirect=component.get("v.doRedirect");
                    if(!isCommunity && doRedirect)
                    {
                    	window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");    
                    }
                else if(isCommunity&&doRedirect)
                {
                    window.open('/lex/s/opportunity/'+ID+'/view',"_self");
                }
                
                	
                    var name = a.getReturnValue();
            }
            else if (state === "ERROR") {
     			component.set("v.isStatusMessage", "errorStatus");
                component.set("{!v.recordStatusMessage}", "There is an error while saving record");
            }
              component.set("v.isLoading", false);
        });

        $A.enqueueAction(action);  
    },
     hideStatusMesssage : function(component, event, helper) {
        
        component.set("v.isStatusMessage", "hideMessage");
    },
    onCancel : function(component, event, helper) {
        
       var ID= component.get("v.recordId");
       window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");       
} 
})