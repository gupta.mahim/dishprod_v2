({
    getParentRecord : function(component, event, helper, parentRecordId, parentApiName, objectAPIName, recordTypeId) {
        var parentRecord;
        var action = component.get("c.searchRecord");
        action.setParams({
            "recordId": parentRecordId,
            "objectName": parentApiName
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue()){
                    parentRecord = response.getReturnValue();
                    helper.openRecordCreator(component, event, helper, objectAPIName, recordTypeId, parentRecord, parentApiName);
                }
            }
        })
        $A.enqueueAction(action);
    },
    
    openRecordCreator : function(component, event, helper, objectAPIName, recordTypeId, parentRecord, parentApiName) {
        var createRecordEvent = $A.get("e.force:createRecord");
        console.log('Inside Open Record:');
        console.log('recordTypeId: ' + recordTypeId);
        console.log('parentRecord: ' + parentRecord);
        console.log('parentApiName: ' + parentApiName);
        if(parentRecord && parentApiName == 'Opportunity'){
            console.log('Inside with Parent: ' + parentApiName);
            createRecordEvent.setParams({
                "entityApiName": objectAPIName,
                "recordTypeId" : recordTypeId,
                "defaultFieldValues": {
                    'SBQQ__Opportunity2__c' : parentRecord.Id,
                    'No_of_Units__c' : parentRecord.Number_of_Units__c 
                }
            });
        }
        else{
            console.log('Updated objectAPIName: ' + objectAPIName);
            createRecordEvent.setParams({
                "entityApiName": objectAPIName,
                "recordTypeId" : recordTypeId,
                "defaultFieldValues": {
                }
            });
        }
        console.log(JSON.stringify(createRecordEvent.getParams()));
        createRecordEvent.fire();
    }
})