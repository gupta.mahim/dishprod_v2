({
	doInit : function(component, event, helper) {
		var pageRef= component.get("v.pageReference");
        var state = pageRef.state; // state holds any query params
        //console.log(JSON.stringify(state));
        var addressableContext;
        var objectApiName;
        var parentApiName;
        var recordTypeId;
        var parentRecordId;
 		var urlAddress;
        console.log('OBJECT API Name: ' + JSON.stringify(component.get("v.pageReference").attributes.objectApiName));
        var base64Context = state.inContextOfRef;
        if(base64Context){
            if (base64Context.startsWith("1\.")) {
                base64Context = base64Context.substring(2);
            }
            addressableContext = JSON.parse(window.atob(base64Context));
            console.log(addressableContext);
            parentApiName = addressableContext.attributes.objectApiName;
            recordTypeId = component.get("v.pageReference").state.recordTypeId;
            objectApiName = component.get("v.pageReference").attributes.objectApiName;
            console.log(addressableContext);
            console.log(addressableContext.attributes.recordId);
            console.log(addressableContext.attributes.objectApiName);
            console.log("recordId: "+ addressableContext.attributes.recordId);
            console.log("objectApiName: "+ objectApiName);
            parentRecordId = addressableContext.attributes.recordId;
        }
        
        if(parentRecordId){
            helper.getParentRecord(component, event, helper, parentRecordId, parentApiName, objectApiName, recordTypeId);
        }
        else{
            console.log("Inside ELse");
            helper.openRecordCreator(component, event, helper, objectApiName, recordTypeId, parentRecordId, parentApiName);
        }
	}
})