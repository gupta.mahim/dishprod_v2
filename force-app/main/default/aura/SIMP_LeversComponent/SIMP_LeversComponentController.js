({
    doInit : function(component, event, helper){
        //alert(component.get("v.recordId"));
        var pageRef= component.get("v.pageReference");
        var state = pageRef.state;
        var recordId = state.c__Id;
        var url = '/apex/sbqq__sb?id=' + recordId;
        component.set("v.retUrl", url);
        console.log(url);
        var columns = component.get("v.numberOfColumns");
        columns.push(1);
        var fieldList = component.get("v.fieldList");
        var fieldNames = ['ARPU','Upfront Door Money','Number of Units','Number of Buildings'];
        fieldNames.forEach(function(field){
            fieldList.push(helper.createObj(field, 0));
        });
        component.set("v.fieldList", fieldList);
        console.log(fieldList);
        var action = component.get("c.getQuoteWithLine");
        action.setParams({
            "quoteId": recordId,
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state: ' + state);
            if (state === "SUCCESS") {
                console.log('Quote: ' + JSON.stringify(response.getReturnValue().Id));
                component.set("v.quoteRec", response.getReturnValue());
                var quoteLine = response.getReturnValue().SBQQ__LineItems__r[0];
                component.set("v.quoteLineRec", quoteLine);
                // IF THERE'S NO QUOTELINE ITEM, RETURN ERROR TO QUICK SAVE QUOTELINE FIRST BEFORE USING LEVERS
                //console.log('QuoteLine: ' + quoteLine);
                //console.log('Id: ' + JSON.stringify(quoteLine.Id));
                //console.log('ARPU: ' + JSON.stringify(quoteLine.ARPU__c));
                component.set('v.hasLoaded', true);
            }
        })
        $A.enqueueAction(action);
        
        var getProfile = component.get("c.getProfileCustomSet");
        getProfile.setCallback(this, function(response){
            var state = response.getState();
            console.log('state: ' + state);
            if (state === "SUCCESS") {
                //console.log('Quote: ' + JSON.stringify(response.getReturnValue().Id));
                component.set("v.isVisible", response.getReturnValue());
                console.log('RETVAL',response.getReturnValue());
                //var quoteLine = response.getReturnValue().SBQQ__LineItems__r[0];
               // component.set("v.quoteLineRec", quoteLine);
                // IF THERE'S NO QUOTELINE ITEM, RETURN ERROR TO QUICK SAVE QUOTELINE FIRST BEFORE USING LEVERS
                //console.log('QuoteLine: ' + quoteLine);
                //console.log('Id: ' + JSON.stringify(quoteLine.Id));
                //console.log('ARPU: ' + JSON.stringify(quoteLine.ARPU__c));
                //component.set('v.hasLoaded', true);
            }
        })
        $A.enqueueAction(getProfile);
    },
    
    handleClick : function(component, event, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": component.get("v.retUrl")
        });
        //helper.refresh(component, event, helper);
        urlEvent.fire();
        component.destroy();
        $A.get('e.force:refreshView').fire();
    },

    reinit : function(component, event, helper){
        $A.get('e.force:refreshView').fire();
    },
    
    handleRow : function(component, event, helper){
        var columns = component.get("v.numberOfColumns");
        //console.log(columns[columns.length - 1] + 1);
        columns.push(columns[columns.length - 1] + 1);
        component.set("v.numberOfColumns", columns);
    },
})