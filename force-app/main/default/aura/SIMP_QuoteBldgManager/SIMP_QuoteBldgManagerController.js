({
    doInit : function(component, event, helper) {
       	helper.getBldgCount(component, event);
    },
    
    createBldg : function(component, event, helper){
        helper.createRecord(component, event);
    },
    
    deleteAllBldg : function(component, event, helper){
        if(confirm("Are you sure you wanna delete all Building Records?")){
          helper.deleteAllRecord(component, event);
           
        }
    },
    
    updateCopyCount : function(component, event, helper){
        var count = component.find("numberOfBLDGCopies").get("v.value").substring(0,1) == 0 ? '' : component.find("numberOfBLDGCopies").get("v.value");
        component.set("v.bldgCount", count);
    },
    
    handleShowToast : function(component, event, helper){
        //console.log("Show Final Toast"); 
        //$A.get('e.force:refreshView').fire();
        helper.callCreateBldgCopies(component, event);
    },
    
    checkStatus : function(component, event, helper){
        //$A.get('e.force:refreshView').fire();
        helper.getRecordType(component, event);
    },
    
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
})