trigger DISH_Contract_AutoBothTrigger on DISH_Contract_Auto__c (after insert, before insert, before update, after update, before delete, after delete) { 
    FSTR.COTriggerHandler.handleBothTrigger();
}