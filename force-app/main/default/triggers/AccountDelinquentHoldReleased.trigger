trigger AccountDelinquentHoldReleased on Account (After Update) {

Account AC = Trigger.New[0];
 
    Set<Id> ACIds = new Set<Id>();
            ACIds.add(AC.Id);

       list<account> ACSNOH = [select id, Flags__c from account where Id in :ACIds AND Flags__c EXCLUDES ('Operations Hold') ];
       list<account> ACSAH = [select id, Flags__c from account where Id in :ACIds AND Flags__c INCLUDES ('Audit Hold') ];
       list<case> CAS = [select id, OwnerID from case where Case.AccountID in :ACSNOH AND Case.OwnerID = '00G60000002Ia7P' AND (Status = 'Ready for Processing' OR Status = 'Form Received' OR Status = 'Form Submitted') ];
    
       if(CAS.size() > 0 && ACSNOH.size() > 0 && ACSAH.size() < 1){            for (Case CAS2: CAS) {                  { CAS2.OwnerID= '00G60000001Na1r';}           }           update(CAS);        }
       if(CAS.size() > 0 && ACSNOH.size() > 0 && ACSAH.size() > 0){            for (Case CAS2: CAS) {                   { CAS2.OwnerID= '00G60000002Ibsh';}            }            update(CAS);        }}