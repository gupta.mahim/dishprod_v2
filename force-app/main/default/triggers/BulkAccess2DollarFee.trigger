Trigger BulkAccess2DollarFee on OpportunityLineItem (After Insert) {

    List<String> Ids = new List<String>{};

    OpportunityLineItem Oli = trigger.new[0];
        {
            Set<Id> oliIds = new Set<Id>();
            Set<Id> oppIds = new Set<Id>();
               oliIds.add(Oli.Id);
               oppIds.add(Oli.OpportunityId);
                    System.debug('RESULTS of the oliIds.add ' +oliIds);
                    System.debug('RESULTS of the oppIds.add ' +oppIds);
 
            List<Opportunity> oppList = [Select Id, Launch_Date__c, Pricebook2.Id, Opportunity_Exception__c, Property_Status__c From Opportunity Where Id in :oppIds LIMIT 1];
                System.debug('RESULTS of the oppList ' +oppList);
        
                Set<Id> pbIds = new Set<Id>();
                    pbIds.add(oppList[0].Pricebook2.Id);
                        System.debug('RESULTS of the pbIds.add ' +pbIds);
        
            if( OppList.size() > 0 )
                {
                    Date myDate = Date.today();
                    Date startDate = date.newInstance(2016, 11, 17);
                    Date launchDate = oppList[0].Launch_Date__c;

                    if ( startDate <= launchDate && OppList[0].Property_Status__c != NULL  && OppList[0].Property_Status__c != 'TransFerred' && OppList[0].Property_Status__c != 'Disconnected' && OppList[0].Opportunity_Exception__c != '$2 Bulk Local Access Fee')
                       {
                            List<opportunityLineItem> olis = [select id, OpportunityId, Opportunity.Id, Quantity, ProductCode from OpportunityLineItem where Opportunity.Id in :oppList ];                
                                System.debug('RESULTS of the LIST Oli ' +Oli);

                            List<PriceBookEntry> pbe = [select id, PriceBook2.Id, ProductCode from PriceBookEntry where Pricebook2.Id in :pbIds ];                
                                System.debug('RESULTS of the LIST pbe ' +pbe);

                           Set<Id> pbeIds = new Set<Id>();
                               pbeIds.add(pbe[0].Id);
                                   System.debug('RESULTS of the pbeIds.add ' +pbeIds);
 
                            Set<Id> olisIds = new Set<Id>();
                                olisIds.add(olis[0].OpportunityId);
                                    System.debug('RESULTS of the olisIds.add ' +olisIds);

                            Integer s1 = oppList.size();
                                System.debug('RESULTS of the oppList.size ' +s1);
                            Integer s2 = olis.size();
                                System.debug('RESULTS of the olis.size ' +s2);

                            if( olis.size() > 0 )
                            {   
                                for (OpportunityLineItem oli2: Trigger.New)
                                {
                                // If we have a Local or RSN go keep going
                                if ( olis[0].ProductCode == 'TN' || olis[0].ProductCode == '67' || olis[0].ProductCode == 'TW' || olis[0].ProductCode == '>|' || olis[0].ProductCode == '7!' || olis[0].ProductCode == '66' || olis[0].ProductCode == ';!' || olis[0].ProductCode == '65' || olis[0].ProductCode == '6~' || olis[0].ProductCode == '7/' || olis[0].ProductCode == '7{' || olis[0].ProductCode == '68' || olis[0].ProductCode == '7?' || olis[0].ProductCode == '6=' || olis[0].ProductCode == '=T' || olis[0].ProductCode == 'T(' || olis[0].ProductCode == '7;' || olis[0].ProductCode == '6{' || olis[0].ProductCode == '7:' || olis[0].ProductCode == '7=' || olis[0].ProductCode == '3G' || olis[0].ProductCode == '7-' || olis[0].ProductCode == '7.' || olis[0].ProductCode == '7<' || olis[0].ProductCode == '69' || olis[0].ProductCode == '/{' || olis[0].ProductCode == 'OC8' )
                                { 
                            Set<Id> plo = new Set<Id>();        for (OpportunityLineItem oli3: Trigger.New) { 
                                        // These should be only if we have a local or rsn then if we have a low core
                                        if( olis[0].ProductCode == 'YQ' || olis[0].ProductCode == '~Z' || olis[0].ProductCode == '9S' || olis[0].ProductCode == 'BQ8' || olis[0].ProductCode == 'BR1' || olis[0].ProductCode == 'BR3' || olis[0].ProductCode == 'M=' || olis[0].ProductCode == 'Z.' || olis[0].ProductCode == 'AI' ) { for (OpportunityLineItem oli4: Trigger.New) {
                                                        // These should be only if we have a local or rsn and a low core + These high cores
                                                        if( olis[0].ProductCode == 'YQ' || olis[0].ProductCode == '~Z' || olis[0].ProductCode == '9S' || olis[0].ProductCode == 'BQ8' || olis[0].ProductCode == 'BR1' || olis[0].ProductCode == 'BR3' || olis[0].ProductCode == 'M=' || olis[0].ProductCode == 'Z.' || olis[0].ProductCode == 'AI' )                                   
                                                           {
                                                               // Since we also have a high core / Exception, let's do nothing
                                                           }
                                                        else
                                                        for (OpportunityLineItem oli5: Trigger.New) { if ( olis[0].ProductCode == 'OB1')
                                                                    {
                                                                        // Since they already have an OB1, let's do nothng
                                                                    }
                                                                else
                                                                if(oppList[0].Weighted_Average__c != null) { OpportunityLineItem Oli7 = new OpportunityLineItem( Quantity = oppList[0].Weighted_Average__c, OpportunityId = oppList[0].Id, PricebookentryId = pbe[0].id ); }
                                                                else
                                                                if(oppList[0].Weighted_Average__c == null)                                                                     { OpportunityLineItem Oli7 = new OpportunityLineItem( Quantity = oppList[0].Number_of_Units__c, OpportunityId = oppList[0].Id, PricebookentryId = pbe[0].id ); Insert oli; }
                                    }
  }}}}}}}}}}