trigger FTGClone on Lead (after insert)
{
Lead L = trigger.new[0];
if(L.Category__c  == 'FTG' && L.Clone_Version__c == '0' && L.LeadSource != 'Self Generated' && L.LeadSource != 'Data.com' && L.LeadSource != 'Jigsaw' && L.LeadSource != 'Tradeshow' && L.Number_of_Units__c >= 50)
{
Lead[] LeadClone = new Lead[0];

map<id,Lead> entries = new map<id,Lead>(); 

for(Lead record:trigger.new)
entries.put(record.id,null); 
entries.putall([select id,Name, Company, LastName, FirstName,Lead_Source_External__c, Number_of_TVs__c, Email, Location_of_Service__c, Building_Stories__c, MobilePhone, Phone, Business_Phone_Ext__c, Business_Phone__c, Number_of_Units__c, Number_of_Locations__c, Type_of_Business__c, Street, State, City, PostalCode, HasOptedOutOfEmail, Email_Opt_Out_Reason__c from Lead where id in :entries.keyset()]); 


for(Lead record:trigger.new) 
LeadClone.add(new Lead ( Ownerid='00G60000001G9uA', Category__c='FTG', Original_Lead_Id__c=record.Id,Company=entries.get(record.id).Company,LastName=entries.get(record.id).LastName,FirstName=entries.get(record.id).FirstName,Clone_Version__c='1',Lead_Source_External__c=entries.get(record.id).Lead_Source_External__c,Number_of_TVs__c=entries.get(record.id).Number_of_TVs__c, Email=entries.get(record.id).Email, Location_of_Service__c=entries.get(record.id).Location_of_Service__c, Building_Stories__c=entries.get(record.id).Building_Stories__c, MobilePhone=entries.get(record.id).MobilePhone, Phone=entries.get(record.id).Phone, Business_Phone_Ext__c=entries.get(record.id).Business_Phone_Ext__c, Business_Phone__c=entries.get(record.id).Business_Phone__c, Number_of_Units__c=entries.get(record.id).Number_of_Units__c, Number_of_Locations__c=entries.get(record.id).Number_of_Locations__c, Type_of_Business__c=entries.get(record.id).Type_of_Business__c, Street=entries.get(record.id).Street, State=entries.get(record.id).State, City=entries.get(record.id).City, PostalCode=entries.get(record.id).PostalCode, Email_Opt_Out_Reason__c=entries.get(record.id).Email_Opt_Out_Reason__c, HasOptedOutOfEmail=entries.get(record.id).HasOptedOutOfEmail ));

for(Lead record:trigger.new) 
LeadClone.add(new Lead ( Ownerid='00G60000001G9uF', Category__c='FTG', Original_Lead_Id__c=record.Id,Company=entries.get(record.id).Company,LastName=entries.get(record.id).LastName,FirstName=entries.get(record.id).FirstName,Clone_Version__c='2',Lead_Source_External__c=entries.get(record.id).Lead_Source_External__c,Number_of_TVs__c=entries.get(record.id).Number_of_TVs__c, Email=entries.get(record.id).Email, Location_of_Service__c=entries.get(record.id).Location_of_Service__c, Building_Stories__c=entries.get(record.id).Building_Stories__c, MobilePhone=entries.get(record.id).MobilePhone, Phone=entries.get(record.id).Phone, Business_Phone_Ext__c=entries.get(record.id).Business_Phone_Ext__c, Business_Phone__c=entries.get(record.id).Business_Phone__c, Number_of_Units__c=entries.get(record.id).Number_of_Units__c, Number_of_Locations__c=entries.get(record.id).Number_of_Locations__c, Type_of_Business__c=entries.get(record.id).Type_of_Business__c, Street=entries.get(record.id).Street, State=entries.get(record.id).State, City=entries.get(record.id).City, PostalCode=entries.get(record.id).PostalCode, Email_Opt_Out_Reason__c=entries.get(record.id).Email_Opt_Out_Reason__c, HasOptedOutOfEmail=entries.get(record.id).HasOptedOutOfEmail ));


insert LeadClone; }}