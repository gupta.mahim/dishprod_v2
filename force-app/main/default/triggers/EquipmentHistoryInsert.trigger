trigger EquipmentHistoryInsert on Equipment__c (After Insert) {

   for (Equipment__c OD: Trigger.New) {
           {Head_End_Equipment_Log__c OAD = new Head_End_Equipment_Log__c(
               Opportunity__c = OD.Opportunity__c,
               Name = OD.Name,
               Smartcard__c = OD.Receiver_S__c,
               Action__c = OD.Action__c,
               Trigger_Action__c = 'Insert');
           insert OAD;
       }
    }
}