trigger OppPropertyType on Opportunity (Before Update, Before Insert) { 
Opportunity OPT = trigger.new[0];
// Private
if(OPT.CSG_Account_Number__c >= '8255707080000000' && OPT.CSG_Account_Number__c <= '8255707089999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null)
{ OPT.Property_Type__c = 'Private'; OPT.Prin_s__c = '7000'; OPT.Agent__c = '7080'; OPT.Category__c = 'P/P'; }
else
// Private Puerto Rico
if(OPT.CSG_Account_Number__c >= '8255490000000000' && OPT.CSG_Account_Number__c <= '8255499999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null)
{ OPT.Property_Type__c = 'Private - Puerto Rico'; OPT.Prin_s__c = '4900'; OPT.Agent__c = '0010'; OPT.Category__c = 'P/P'; }
else
// Public Puerto Rico
if(OPT.CSG_Account_Number__c >= '8255470000000000' && OPT.CSG_Account_Number__c <= '8255479999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'Public - Puerto Rico'; OPT.Prin_s__c = '4700'; OPT.Agent__c = '0010'; OPT.Category__c = 'P/P'; }
else
// Public
if(OPT.CSG_Account_Number__c >= '8255707050000000' && OPT.CSG_Account_Number__c <= '8255707059999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'Public'; OPT.Prin_s__c = '7000'; OPT.Agent__c = '7050'; OPT.Category__c = 'P/P'; }
else
// BULK MDU Puerto Rico
if(OPT.CSG_Account_Number__c >= '8255560000000000' && OPT.CSG_Account_Number__c <= '8255569999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'MDU Bulk - Puerto Rico'; OPT.Prin_s__c = '5600'; OPT.Agent__c = '0010'; OPT.Category__c = 'MDU'; }
else
// MDU Bulk
if(OPT.CSG_Account_Number__c >= '8255707030000000' && OPT.CSG_Account_Number__c <= '8255707039999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'MDU Bulk'; OPT.Prin_s__c = '7000'; OPT.Agent__c = '7030'; OPT.Category__c = 'MDU'; }
else
// BULK FTG Puerto Rico
if(OPT.CSG_Account_Number__c >= '8255540000000000' && OPT.CSG_Account_Number__c <= '8255549999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'FTG Bulk - Puerto Rico'; OPT.Prin_s__c = '5400'; OPT.Agent__c = '0010'; OPT.Category__c = 'FTG'; }
else
// Bulk FTG
if(OPT.CSG_Account_Number__c >= '8255707020000000' && OPT.CSG_Account_Number__c <= '8255707029999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'FTG Bulk'; OPT.Prin_s__c = '7000'; OPT.Agent__c = '7020'; OPT.Category__c = 'FTG'; }
else
// Bulk Hospital & Nursing
if(OPT.CSG_Account_Number__c >= '8255707010000000' && OPT.CSG_Account_Number__c <= '8255707019999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'Bulk Hospital - Nursing'; OPT.Prin_s__c = '7000'; OPT.Agent__c = '7010'; OPT.Category__c = 'FTG'; }
else
// Muzak Puerto Rico
if(OPT.CSG_Account_Number__c >= '8255620010000000' && OPT.CSG_Account_Number__c <= '8255620019999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'Muzak - Puerto Rico'; OPT.Prin_s__c = '6200'; OPT.Agent__c = '0010'; OPT.Category__c = 'MUZ'; }
else
// Muzak
if(OPT.CSG_Account_Number__c >= '8255707070000000' && OPT.CSG_Account_Number__c <= '8255707079999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'Muzak'; OPT.Prin_s__c = '7000'; OPT.Agent__c = '7070'; OPT.Category__c = 'MUZ'; }
else
// UVTV Transport
if(OPT.CSG_Account_Number__c >= '8255290000000000' && OPT.CSG_Account_Number__c <= '8255299999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'UVTV'; OPT.Prin_s__c = '2900'; OPT.Agent__c = '0010'; OPT.Category__c = 'TRP'; }
else
// Airline
if(OPT.CSG_Account_Number__c >= '8255260010000000' && OPT.CSG_Account_Number__c <= '8255260019999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'Airline'; OPT.Prin_s__c = '2600'; OPT.Agent__c = '0010'; OPT.Category__c = 'AIR'; }
else
// PCO MDU Residential
if(OPT.CSG_Account_Number__c >= '8255250000000000' && OPT.CSG_Account_Number__c <= '8255259999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'PCO - MDU Residential'; OPT.Prin_s__c = '2500'; OPT.Agent__c = '0010'; OPT.Category__c = 'SDS'; }
else
// PCO Analog
if(OPT.CSG_Account_Number__c >= '8255240000000000' && OPT.CSG_Account_Number__c <= '8255249999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'PCO - Analog'; OPT.Prin_s__c = '2400'; OPT.Agent__c = '0010'; OPT.Category__c = 'PCO'; }
else
// PCO MDU More DISH
if(OPT.CSG_Account_Number__c >= '8255909000000000' && OPT.CSG_Account_Number__c <= '8255909999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'Residential SDS'; OPT.Prin_s__c = '9000'; OPT.Agent__c = '0000'; OPT.Category__c = 'SDS'; }
else
// Hierarchy
if(OPT.CSG_Account_Number__c >= '8255220000000000' && OPT.CSG_Account_Number__c <= '8255229999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'Hierarchy'; OPT.Prin_s__c = '2200'; OPT.Agent__c = '0010'; OPT.Category__c = 'HRY'; }
else
// MDU More DISH
if(OPT.CSG_Account_Number__c >= '8255160000000000' && OPT.CSG_Account_Number__c <= '8255169999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'MDU More DISH'; OPT.Prin_s__c = '1600'; OPT.Agent__c = '0010'; OPT.Category__c = 'SDS'; }
else
// SDS (MDU Residential) Puerto Rico
if(OPT.CSG_Account_Number__c >= '8255910000000000' && OPT.CSG_Account_Number__c <= '8255919999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'SDS - Puerto Rico'; OPT.Prin_s__c = '9100'; OPT.Agent__c = '0000'; OPT.Category__c = 'SDS'; }
else
// SDS (MDU Residential)
if(OPT.CSG_Account_Number__c >= '8255130010000000' && OPT.CSG_Account_Number__c <= '8255130019999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'SDS'; OPT.Prin_s__c = '1300'; OPT.Agent__c = '0010'; OPT.Category__c = 'SDS'; }
else
// Institutional Residential Puerto Rico
if(OPT.CSG_Account_Number__c >= '8255420000000000' && OPT.CSG_Account_Number__c <= '8255429999999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'Institutional Residential - Puerto Rico'; OPT.Prin_s__c = '4200'; OPT.Agent__c = '0010'; OPT.Category__c = 'I/R'; }
else
// Institutional Residential
if(OPT.CSG_Account_Number__c >= '8255100010000000' && OPT.CSG_Account_Number__c <= '8255100019999999' && OPT.Property_Type__c == Null && OPT.Category__c == Null )
{ OPT.Property_Type__c = 'Institutional Residential'; OPT.Prin_s__c = '1000'; OPT.Agent__c = '0010'; OPT.Category__c = 'I/R'; }
// else
//  if( (OPT.CSG_Account_Number__c != '') 
//  && (OPT.CSG_Account_Number__c < '8255707080000000' && OPT.CSG_Account_Number__c > '8255707089999999' ) 
//  && (OPT.CSG_Account_Number__c < '825510000000' && OPT.CSG_Account_Number__c > '8255490019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255470010000000' && OPT.CSG_Account_Number__c > '8255470019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255707050000000' && OPT.CSG_Account_Number__c > '8255707059999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255560010000000' && OPT.CSG_Account_Number__c > '8255560019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255707030000000' && OPT.CSG_Account_Number__c > '8255707039999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255540010000000' && OPT.CSG_Account_Number__c > '8255540019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255707020000000' && OPT.CSG_Account_Number__c > '8255707029999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255707010000000' && OPT.CSG_Account_Number__c > '8255707019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255620010000000' && OPT.CSG_Account_Number__c > '8255620019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255707070000000' && OPT.CSG_Account_Number__c > '8255707079999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255290010000000' && OPT.CSG_Account_Number__c > '8255290019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255260010000000' && OPT.CSG_Account_Number__c > '8255260019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255250010000000' && OPT.CSG_Account_Number__c > '8255250019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255240010000000' && OPT.CSG_Account_Number__c > '8255240019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255230010000000' && OPT.CSG_Account_Number__c > '8255230019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255220010000000' && OPT.CSG_Account_Number__c > '8255220019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255160010000000' && OPT.CSG_Account_Number__c > '8255160019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255450010000000' && OPT.CSG_Account_Number__c > '8255450019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255130010000000' && OPT.CSG_Account_Number__c > '8255130019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255420010000000' && OPT.CSG_Account_Number__c > '8255420019999999' ) 
//  && (OPT.CSG_Account_Number__c < '8255420010000000' && OPT.CSG_Account_Number__c > '8255420019999999' )
//  && (OPT.CSG_Account_Number__c < '8255100010000000' && OPT.CSG_Account_Number__c > '8255100019999999' )
//  )
//  { OPT.Property_Type__c = 'Invalid CSG #'; OPT.Prin_s__c = 'Invalid CSG #'; OPT.Agent__c = 'Invalid CSG #'; OPT.Category__c = 'Invalid CSG #'; }
}