trigger CaseCopyToCaseEmailfromOpp on Case (Before Insert)
   {
       Set<Id> caseIds=new Set<Id>();
        for(Case CA:Trigger.New)
         {
            if(CA.Contact_Email_Address__c == Null && CA.Contact_Email__c == Null ){
             caseIds.add(CA.Opportunity__c);

         Map<Id,Opportunity> opptyMap=new  Map<Id,Opportunity> ([select Id,Contact_Email__c from Opportunity where Id in :caseIds ]);
         {
         if(OpptyMap.size() > 0){
              CA.Contact_Email_Address__c = opptyMap.get(CA.Opportunity__c).Contact_Email__c;
         }}
    }}}