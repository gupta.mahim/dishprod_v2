trigger LeadCategorySelectionFTG on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];

//Thinh 9/15 fixing hospital leads
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG' ){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hospitals' && ( L.Location_of_Service__c == Null || L.Location_of_Service__c == '') && L.Category__c != 'FTG' && L.Number_of_Units__c > 1 ){L.Category__c = 'FTG';}
else
if(L.Lead_Source_External__c == 'Hospitals' && ( L.Location_of_Service__c == Null || L.Location_of_Service__c == '') && L.Category__c != 'FTG' && L.Number_of_Units__c > 1 ){L.Category__c = 'FTG';} 
else
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c == ''      && L.Category__c != 'FTG' && L.Number_of_Units__c == NULL ){L.Category__c = 'FTG';}

else
if(L.Type_of_Business__c == 'Prisons' && L.Location_of_Service__c == 'Cells' && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Casinos' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == NULL && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c != 'Common Area'  && L.Location_of_Service__c != 'Food Service' && L.Location_of_Service__c != 'Rooms' && L.Category__c != 'FTG')
{L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Prisons' && L.Location_of_Service__c == 'Cells' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Casinos' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Rooms' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == NULL && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c != 'Common Area'  && L.Location_of_Service__c != 'Food Service' && L.Location_of_Service__c != 'Rooms' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'Hotels' && L.Category__c != 'FTG'){L.Category__c = 'FTG'; L.Type_of_Business__c='Hotel/Motel';}
else
if(L.Type_of_Business__c == 'RV Parks Marinas' && L.Category__c != 'FTG'){L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'RV Park' && L.Location_of_Service__c == 'Connection' && L.Category__c != 'FTG' && L.Category__c != 'FTG' ){L.Category__c = 'FTG'; }
else
if(L.Type_of_Business__c == 'Marinas' && L.Location_of_Service__c == 'Dock Hookup' && L.Category__c != 'FTG' && L.Category__c != 'FTG' ){L.Category__c = 'FTG'; }
else
if(L.Type_of_Business__c == 'other' && L.Category__c != 'FTG' ){L.Category__c = 'FTG'; }
else
if(L.Type_of_Business__c == 'bulk' && L.Category__c != 'FTG' ){L.Category__c = 'FTG'; }
else

//Evolve - Thinh 7/28/2020
if(L.Type_of_Business__c == 'evolve' && L.Lead_Source_External__c == 'evolve' ){ L.Category__c = 'FTG'; L.Type_of_Business__c='Hotel/Motel';}
else
if(L.Type_of_Business__c == 'smartbox' && L.Lead_Source_External__c == 'smartbox' ){ L.Category__c = 'FTG'; }
else
if(L.Type_of_Business__c == 'University'  ){ L.Category__c = 'FTG';}
else
if(L.Type_of_Business__c == 'universities' ){ L.Category__c = 'FTG';}

// 8/26 Thinh add Number of Units to Hospitals

}