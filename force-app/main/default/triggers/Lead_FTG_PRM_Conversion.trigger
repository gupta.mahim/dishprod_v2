// Converts FTG PRM Owned Leads into an opportunity - No Contact - Attaches to PRM owners Partner Account

trigger Lead_FTG_PRM_Conversion on Lead (After Update) 
    {for (Lead L: Trigger.New){if(L.RecordTypeId=='0126000000019IE' && L.MDU_Convert_Lead__c == True && L.isConverted == false )        {Opportunity o = new Opportunity(
             Name = L.Company,  
             AccountId=UserUtil.CurrentUser.Account__c,  
             OwnerId=L.OwnerId,   
             RecordTypeId='01260000000LuQM',   
             Property_Status__c = 'Pending Activation',  
             Address__c = L.Street,   
             City__c=L.City,   
             State__c=L.State,   
             Zip__c=L.PostalCode,   
             Phone__c=L.Phone,   
             Property_Type__c=L.Type_of_Business__c, 
             LeadSource=L.LeadSource,   
             External_Lead_Source__c=L.Lead_Source_External__c,  
             Building_Stories__c=L.Building_Stories__c,   
             Number_of_Units__c=L.Number_of_Units__c,  
             Category__c=L.Category__c,   
             Contact_Name__c=L.Name,    
             Contact_Email__c=L.Email,    
             Contact_Phone__c=L.Phone,    
             StageName='Under Contract',    
             CloseDate=system.today());                        insert o; Database.LeadConvert lc = new Database.LeadConvert();        lc.setLeadId(L.Id);     lc.setAccountId(UserUtil.CurrentUser.Account__c);         lc.setDoNotCreateOpportunity(true);          lc.ConvertedStatus='Converted';        
Database.LeadConvertResult     lcr = Database.convertLead(lc);          System.assert(lcr.isSuccess()); }}}