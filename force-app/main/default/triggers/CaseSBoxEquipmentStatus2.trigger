trigger CaseSBoxEquipmentStatus2 on Case (After Insert) {

Case cas = trigger.new[0];
         {
            Set<Id> caseIds = new Set<Id>();
            Set<Id> opIds = new Set<Id>();
               opIds.add(cas.Opportunity__c);
               caseIds.add(cas.Id);
        if ( Cas.RecordTypeID == '01260000000YLR2' || Cas.RecordTypeID == '012f20000005Pu4' || Cas.RecordTypeID == '0126000000017LQ' || Cas.RecordTypeID == '012600000001Ccy' || Cas.RecordTypeID == '0126000000018w6' || Cas.RecordTypeID == '01260000000JO8Y' )
        {
        list<smartbox__c> sbxequp = [select id, Opportunity__c, Status__c, Submitted__c, CaseID__c, Change_Schedule__c from smartbox__c where Opportunity__c in :opIds AND CaseID__C not in :caseIds AND (Status__c = 'Activation Requested' OR Status__c = 'Drop Requested') AND Submitted__c != True ];
        list<case> xc2 = [select id, Status, CaseNumber, RecordType.Name, Pre_Activation_Date__c, RA__c, Hidden_Case_History__c, RequestedAction__c, CreatedDate, Product_Change_Request_s__c, Requested_Actvation_Date_Time__c from case where Id in :caseIds];
        list<opportunity> xopp = [select id, Hidden_Smartbox_Equipment__c from opportunity where Id in :opIds ];
        
        if(sbxequp.size() > 0 && xc2.size() > 0 && xopp.size() > 0)
            {
            for (smartbox__c sbxequp2: sbxequp) 
                {
                   // if(xequp2.Statis__c == 'Add Requested' && xequp2.CaseID__c == NULL)
                        {
                           sbxequp2.CaseID__c = xc2[0].Id;
                           sbxequp2.Change_Schedule__c = xc2[0].Requested_Actvation_Date_Time__c;
                           sbxequp2.Submitted__c = True;
                           sbxequp2.Most_Recent_Case_Create_Date__c = xc2[0].CreatedDate;
                           sbxequp2.Most_Recent_Case_ID__c = xc2[0].id;
                           sbxequp2.Most_Recent_Case_Number__c = xc2[0].CaseNumber;
                           sbxequp2.Most_Recent_Case_Pre_Activation_Date__c = xc2[0].Pre_Activation_Date__c;
                           sbxequp2.Most_Recent_Case_RA__c = xc2[0].RA__c;
                           sbxequp2.Most_Recent_Case_Record_Type__c = xc2[0].RecordType.Name;
                           sbxequp2.Most_Recent_Case_Request_Completion_Date__c = xc2[0].Requested_Actvation_Date_Time__c;
                           sbxequp2.Most_Recent_Case_Type__c = xc2[0].RequestedAction__c;
                        }
               }
            for (opportunity xopp2: xopp) 
                {
                    xopp2.Hidden_Smartbox_Equipment__c = Null;
                }
        {
            Database.update(sbxequp);
            Database.update(xopp);
        }
    }
}}}