trigger Verizon_CalloutTrigger on Smartbox__c (before update) {

// Login Process 1st get the M2M token using the key, secerete and such then use the M2M token to get the VZ Token. All other request require sending the VZ Token.
Smartbox__c S = trigger.new[0];

// Get the M2M Token
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == true && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == false)
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_GetM2MToken.makeCalloutM2M(S.Id); System.debug('IF 1' +S);}
else

// Get the VZ-M2M Token
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == true && S.Verizon_Do_It_Action__c == false)
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_GetVZToken.makeCallout(S.Id); System.debug('IF 2' +S);}
else

// Function Suspend
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Suspend')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Suspend.makeCalloutSuspend(S.Id); System.debug('IF 3' +S);}
else

// Function Restore
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Restore')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Restore.makeCalloutRestore(S.Id); System.debug('IF 4' +S);}
else

// Function Deactivate
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Deactivate')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Deactivate.makeCalloutDeactivate(S.Id); System.debug('IF 5' +S);}
else

// Function Activate
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Activate')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Activate.makeCalloutActivate(S.Id); System.debug('IF 6' +S);}
else

// Function Add new Account
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'CreateAccount')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_CreateAccount.makeCalloutCreateAccount(S.Id); System.debug('IF 7' +S);}
else

// Function SMS Up - 120
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSUp120')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Up120.makeCalloutSMS_Up120(S.Id); System.debug('IF 8' +S);}
else

// Function SMS Up
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSUp')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Up.makeCalloutSMS_Up(S.Id); System.debug('IF 10' +S);}
else    
    
// Function SMS Down
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSDown')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Down.makeCalloutSMS_Down(S.Id); System.debug('IF 9' +S);}
else

// Function SMS Up - 120 4G
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSUP120-4G')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Up120_4G.makeCalloutSMS_Up120(S.Id); System.debug('IF 10' +S);}
else

// Function SMS Up 4G
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSUP4G')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Up_4G.makeCalloutSMS_Up(S.Id); System.debug('IF 11' +S);}
else    
    
// Function SMS Down 4G
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'SMSDown-4G')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_SMS_Down_4G.makeCalloutSMS_Down(S.Id); System.debug('IF 12' +S);}
else

// Function Activate 4G
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Activate-4G')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Activate_4G.makeCalloutActivate(S.Id); System.debug('IF 13' +S);}
else 

// Function Deactivate 4G
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Deactivate-4G')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Deactivate_4G.makeCalloutDeactivate(S.Id); System.debug('IF 14' +S);}
else

// Function Suspend 4G
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Suspend-4G')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Suspend_4G.makeCalloutSuspend(S.Id); System.debug('IF 15' +S);}
else

// Function Restore 4G
if(S.ModemID__c != '' && S.Verizon_Do_It_Get_M2M_Token__c == false && S.Verizon_Do_It_Login__c == false && S.Verizon_Do_It_Action__c == true && S.Verizon_ServiceState__c == 'Restore-4G')
{ S.Verizon_Do_It_Get_M2M_Token__c = false; S.Verizon_Do_It_Login__c = false; S.Verizon_Do_It_Action__c = false; Verizon_CalloutClass_Restore_4G.makeCalloutRestore(S.Id); System.debug('IF 16' +S);}

}