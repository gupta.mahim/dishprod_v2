trigger shareFilesWithCommunityUsers on ContentDocumentLink(before insert, before update){

   Schema.DescribeSObjectResult r = DISH_Contract__c.sObjectType.getDescribe();
    String keyPrefix = r.getKeyPrefix();

      for(ContentDocumentLink cdl:trigger.new){
        if((String.valueOf(cdl.LinkedEntityId)).startsWith(keyPrefix)){ cdl.ShareType = 'I'; cdl.Visibility = 'AllUsers'; } }}