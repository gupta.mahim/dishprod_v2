// Converts PRM Owned Leads into an opportunity - No Contact - Attaches to PRM owners Partner Account (MDU and FTG)
// No bulk converts!

trigger Lead_PRM_Conversion on Lead (After Update) {
Lead L = trigger.new[0];

    Set<Id> acctId = new Set<Id>();
        acctId.add(UserUtil.CurrentUser.Contact.Account.id);


// if(L.Convert_Lead__c == 'Ready to Activate' && L.isConverted == false && UserUtil.CurrentUser.Contact.Account.ToP__c.CONTAINS( 'Integrator') ){
if(L.Convert_Lead__c == 'Ready to Activate' && L.isConverted == false ) {

    list<account> a1 = [select id, ToP__c from account where Id in :acctId AND ToP__c INCLUDES ('Integrator') ];    list<account> a2 = [select id, ToP__c from account where Id in :acctId AND ToP__c INCLUDES ('PCO') ];    list<account> a3 = [select id, ToP__c from account where Id in :acctId AND ToP__c INCLUDES ('Retailer Bulk') ];
    
        // For Integrators
        if(a1.size() > 0 && a1.size() < 1000 )             {                     Opportunity o = new Opportunity(
                        accountId=UserUtil.CurrentUser.Contact.Account.Id,
                        Name = L.Company,
                        Legal_Name__c=L.Company,
                        Business_Legal_Name__c=L.Company,
                        OwnerId=L.OwnerId,
                        RecordTypeId='01260000000LuQM',
                        Property_Status__c = 'Inactive',
                        Address__c = L.Street,
                        City__c=L.City,
                        State__c=L.State,
                        Zip__c=L.PostalCode,
                        Business_Address__c = L.Street,
                        Business_City__c=L.City,
                        Business_State__c=L.State,
                        Business_Zip_Code__c=L.PostalCode,
                        Phone__c=L.Phone,
                        Fax__c=L.Fax,
                        Name_of_Property_Representative__c=L.FirstName + ' ' + L.LastName,
                        Property_Representative_Title__c=L.Contact_Role__c,
                        Property_Representative_s_email__c=L.Email,
                        Comments__c=L.Lead_Notes__c,
                        LeadSource=L.LeadSource,
                        External_Lead_Source__c=L.Lead_Source_External__c,
                        Description=L.Description,
                        Number_of_Units__c=L.Number_of_Units__c,
                        Property_Type__c=L.Type_of_Business__c,
                        Building_Type__c=L.Building_Type__c,
                        LeadID_18__c=L.LeadID_18__c,
                        StageName='Ready for Activation',
                        CSG_Account_Number__c=L.CSG_Account_Number__c,
                        CloseDate=system.today());
                        
                        insert o;                          Contact c = new Contact(
                        accountId=UserUtil.CurrentUser.Contact.Account.Id,
                        OwnerId=L.OwnerId,
                        RecordTypeId='012600000001B8p',
                        FirstName=L.FirstName,
                        LastName=L.LastName,
                        Best_Time_to_Contact__c=L.Best_Time_to_Contact__c,
                        Email=L.Email,
                        Fax=L.Fax,
                        MobilePhone=L.MobilePhone,
                        Phone=L.Phone,
                        Status__c='Inactive');
                        
                        insert c;                                       OpportunityContactRole oc = new OpportunityContactRole(
                        OpportunityId=o.id,
                        ContactId=c.id,
                        Role='Business Owner',
                        IsPrimary=TRUE);
                        
                        insert oc;                       
                    
                    Database.LeadConvert lc = new Database.LeadConvert();                             lc.setLeadId(L.Id);                         lc.setContactId(c.Id);                          lc.setAccountId(UserUtil.CurrentUser.Contact.Account.Id);                         lc.setDoNotCreateOpportunity(true);                              lc.ConvertedStatus='Converted';     
                    
                    
                    Database.LeadConvertResult                       lcr = Database.convertLead(lc);                                 System.assert(lcr.isSuccess());
                       }  

                   // For PCO's    
                   if(a1.size() < 1 && a2.size() > 0 )                     {                     Opportunity o = new Opportunity(
                        accountId=UserUtil.CurrentUser.Contact.Account.Id,
                        Name = L.Company,
                        Legal_Name__c=L.Company,
                        Business_Legal_Name__c=L.Company,
                        OwnerId=L.OwnerId,
                        RecordTypeId='012600000005Bu3',
                        Property_Status__c = 'Inactive',
                        Address__c = L.Street,
                        City__c=L.City,
                        State__c=L.State,
                        Zip__c=L.PostalCode,
                        Business_Address__c = L.Street,
                        Business_City__c=L.City,
                        Business_State__c=L.State,
                        Business_Zip_Code__c=L.PostalCode,
                        Phone__c=L.Phone,
                        Fax__c=L.Fax,
                        Name_of_Property_Representative__c=L.FirstName + ' ' + L.LastName,
                        Property_Representative_Title__c=L.Contact_Role__c,
                        Property_Representative_s_email__c=L.Email,
                        Comments__c=L.Lead_Notes__c,
                        LeadSource=L.LeadSource,
                        External_Lead_Source__c=L.Lead_Source_External__c,
                        Description=L.Description,
                        Number_of_Units__c=L.Number_of_Units__c,
                        Property_Type__c=L.Type_of_Business__c,
                        Building_Type__c=L.Building_Type__c,
                        LeadID_18__c=L.LeadID_18__c,
                        StageName='Ready for Activation',
                        CSG_Account_Number__c=L.CSG_Account_Number__c,
                        CloseDate=system.today());
                        
                        insert o;                                        Contact c = new Contact(
                        accountId=UserUtil.CurrentUser.Contact.Account.Id,
                        OwnerId=L.OwnerId,
                        RecordTypeId='012600000001B8p',
                        FirstName=L.FirstName,
                        LastName=L.LastName,
                        Best_Time_to_Contact__c=L.Best_Time_to_Contact__c,
                        Email=L.Email,
                        Fax=L.Fax,
                        MobilePhone=L.MobilePhone,
                        Phone=L.Phone,
                        Status__c='Inactive');
                        
                        insert c;                                        OpportunityContactRole oc = new OpportunityContactRole(
                        OpportunityId=o.id,
                        ContactId=c.id,
                        Role='Business Owner',
                        IsPrimary=TRUE);
                        
                        insert oc;                                                              Database.LeadConvert lc = new Database.LeadConvert();                             lc.setLeadId(L.Id);                         lc.setContactId(c.Id);                          lc.setAccountId(UserUtil.CurrentUser.Contact.Account.Id);                         lc.setDoNotCreateOpportunity(true);                               lc.ConvertedStatus='Converted';     
                    
                    
                    Database.LeadConvertResult                       lcr = Database.convertLead(lc);                                 System.assert(lcr.isSuccess());
                       }  
                       
                       // For Bulk Retailer   
                   if(a1.size() < 1 && a2.size() < 1 && a3.size() > 0 )                     {                     Opportunity o = new Opportunity(
                        accountId=UserUtil.CurrentUser.Contact.Account.Id,
                        Name = L.Company,
                        Legal_Name__c=L.Company,
                        Business_Legal_Name__c=L.Company,
                        OwnerId=L.OwnerId,
                        RecordTypeId='0126000000053vz',
                        Property_Status__c = 'Inactive',
                        Address__c = L.Street,
                        City__c=L.City,
                        State__c=L.State,
                        Zip__c=L.PostalCode,
                        Business_Address__c = L.Street,
                        Business_City__c=L.City,
                        Business_State__c=L.State,
                        Business_Zip_Code__c=L.PostalCode,
                        Phone__c=L.Phone,
                        Fax__c=L.Fax,
                        Name_of_Property_Representative__c=L.FirstName + ' ' + L.LastName,
                        Property_Representative_Title__c=L.Contact_Role__c,
                        Property_Representative_s_email__c=L.Email,
                        Comments__c=L.Lead_Notes__c,
                        LeadSource=L.LeadSource,
                        External_Lead_Source__c=L.Lead_Source_External__c,
                        Description=L.Description,
                        Number_of_Units__c=L.Number_of_Units__c,
                        Property_Type__c=L.Type_of_Business__c,
                        Building_Type__c=L.Building_Type__c,
                        LeadID_18__c=L.LeadID_18__c,
                        StageName='Ready for Activation',
                        CSG_Account_Number__c=L.CSG_Account_Number__c,
                        CloseDate=system.today());
                        
                        insert o;
                    
                    Contact c = new Contact(
                        accountId=UserUtil.CurrentUser.Contact.Account.Id,
                        OwnerId=L.OwnerId,
                        RecordTypeId='012600000001B8p',
                        FirstName=L.FirstName,
                        LastName=L.LastName,
                        Best_Time_to_Contact__c=L.Best_Time_to_Contact__c,
                        Email=L.Email,
                        Fax=L.Fax,
                        MobilePhone=L.MobilePhone,
                        Phone=L.Phone,
                        Status__c='Inactive');
                        
                        insert c;                                       OpportunityContactRole oc = new OpportunityContactRole(
                        OpportunityId=o.id,
                        ContactId=c.id,
                        Role='Business Owner',
                        IsPrimary=TRUE);
                        
                        insert oc;                                                             Database.LeadConvert lc = new Database.LeadConvert();                             lc.setLeadId(L.Id);                         lc.setContactId(c.Id);                          lc.setAccountId(UserUtil.CurrentUser.Contact.Account.Id);                         lc.setDoNotCreateOpportunity(true);                          lc.ConvertedStatus='Converted';                                                                 Database.LeadConvertResult                       lcr = Database.convertLead(lc);                                 System.assert(lcr.isSuccess());
                       }  
        
   
   }}