trigger Fiber_IDF_Detail_Trigger on Fiber_IDF_Detail__c (before insert, after insert, after update, before update) {
    
    if (Trigger.isBefore) {
        if (Trigger.isInsert){
            SIMP_FiberBuildingIDFClass.beforeInsert(Trigger.New,'IDF');
        }
         
        if (Trigger.isUpdate){
            SIMP_FiberBuildingIDFClass.afterInsert(Trigger.New, Trigger.oldMap,'IDF');
        }
    }
}