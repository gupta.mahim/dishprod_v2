trigger ExecSumSumbitforsecondApproval on Executive_Summary__c (after update) {

 List<Approval.ProcessSubmitRequest> lstprocess=new List<Approval.ProcessSubmitRequest>();

 for (Executive_Summary__c e : Trigger.New) {
    if (e.Status__c == 'GM Approved' || e.Status__c == 'GM Rejected' ) {

  // Create an approval request for the account
  Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
  req1.setComments('Please approve or reject this Executive Summary.');
  req1.setObjectId(e.id);
  lstprocess.add(req1);
  }    
    // Submit the approval request for the account
       List<Approval.ProcessResult> resultlist = Approval.process(lstprocess);
 }
}