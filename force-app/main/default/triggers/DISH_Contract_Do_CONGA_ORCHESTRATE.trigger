/*-------------------------------------------------------------
Author: Jerry Clifft
Created on: 24th March 2020
Created for: CONGA Orchestrate - Product BUG Work-Around related to Customer Object with Master-Detail being used as an Initator Object.
Description: Trigger will cause insertion of Intator record.

DO NOT USE THIS TRIGGER - This causes a bug where the DISH Contract field ACCOUNT will change to a new Account based on the AccountID of the Contact__c chosen.

-------------------------------------------------------------*/

trigger DISH_Contract_Do_CONGA_ORCHESTRATE on DISH_Contract__c (after insert, before insert, before update, after update) { 
    FSTR.COTriggerHandler.handleProcessObjectTrigger();
}