/*-------------------------------------------------------------
Author: Jerry Clifft
Created on: 30th March 2020
Created for: Project-PJ-000472
Description: Update the Front Office Request to Approved if a SVP or EVP approved the Executive Summary.
-------------------------------------------------------------*/

Trigger ExecSumDRFStatusUpdate2 on Executive_Summary__c (After Update) {
Set<Id> FORIds = new Set<Id>();

executive_summary__c exs= trigger.new[0];
   if (exs.Status__c == 'SVP Approved')
       {

       for (Executive_Summary__c c: Trigger.New){ if (c.Status__c == 'SVP Approved' ) FORIds.add(c.Front_Office_Request__c);            }
             for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from Front_Office_Request__c where Id in :FORIds]) {            if(fo.Front_Office_Decision__c != 'Approved'){ fo.Front_Office_Decision__c = 'Approved';} Update fo;
         }
  }
  else if (exs.Status__c == 'EVP Approved')
       {

       for (Executive_Summary__c c: Trigger.New){
         if (c.Status__c == 'EVP Approved' )
         FORIds.add(c.Front_Office_Request__c);            }
             for (Front_Office_Request__c fo: [select id, Front_Office_Decision__c, RecordTypeId  from Front_Office_Request__c where Id in :FORIds]) {
             if(fo.Front_Office_Decision__c != 'Approved'){ fo.Front_Office_Decision__c = 'Approved';
             }
             Update fo;
         }
  }
}