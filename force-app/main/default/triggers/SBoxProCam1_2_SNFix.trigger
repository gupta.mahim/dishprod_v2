trigger SBoxProCam1_2_SNFix on Smartbox__c (before insert, before update) {

    Map<String, Smartbox__c> SBoxMap =new Map <String, Smartbox__c>();
    for (Smartbox__c s : System.Trigger.new) {
        
        // Make sure we don't treat a Serial_Number__c that isn't changing during an update as a duplicate.
    
        if ((Smartbox__c.Serial_Number__c !=null ) && (s.NoTrigger__c == False )&& (System.Trigger.isInsert || (s.Unique_SN_no_CAID__c != System.Trigger.oldMap.get(s.Id).Unique_SN_no_CAID__c))) {
    
   
            if (SBoxMap.containsKey(s.Unique_SN_no_CAID__c)) { s.Second_ProCam__c = TRUE;}
            else {
                SBoxMap.put(s.Unique_SN_no_CAID__c, s);
            }
       }
    }
    
    // Using a single database query, find all the Smartbox__c's in
    // the database that have the same Serial_Number__c address as any
    // of the Smartbox__cs being inserted or updated.
    
    for (Smartbox__c s : [SELECT Unique_SN_no_CAID__c FROM Smartbox__c
                      WHERE Unique_SN_no_CAID__c IN :SboxMap.KeySet() AND Second_ProCam__c != TRUE ]) { Smartbox__c newSbox = SBoxMap.get(s.Unique_SN_no_CAID__c); newSbox.Second_ProCam__c = TRUE; }
}