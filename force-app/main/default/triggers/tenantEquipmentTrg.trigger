/*
* Author: M Fazal Ur Rehan
* Master trigger to hold the logic for AmDocs_BulkTenantCalloutTrigger, teCreateCaseOnEquipDupesTrg and teValidateAdressTrg triggers
* Invokes the appropriate TenantEquipmentTrgHandler mehtods for event handling
*/
trigger tenantEquipmentTrg on Tenant_Equipment__c (before insert,before update,after insert)
{
 
   
    if(Trigger.isBefore && Trigger.IsInsert)
    {
        // handles the logic to locate equipment dupes if data size is less than or equal 500
        // handles the logic to do address validation throuth netquals
        TenantEquipmentTrgHandler.doBeforeInsert(Trigger.new);
    }
    if(Trigger.isBefore && Trigger.IsUpdate)
    {
        //handles the logic to perform AmDocs Bulk Tenant Callout
        // handles the logic to do address validation throuth netquals
        TenantEquipmentTrgHandler.doBeforeUpdate(Trigger.newMap,Trigger.oldMap);
    }
    if(Trigger.isAfter && Trigger.IsInsert)
    {
        // handles the logic to locate equipment dupes if data size is greater than 500 through batch and creates case for duplicate equipments
        //handles the logic to creates cases for duplicate equipments if data size is less than or equal 500
        TenantEquipmentTrgHandler.doAfterInsert(Trigger.newMap,Trigger.oldMap);
    }
   }