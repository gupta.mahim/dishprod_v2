trigger smartboxTrg on Smartbox__c (before insert, after insert, before update, after update)
{
    if(Trigger.isBefore && Trigger.IsInsert)
    {
         SmartboxTrgHandler.doBeforeInsert(Trigger.new);
    }
    if(Trigger.isAfter && Trigger.IsInsert)
    {
        SmartboxTrgHandler.doAfterInsert(Trigger.newMap,Trigger.oldMap);
    }
    if(Trigger.isBefore && Trigger.IsUpdate)
    {
        SmartboxTrgHandler.doBeforeUpdate(Trigger.newMap,Trigger.oldMap);
    }
    if(Trigger.isAfter && Trigger.IsUpdate)
    {
        SmartboxTrgHandler.doAfterUpdate(Trigger.newMap,Trigger.oldMap);
    }
}