trigger TenantEquipmentHistoryDelete on Tenant_Equipment__c (After Delete) {

   for (Tenant_Equipment__c OD: Trigger.Old) {
           {Tenant_Equipment_Log__c OAD = new Tenant_Equipment_Log__c(
               Opportunity__c = OD.Opportunity__c,
               Name = OD.Name,
               Smart_Card__c = OD.Smart_Card__c,
               Address__c = OD.Address__c,
               City__c = OD.City__c,
               State__c = OD.State__c,
               Zip__c = OD.Zip_Code__c,
               Unit__c = OD.Unit__c,
               Phone_Number__c = OD.Phone_Number__c,
               Email_Address__c = OD.Email_Address__c,
               Customer_First_Name__c = OD.Customer_First_Name__c,
               Customer_Last_Name__c = OD.Customer_Last_Name__c,
               Action__c = OD.Action__c,
               Trigger_Action__c = 'Delete');
           insert OAD;
       }
    }
}