trigger Direct_Lead_Conversion on Lead (After Update) {
// Converts ALL Direct Leads MDU & P/P



for (Lead L: Trigger.New){

if(L.RecordTypeId=='01260000000Lte8' && L.MDU_Convert_Lead__c == true && L.isConverted == false){Account a = new Account(    Name = L.Company,    Status__c ='Inactive',    RecordTypeId='0126000000053v4',    OwnerId=L.OwnerId,    Pyscical_Address__c=L.Street,    City__c=L.City,    State__c=L.State,    Zip__c=L.PostalCode,    Phone=L.Business_Phone__c,    Fax=L.Fax,    Website=L.Website);    insert a;
  
    
Opportunity o = new Opportunity(    Name = L.Company + ' Property',    AccountId=a.Id,    OwnerId=L.OwnerId,    RecordTypeId='0126000000053vp',    Property_Status__c = 'Inactive',    Address__c = L.Street,    City__c=L.City,    State__c=L.State,    Zip__c=L.PostalCode,    Phone__c=L.Phone,    Number_of_Units__c=L.Number_of_Units__c,    Property_Type__c=L.Type_of_Business__c,    Building_Type__c=L.Building_Type__c,    StageName='Site Survey Performed',    CloseDate=system.today());     insert o;
    
  
Contact c = new Contact(
    AccountId=a.Id,
    OwnerId=L.OwnerId,
    FirstName=L.FirstName,
    LastName=L.LastName,
    Best_Time_to_Contact__c=L.Best_Time_to_Contact__c,
    Email=L.Email,
    Fax=L.Fax,
    MobilePhone=L.MobilePhone,
    Phone=L.Phone);
    
    insert c; Database.LeadConvert lc = new Database.LeadConvert();        lc.setLeadId(L.Id);        lc.setAccountId(a.Id);                lc.setContactId(c.Id);             lc.setDoNotCreateOpportunity(true);                       lc.ConvertedStatus='Converted';              Database.LeadConvertResult lcr = Database.convertLead(lc);               System.assert(lcr.isSuccess());  
        
}    
    
if(L.RecordTypeId=='012600000005DVO' && L.MDU_Convert_Lead__c == true && L.isConverted == false && L.Single_or_Multi_Site__c == 'Single Site'){ Account ac = [select ID from Account where RecordTypeId = '0126000000018Rc' and OwnerId = :UserInfo.getUserId() Limit 1];Opportunity o = new Opportunity(
    Name = L.Company,
    AccountId=ac.Id,
    OwnerId=L.OwnerId,
    RecordTypeId='0126000000017Tj',
    Property_Status__c = 'Inactive',
    Address__c = L.Street,
    City__c=L.City,
    State__c=L.State,
    Zip__c=L.PostalCode,
    Phone__c=L.Phone,
    Property_Type__c=L.Type_of_Business__c,
    StageName='Needs Analysis',
    CloseDate=system.today());
    
    insert o;
    
  
Contact c = new Contact(
    AccountId=ac.Id,
    OwnerId=L.OwnerId,
    FirstName=L.FirstName,
    LastName=L.LastName,
    Best_Time_to_Contact__c=L.Best_Time_to_Contact__c,
    Email=L.Email,
    Fax=L.Fax,
    MobilePhone=L.MobilePhone,
    Phone=L.Phone);
    
    insert c;
    
Database.LeadConvert lc = new Database.LeadConvert();        lc.setLeadId(L.Id);        lc.setAccountId(ac.Id);                lc.setContactId(c.Id);             lc.setDoNotCreateOpportunity(true);                        lc.ConvertedStatus='Converted';              Database.LeadConvertResult lcr = Database.convertLead(lc);         System.assert(lcr.isSuccess());  
        
}        
    
if(L.RecordTypeId=='012600000005DVO' && L.MDU_Convert_Lead__c == true && L.isConverted == false 
&& L.Single_or_Multi_Site__c == 'Multi Site'){

Account ac = [select ID from Account where RecordTypeId = '0126000000018Rc' and OwnerId = :UserInfo.getUserId() Limit 1]; Account a = new Account(
    ParentId=ac.Id,
    Name = L.Company,
    Status__c ='Inactive',
    RecordTypeId='0126000000053vk',
    OwnerId=L.OwnerId,
    Pyscical_Address__c=L.Street,
    City__c=L.City,
    State__c=L.State,
    Zip__c=L.PostalCode,
    Phone=L.Business_Phone__c,
    Fax=L.Fax,
    Website=L.Website);
    
    insert a; Opportunity o = new Opportunity(
    Name = L.Company,
    AccountId=a.Id,
    OwnerId=L.OwnerId,
    RecordTypeId='0126000000053vu',
    Property_Status__c = 'Inactive',
    Address__c = L.Street,
    City__c=L.City,
    State__c=L.State,
    Zip__c=L.PostalCode,
    Phone__c=L.Phone,
    Type_of_Business__c=L.Type_of_Business__c,
    StageName='Needs Analysis',
    CloseDate=system.today());
    
    insert o; Contact c = new Contact(
    AccountId=a.Id,
    OwnerId=L.OwnerId,
    FirstName=L.FirstName,
    LastName=L.LastName,
    Best_Time_to_Contact__c=L.Best_Time_to_Contact__c,
    Email=L.Email,
    Fax=L.Fax,
    MobilePhone=L.MobilePhone,
    Phone=L.Phone);
    
    insert c; Database.LeadConvert lc = new Database.LeadConvert();        lc.setLeadId(L.Id);        lc.setAccountId(a.Id);                lc.setContactId(c.Id);             lc.setDoNotCreateOpportunity(true);                        lc.ConvertedStatus='Converted';              Database.LeadConvertResult lcr = Database.convertLead(lc);               System.assert(lcr.isSuccess());  
        
}    

}
}