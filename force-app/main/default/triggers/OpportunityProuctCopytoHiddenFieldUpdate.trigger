trigger OpportunityProuctCopytoHiddenFieldUpdate on Opportunity (before insert, before update) 
    {
        Opportunity op1 = trigger.new[0];  //Issue line.
            Set<Id> opIds = new Set<Id>();

            for (Opportunity o: Trigger.New){

                opIds.add(op1.Id);  //This is correct
    }
    list<opportunityLineItem> OpLine = [SELECT Status__c, Submitted__c, CreatedBy.Name, Quantity, TotalPrice, LastModifiedDate, LastModifiedBy.Name, PriceBookEntry.Name, UnitPrice, ID, OpportunityId FROM OpportunityLineItem WHERE OpportunityId in : opIds AND (Action__c ='ADD' OR Action__c ='REMOVE') AND Submitted__c != TRUE ] ;

     if(OpLine.size() > 0 )    
        {
            for(Opportunity t:trigger.new)
                  {
                    List<String> NewList= new List<String>();
            
                    if (t.HasOpportunityLineItem == true) 
                    {
                         for(OpportunityLineItem XOpLine: OpLine ) 
                        {
                         if(XOpLine.opportunityid==t.id) // Check for current opportunity. new code
                            {
                            NewList.add(XOpLine.Status__c);
                            NewList.add(', ');
                            NewList.add(XOpLine.PriceBookEntry.Name);
                            NewList.add(', @ ');
                            String str = '' + XOpLine.Quantity;
                            NewList.add(str);
                            NewList.add(', Units Passed, ');
                            NewList.add(' @ ');
                            String str2 = '$' + XOpLine.UnitPrice;
                            NewList.add(str2);
                            NewList.add(', Per Unit/Month  ');
                            NewList.add(XOpLine.LastModifiedBy.Name);
                            NewList.add(', on ');
                            String str1 = '' + XOpLine.lastModifiedDate;
                            NewList.add(str1);
                            NewList.add(' GMT 0');
                            NewList.add(' <br />');
                           }
                           }
                                
            
                                String s = '';
                        for(String c : NewList){
                            s = s + c;
                            system.debug('********' +  t.Hidden_Products__c);
                        }
                    {    t.Hidden_Products__c = s; t.Hidden_Case_History__c = True;}
                    }
                }
             }}