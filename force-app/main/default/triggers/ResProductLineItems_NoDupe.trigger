trigger ResProductLineItems_NoDupe on Res_Product_Line_Item__c (Before Insert, Before Update) {
Res_Product_Line_Item__c RPL = trigger.new[0];
RPL.Unique_Check__c = RPL.Unique_String__c;
}