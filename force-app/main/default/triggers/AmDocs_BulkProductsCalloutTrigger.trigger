trigger AmDocs_BulkProductsCalloutTrigger on OpportunityLineItem (before update) {
    
    // Processes for SF->AmDocs to call @Future API callout triigers.
    OpportunityLineItem S = trigger.new[0];
    
    // UpdateBulk
    // if(S.AmDocs_Action__c == True)
    // { S.AmDocs_Action__c = false; AmDocs_CalloutClass_BulkProductsUpdate.AmDocsMakeCalloutBulkProductsUpdate(S.Id); System.debug('IF 1' +S);}
    // else
    //Coronavirus - covid-19 FTG AT120 Reduced Rate
    if(S.Action__c == 'Covid19ReduceRate') { 
        S.Action__c = 'Covid19NOWReducingRate'; 
        AmDocs_CalloutClass_BulkUpdtCoronavirus.ReduceRateNOW(S.Id); 
        System.debug('IF 1' +S);
    }
}