trigger OpportunitySmartBoxPurchaseConvert2Lease on Opportunity (before update) 
    {
        Opportunity op = trigger.new[0];
            Set<Id> opIds = new Set<Id>();
               opIds.add(op.Id);
                   System.debug('RESULTS of the opIds.add ' +opIds);
                
        Opportunity oldOp = Trigger.oldMap.get(op.Id);

        Date today = Date.today() -30;
        Date launchDate = op.Launch_Date__c;
               System.debug('RESULTS of the --*STRING Today = ' +today);
               System.debug('RESULTS of the --*String launchDate = ' +launchDate);

            if(op.Smartbox_Leased__c == TRUE && op.Smartbox_Leased__c != oldOp.Smartbox_Leased__c) {
            if(Today <= launchDate && op.Launch_Date__c != Null) {
               System.debug('RESULTS of the --*Smartbox Leased Value has changed*--');
               System.debug('RESULTS of the --**Old Smartbox Leased Value :'+oldOp.Smartbox_Leased__c);
               System.debug('RESULTS of the --**New Smartbox Leased Value :'+op.Smartbox_Leased__c);
            List<Smartbox__c> sbs = [select id, Chassis_Serial__c, Opportunity__c, CAID__c, CSG_Sub_Account__c, MEID__c, CSG_Account_Number__c, Opp_Launch_Date__c, Part_Number__c, Serial_Number__c, SmartCard__c, SF_Status__c, Status__c, CSG_Status__c, CreatedDate, LastModifiedDate, CaseRequestedCompletionDate__c, CaseRequestType__c, Case_Number__c, Most_Recent_Case_Create_Date__c, Most_Recent_Case_ID__c, Most_Recent_Case_Number__c, Most_Recent_Case_Pre_Activation_Date__c, Most_Recent_Case_RA__c, Most_Recent_Case_Record_Type__c, Most_Recent_Case_Request_Completion_Date__c, Most_Recent_Case_Type__c, Most_Recent_Property_Bulk_Launch_Date__c, Most_Recent_Property_Status__c, Type_of_Equipment__c  from Smartbox__c where Opportunity__r.Id in :opIds ]; 
               
            if(sbs.size() > 0)
                {
                   for (Smartbox__c SB: sbs) 
                    {
                    Smartbox_Accounting__c SBA = new Smartbox_Accounting__c(
                    Chassis_Serial__c = SB.Chassis_Serial__c,  
                    CAID__c = SB.CAID__c,
                    CSG_Sub_Account__c = SB.CSG_Sub_Account__c,
                    MEID__c = SB.MEID__c,
                    Opportunity__c = SB.Opportunity__c,
                    CSG_Master__c = SB.CSG_Account_Number__c,
                    Opp_Launch_Date__c = SB.Opp_Launch_Date__c,
                    Part_Number__c = SB.Part_Number__c,    
                    Serial_Number__c = SB.Serial_Number__c,
                    SmartCard__c = SB.SmartCard__c,
                    Opportunity_Status__c = SB.SF_Status__c,
                    SF_Status__c = SB.Status__c + ' - The property just transfered from Purchased to Leased',
                    CSG_Status__c = SB.CSG_Status__c,
                    Equipment_Added_to_SF_Date__c = SB.CreatedDate,
                    Equipment_Last_Modified_Date__c = SB.LastModifiedDate,
                    CaseRequestedCompletionDate__c = SB.CaseRequestedCompletionDate__c,
                    CaseRequestType__c = SB.CaseRequestType__c,
                    Case_Number__c = SB.Case_Number__c,
                    Most_Recent_Case_Create_Date__c = SB.Most_Recent_Case_Create_Date__c,
                    Most_Recent_Case_ID__c = SB.Most_Recent_Case_ID__c,
                    Most_Recent_Case_Number__c = SB.Most_Recent_Case_Number__c,
                    Most_Recent_Case_Pre_Activation_Date__c = SB.Most_Recent_Case_Pre_Activation_Date__c,
                    Most_Recent_Case_RA__c = SB.Most_Recent_Case_RA__c,
                    Most_Recent_Case_Record_Type__c = SB.Most_Recent_Case_Record_Type__c,
                    Most_Recent_Case_Request_Completion_Date__c = SB.Most_Recent_Case_Request_Completion_Date__c,
                    Most_Recent_Case_Type__c = SB.Most_Recent_Case_Type__c,
                    Most_Recent_Property_Bulk_Launch_Date__c = SB.Most_Recent_Property_Bulk_Launch_Date__c,
                    Most_Recent_Property_Status__c = SB.Most_Recent_Property_Status__c,
                    Type_of_Equipment__c = SB.Type_of_Equipment__c);
     
                insert SBA; 
            }}
              }
               }
}